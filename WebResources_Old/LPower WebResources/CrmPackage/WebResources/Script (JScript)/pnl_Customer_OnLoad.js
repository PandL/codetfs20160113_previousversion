/// <reference path="XrmServiceToolkit.js" />
function onLoad() {
    var CRM_FORM_TYPE = Xrm.Page.ui.getFormType();
    if (CRM_FORM_TYPE != 1)
        DisableTabAttributes();

    //disableRibbonButton("account|NoRelationship|Form|Mscrm.Form.account.Deactivate-Medium");
}

function DisableTabAttributes() {
    var toggleSection = Xrm.Page.getAttribute('pnl_prontocustomer').getValue();    
    var accountId = Xrm.Page.data.entity.getId();
    var state = null;

    XrmServiceToolkit.Rest.Retrieve(
		accountId,
		"AccountSet",
		null, null,
		function (result) {
		    state = result.StateCode.Value;
		},
		function (error) {
		    alert("JS Error: " + error.message);
		},
		false
	);

    if ((toggleSection != null && !HasRole()) || state == 1) {
        tabToggle("tab_6", true);
        tabToggle("tab_7", true);
    }
    else {
        tabToggle("tab_6", false);
        tabToggle("tab_7", false);
    }
}

function sectionToggle(sectionName, disableStatus) {
    var ctrlName = Xrm.Page.ui.controls.get();
    for (var i in ctrlName) {
        var ctrl = ctrlName[i];
        var ctrlSection = ctrl.getParent().getName();
        if (ctrlSection == sectionName) {
            if (!ctrl.getDisabled())
                ctrl.setDisabled(disableStatus);
        }
    }
}

function tabToggle(tabName, disableStatus) {
    var tab = Xrm.Page.ui.tabs.get(tabName);
    if (tab == null)
        alert("Error: The tab: " + tabName + " is not on the form!");
    else {
        var tabSections = tab.sections.get();
        for (var i in tabSections) {

            var secName = tabSections[i].getName();

            sectionToggle(secName, disableStatus);
        }
    }
}

function disableRibbonButton(buttonId) {
    var buttonID = buttonId;
    var btn = window.top.document.getElementById(buttonID);
    
    var intervalId = window.setInterval(function () {
        if (btn != null) {
            window.clearInterval(intervalId);
            btn.disabled = true;
            //btn.style.display='none';//Hide
            //Xrm.Page.ui.refreshRibbon();
        }

    }, 50);
}

function CanDeactivate(recordId) {
    var canDeactivate = false;
    if (HasRole())
        canDeactivate = true;
    else
    {
        var xml = "<?xml version='1.0' encoding='utf-8'?>" +
                "<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'" +
                " xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'" +
                " xmlns:xsd='http://www.w3.org/2001/XMLSchema'>" +
                GenerateAuthenticationHeader() +
                "<soap:Body>" +
                "<Retrieve xmlns='http://schemas.microsoft.com/crm/2007/WebServices'>" +
                "<entityName>account</entityName>" +
                "<id>" + recordId + "</id>" +
                "<columnSet xmlns:q1='http://schemas.microsoft.com/crm/2006/Query' xsi:type='q1:ColumnSet'>" +
                    "<q1:Attributes>" +
                        "<q1:Attribute>ownerid</q1:Attribute>" +
                        "<q1:Attribute>statuscode</q1:Attribute>" +
                    "</q1:Attributes>" +
                "</columnSet>" +
                "</Retrieve>" +
                "</soap:Body>" +
                "</soap:Envelope>";
        var xmlHttpRequest = new ActiveXObject("Msxml2.XMLHTTP");
        xmlHttpRequest.Open("POST", "/mscrmservices/2007/CrmService.asmx", false);
        xmlHttpRequest.setRequestHeader("SOAPAction", "http://schemas.microsoft.com/crm/2007/WebServices/Retrieve");
        xmlHttpRequest.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
        xmlHttpRequest.setRequestHeader("Content-Length", xml.length);
        xmlHttpRequest.send(xml);
        var resultXml = xmlHttpRequest.responseXML;

        if (resultXml.selectSingleNode("//q1:ownerid") != null) {
            var owner = resultXml.selectSingleNode("//q1:ownerid").nodeTypedValue;
            var isOwner = false;
            if (owner == Xrm.Page.context.getUserId())
                isOwner = true;
            if (resultXml.selectSingleNode("//q1:statuscode") != null) {
                var status = resultXml.selectSingleNode("//q1:statuscode").nodeTypedValue;
                if (status == 125760000 && isOwner)
                    canDeactivate = true;
            }
        }
    }

    return canDeactivate;
}

function HasRole() {
    //if (UserHelper.UserHasRoles("Admin Support Staff"))
    //    return true;
    //else
    //    return false;
    return UserHasRole("Admin Support Staff");
}

function UserHasRole(roleName) {
    var oXml = GetCurrentUserRoles();
    if (oXml != null) {
        var roles = oXml.selectNodes("//BusinessEntity/q1:name");
        if (roles != null) {
            for (i = 0; i < roles.length; i++) {
                if (roles[i].text == roleName) {
                    return true;
                }
            }
        }
    }
    return false;
}

function GetCurrentUserRoles() {
    var xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
	"<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" +
	GenerateAuthenticationHeader() +
	" <soap:Body>" +
	" <RetrieveMultiple xmlns=\"http://schemas.microsoft.com/crm/2007/WebServices\">" +
	" <query xmlns:q1=\"http://schemas.microsoft.com/crm/2006/Query\" xsi:type=\"q1:QueryExpression\">" +
	" <q1:EntityName>role</q1:EntityName>" +
	" <q1:ColumnSet xsi:type=\"q1:ColumnSet\">" +
	" <q1:Attributes>" +
	" <q1:Attribute>name</q1:Attribute>" +
	" </q1:Attributes>" +
	" </q1:ColumnSet>" +
	" <q1:Distinct>false</q1:Distinct>" +
	" <q1:LinkEntities>" +
	" <q1:LinkEntity>" +
	" <q1:LinkFromAttributeName>roleid</q1:LinkFromAttributeName>" +
	" <q1:LinkFromEntityName>role</q1:LinkFromEntityName>" +
	" <q1:LinkToEntityName>systemuserroles</q1:LinkToEntityName>" +
	" <q1:LinkToAttributeName>roleid</q1:LinkToAttributeName>" +
	" <q1:JoinOperator>Inner</q1:JoinOperator>" +
	" <q1:LinkEntities>" +
	" <q1:LinkEntity>" +
	" <q1:LinkFromAttributeName>systemuserid</q1:LinkFromAttributeName>" +
	" <q1:LinkFromEntityName>systemuserroles</q1:LinkFromEntityName>" +
	" <q1:LinkToEntityName>systemuser</q1:LinkToEntityName>" +
	" <q1:LinkToAttributeName>systemuserid</q1:LinkToAttributeName>" +
	" <q1:JoinOperator>Inner</q1:JoinOperator>" +
	" <q1:LinkCriteria>" +
	" <q1:FilterOperator>And</q1:FilterOperator>" +
	" <q1:Conditions>" +
	" <q1:Condition>" +
	" <q1:AttributeName>systemuserid</q1:AttributeName>" +
	" <q1:Operator>EqualUserId</q1:Operator>" +
	" </q1:Condition>" +
	" </q1:Conditions>" +
	" </q1:LinkCriteria>" +
	" </q1:LinkEntity>" +
	" </q1:LinkEntities>" +
	" </q1:LinkEntity>" +
	" </q1:LinkEntities>" +
	" </query>" +
	" </RetrieveMultiple>" +
	" </soap:Body>" +
	"</soap:Envelope>";

    var xmlHttpRequest = new ActiveXObject("Msxml2.XMLHTTP");
    xmlHttpRequest.Open("POST", "/mscrmservices/2007/CrmService.asmx", false);
    xmlHttpRequest.setRequestHeader("SOAPAction", " http://schemas.microsoft.com/crm/2007/WebServices/RetrieveMultiple");
    xmlHttpRequest.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
    xmlHttpRequest.setRequestHeader("Content-Length", xml.length);
    xmlHttpRequest.send(xml);

    var resultXml = xmlHttpRequest.responseXML;
    return (resultXml);
}