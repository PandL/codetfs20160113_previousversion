///<reference path="..\XrmPage-vsdoc.js"/>

var UserHelper = {
    _globalRoles: null,

    UserHasRoles: function (roleNames) {
        if (UserHelper._globalRoles == null) {
            UserHelper.CreateRoleArray();
        }

        var names = roleNames.split("|");
        for (var i = 0; i < names.length; i++) {
            for (var k = 0; k < UserHelper._globalRoles.length; k++) {
                if (names[i] == UserHelper._globalRoles[k]) {
                    return true;
                }
            }
        }
        return false;
    },

    CreateRoleArray: function () {
        if (typeof JQueryHelper != "undefined") {
            JQueryHelper.retrieveMultipleSync(
                "/SystemUserSet?$select=systemuserroles_association/Name,systemuserroles_association/RoleId&$expand=systemuserroles_association&$filter=SystemUserId eq guid'" + Xrm.Page.context.getUserId() + "'",
                function (entityArr) {
                    UserHelper._globalRoles = new Array();
                    for (var i = 0; i < entityArr[0].systemuserroles_association.results.length; i++) {
                        UserHelper._globalRoles[i] = entityArr[0].systemuserroles_association.results[i].Name;
                    }
                },
                null);
        }
        else {
            UserHelper._globalRoles = new Array();
            var userId = Xrm.Page.context.getUserId().replace("{", "").replace("}", "");
            var service = UserHelper.GetJSONResponse("SystemUserSet?$select=systemuserroles_association/Name,systemuserroles_association/RoleId&$expand=systemuserroles_association&$filter=SystemUserId eq guid'" + userId + "'");

            if (service != null) {
                var requestResults = eval('(' + service.responseText + ')').d;

                if (requestResults != null && requestResults.results.length == 1) {
                    var user = requestResults.results[0];

                    for (var i = 0; i < requestResults.results[0].systemuserroles_association.results.length; i++) {
                        UserHelper._globalRoles[i] = requestResults.results[0].systemuserroles_association.results[i].Name;
                    }
                }
            }
        }
    },

    GetJSONResponse: function (oDataEndpointUrlAppend) {
        var serverUrl = Xrm.Page.context.getServerUrl();
        var oDataEndpointUrl = serverUrl + "/XRMServices/2011/OrganizationData.svc/";
        oDataEndpointUrl += oDataEndpointUrlAppend;
        var service = UserHelper.GetRequestObject();
        if (service != null) {
            service.open("GET", oDataEndpointUrl, false);
            service.setRequestHeader("X-Requested-Width", "XMLHttpRequest");
            service.setRequestHeader("Accept", "application/json, text/javascript, */*");
            service.send(null);
            return service;
        }
    },

    GetRequestObject: function () {
        if (window.XMLHttpRequest) {
            return new window.XMLHttpRequest;
        }
        else {
            try {
                return new ActiveXObject("MSXML2.XMLHTTP.3.0");
            }
            catch (ex) {
                return null;
            }
        }
    }
}