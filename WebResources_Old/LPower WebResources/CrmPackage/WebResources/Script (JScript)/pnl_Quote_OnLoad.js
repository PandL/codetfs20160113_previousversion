/// <reference path="XrmServiceToolkit.js" />
var currentDateTime = new Date();

function onLoad() {
    var CRM_FORM_TYPE = Xrm.Page.ui.getFormType();
    if (CRM_FORM_TYPE != 1) {
        var expiryDate = Xrm.Page.getAttribute("pnl_quoteexpirationdate").getValue();
        if (expiryDate < currentDateTime) {
            //Disable activate button
            //disableRibbonButton("quote|NoRelationship|Form|pnl.Form.quote.MainTab.Actions.ActivateQuote-Large");

            //Show notification
            var status = Xrm.Page.getAttribute("statecode").getValue();
            if (status == 0 && status == 1)
                XrmServiceToolkit.Common.AddNotification('This quote can not be activated because the quote pricing has expired!', 3);
        }
		DisableSalesAidAttributes();
    }
		
    SalesAidRequested_OnChange();
	SalesAidApproved_OnChange();
	
	SetWorksheetStyle();
	
    //hide/show sections based on experlogics data
    if (Xrm.Page.getAttribute("pnl_savedfromexperlogix").getValue() == 1) {
        Xrm.Page.ui.tabs.get("machinecosts").setDisplayState("expanded");
        Xrm.Page.ui.tabs.get("salesgp").setDisplayState("expanded");
    }	
}

function lostQuote() {
    //var dateLost = Xrm.Page.getAttribute("pnl_datelost").getValue();
    //if (dateLost == null) {
    //    Xrm.Page.getAttribute("pnl_datelost").setValue(currentDateTime);
    //	Xrm.Page.getAttribute("pnl_datelost").setSubmitMode("always"); 
    //    Xrm.Page.data.entity.save();
    //}
    //else {
    //    alert("This quote has already been closed!");
    //}

    var dialogId = '67257112-0D1B-4CB7-BBB0-2E363F4F31EF'; //This is the GUID of the target dialog process
    var entityName = 'quote';
    var recordId = Xrm.Page.data.entity.getId();

    LaunchDialog(dialogId, entityName, recordId);
}

function LaunchDialog(dialogId, entityName, recordId) {
    var serverUrl = Xrm.Page.context.getServerUrl();
    recordId = recordId.replace("{", "");
    recordId = recordId.replace("}", "");

    var dialogPath = serverUrl + '/cs/dialog/rundialog.aspx?DialogId=%7b' + dialogId + '%7d&EntityName=' + entityName + '&ObjectId=%7b' + recordId + '%7d';
    window.open(dialogPath);
}

function disableRibbonButton(buttonId) {
    var buttonID = buttonId;
    var btn = window.top.document.getElementById(buttonID);

    var intervalId = window.setInterval(function () {
        if (btn != null) {
            window.clearInterval(intervalId);
            btn.disabled = true;
            //btn.style.display='none';//Hide
        }
    }, 50);
}

function isLost(recordId) {
    //var formType = Xrm.Page.ui.getFormType();

    //if (formType == 1 || Xrm.Page.getAttribute("pnl_datelost").getValue() != null)
    //    return true;
    //else
    //    return false;
	
    var isLost = false;
    var xml = "<?xml version='1.0' encoding='utf-8'?>" +
   "<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'" +
   " xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'" +
   " xmlns:xsd='http://www.w3.org/2001/XMLSchema'>" +
   /* Start of comment -- changed by ranjith 21/10/2013
   Upgrading the js to work smoothly in 2013 -- GenerateAuthenticationHeader no more needed 
   GenerateAuthenticationHeader() +
   End of comment -- changed by ranjith 21/10/2013
   */
   "<soap:Body>" +
   "<Retrieve xmlns='http://schemas.microsoft.com/crm/2007/WebServices'>" +
   "<entityName>quote</entityName>" +
   "<id>" + recordId + "</id>" +
   "<columnSet xmlns:q1='http://schemas.microsoft.com/crm/2006/Query' xsi:type='q1:ColumnSet'>" +
       "<q1:Attributes>" +
           "<q1:Attribute>pnl_datelost</q1:Attribute>" +
       "</q1:Attributes>" +
   "</columnSet>" +
   "</Retrieve>" +
   "</soap:Body>" +
   "</soap:Envelope>";
    var xmlHttpRequest = new ActiveXObject("Msxml2.XMLHTTP");
    xmlHttpRequest.Open("POST", "/mscrmservices/2007/CrmService.asmx", false);
    xmlHttpRequest.setRequestHeader("SOAPAction", "http://schemas.microsoft.com/crm/2007/WebServices/Retrieve");
    xmlHttpRequest.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
    xmlHttpRequest.setRequestHeader("Content-Length", xml.length);
    xmlHttpRequest.send(xml);
    var resultXml = xmlHttpRequest.responseXML;

    if (resultXml.selectSingleNode("//q1:pnl_datelost") != null) {
        isLost = true;
    }

    return isLost;
}

function SalesAidRequested_OnChange() {
    if (Xrm.Page.getAttribute("pnl_salesaidrequested").getValue() != null)
        Xrm.Page.getAttribute("pnl_salesaidrequestedreason").setRequiredLevel("required");
    else
        Xrm.Page.getAttribute("pnl_salesaidrequestedreason").setRequiredLevel("none");
}

function SalesAidApproved_OnChange() {
	var saStatus = Xrm.Page.getAttribute("pnl_salesaidapprovedstatus");
	if(saStatus != null && saStatus.getValue() == 20 || saStatus.getValue() == 30)
		disableFormFields(true);
}

function QuoteExpired(recordId) {
    var expired = false;
    //XrmServiceToolkit.Rest.Retrieve(
	//	recordId,
	//	"QuoteSet",
	//	null, null,
	//	function (result) {
	//	    var expiryDate = result.pnl_QuoteExpirationDate;
	//	    if (expiryDate < currentDateTime)
	//	        expired = true;
	//	},
	//	function (error) {
	//	    alert("Error getting expiry date: " + error.message);
	//	},
	//	false
    //);

    var xml = "<?xml version='1.0' encoding='utf-8'?>" +
   "<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'" +
   " xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'" +
   " xmlns:xsd='http://www.w3.org/2001/XMLSchema'>" +
    /* Start of comment -- changed by ranjith 21/10/2013
   Upgrading the js to work smoothly in 2013 -- GenerateAuthenticationHeader no more needed 
   GenerateAuthenticationHeader() +
   End of comment -- changed by ranjith 21/10/2013
   */
   
   "<soap:Body>" +
   "<Retrieve xmlns='http://schemas.microsoft.com/crm/2007/WebServices'>" +
   "<entityName>quote</entityName>" +
   "<id>" + recordId + "</id>" +
   "<columnSet xmlns:q1='http://schemas.microsoft.com/crm/2006/Query' xsi:type='q1:ColumnSet'>" +
       "<q1:Attributes>" +
           "<q1:Attribute>pnl_quoteexpirationdate</q1:Attribute>" +
       "</q1:Attributes>" +
   "</columnSet>" +
   "</Retrieve>" +
   "</soap:Body>" +
   "</soap:Envelope>";
    var xmlHttpRequest = new ActiveXObject("Msxml2.XMLHTTP");
    xmlHttpRequest.Open("POST", "/mscrmservices/2007/CrmService.asmx", false);
    xmlHttpRequest.setRequestHeader("SOAPAction", "http://schemas.microsoft.com/crm/2007/WebServices/Retrieve");
    xmlHttpRequest.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
    xmlHttpRequest.setRequestHeader("Content-Length", xml.length);
    xmlHttpRequest.send(xml);
    var resultXml = xmlHttpRequest.responseXML;

    if (resultXml.selectSingleNode("//q1:pnl_quoteexpirationdate") != null) {
        var expiryDate = resultXml.selectSingleNode("//q1:pnl_quoteexpirationdate").nodeTypedValue;
        if (expiryDate < currentDateTime)
        	expired = true;
    }

    return expired;
}

function SalesAidRequest()
{
	var sa = Xrm.Page.getAttribute("pnl_salesaidrequest");

	if(sa == null || !sa.getValue())
	{
		sa.setSubmitMode("always");
		sa.setValue(true);
		Xrm.Page.data.entity.save();
	}
	else
		alert("Sales Aid has already been requested!");
}

function SalesAidApprove()
{
	var saStatus = Xrm.Page.getAttribute("pnl_salesaidapprovedstatus");
	saStatus.setSubmitMode("always");
	saStatus.setValue(20);
	
	Xrm.Page.data.entity.save();
}

function SalesAidDecline()
{
	var saStatus = Xrm.Page.getAttribute("pnl_salesaidapprovedstatus");
	saStatus.setSubmitMode("always");
	saStatus.setValue(30);
	
	Xrm.Page.data.entity.save();
}

function DisableSalesAidAttributes() {
    var saRequest = Xrm.Page.getAttribute('pnl_salesaidrequest').getValue();

	if(saRequest)
	{
		if( HasRole("Product Manager") || HasRole("System Administrator"))
		{
			Xrm.Page.ui.controls.get('pnl_salesaidrequested').setDisabled(true);
			Xrm.Page.ui.controls.get('pnl_salesaidrequestedreason').setDisabled(true);

		}
		else
		{
		                 //tabToggle("tab_7", true);
			disableFormFields(true);

		}
	}
}

function sectionToggle (sectionName, disableStatus)
{
    var ctrlName = Xrm.Page.ui.controls.get();
    for(var i in ctrlName) {
         var ctrl = ctrlName[i];
         var ctrlSection = ctrl.getParent().getName();
         if (ctrlSection == sectionName) {
               ctrl.setDisabled(disableStatus);
        }
    }
}

function tabToggle (tabName, disableStatus)
{
	var tab = Xrm.Page.ui.tabs.get(tabName);
	if (tab == null) 
		alert("Error: The tab: " + tabName + " is not on the form!");
	else {
		var tabSections =  tab.sections.get();
		for (var i in tabSections) {
			var secName = tabSections[i].getName();
			sectionToggle(secName, disableStatus);
		}
	}
}

function HasRole(roleName)
{
	if(UserHelper.UserHasRoles(roleName))
		return true;
	else
		return false;
}

function doesControlHaveAttribute(control) {
    var controlType = control.getControlType();
    return controlType != "iframe" && controlType != "webresource" && controlType != "subgrid";
}

function disableFormFields(onOff) {

    Xrm.Page.ui.controls.forEach(function (control, index) {
        if (doesControlHaveAttribute(control)) {
            control.setDisabled(onOff);
        }
    });
}

function ActivateQuote() {
 
    if (Xrm.Page.data.entity.getIsDirty()) {
        alert("Please save your changes before cancelling the quote.");
        return;
    }
 
    // create the request
    var request = "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">";
    request += "<s:Body>";
    request += "<Execute xmlns=\"http://schemas.microsoft.com/xrm/2011/Contracts/Services\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">";
    request += "<request i:type=\"b:SetStateRequest\" xmlns:a=\"http://schemas.microsoft.com/xrm/2011/Contracts\" xmlns:b=\"http://schemas.microsoft.com/crm/2011/Contracts\">";
    request += "<a:Parameters xmlns:c=\"http://schemas.datacontract.org/2004/07/System.Collections.Generic\">";
    request += "<a:KeyValuePairOfstringanyType>";
    request += "<c:key>EntityMoniker</c:key>";
    request += "<c:value i:type=\"a:EntityReference\">";
    request += "<a:Id>" + Xrm.Page.data.entity.getId() + "</a:Id>";
    request += "<a:LogicalName>Quote</a:LogicalName>";
    request += "<a:Name i:nil=\"true\" />";
    request += "</c:value>";
    request += "</a:KeyValuePairOfstringanyType>";
    request += "<a:KeyValuePairOfstringanyType>";
    request += "<c:key>State</c:key>";
    request += "<c:value i:type=\"a:OptionSetValue\">";
    request += "<a:Value>1</a:Value>";
    request += "</c:value>";
    request += "</a:KeyValuePairOfstringanyType>";
    request += "<a:KeyValuePairOfstringanyType>";
    request += "<c:key>Status</c:key>";
    request += "<c:value i:type=\"a:OptionSetValue\">";
    request += "<a:Value>3</a:Value>";
    request += "</c:value>";
    request += "</a:KeyValuePairOfstringanyType>";
    request += "</a:Parameters>";
    request += "<a:RequestId i:nil=\"true\" />";
    request += "<a:RequestName>SetState</a:RequestName>";
    request += "</request>";
    request += "</Execute>";
    request += "</s:Body>";
    request += "</s:Envelope>";
 
    //send set state request
    $.ajax({
        type: "POST",
        contentType: "text/xml; charset=utf-8",
        datatype: "xml",
        url: Xrm.Page.context.getServerUrl() + "/XRMServices/2011/Organization.svc/web",
        data: request,
        beforeSend: function (XMLHttpRequest) {
            XMLHttpRequest.setRequestHeader("Accept", "application/xml, text/xml, */*");
            XMLHttpRequest.setRequestHeader("SOAPAction", "http://schemas.microsoft.com/xrm/2011/Contracts/Services/IOrganizationService/Execute");
        },
        success: function (data, textStatus, XmlHttpRequest) {
            //Xrm.Page.ui.close();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });    
}

function SetWorksheetStyle() {
    try {
        SetCurrencyEditStyle("pnl_estimatedrevenue");

        // Machine Costs Section
        SetCurrencyEditStyle("pnl_subtotallistprice");
        SetCurrencyEditStyle("pnl_subtotaldealernetprice");
        SetCurrencyEditStyle("pnl_subtotaldiscountpercentage");
                             
        SetCurrencyEditStyle("pnl_totallocallysourceditemslistprice");
        SetCurrencyEditStyle("pnl_totallocallysourceditemsdealernetprice");
        SetCurrencyEditStyle("pnl_totallocallysourceditemsdiscountpercentag");
                             
        SetCurrencyEditStyle("pnl_otherdealercostsfreight");
        SetCurrencyEditStyle("pnl_otherdealercostspredelivery");
        SetCurrencyEditStyle("pnl_otherdealerinterestsubsidy");
        SetCurrencyEditStyle("pnl_totalotherdealercosts");
                             
        SetCurrencyEditStyle("pnl_salesaidapproved");
        SetCurrencyEditStyle("pnl_totallistprice");
        SetCurrencyEditStyle("pnl_scenario1totaldealernetcost");
        SetCurrencyEditStyle("pnl_potentialgpatlistprice");



        // Gross Profit Calculation section
        SetLabelStyle("pnl_scenarioonelabel", true);
        SetLabelStyle("pnl_scenariotwolabel", true);
        SetLabelStyle("pnl_scenariothreelabel", true);

        SetLabelStyle("pnl_dealernetcostlabel", false);
        SetCurrencyEditStyle("pnl_scenario1totaldealernetcost1");
        SetCurrencyEditStyle("pnl_scenario2totaldealernetcost");
        SetCurrencyEditStyle("pnl_scenario3totaldealernetcost");

        SetLabelStyle("pnl_grossprofitpercentagelabel", false);
        SetCurrencyEditStyle("pnl_scenario1grossprofitpercentage");
        SetCurrencyEditStyle("pnl_scenario2grossprofitpercentage");
        SetCurrencyEditStyle("pnl_scenario3grossprofitpercentage");

        SetLabelStyle("pnl_grossprofitdollarlabel", false);
        SetCurrencyEditStyle("pnl_scenario1grossprofitdollar");
        SetCurrencyEditStyle("pnl_scenario2grossprofitdollar");
        SetCurrencyEditStyle("pnl_scenario3grossprofitdollar");

        SetLabelStyle("pnl_netsellingpricegplabel", false);
        SetCurrencyEditStyle("pnl_scenario1netsellingprice");
        SetCurrencyEditStyle("pnl_scenario2netsellingpriceinternal");
        SetCurrencyEditStyle("pnl_scenario3netsellingpriceinternal");

        // Changeover Price Calculation section
        SetLabelStyle("pnl_listpricenewmachineschangeoverlabel", false);
        SetCurrencyEditStyle("pnl_totallistprice1");
        SetCurrencyEditStyle("pnl_totallistprice2");
        SetCurrencyEditStyle("pnl_totallistprice3");

        SetLabelStyle("pnl_lessdiscountschangeoverlabel", false);
        SetCurrencyEditStyle("pnl_scenario1changeoverdiscounts");
        SetCurrencyEditStyle("pnl_scenario2changeoverdiscounts");
        SetCurrencyEditStyle("pnl_scenario3changeoverdiscounts");

        SetLabelStyle("pnl_netsellingpricechangeoverlabel", false);
        SetCurrencyEditStyle("pnl_scenario1netsellingprice1");
        SetCurrencyEditStyle("pnl_scenario2netsellingprice");
        SetCurrencyEditStyle("pnl_scenario3netsellingprice");

        // Trade-Ins section
        // if tradein 1 is empty then s2&3 fields are readonly else writeable
        var readOnly = EditIsEmpty("pnl_tradeinmachine1description");
        SetReadOnly(readOnly, "pnl_tradeinmachine1scenario2value");
        SetReadOnly(readOnly, "pnl_tradeinmachine1scenario3value");

        // if tradein 2 is empty then s2&3 fields are readonly
        var readOnly = EditIsEmpty("pnl_tradeinmachine2description");
        SetReadOnly(readOnly, "pnl_tradeinmachine2scenario2value");
        SetReadOnly(readOnly, "pnl_tradeinmachine2scenario3value");

        // if tradein 3 is empty then s2&3 fields are readonly
        var readOnly = EditIsEmpty("pnl_tradeinmachine3description");
        SetReadOnly(readOnly, "pnl_tradeinmachine3scenario2value");
        SetReadOnly(readOnly, "pnl_tradeinmachine3scenario3value");

        SetLabelStyle("pnl_tradeinmachine1description", false);
        SetCurrencyEditStyle("pnl_tradeinmachine1bookvalue");
        SetCurrencyEditStyle("pnl_tradeinmachine1scenario2value");
        SetCurrencyEditStyle("pnl_tradeinmachine1scenario3value");

        SetLabelStyle("pnl_tradeinmachine2description", false);
        SetCurrencyEditStyle("pnl_tradeinmachine2bookvalue");
        SetCurrencyEditStyle("pnl_tradeinmachine2scenario2value");
        SetCurrencyEditStyle("pnl_tradeinmachine2scenario3value");

        SetLabelStyle("pnl_tradeinmachine3description", false);
        SetCurrencyEditStyle("pnl_tradeinmachine3bookvalue");
        SetCurrencyEditStyle("pnl_tradeinmachine3scenario2value");
        SetCurrencyEditStyle("pnl_tradeinmachine3scenario3value");

        // Totals section
        SetLabelStyle("pnl_totalstradeinlabel", false);
        SetCurrencyEditStyle("pnl_totalbookvalue");
        SetCurrencyEditStyle("pnl_scenario2totaltradedvalue");
        SetCurrencyEditStyle("pnl_scenario3totaltradedvalue");

        SetLabelStyle("pnl_changeoverpricelabel", false);
        SetCurrencyEditStyle("pnl_scenario1changeoverpriceexclgst");
        SetCurrencyEditStyle("pnl_scenario2changeoverpriceexclgst");
        SetCurrencyEditStyle("pnl_scenario3changeoverpriceexclgst");

        SetLabelStyle("pnl_totalsgrossprofitpercentagelabel", false);
        SetCurrencyEditStyle("pnl_scenario1changeovergrossprofitpercentage");
        SetCurrencyEditStyle("pnl_scenario2changeovergrossprofitpercentage");
        SetCurrencyEditStyle("pnl_scenario3changeovergrossprofitpercentage");

        SetLabelStyle("pnl_overtradeamountlabel", false);
        SetCurrencyEditStyle("pnl_scenario2overtradeamount");
        SetCurrencyEditStyle("pnl_scenario3overtradeamount");
    }
    catch (err) {
        alert(err.message + '  NB this Form may not display correctly in browsers other than Internet Explorer.');
    }
}

function EditIsEmpty(editName) {
    var thisEdit = document.getElementById(editName);

    if (thisEdit == null) {
        throw "Failed to locate control: " + editName;
    }
    return (thisEdit.value == "");
}

function SetReadOnly(IsReadOnly, editName) {
    var thisEdit = document.getElementById(editName);

    if (thisEdit == null) {
        throw "Failed to locate control: " + editName;
    }
	if(thisEdit.disabled == false)
		thisEdit.disabled = IsReadOnly;
}

function SetLabelStyle(labelName, isRightAligned) {

    var labelEdit = document.getElementById(labelName);

        if (labelEdit == null) {
            throw "Failed to locate control: " + labelName;
        }

    var labelEditContainer = document.getElementById(labelName+"_d");

        labelEdit.disabled = true;
        if(isRightAligned)
        {
            labelEdit.style.textAlign = "right";
            labelEditContainer.style.width = "20%";
        }
        else
        {
            labelEdit.style.textAlign = "left";
            labelEditContainer.style.width = "40%";
        }

        labelEdit.style.backgroundColor = "rgb(246, 248, 250)";
        labelEdit.style.borderColor = "rgb(246, 248, 250)";
        labelEdit.style.border = "0px none";

        var parentDiv = labelEdit.parentElement;
        if (parentDiv !== null)
        {
            parentDiv.style.backgroundColor = "rgb(246, 248, 250)";
            parentDiv.style.borderColor = "rgb(246, 248, 250)";
            parentDiv.style.border = "0px none";
        }
        labelEdit.style.color = "black";
}

function SetCurrencyEditStyle(editName) {

    var thisEdit = document.getElementById(editName);

    if (thisEdit == null) {
        throw "Failed to locate control: " + editName;
    }

    var currencyEditContainer = document.getElementById(editName + "_d");
    currencyEditContainer.style.width = "20%";

    thisEdit.style.textAlign = 'right';

    thisEdit.style.color = "black";
    if (thisEdit.disabled == false) {
        thisEdit.style.borderBottomColor = 'yellow';
        thisEdit.style.backgroundColor = 'yellow';

        var currencySymbolContainer = document.getElementById(editName + "_sym");
        if (currencySymbolContainer != null) {
            currencySymbolContainer.style.borderBottomColor = 'yellow';
            currencySymbolContainer.style.backgroundColor = 'yellow';
        }
    }

    thisEdit.style.display = 'none'; 
    var redrawFix = thisEdit.offsetHeight;

    thisEdit.style.display = 'block';
}