DECLARE @pExpLineNumber int;
SET @pExpLineNumber=1;
DECLARE @pOrderNumber NVARCHAR(MAX);
SET @pOrderNumber = 'ORD-01735-D5B0';

SELECT
	salesorderdetail.productdescription, 
	salesorderdetail.pnl_tiserialnumber, 
	salesorderdetail.pnl_timake, 
	salesorderdetail.pnl_timodel, 
	salesorderdetail.pnl_timarketprice, 
	salesorderdetail.pnl_tirepaircost, 
	salesorderdetail.pnl_tisellingmargin, 
	salesorderdetail.pnl_tispecornotes, 	
	ord.pnl_tradeinmachine1description AS ord_pnl_tradeinmachine1description, 	
	ord.pnl_tradeinmachine2description AS ord_pnl_tradeinmachine2description, 	
	ord.pnl_tradeinmachine1scenario3value AS ord_pnl_tradeinmachine1scenario3value, 	
	ord.pnl_tradeinmachine2scenario3value AS ord_pnl_tradeinmachine2scenario3value, 	
	ord.pnl_tradeinmachine3scenario3value AS ord_pnl_tradeinmachine3scenario3value
FROM FilteredSalesOrderDetail AS salesorderdetail
INNER JOIN FilteredSalesOrder AS ord ON salesorderdetail.salesorderid = ord.salesorderid
WHERE salesorderdetail.pnl_istradein = 1
AND salesorderdetail.exp_linenumber = @pExpLineNumber
AND ord.ordernumber = @pOrderNumber


--<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">
--  <entity name="salesorderdetail" >
--    <attribute name="productdescription" />

--   <attribute name="pnl_tiserialnumber" />
--   <attribute name="pnl_timake" />
--   <attribute name="pnl_timodel" />
--   <attribute name="pnl_timarketprice" />
--   <attribute name="pnl_tirepaircost" />
--   <attribute name="pnl_tisellingmargin" />
--   <attribute name="pnl_tispecornotes" />

--       <filter type="and">
--	<condition attribute="pnl_istradein" operator="eq"   value="1" />
--	<condition attribute="exp_linenumber" operator="eq"   value="@pExpLineNumber" />
--      </filter> 


--   <link-entity name="salesorder" from="salesorderid" to="salesorderid" alias="ord">
--    <attribute name="ordernumber" />	

--    <attribute name="pnl_tradeinmachine1description" />	
--    <attribute name="pnl_tradeinmachine2description" />	
--    <attribute name="pnl_tradeinmachine3description" />	

--    <attribute name="pnl_tradeinmachine1scenario3value" />	
--    <attribute name="pnl_tradeinmachine2scenario3value" />	
--    <attribute name="pnl_tradeinmachine3scenario3value" />	

-- <!--  <attribute name="pPricePerUnit" value="@pPricePerUnit" /> -->
       
--<filter type="and">
--	<condition attribute="ordernumber" operator="eq"   value="@pOrderNumber" />

--      </filter> 
--    </link-entity>		

--  </entity>
--</fetch>