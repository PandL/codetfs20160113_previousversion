DECLARE @ordernumber NVARCHAR(MAX);
SET @ordernumber = 'ORD-02071-Q0Q9';

SELECT TOP 1 
	Acc1.address1_city AS Acc1_address1_city, 
	Acc1.address1_country AS Acc1_address1_country, 
	Acc1.address1_line1 AS Acc1_address1_line1, 
	Acc1.address1_line2 AS Acc1_address1_line2, 
	Acc1.address1_postalcode AS Acc1_address1_postalcode, 
	Acc1.pnl_contactname AS Acc1_pnl_contactname, 
	BUnit.address1_city AS BUnit_address1_city,
	BUnit.address1_line1 AS BUnit_address1_line1, 
	BUnit.address1_postalcode AS BUnit_address1_postalcode, 
	BUnit.name AS BUnit_name, 
	CRMAF_SalesOrder.name, 
	CRMAF_SalesOrder.ordernumber, 
	[User].address1_telephone1 AS User_address1_telephone1, 
	[User].fullname AS User_fullname, 
	[User].personalemailaddress AS User_personalemailaddress, 
	quote.pnl_landpowerquoteid AS quote_pnl_landpowerquoteid
FROM FilteredSalesOrder AS CRMAF_SalesOrder	
LEFT OUTER JOIN FilteredQuote AS quote ON CRMAF_SalesOrder.quoteid = quote.quoteid	
LEFT OUTER JOIN FilteredAccount AS Acc1 ON CRMAF_SalesOrder.customerid = Acc1.accountid
LEFT OUTER JOIN FilteredSystemUser AS [User] ON CRMAF_SalesOrder.owninguser = [User].systemuserid 
LEFT OUTER JOIN FilteredBusinessUnit AS BUnit ON [USer].businessunitid = BUnit.businessunitid
ORDER BY 
CRMAF_SalesOrder.modifiedOn DESC, 
CRMAF_SalesOrder.Name