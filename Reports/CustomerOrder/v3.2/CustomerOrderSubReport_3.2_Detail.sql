DECLARE @ordernumber NVARCHAR(MAX);
SET @ordernumber = 'ORD-02137-Y7K0';

SELECT
	IsNull(qotd.pnl_SuperCatDesc, 
		CASE WHEN salesorderdetail.pnl_islocallysourced = 1 OR locallysourceditem.pnl_locallysourceditemid IS NOT NULL THEN 'Locally Sourced' ELSE
		(SELECT TOP 1 d2.productdescription 
		FROM FilteredQuoteDetail d2 
		WHERE d2.quoteid=qotd.quoteid AND d2.pnl_spectypename='Section' 
		AND d2.pnl_specs_sales_seq_no<qotd.pnl_specs_sales_seq_no 
		AND d2.exp_linenumber=qotd.exp_linenumber
		ORDER BY d2.pnl_specs_sales_seq_no DESC) END) 
		AS pnl_SuperCatDesc,
	IsNull(qotd.pnl_specs_sales_seq_no, 999999) as pnl_specs_sales_seq_no,
	locallysourceditem.pnl_discountpercentage AS locallysourceditem_pnl_discountpercentage,
	locallysourceditem.pnl_linenumber AS locallysourceditem_pnl_linenumber,
	locallysourceditem.pnl_listprice AS locallysourceditem_pnl_listprice, 
	locallysourceditem.pnl_locallysourceddnp AS locallysourceditem_pnl_locallysourceddnp,
	locallysourceditem.pnl_name AS locallysourceditem_pnl_name, 
	ord.ordernumber AS ord_ordernumber, 	
	ord.pnl_salesaidapproved AS ord2_pnl_salesaidapproved, 
	ord.pnl_totalotherdealercosts AS ord2_pnl_totalotherdealercosts, 	
	qotd.pnl_specs_sales_seq_no AS qotd_pnl_specs_sales_seq_no,
	IsNull(salesorderdetail.exp_linenumber,locallysourceditem.pnl_confignumber) AS ConfigNumber, 
	salesorderdetail.extendedamount, 
	salesorderdetail.pnl_basepriceandselectedspecs,
	salesorderdetail.pnl_bpass_dnpdollar,
	salesorderdetail.pnl_instkisdealerstock, 
	salesorderdetail.pnl_instklocation,  
	salesorderdetail.pnl_instkmachinestatusname,  
	salesorderdetail.pnl_instkprovisional_serial_no,  
	salesorderdetail.pnl_instkserial_no,
	CASE WHEN salesorderdetail.pnl_islocallysourced=0 AND locallysourceditem.pnl_locallysourceditemid IS NULL THEN 0 ELSE 1 END AS pnl_islocallysourced,  
	salesorderdetail.pnl_istradein,
	salesorderdetail.lineitemnumber,
	salesorderdetail.pnl_locallysourceddnp,
	salesorderdetail.pnl_machinecostsindentdiscount, 
	salesorderdetail.pnl_machinecostsmonthlyprogdollar, 
	salesorderdetail.pnl_machinecostsmonthlyprogpercent, 
	salesorderdetail.pnl_machinecostsstandarddiscount, 
	salesorderdetail.pnl_mod_sup_stock_code, 
	salesorderdetail.pnl_quoteproductlocallysourceditemname,
	salesorderdetail.pnl_spec_stock_code, 
	salesorderdetail.pnl_specs_sales_stock_code, 
	salesorderdetail.pnl_spectypename, 
	salesorderdetail.priceperunit * salesorderdetail.quantity as listPrice,
	salesorderdetail.priceperunit, 
	salesorderdetail.productdescription, 
	salesorderdetail.productid, 
	salesorderdetail.quantity
FROM FilteredSalesorderDetail AS salesorderdetail
INNER JOIN FilteredSalesOrder AS ord ON salesorderdetail.salesorderid = ord.salesorderid
LEFT OUTER JOIN FilteredQuoteDetail AS qotd ON (salesorderdetail.exp_recordid = qotd.exp_recordid AND salesorderdetail.exp_linenumber = qotd.exp_linenumber AND qotd.createdonbehalfby IS NOT NULL)
LEFT OUTER JOIN Filteredpnl_locallysourceditem AS locallysourceditem ON salesorderdetail.pnl_quoteproductlocallysourceditem = locallysourceditem.pnl_locallysourceditemid
WHERE ord.ordernumber = @ordernumber
AND (salesorderdetail.pnl_spectypename IS NULL OR salesorderdetail.pnl_spectypename <> 'Section')
ORDER BY IsNull(salesorderdetail.exp_linenumber, locallysourceditem.pnl_confignumber), 
	salesorderdetail.pnl_istradein, 
	CASE WHEN salesorderdetail.pnl_islocallysourced=0 AND locallysourceditem.pnl_locallysourceditemid IS NULL THEN 0 ELSE 1 END, 
	IsNull(qotd.pnl_specs_sales_seq_no, 999999)
