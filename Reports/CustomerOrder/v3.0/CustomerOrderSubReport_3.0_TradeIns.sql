DECLARE @ordernumber NVARCHAR(MAX);
SET @ordernumber = 'ORD-01735-D5B0';

SELECT 
	salesorderdetail.exp_linenumber, 
	salesorderdetail.pnl_timake, 
	salesorderdetail.pnl_timarketprice, 
	salesorderdetail.pnl_timodel, 
	salesorderdetail.pnl_tirepaircost, 
	salesorderdetail.pnl_tisellingmargin, 
	salesorderdetail.pnl_tiserialnumber, 
	salesorderdetail.pnl_tispecornotes, 
	salesorderdetail.productdescription	
FROM FilteredSalesorderDetail AS salesorderdetail
INNER JOIN FilteredSalesOrder AS ord ON salesorderdetail.salesorderid = ord.salesorderid
WHERE salesorderdetail.pnl_istradein = 1
AND ord.ordernumber = @ordernumber