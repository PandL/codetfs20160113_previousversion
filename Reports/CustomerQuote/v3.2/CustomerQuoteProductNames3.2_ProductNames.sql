DECLARE @pQuote uniqueidentifier;
SET @pQuote = '2F54D53A-153F-E511-80FA-0050568A79C5'

SELECT quotedetail.productdescription
FROM FilteredQuoteDetail quotedetail
WHERE quotedetail.quoteid = @pQuote
AND quotedetail.pnl_spectype = 125760000
ORDER BY pnl_basepriceandselectedspecs DESC


<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">
  <entity name="quotedetail" >
    <attribute name="productdescription" />
    <order attribute="pnl_basepriceandselectedspecs" descending="true" />
    <filter type="and">
        <condition attribute="quoteid" operator="eq" value="@pQuote" />
    </filter>
    <filter type="and">
        <condition attribute="pnl_spectype" operator="eq" value="125760000" />
    </filter>
  </entity>
</fetch>