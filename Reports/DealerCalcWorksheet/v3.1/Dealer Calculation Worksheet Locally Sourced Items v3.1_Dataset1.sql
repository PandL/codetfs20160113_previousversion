DECLARE @pQuote uniqueidentifier; SET @pQuote='BE8F0D50-914B-E511-80FA-0050568A79C5';

SELECT
   pnl_locallysourcedmachinename,
   productdescription,
   priceperunit,
   pnl_locallysourceddnp,
   pnl_locallysourcedstandarddiscount,
   'quotedetail' as itemsource
FROM Filteredquotedetail
WHERE quoteid = @pQuote
AND pnl_islocallysourcedname = 'Yes'
UNION ALL
SELECT
   pnl_locallysourcedmachinename AS pnl_locallysourcedmachinename,
   pnl_name as productdescription,
   pnl_listprice AS priceperunit,
   pnl_locallysourceddnp,
   pnl_discountpercentage AS pnl_locallysourcedstandarddiscount,
   'locallysourceditem' as itemsource
FROM Filteredpnl_locallysourceditem
WHERE pnl_quote = @pQuote
ORDER BY 1 DESC
