DECLARE @pQuote uniqueidentifier; SET @pQuote='AB2D6C87-ED49-E511-80FA-0050568A79C5';

SELECT
   productdescription,
   pnl_basepriceandselectedspecs,
   pnl_bpass_dnpdollar,
   pnl_machinecostsstandarddiscount,
   pnl_machinecostsindentdiscount,
   pnl_machinecostsmonthlyprogpercent,
   pnl_machinecostsmonthlyprogdollar,
   pnl_instkisdealerstock,
   pnl_instkprovisional_serial_no,
   pnl_instkserial_no,
   quotedetailid
FROM Filteredquotedetail
WHERE quoteid = @pQuote
AND pnl_islocallysourced <> 1
AND pnl_istradein <> 1 
AND pnl_spectypename = 'Product'
ORDER BY productdescription DESC