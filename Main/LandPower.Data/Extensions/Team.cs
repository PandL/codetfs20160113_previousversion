﻿using System;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace LandPower.Data
{
	public interface ITeamExtensions
	{
		Team FetchTeamByName(string teamName);
	}

	public static class TeamExtension
	{
		public static Func<CrmServiceContext, ITeamExtensions> TeamFactory = serviceContext => new TeamExtensions(serviceContext); // Could be internal with InternalsVisibleTo in tests project.

		public static ITeamExtensions Team(this CrmServiceContext serviceContext)
		{
			return TeamFactory(serviceContext);
		}
	}

	internal class TeamExtensions : ITeamExtensions
	{
		private readonly CrmServiceContext _serviceContext;

		public TeamExtensions(CrmServiceContext serviceContext)
		{
			_serviceContext = serviceContext;
		}

		public Team FetchTeamByName(string teamName)
		{
			return (from t in _serviceContext.TeamSet
					where t.Name == teamName
					select t).FirstOrDefault();
		}
	}
}
