﻿using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace LandPower.Data
{
	public interface IComparativeOperatorExtensions
	{
		IEnumerable<pnl_comparativeoperator> FetchComparativeOperators();
	}

	public static class ComparativeOperatorExtension
	{
		internal static Func<CrmServiceContext, IComparativeOperatorExtensions> OperatorFactory = serviceContext => new ComparativeOperatorExtensions(serviceContext);

		public static IComparativeOperatorExtensions ComparativeOperator(this CrmServiceContext serviceContext)
		{
			return OperatorFactory(serviceContext);
		}
	}

	internal class ComparativeOperatorExtensions : IComparativeOperatorExtensions
	{
		private readonly CrmServiceContext _serviceContext;

		public ComparativeOperatorExtensions(CrmServiceContext serviceContext)
		{
			_serviceContext = serviceContext;
		}

		public IEnumerable<pnl_comparativeoperator> FetchComparativeOperators()
		{
			return (from c in _serviceContext.pnl_comparativeoperatorSet
					where c.statecode == pnl_comparativeoperatorState.Active
					select c);
		}
	}
}
