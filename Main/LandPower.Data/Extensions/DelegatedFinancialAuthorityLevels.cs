﻿using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace LandPower.Data
{
	public interface IDelegatedFinancialAuthorityLevelExtensions
	{
		IEnumerable<pnl_delegatedfinancialauthoritylevel> FetchDelegatedFinancialAuthorityLevelsForTradeIn();
		bool IsDuplicate(string level, int delegatedFinancialAuthorityLevelType, Guid currentRecordId);
	}

	public static class DelegatedFinancialAuthorityLevelExtension
	{
		// Could be internal with InternalsVisibleTo in tests project.
		public static Func<CrmServiceContext, IDelegatedFinancialAuthorityLevelExtensions> DfaFactory = serviceContext => new DelegatedFinancialAuthorityLevelExtensions(serviceContext);

		public static IDelegatedFinancialAuthorityLevelExtensions DelegatedFinancialAuthorityLevel(this CrmServiceContext serviceContext)
		{
			return DfaFactory(serviceContext);
		}
	}

	internal class DelegatedFinancialAuthorityLevelExtensions : IDelegatedFinancialAuthorityLevelExtensions
	{
		private readonly CrmServiceContext _serviceContext;

		public DelegatedFinancialAuthorityLevelExtensions(CrmServiceContext serviceContext)
		{
			_serviceContext = serviceContext;
		}

		public IEnumerable<pnl_delegatedfinancialauthoritylevel> FetchDelegatedFinancialAuthorityLevelsForTradeIn()
		{
			return (from d in _serviceContext.pnl_delegatedfinancialauthoritylevelSet
					where d.statecode == pnl_delegatedfinancialauthoritylevelState.Active
					&& d.pnl_Type.Value == (int)pnl_delegatedfinancialauthoritytype.Tradein // pnl_TypeEnum causes attributename error.
					select d);
		}

		public bool IsDuplicate(string level, int delegatedFinancialAuthorityLevelType, Guid currentRecordId)
		{
			// Note, Any method is not supported by CRM.
			var existingRecord = _serviceContext.pnl_delegatedfinancialauthoritylevelSet.FirstOrDefault(d => d.pnl_Level == level
																										&& d.pnl_Type.Value == delegatedFinancialAuthorityLevelType
																										&& d.Id != currentRecordId);
			return existingRecord != null;
		}
	}
}