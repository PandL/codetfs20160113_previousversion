﻿using System;
using System.Collections.Generic;
using System.Linq;

using DynamicExpresso;
using Microsoft.Xrm.Sdk;

using LandPower.Data;

namespace LandPower.BusinessLogic
{
	public static class AppraisalPhotoExtension
	{
		public static int GetPhotoCount(this pnl_appraisalphoto appraisalPhoto, CrmServiceContext serviceContext)
		{
			return serviceContext.AppraisalPhoto().GetPhotoCount(appraisalPhoto.Id);
		}
	}
}
