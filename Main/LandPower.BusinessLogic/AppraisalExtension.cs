﻿using System;
using System.Collections.Generic;
using System.Linq;

using DynamicExpresso;
using Microsoft.Xrm.Sdk;

using LandPower.Data;

namespace LandPower.BusinessLogic
{
    public static class AppraisalExtension
    {
	    private const string DefaultValue = "<Not Specified>";

	    public static string GenerateName(this pnl_appraisal appraisal, CrmServiceContext serviceContext)
	    {
			// Customer : appraisaldate : machinetype
			const string format = "{0} : {1} : {2}";

			var customer = serviceContext.Appraisal().FetchAppraisalCustomer(appraisal.Id);
			if (string.IsNullOrEmpty(customer)) customer = DefaultValue;

			var machineType = serviceContext.Appraisal().FetchAppraisalMachineType(appraisal.Id);
			if (string.IsNullOrEmpty(machineType)) machineType = DefaultValue;

		    var appraisalDate = appraisal.pnl_appraisaldate.GetValueOrDefault();
			var appraisalDateValue = appraisalDate == DateTime.MinValue
				? DefaultValue
				: string.Format("{0:dd/M/yy}", appraisalDate.ToLocalTime());

			return string.Format(format, customer, appraisalDateValue, machineType);
	    }

	    public static void SetAppraisalItemsReadOnly(this pnl_appraisal appraisal, IOrganizationService organizationService, bool readOnly = true)
	    {
		    var context = new CrmServiceContext(organizationService);
		    var items = context.Appraisal().FetchAppraisalItems(appraisal.Id);

		    foreach (var item in items)
		    {
			    SetAppraisalItemReadOnly(item.Id, organizationService, readOnly);
		    }
	    }

	    public static decimal GetAppraisalItemValue(this pnl_appraisal appraisal, string itemName,
		    IOrganizationService organizationService, string itemGroupName = "Costs")
	    {
		    using (var xrmContext = new CrmServiceContext(organizationService))
		    {
				var item = xrmContext.AppraisalItem().FetchAppraisalItemByNameAndGroupName(appraisal.Id, itemName, itemGroupName);
			    return null == item ? 0m : item.GetItemValue();
		    }
	    }

		public static void UpdateAppraisalItemValue(this pnl_appraisal appraisal, string itemName, Money value, IOrganizationService organizationService, string itemGroupName = "Costs")
		{
			using (var xrmContext = new CrmServiceContext(organizationService))
			{
				var item = xrmContext.AppraisalItem().FetchAppraisalItemByNameAndGroupName(appraisal.Id, itemName, itemGroupName);
				if (null == item) return; // Maybe we should create the item in this case?

				var itemValue = item.GetItemValue();
				var newValue = value == null ? 0m : Math.Round(value.Value, 0, MidpointRounding.AwayFromZero);
				if (itemValue == newValue) return;

				var entity = new Entity("pnl_appraisalitem") {Id = item.Id};
				entity.Attributes.Add("pnl_value", newValue.ToString("N0")); // e.g. 61,453 (group separator, 0 decimal places).
				organizationService.Update(entity);
			}
		}

	    internal static void SetAppraisalItemReadOnly(Guid appraisalItemId, IOrganizationService organizationService, bool readOnly = true)
	    {
		    var entity = new Entity("pnl_appraisalitem") {Id = appraisalItemId};
			entity.Attributes.Add("pnl_isreadonly", readOnly);
			organizationService.Update(entity);
	    }

		public static string BuildAppraisalLink(this pnl_appraisal appraisal, string organizationName)
		{
			var random = new Random();
			var histKey = random.Next(100000000, 900000000);
			var url = string.Format("https://{0}.landpower.co.nz/main.aspx?etn=pnl_appraisal&histKey={1}&id={2}&pagetype=entityrecord",
				organizationName, histKey, appraisal.Id);
			url = Uri.EscapeUriString(url);

			return string.Format("<a href=\"{0}\">{1}</a>", url, System.Net.WebUtility.HtmlEncode(appraisal.pnl_name));
		}

	    public static bool CanApplyChanges(this pnl_appraisal appraisal, Guid userId, CrmServiceContext serviceContext)
	    {
			var user = new SystemUser { Id = userId };

		    if (appraisal.statuscode.Value == (int)pnl_appraisal_statuscode.ApprovedApprasial_125760001 || appraisal.statuscode.Value ==
				    (int)pnl_appraisal_statuscode.ApprovedApprasial_125760003)
		    {
				return user.IsInRole("System Administrator", serviceContext);
		    }

		    if (appraisal.statuscode.Value == (int) pnl_appraisal_statuscode.Inspection_1 ||
		        appraisal.statuscode.Value == (int) pnl_appraisal_statuscode.Inspection_2)
		    {
			    return true;
		    }

		    return user.IsTeamMember("Appraisal Approvers", serviceContext);
	    }

		public static bool CanUploadPhotos(this pnl_appraisal appraisal, CrmServiceContext serviceContext)
		{
			return (appraisal.statuscode.Value == (int)pnl_appraisal_statuscode.Inspection_1 ||
				appraisal.statuscode.Value == (int)pnl_appraisal_statuscode.Inspection_2);
		}

	    public static bool HasRequiredItemValues(this pnl_appraisal appraisal, CrmServiceContext serviceContext)
	    {
		    var items = serviceContext.Appraisal().FetchAppraisalValuationItems(appraisal.Id);
		    
			return items.All(item => !string.IsNullOrEmpty(item.pnl_Value));
	    }

		public static bool HaveAllPhotosUploaded(this pnl_appraisal appraisal, CrmServiceContext serviceContext)
		{
			var photoCount = serviceContext.Appraisal().FetchAppraisalPhotoAttachmentCount(appraisal.Id);

			return photoCount == appraisal.pnl_itradephotocount.GetValueOrDefault();
		}

		public static IEnumerable<ActivityParty> GetActivityPartiesForApproval(this pnl_appraisal appraisal,
		    CrmServiceContext serviceContext)
	    {
		    var approvers = new List<ActivityParty>();
			// This code works as originally specified but use single to and cc user lookup for now.
			//ActivityParty toParty = null;
			//var ccParties = new Dictionary<Guid, ActivityParty>();

			var dfa = serviceContext.DelegatedFinancialAuthority().FetchDelegatedFinancialAuthorityForTradeInForUser(appraisal.OwnerId.Id);
			if (null == dfa) return approvers;
			//var dfaEntity = dfa.ToEntity<Entity>();

			//var levels = serviceContext.DelegatedFinancialAuthorityLevel().FetchDelegatedFinancialAuthorityLevelsForTradeIn();
			//var operators = serviceContext.ComparativeOperator().FetchComparativeOperators().ToDictionary(o => o.Id, o => o.pnl_symbol);

			//foreach (var level in levels)
			//{
			//	var approverReference = dfaEntity.GetAttributeValue<EntityReference>("pnl_level" + Utility.GetNumbers(level.pnl_Level) + "approverid");
			//	if (null == approverReference) continue;
			//	var approver = serviceContext.User().FetchSystemUserById(approverReference.Id);

			//	var recommendedTradePrice = appraisal.pnl_RecommendedTradePrice == null
			//		? 0m
			//		: appraisal.pnl_RecommendedTradePrice.Value;
			//	if (EvaluateAppraisalDfaLevel(recommendedTradePrice, operators[level.pnl_ComparativeID.Id], level.pnl_Amount.GetValueOrDefault()) && !approver.IsAway())
			//	{
			//		toParty = CreateToParty(approver);
			//	}
			//	else if (!ccParties.ContainsKey(approver.Id))
			//	{
			//		ccParties.Add(approver.Id, CreateCcParty(approver));
			//	}

			//	if (null == toParty) continue;
			//	approvers.Add(toParty);
			//	break;
			//}

			//if (null != toParty && ccParties.Keys.Contains(toParty.PartyId.Id))
			//{
			//	ccParties.Remove(toParty.PartyId.Id);
			//}
			//approvers.AddRange(ccParties.Values);

			var toUser = serviceContext.User().FetchSystemUserById(dfa.pnl_EmailTo.Id);
			if (dfa.pnl_EmailCC != null)
			{
				var ccUser = serviceContext.User().FetchSystemUserById(dfa.pnl_EmailCC.Id);
				approvers.Add(CreateCcParty(ccUser));
			}
			approvers.Add(CreateToParty(toUser));

		    return approvers;
	    }

	    internal static ActivityParty CreateToParty(SystemUser approver)
	    {
			var toParty = new ActivityParty
			{
				PartyId = new EntityReference("systemuser", approver.Id),
				ParticipationTypeMaskEnum = activityparty_participationtypemask.ToRecipient
			};
			toParty.PartyId.Name = approver.FullName;

		    return toParty;
	    }

	    internal static ActivityParty CreateCcParty(SystemUser approver)
	    {
			var ccParty = new ActivityParty
			{
				PartyId = new EntityReference("systemuser", approver.Id),
				ParticipationTypeMaskEnum = activityparty_participationtypemask.CCRecipient
			};
			ccParty.PartyId.Name = approver.FullName;

		    return ccParty;
	    }

	    internal static bool EvaluateAppraisalDfaLevel(decimal recommendedTradePrice, string operatorSymbol, decimal levelAmount)
	    {
			// Don't use DynamicExpresso because the 
		    var interpreter = new Interpreter();
		    var expression = recommendedTradePrice + operatorSymbol + levelAmount;

			return (bool) interpreter.Eval(expression);
	    }

	    public static decimal GetAverageValuation(this pnl_appraisal appraisal, CrmServiceContext serviceContext)
	    {
			//Average the non-zero items.
		    var items = serviceContext.Appraisal().FetchAppraisalValuationItems(appraisal.Id);
		    var totalValue = 0m;
		    var itemCount = 0;

		    foreach (var item in items)
		    {
			    var itemValue = item.GetItemValue();
			    if (itemValue == 0m) continue;
			    itemCount++;
			    totalValue += itemValue;
		    }

		    return itemCount > 0 ? Math.Round(totalValue/itemCount, 0, MidpointRounding.AwayFromZero) : 0m;
	    }

	    public static decimal GetTotalCost(this pnl_appraisal appraisal)
	    {
			var totalCost = appraisal.pnl_OtherCosts == null ? 0m : Math.Round(appraisal.pnl_OtherCosts.Value, 0, MidpointRounding.AwayFromZero);
			totalCost += (appraisal.pnl_RepairCosts == null ? 0m : Math.Round(appraisal.pnl_RepairCosts.Value, 0, MidpointRounding.AwayFromZero));
			totalCost += (appraisal.pnl_Freight == null ? 0m : Math.Round(appraisal.pnl_Freight.Value, 0, MidpointRounding.AwayFromZero));
			totalCost += (appraisal.pnl_PreDeliveryExpenses == null ? 0m : Math.Round(appraisal.pnl_PreDeliveryExpenses.Value, 0, MidpointRounding.AwayFromZero));

		    return totalCost;
	    }

	    public static decimal GetExpectedRetainedMarginDollars(this pnl_appraisal appraisal)
	    {
		    if (null == appraisal.pnl_RecommededListPrice) return 0m;

		    return Math.Round((appraisal.pnl_RecommededListPrice.Value*appraisal.pnl_ExpectedRetainedMarginPercentage.GetValueOrDefault())/
		           100m, 0, MidpointRounding.AwayFromZero);
	    }

	    public static decimal GetCalculatedTradePrice(this pnl_appraisal appraisal, decimal averageValuation)
	    {
		    //var calculatedTradePrice = averageValuation - appraisal.GetTotalCost() - appraisal.GetExpectedRetainedMarginDollars();
			var calculatedTradePrice = (appraisal.pnl_RecommededListPrice == null
										   ? 0m
										   : Math.Round(appraisal.pnl_RecommededListPrice.Value, 0, MidpointRounding.AwayFromZero)) - appraisal.GetTotalCost() - appraisal.GetExpectedRetainedMarginDollars();

		    return calculatedTradePrice;
	    }

	    public static decimal GetActualRetainedMarginDollars(this pnl_appraisal appraisal)
	    {
		    var averageValuation = appraisal.pnl_CalculatedAverage == null ? 0m : Math.Round(appraisal.pnl_CalculatedAverage.Value, 0, MidpointRounding.AwayFromZero);
		    var totalCost = appraisal.pnl_TotalCosts == null ? 0m : Math.Round(appraisal.pnl_TotalCosts.Value, 0, MidpointRounding.AwayFromZero);
		    var approvedPrice = appraisal.pnl_ApprovedTradePrice == null ? 0m : Math.Round(appraisal.pnl_ApprovedTradePrice.Value, 0, MidpointRounding.AwayFromZero);

		    return averageValuation - totalCost - approvedPrice;
	    }

	    public static decimal GetActualRetainedMarginPercentage(this pnl_appraisal appraisal)
	    {
		    var retainedMargin = appraisal.GetActualRetainedMarginDollars();
		    var recommendedListPrice = appraisal.pnl_RecommededListPrice == null
			    ? 0m
			    : Math.Round(appraisal.pnl_RecommededListPrice.Value, 0, MidpointRounding.AwayFromZero);

		    return recommendedListPrice == 0m ? 0m : Math.Round((retainedMargin/recommendedListPrice)*100, 0, MidpointRounding.AwayFromZero);
	    }
    }
}
