﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
//using LandPower.Data;


//DURING PLUGIN RE-WRITE
// Please change the context depth to "context.Depth == 3 || context.Depth == 1"  

namespace AppraisalSourceNameValue
{
    public class AppraisalSourceNameValue : IPlugin
    {

        public void Execute(IServiceProvider serviceProvider)
        {
                Microsoft.Xrm.Sdk.IPluginExecutionContext context = (Microsoft.Xrm.Sdk.IPluginExecutionContext)
                serviceProvider.GetService(typeof(Microsoft.Xrm.Sdk.IPluginExecutionContext));

                if (context.Depth == 1)
                {
                    IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                    IOrganizationService _organizationService = serviceFactory.CreateOrganizationService(context.UserId);
                    Entity entity = (Entity)context.InputParameters["Target"];
                    Entity postEntity = (Entity)context.PostEntityImages["entity"];
                    
                    string sourceCode = "";
                    if (entity.Contains("pnl_source1name") || entity.Contains("pnl_source1value"))
                    {
                        sourceCode = "SOURCE_1";
                        var fetchxml = GetFetchXml(entity, sourceCode);
                        var entityResults = _organizationService.RetrieveMultiple(new FetchExpression(fetchxml));
                        if (entityResults.Entities == null) return;

                        if (entityResults.Entities.Count == 1)
                        {

                            foreach (var o in entityResults.Entities)
                            {
                                o.Attributes["pnl_displayname"] = (postEntity.Contains("pnl_source1name")) ?
                                    postEntity.Attributes["pnl_source1name"].ToString() : null;
                                o.Attributes["pnl_value"] = (postEntity.Contains("pnl_source1value")) ?
                                    (((Money)postEntity.Attributes["pnl_source1value"]).Value).ToString("0,0") : null;

                                _organizationService.Update(o);
                            }
                        }
                    }
                    if (entity.Contains("pnl_source2name") || entity.Contains("pnl_source2value"))
                    {
                        sourceCode = "Source_2";
                        var fetchxml = GetFetchXml(entity, sourceCode);
                        var entityResults = _organizationService.RetrieveMultiple(new FetchExpression(fetchxml));
                        if (entityResults.Entities == null) return;
                        if (entityResults.Entities.Count == 1)
                        {
                            foreach (var o in entityResults.Entities)
                            {
                                o.Attributes["pnl_displayname"] = postEntity.Contains("pnl_source2name") ?
                                    postEntity.Attributes["pnl_source2name"].ToString() : null;
                                o.Attributes["pnl_value"] = (postEntity.Contains("pnl_source2value")) ?
                                     (((Money)postEntity.Attributes["pnl_source2value"]).Value).ToString("0,0") : null;
                                _organizationService.Update(o);
                            }
                        }
                    }
                    if (entity.Contains("pnl_source3name") || entity.Contains("pnl_source3value"))
                    {
                        sourceCode = "Source_3";
                        var fetchxml = GetFetchXml(entity, sourceCode);
                        var entityResults = _organizationService.RetrieveMultiple(new FetchExpression(fetchxml));
                        if (entityResults.Entities == null) return;
                        if (entityResults.Entities.Count == 1)
                        {
                            foreach (var o in entityResults.Entities)
                            {
                                o.Attributes["pnl_displayname"] = (postEntity.Contains("pnl_source3name")) ?
                                    postEntity.Attributes["pnl_source3name"].ToString() : null;
                                o.Attributes["pnl_value"] = (postEntity.Contains("pnl_source3value")) ?
                                     (((Money)postEntity.Attributes["pnl_source3value"]).Value).ToString("0,0") : null;
                                _organizationService.Update(o);
                            }
                        }

                    }
                    if (entity.Contains("pnl_source4name") || entity.Contains("pnl_source4value"))
                    {
                        sourceCode = "Source_4";
                        var fetchxml = GetFetchXml(entity, sourceCode);
                        var entityResults = _organizationService.RetrieveMultiple(new FetchExpression(fetchxml));
                        if (entityResults.Entities == null) return;
                        if (entityResults.Entities.Count == 1)
                        {
                            foreach (var o in entityResults.Entities)
                            {
                                o.Attributes["pnl_displayname"] = (postEntity.Contains("pnl_source4name")) ?
                                    postEntity.Attributes["pnl_source4name"].ToString() : null;
                                o.Attributes["pnl_value"] = (postEntity.Contains("pnl_source4value")) ?
                                     (((Money)postEntity.Attributes["pnl_source4value"]).Value).ToString("0,0") : null;
                                _organizationService.Update(o);
                            }
                        }

                    }
                    if (entity.Contains("pnl_workshopnotes"))
                    {
                        sourceCode = "WORKSHOP_NOTES";
                        var fetchxml = GetFetchXml(entity, sourceCode);
                        var entityResults = _organizationService.RetrieveMultiple(new FetchExpression(fetchxml));
                        if (entityResults.Entities == null) return;
                        if (entityResults.Entities.Count == 1)
                        {
                            foreach (var o in entityResults.Entities)
                            {
                                o.Attributes["pnl_value"] = (postEntity.Contains("pnl_workshopnotes")) ?
                                    postEntity.Attributes["pnl_workshopnotes"].ToString() : null;
                                _organizationService.Update(o);
                            }
                        }

                    }
                    if (entity.Contains("pnl_descriptionnotes"))
                    {
                        sourceCode = "DESCRIPTION_NOTES";
                        var fetchxml = GetFetchXml(entity, sourceCode);
                        var entityResults = _organizationService.RetrieveMultiple(new FetchExpression(fetchxml));
                        if (entityResults.Entities == null) return;
                        if (entityResults.Entities.Count == 1)
                        {
                            foreach (var o in entityResults.Entities)
                            {
                                o.Attributes["pnl_value"] = (postEntity.Contains("pnl_descriptionnotes")) ?
                                    postEntity.Attributes["pnl_descriptionnotes"].ToString() : null;
                                _organizationService.Update(o);
                            }
                        }

                    }
                    if (entity.Contains("pnl_expectedretainedmarginpercentage"))
                    {
                        sourceCode = "MARGIN_PERCENT";
                        var fetchxml1 = GetFetchXml(entity, sourceCode);
                        var entityResults1 = _organizationService.RetrieveMultiple(new FetchExpression(fetchxml1));
                        if (entityResults1.Entities == null) return;

                        if (entityResults1.Entities.Count == 1)
                        {
                            foreach (var o in entityResults1.Entities)
                            {
                                o.Attributes["pnl_value"] = (postEntity.Contains("pnl_expectedretainedmarginpercentage")) ?
                                   ((decimal) postEntity.Attributes["pnl_expectedretainedmarginpercentage"]).ToString("0") : null;
                                _organizationService.Update(o);
                            }
                        }
                    }                    
                    if (entity.Contains("pnl_expectedretainedmargindollar"))
                    {
                        sourceCode = "RETAINED_MARGIN";
                        var fetchxml = GetFetchXml(entity, sourceCode);
                        var entityResults = _organizationService.RetrieveMultiple(new FetchExpression(fetchxml));
                        if (entityResults.Entities == null) return;

                        if (entityResults.Entities.Count == 1)
                        {
                            foreach (var o in entityResults.Entities)
                            {
                                o.Attributes["pnl_value"] = (postEntity.Contains("pnl_expectedretainedmargindollar")) ?
                                    (((Money)postEntity.Attributes["pnl_expectedretainedmargindollar"]).Value).ToString("0,0") : null;
                                _organizationService.Update(o);
                            }
                        }
                    }
                    if (entity.Contains("pnl_recommededlistprice"))
                    {
                        sourceCode = "REC_LIST_PRICE";
                        var fetchxml = GetFetchXml(entity, sourceCode);
                        var entityResults = _organizationService.RetrieveMultiple(new FetchExpression(fetchxml));
                        if (entityResults.Entities == null) return;

                        if (entityResults.Entities.Count == 1)
                        {
                            foreach (var o in entityResults.Entities)
                            {
                                o.Attributes["pnl_value"] = (postEntity.Contains("pnl_recommededlistprice")) ?
                                    (((Money)postEntity.Attributes["pnl_recommededlistprice"]).Value).ToString("0,0") : null;
                                _organizationService.Update(o);
                            }
                        }
                    }
                    if (entity.Contains("pnl_recommendedtradeprice"))
                    {
                        sourceCode = "REC_TRADE_PRICE";
                        var fetchxml = GetFetchXml(entity, sourceCode);
                        var entityResults = _organizationService.RetrieveMultiple(new FetchExpression(fetchxml));
                        if (entityResults.Entities == null) return;

                        if (entityResults.Entities.Count == 1)
                        {
                            foreach (var o in entityResults.Entities)
                            {
                                o.Attributes["pnl_value"] = (postEntity.Contains("pnl_recommendedtradeprice")) ?
                                    (((Money)postEntity.Attributes["pnl_recommendedtradeprice"]).Value).ToString("0,0") : null;
                                _organizationService.Update(o);
                            }
                        }
                    }
                }
        }
        private string GetFetchXml(Entity entity, string code)
          {
            string appItem = @"<fetch distinct='false' mapping='logical'>" +
                                                "<entity name='pnl_appraisalitem'>" +
                                                    "<attribute name='pnl_displayname'/>" +
                                                    "<attribute name='pnl_value'/>" +
                                                    "<attribute name='pnl_calccode'/>" +
                                                    "<filter type='and'>" +
                                                          "<condition attribute='pnl_calccode' operator='eq' value='" + code + "'></condition>" +
                                                          "<condition attribute='pnl_appraisalid' operator='eq' uitype='"+ entity.LogicalName +"' value='" + entity.Id + "'></condition>" +
                                                          "</filter>" +
                                                        "</entity>" +
                                                       "</fetch>";
            return appItem;
        }           
   }
}
  