﻿using LandPower;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

namespace CustomerMachine_Update
{
    public class CustomerMachine_Update : IPlugin
    {
        IOrganizationService _service;

        public void Execute(IServiceProvider serviceProvider)
        {

            ITracingService tracingService =
                (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));

            _service = serviceFactory.CreateOrganizationService(context.UserId);
            
            if (context.Depth > 1)
                return;
            crmServiceContext _ctx = new LandPower.crmServiceContext(_service);


            if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
            {
                if (context.MessageName.ToUpper() == "CREATE" || context.MessageName.ToUpper() == "UPDATE")
                {
                    Entity postEntity = (Entity)context.PostEntityImages["entity"];

                    if (postEntity.Contains("pnl_machineid") && postEntity.Attributes["pnl_machineid"] != null
                    && postEntity.Contains("pnl_customerid") && postEntity.Attributes["pnl_customerid"] != null)
                    {
                        EntityReference machine = (EntityReference)postEntity.Attributes["pnl_machineid"];
                        EntityReference customer = (EntityReference)postEntity.Attributes["pnl_customerid"];

                        ColumnSet attributes = new ColumnSet(new string[] { "pnl_name", "pnl_currentcustomerid" });
                        pnl_machine relatedMachine = new pnl_machine();
                        relatedMachine = (pnl_machine)_service.Retrieve(machine.LogicalName, machine.Id, attributes);
                        if (!postEntity.Contains("pnl_enddate") && relatedMachine.Contains("pnl_currentcustomerid")
                            && relatedMachine.pnl_CurrentCustomerid.Id != customer.Id)
                        {
                            Entity updateMachine = new Entity("pnl_machine");
                            updateMachine.Id = relatedMachine.Id;
                            updateMachine.Attributes["pnl_currentcustomerid"] = (EntityReference)customer;
                            _service.Update(updateMachine);
                            //throw new InvalidPluginExecutionException("Current Customer on the machine does not mactch with Customer on Customer-Machine, please verify.");
                        }
                        else if (postEntity.Contains("pnl_enddate") && postEntity.Attributes["pnl_enddate"] != null)
                        {
                            var custm = (from cm in _ctx.pnl_customermachineSet
                                         where cm.pnl_Machineid.Id.Equals(machine.Id)
                                               && cm.pnl_EndDate == null
                                         orderby cm.pnl_StartDate descending
                                         select cm).FirstOrDefault();
                            if (custm != null)
                            {
                                Entity updateMachine = new Entity("pnl_machine");
                                updateMachine.Id = custm.pnl_Machineid.Id;
                                updateMachine.Attributes["pnl_currentcustomerid"] = custm.pnl_CustomerId;
                                _service.Update(updateMachine);
                            }
                            else
                            {
                                Entity updateMachineCust = new Entity("pnl_machine");
                                updateMachineCust.Id = machine.Id;
                                updateMachineCust.Attributes["pnl_currentcustomerid"] = null;
                                _service.Update(updateMachineCust);
                            }

                        }
                    }
                }
            }
                       // else if ((postEntity.Contains("pnl_enddate") && postEntity.Attributes["pnl_enddate"] != null)
                            //&& assosicatedMachine.pnl_CurrentCustomerid.Id != customer.Id)
            if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is EntityReference)
            {
                if (context.MessageName.ToUpper() == "DELETE")
                {
                    Entity preEntity = (Entity)context.PreEntityImages["entity"];
                    EntityReference machine = (EntityReference)preEntity.Attributes["pnl_machineid"];
                    //throw new InvalidPluginExecutionException("Current Customer on the machine does not mactch with Customer on Customer-Machine, please verify.");
                    var custm = (from cm in _ctx.pnl_customermachineSet
                                 where cm.pnl_Machineid.Id.Equals(machine.Id)
                                       && cm.pnl_EndDate == null
                                 orderby cm.pnl_StartDate descending
                                 select cm).FirstOrDefault();
                    if (custm != null)
                    {
                        Entity updateMachine = new Entity("pnl_machine");
                        updateMachine.Id = custm.pnl_Machineid.Id;
                        updateMachine.Attributes["pnl_currentcustomerid"] = custm.pnl_CustomerId;
                        _service.Update(updateMachine);
                    }
                    else
                    {
                        Entity updateMachineCust = new Entity("pnl_machine");
                        updateMachineCust.Id = machine.Id;
                        updateMachineCust.Attributes["pnl_currentcustomerid"] = null;
                        _service.Update(updateMachineCust);
                    }
                }
            }
                    
                
            }
        }
    }


   