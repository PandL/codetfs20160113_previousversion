using System;
using System.Linq;
using System.ServiceModel;
using Microsoft.Xrm.Sdk;
using LandPower;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using System.Data.SqlTypes;
using System.Text;
using System.Collections.Generic;


namespace LandPowerPlugins
{
    public class QuotePlugin2 : IPlugin
    {
        private const char HYPHEN_CHAR = '-';
        private const decimal DECIMAL100 = 100.0M;
        private const decimal DECIMAL0 = 0.0M;
        private const decimal DECIMAL1 = 1.0M;

        IOrganizationService _service;
        crmServiceContext _ctx;
        Entity _updateEntity;

        public void Execute(IServiceProvider serviceProvider)
        {
            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            // Obtain the execution context from the service provider.
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            //if (!context.Stage.Equals(10))
            //    return;
           
            //if (context.Depth > 1)
            //{
            //    return;
            //}
            
            // don't want to redo create logic when resyncing online
            if (context.IsOfflinePlayback)
                return;

            // The InputParameters collection contains all the data passed in the message request.
            if (!context.InputParameters.Contains("Target") || !(context.InputParameters["Target"] is Entity))
                return;

            // Obtain the target entity from the input parameters.
            _updateEntity = (Entity)context.InputParameters["Target"];

            if (_updateEntity.Attributes.Contains("pnl_lastsavedfromlocallysourceditemon") || _updateEntity.Attributes.Contains("pnl_lastsavedfromlocallysourceditemon"))
            {
            }
            else
            {
                if (context.Depth > 1)
                {
                    return;
                }
            }

            // Verify that the target entity represents an account.
            // If not, this plug-in was not registered correctly.
            if (_updateEntity.LogicalName != "quote")
                return;

            ExecuteMultipleRequest requestWithContinueOnError = 
                new ExecuteMultipleRequest() { Settings = new ExecuteMultipleSettings() { ContinueOnError = false, ReturnResponses = true }, Requests = new OrganizationRequestCollection() };

            try
            {
                // Obtain the organization service reference.
                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                _service = serviceFactory.CreateOrganizationService(context.UserId);

                // Use reference to invoke pre-built types service 
                _ctx = new LandPower.crmServiceContext(_service);

                // next get the PreImage of the quote
                Entity quotePreImageEntity = null;
                if (context.PreEntityImages.Contains("quote") && (context.PreEntityImages["quote"] is Entity))
                {
                    quotePreImageEntity = (Entity)context.PreEntityImages["quote"];
                }
                if (quotePreImageEntity == null || quotePreImageEntity.LogicalName != "quote")
                {
                    throw new InvalidPluginExecutionException("Incorrect Plugin registration: a Pre image has not been correctly configured for this plugin.");
                }

                Quote quoteUpdate = _updateEntity.ToEntity<Quote>();

                Quote quotePreImage = quotePreImageEntity.ToEntity<Quote>();

                UpdateEntityAttributes(quoteUpdate, quotePreImageEntity);

                Quote quotePreImageWithUpdates = quotePreImageEntity.ToEntity<Quote>();


                // recalculate the description and book value for trade-ins 1-3 for the Quote along with its locally sourced and machine Quote Products
                Guid? stockClassId = null;
                CalculateQuoteValues(quotePreImageWithUpdates, context, requestWithContinueOnError, ref stockClassId, quotePreImage);

                // updates to the quoteUpdate object will be automatically applied so no need to call update method here
                //_service.Update(_updateEntity);

//                Uri link = new Uri(string.Format("https://{0}.landpower.co.nz/main.aspx?etn=quote&etc=1084&id={1}&pagetype=entityrecord", context.OrganizationName, quotePreImageWithUpdates.QuoteId.ToString()));
                  Uri link = new Uri(string.Format("https://{0}.landpower.co.nz/main.aspx?etn=quote&id={1}&pagetype=entityrecord", context.OrganizationName, quotePreImageWithUpdates.QuoteId.ToString()));
                string quoteLink = string.Format("<a href={1}>{0}</a>", string.IsNullOrWhiteSpace(quotePreImageWithUpdates.Name) ? quotePreImageWithUpdates.pnl_LandpowerQuoteID : quotePreImageWithUpdates.Name, link);

                CheckSalesAidRequestEmail(quoteUpdate, quotePreImageWithUpdates, quoteLink, stockClassId);
                CheckSalesAidApprovalOrDeclineEmail(context, quoteUpdate, quotePreImageWithUpdates, quoteLink);

                // set sender to current user if the InStock Wholsesale Approver has not been set on the stock class 
                Guid? instockWholesaleApproverId = null;
                Guid? instockWholesaleApproverCcId = null;

                string recipientNames=null;
                GetInstockApproverIds( ref instockWholesaleApproverId, ref instockWholesaleApproverCcId, ref stockClassId, ref recipientNames);

               CheckInStockWholesaleItemExistsEmail(quoteUpdate, quotePreImageWithUpdates, instockWholesaleApproverId, instockWholesaleApproverCcId, 
                    stockClassId, quoteLink, recipientNames, quotePreImage);
                CheckInStockWholesaleApprovalEmail(quoteUpdate, quotePreImageWithUpdates, context.UserId, quoteLink, recipientNames);

  }
            #region catch - Exception handling
            catch (FaultException<OrganizationServiceFault> ex)
            {
                //// http://msdn.microsoft.com/en-us/library/jj863631.aspx
                //// Check if the maximum batch size has been exceeded. The maximum batch size is only included in the fault if it
                //// the input request collection count exceeds the maximum batch size.
                //if (ex!= null && ex.Detail != null && ex.Detail.ErrorDetails!= null && ex.Detail.ErrorDetails.Contains("MaxBatchSize"))
                //{
                //    int maxBatchSize = Convert.ToInt32(ex.Detail.ErrorDetails["MaxBatchSize"]);
                //    if (maxBatchSize < requestWithContinueOnError.Requests.Count)
                //    {
                //        // Here you could reduce the size of your request collection and re-submit the ExecuteMultiple request.
                //        // For this sample, that only issues a few requests per batch, we will just print out some info. However,
                //        // this code will never be executed because the default max batch size is 1000.
                //        throw new InvalidPluginExecutionException(string.Format("An error occurred in the Quote Update plug-in:-{0}The input request collection contains {1} requests, which exceeds the maximum allowed ({2})",
                //            Environment.NewLine,
                //            requestWithContinueOnError.Requests.Count,
                //            maxBatchSize));
                //    }
                //}
                //else
                //throw new InvalidPluginExecutionException(string.Format("An error occurred in the Quote Update plug-in:-{0}{1}", Environment.NewLine, ex.Message) + ex.InnerException == null ? "" : Environment.NewLine + ex.InnerException.Message);
                throw new InvalidPluginExecutionException(string.Format("An error occurred in the Quote Update plug-in:-{0}{1}", Environment.NewLine, ex.Message) );
            }
            #endregion
        }

        private void CheckSalesAidRequestEmail(Quote quoteUpdate, Quote quotePreImageWithUpdates, string quoteLink, Guid? quoteStockClassId)
        {
            if (quoteUpdate.pnl_SalesAidRequest.HasValue && quoteUpdate.pnl_SalesAidRequest.Value)
            {
                if (quotePreImageWithUpdates.pnl_SalesAidRequest1CreatedDateTime.HasValue)
                {
                    return;
                }
                
                string subject = string.Format("Sales Aid Request for {0}; Quote ID: {1}", quotePreImageWithUpdates.CustomerId.Name, quotePreImageWithUpdates.pnl_LandpowerQuoteID);
                string description = string.Format("A Sales Aid Request of amount {0} for Quote {1}.{2}", string.Format("{0:C}", quotePreImageWithUpdates.pnl_SalesAidRequested.Value), quoteLink, "<br /><br />"); //quotePreImageWithUpdates.Name
                description += string.Format("The reason given for the request is:\"{0}\"{1}", quotePreImageWithUpdates.pnl_SalesAidRequestedReason, "<br /><br />");
                description += string.Format("Please approve the Sales Aid on Quote: {0}{1}", quoteLink, "<br /><br />"); //quotePreImageWithUpdates.Name

                Guid salesRep = quotePreImageWithUpdates.OwnerId.Id;
                string recipientNames = quotePreImageWithUpdates.OwnerId.Name;

                Guid productManager = Guid.Empty; //SalesAid approver
                Guid ccRecep = Guid.Empty;

                if (!quoteStockClassId.HasValue)
                    throw new Exception("Sales Aid Request email: Empty stock class ID.");

                var stockClass = GetStockClass(quoteStockClassId.Value);

                if (stockClass != null)
                {
                    if (stockClass.pnl_SalesAidApprover != null)
                    {
                        productManager = stockClass.pnl_SalesAidApprover.Id;
                        recipientNames = string.Format("{0}, {1}", recipientNames, stockClass.pnl_SalesAidApprover.Name);
                    }
                    else
                        throw new InvalidPluginExecutionException("Sales Aid Approver can not be null! Please update the Sales Aid Approver on the current Stock Class.");

                    if (stockClass.pnl_SalesAidApproverCC != null)
                    {
                        ccRecep = stockClass.pnl_SalesAidApproverCC.Id;
                        recipientNames = string.Format("{0}, {1}", recipientNames, stockClass.pnl_SalesAidApproverCC.Name);
                    }
                }
                else
                    throw new InvalidPluginExecutionException("Stock Class can not be null! Please enter a valid Stock Class.");

                if (productManager != Guid.Empty)
                {
                    GenerateEmail(salesRep, productManager, ccRecep, subject, description, new EntityReference(Quote.EntityLogicalName, quotePreImageWithUpdates.QuoteId.Value), recipientNames);
                    quoteUpdate.pnl_SalesAidRequest1CreatedDateTime = DateTime.Now;
                }
            }
        }

        private void CheckSalesAidApprovalOrDeclineEmail(IPluginExecutionContext context, Quote quoteUpdate, Quote quotePreImageWithUpdates, string quoteLink)
        {
            if (quoteUpdate.pnl_SalesAidApprovedStatus != null && (quoteUpdate.pnl_SalesAidApprovedStatus.Value == 20 || quoteUpdate.pnl_SalesAidApprovedStatus.Value == 30))
            {
                if (quoteUpdate.pnl_SalesAidUserNotification1CreatedDateTime.HasValue)
                {
                    return;
                }

                //throw new InvalidPluginExecutionException("approve/disapprove hit");

                string subject = string.Format("Request for {0} Sales Aid for QuoteID: {1} has been processed.", quotePreImageWithUpdates.CustomerId.Name, quotePreImageWithUpdates.pnl_LandpowerQuoteID);
                string description = string.Empty;

                if (quoteUpdate.pnl_SalesAidApprovedStatus.Value == 20)//Approved
                {
                    description = string.Format("A sales aid amount of: {1} has been approved with the following reason: {0}{2} ", quotePreImageWithUpdates.pnl_SalesAidApprovedReason, string.Format("{0:C}", quotePreImageWithUpdates.pnl_SalesAidApproved.Value), "<br /><br />");
                    description += string.Format("Please provide a PDF version of the quote to {0}.", quotePreImageWithUpdates.CustomerId.Name, "<br /><br />");
                    description += string.Format("If you would like to make any changes to the quote, please go to the quote {0} and revise it.{1}",quoteLink, "<br /><br />");
                }

                if (quoteUpdate.pnl_SalesAidApprovedStatus.Value == 30)//Declined
                {
                    description = string.Format("Your request has been declined due to the following reason: {0}{1} ", quotePreImageWithUpdates.pnl_SalesAidApprovedReason, "<br /><br />");
                    description += string.Format("If you would like to make any changes to the quote, please go to the quote {0} and revise it.{1}", quoteLink, "<br /><br />");
                }

                Guid salesRep = quotePreImageWithUpdates.OwnerId.Id;
                string recipientNames = quotePreImageWithUpdates.OwnerId.Name;
                Guid sender = context.UserId; //SalesAid approver

                if (sender != null)
                {
                    GenerateEmail(sender, salesRep, subject, description, new EntityReference(Quote.EntityLogicalName, quotePreImageWithUpdates.QuoteId.Value), recipientNames);
                    quoteUpdate.pnl_SalesAidUserNotification1CreatedDateTime = DateTime.Now;
                }
            }
        }

        private void GetInstockApproverIds(ref Guid? instockWholesaleApproverId, ref Guid? instockWholesaleApproverCcId, ref Guid? quoteStockClassId, ref string recipientNames)
        {
            recipientNames = null;
            if(quoteStockClassId.HasValue)
            {
                var stockClass = GetStockClass(quoteStockClassId.Value);
                if (stockClass != null)
                {
                    if (stockClass.pnl_InStockWholesaleApprover != null)
                    {
                        instockWholesaleApproverId = stockClass.pnl_InStockWholesaleApprover.Id;
                        recipientNames = stockClass.pnl_InStockWholesaleApprover.Name;
                        
                    }
                    if (stockClass.pnl_InStockWholesaleApproverCC != null)
                    {
                        instockWholesaleApproverCcId = stockClass.pnl_InStockWholesaleApproverCC.Id;
                        recipientNames = recipientNames == null ? stockClass.pnl_InStockWholesaleApproverCC.Name : string.Format("{0}, {1}", recipientNames, stockClass.pnl_InStockWholesaleApproverCC.Name);
                    }
                }
            }
        }

        private void ProcessQuoteProductUpdates(IPluginExecutionContext context, ExecuteMultipleRequest requestWithContinueOnError)
        {
            //if (context.IsExecutingOffline)
            //{
            foreach (var quoteProductUpdate in requestWithContinueOnError.Requests)
            {
                var response = _service.Execute(quoteProductUpdate);
            }
            //}
            //else
            //{
                //ExecuteMultipleResponse responseWithContinueOnError = (ExecuteMultipleResponse)_service.Execute(requestWithContinueOnError);

                ////There should be no responses except for those that contain an error. 
                //if (responseWithContinueOnError.Responses.Count > 0)
                //{
                //    string responseMessage = "<undefined>";
                //    if (responseWithContinueOnError.Responses.Count < requestWithContinueOnError.Requests.Count)
                //    {
                //        responseMessage = "Response collection contain a mix of successful response objects and errors.";
                //    }
                //    foreach (var responseItem in responseWithContinueOnError.Responses)
                //    {
                //        if (responseItem.Fault != null)
                //        {
                //            responseMessage = responseItem.Fault.Message;
                //            break;
                //        }
                //    }
                //    throw new InvalidPluginExecutionException(string.Format("An error occurred in the Quote Plugin while updating a related Quote Product:- {0}", responseMessage));
                //}
            //}
        }

        private void CheckInStockWholesaleItemExistsEmail(Quote quoteUpdate, Quote quotePreImageWithUpdates, Guid? instockWholesaleApproverId, Guid? instockWholesaleApproverCcId, 
            Guid? stockClassId, string quoteLink, string recipientNames, Quote quotePreImage)
        {
            if (quotePreImage.pnl_InStockWholesaleTaskNotification1Sent.HasValue)
                return;

            // if this update is changing the field to true
            if ((quoteUpdate.pnl_InStockWholesale.HasValue && quoteUpdate.pnl_InStockWholesale.Value == true))
            {
                quoteUpdate.pnl_InStockWholesaleTaskNotification1Sent = DateTime.Now;

                string subject = string.Format("Instock Landpower Wholesale machine request for Quote ID: {0}", quotePreImageWithUpdates.pnl_LandpowerQuoteID);
                string description = string.Empty;

                description = string.Format("Please verify that this machine is the best option for this customer: {0}<br /><br />", quotePreImageWithUpdates.CustomerId.Name);
                description += string.Format("This can be done by going to the quote ({0}) and selecting \"Yes\" in the In Stock Wholesale Approval Field (This is in the In Stock Landpower Wholesale Section of the Quote Form).<br /><br />", quoteLink);
                description += "Please note the quote cannot be activated or printed until approval is given. ";

                Guid senderId = quotePreImageWithUpdates.OwnerId.Id;
                recipientNames = recipientNames == null ? quotePreImageWithUpdates.OwnerId.Name : string.Format("{0}, {1}", quotePreImageWithUpdates.OwnerId.Name, recipientNames);

                if (stockClassId == null)
                    throw new InvalidPluginExecutionException("Stock Class can not be null.  Please enter a valid Stock Class.");

                if(!instockWholesaleApproverId.HasValue)
                    throw new InvalidPluginExecutionException("Instock Wholesale Approver can not be null! Please update the Instock Wholesale Approver on the current Stock Class.");

             //   if (instockWholesaleApproverId != null)
                    //GenerateEmail(senderId, instockWholesaleApproverId.Value, instockWholesaleApproverCcId, subject, description, new EntityReference(Quote.EntityLogicalName, quotePreImageWithUpdates.QuoteId.Value), recipientNames);
            }
        }

        private void CheckInStockWholesaleApprovalEmail(Quote quoteUpdate, Quote quotePreImageWithUpdates, Guid senderId, string quoteLink, string recipientNames)
        {
            // if this update is changing the field to true
            if ((quoteUpdate.pnl_InStockWholesaleApproval.HasValue && quoteUpdate.pnl_InStockWholesaleApproval.Value == true) )
            {
                string subject = string.Format("Instock Landpower Wholesale machine request : Quote ID: {0} is ready for completion", quotePreImageWithUpdates.pnl_LandpowerQuoteID);
                string description = string.Empty;

                description = string.Format("Your request on Quote: {0} for customer: {1}  to purchase an In Stock Wholesale Item has been approved.<br /><br />{2}<br /><br />"
                        , string.IsNullOrWhiteSpace(quoteUpdate.Name)? quotePreImageWithUpdates.Name:quoteUpdate.Name
                        , quotePreImageWithUpdates.CustomerId.Name
                        , quoteLink);

                Guid recipientId = quotePreImageWithUpdates.OwnerId.Id;
                recipientNames = recipientNames == null ? quotePreImageWithUpdates.OwnerId.Name : string.Format("{0}, {1}", quotePreImageWithUpdates.OwnerId.Name, recipientNames);

        //        if (senderId != null)
         //           GenerateEmail(senderId, recipientId, subject, description, new EntityReference(Quote.EntityLogicalName, quotePreImageWithUpdates.QuoteId.Value), recipientNames);
            }
        }

        private static void UpdateEntityAttributes(Entity sourceEntity, Entity targetEntity)
        {
            foreach (string sourceAttributeName in sourceEntity.Attributes.Keys)
            {
                targetEntity.Attributes[sourceAttributeName] = sourceEntity.Attributes[sourceAttributeName];
            }
        }

        private void CalculateQuoteValues(Quote qpi, IPluginExecutionContext context, ExecuteMultipleRequest requestWithContinueOnError, ref Guid? quoteStockClassId, Quote quotePreImage)
        {
            #region Iterate over the set of related Quote Detail (AKA Quote PRoducts) records for this Quote
            // declare decimal accumulator variables
            decimal totalBookValue = DECIMAL0;
            decimal subTotalListPrice = DECIMAL0;
            decimal subtotalDealerNetPrice = DECIMAL0;
            decimal totalLocallySourcedItemsListPrice = DECIMAL0;
            decimal totalLocallySourcedItemsDealerNetPrice = DECIMAL0;
            decimal maxBasePriceAndSelectedSpecs = DECIMAL0;
            decimal scenario1GrossProfitPercentage = DECIMAL0;
            decimal basePriceLineItemTotal = DECIMAL0;
            DateTime HIGHEST_DATE = DateTime.Parse("9999-12-31");
            DateTime earliestExpirationDate = HIGHEST_DATE;
            decimal? tradeInMachine1BookValue = null;
            decimal? tradeInMachine2BookValue = null;
            decimal? tradeInMachine3BookValue = null;
            string tradeInMachine1Description = null;
            string tradeInMachine2Description = null;
            string tradeInMachine3Description = null;
            string highestValueMachineName = null;

            //John 6.05.2014 - Added new trade in fields
            //Labels
            string pnl_tradeinmarketpricelabel = null;
            string pnl_tradeinsellingmarginlabel = null;
            string pnl_tradeinsellingmarginlabel1 = null;
            string pnl_tradeinrepaircostlabel = null;
            //Values
            decimal? pnl_tradeinmachine1timarketprice = null;
            decimal? pnl_tradeinmachine1tisellingmargin = null;
            decimal? pnl_tradeinmachine1tirepaircost = null;
            decimal? pnl_tradeinmachine2timarketprice = null;
            decimal? pnl_tradeinmachine2tisellingmargin = null;
            decimal? pnl_tradeinmachine2tirepaircost = null;
            decimal? pnl_tradeinmachine3timarketprice = null;
            decimal? pnl_tradeinmachine3tisellingmargin = null;
            decimal? pnl_tradeinmachine3tirepaircost = null;

            int tradeInCount = 0;
            bool inStockWholesaleItemExists = false;
            string highestValueMachineStockClass = null;
            string highestValueMachineStockCode = null;

            int ownersBuValue = GetQuoteBuCountryValue(qpi);
            DateTime todaysUtcDateOnly = DateTime.UtcNow;

            #region Calculate prices from locally sourced machines table
            /*            var lsTotals = (
                from p in _ctx.pnl_locallysourceditemSet 
                where (p.pnl_Quote != null && p.pnl_Quote.Id == qpi.Id)
                group p by p into lp 
                select new 
                {
                    ListPrice = lp.Sum(x => ValueAsDecimal(x.pnl_ListPrice)), 
                    DealerNetPrice = lp.Sum(x => ValueAsDecimal(x.pnl_LocallySourcedDNP))
                }).First();

            totalLocallySourcedItemsListPrice = lsTotals.ListPrice;
            totalLocallySourcedItemsDealerNetPrice = lsTotals.DealerNetPrice;*/
            var lsItems = (
                from p in _ctx.pnl_locallysourceditemSet
                where (p.pnl_Quote != null && p.pnl_Quote.Id == qpi.Id)
                select p).ToList();

            foreach (var lsItem in lsItems)
            {
                totalLocallySourcedItemsListPrice += ValueAsDecimal(lsItem.pnl_ListPrice);
                totalLocallySourcedItemsDealerNetPrice += ValueAsDecimal(lsItem.pnl_LocallySourcedDNP);
            }
            #endregion


            // iterate through the set of Quote Detail (AKA Quote PRoducts) records for this Quote.
            var tradeInSet = from t in _ctx.QuoteDetailSet
                             where t.QuoteId.Id.Equals(qpi.Id)
                             orderby t.ProductDescription   // only works for write in product
                             select t;

            //John 8:07:2014: Locally Sourced Machine - Store the List of Products used to create Locally Sourced Machine records
            List<Tuple<string, string, int?>> productList = new List<Tuple<string, string, int?>>();
            int maxLineNumber = -1;

            foreach (QuoteDetail quoteProduct in tradeInSet)
            {
                // list price for the quote is resued many times so calculate it once here.
                decimal quoteProductListPrice = GetListPrice(quoteProduct);
                decimal basePriceAndSelectedSpecs = ValueAsDecimal(quoteProduct.pnl_BasePriceAndSelectedSpecs);

                if (quoteProduct.LineItemNumber != null && quoteProduct.LineItemNumber.Value > maxLineNumber)
                {
                    maxLineNumber = quoteProduct.LineItemNumber.Value;
                }

                # region Process Trade-ins - function 7
                if (quoteProduct.pnl_IsTradeIn.HasValue && quoteProduct.pnl_IsTradeIn.Value == true)
                {
                    tradeInCount++;
                    if (tradeInCount <= 3)  // NB Only count first 3 as tradeins
                    {
                        totalBookValue += quoteProductListPrice;

                        switch (tradeInCount)
                        {
                            case 1:
                                tradeInMachine1BookValue = quoteProductListPrice;
                                tradeInMachine1Description = GetProductDescription(quoteProduct);
                                //John 6.05.2014 - Added new trade in fields
                                //Labels
                                pnl_tradeinmarketpricelabel = GetTradeInLabelName(quoteProduct.pnl_TIMarketPrice.Value, "Market Price", pnl_tradeinmarketpricelabel);
                                pnl_tradeinsellingmarginlabel = GetTradeInLabelName(quoteProduct.pnl_TISellingMargin.Value, "Selling Margin %", pnl_tradeinsellingmarginlabel);
                                pnl_tradeinsellingmarginlabel1 = pnl_tradeinsellingmarginlabel;
                                pnl_tradeinrepaircostlabel = GetTradeInLabelName(quoteProduct.pnl_TIRepairCost.Value, "Repair Cost", pnl_tradeinrepaircostlabel);
                                 //Values
                                pnl_tradeinmachine1timarketprice = quoteProduct.pnl_TIMarketPrice.Value;
                                pnl_tradeinmachine1tisellingmargin = quoteProduct.pnl_TISellingMargin.Value;
                                pnl_tradeinmachine1tirepaircost = quoteProduct.pnl_TIRepairCost.Value;
                                break;

                            case 2:
                                tradeInMachine2BookValue = quoteProductListPrice;
                                tradeInMachine2Description = GetProductDescription(quoteProduct);
                                //John 6.05.2014 - Added new trade in fields
                                //Labels
                                pnl_tradeinmarketpricelabel = GetTradeInLabelName(quoteProduct.pnl_TIMarketPrice.Value, "Market Price", pnl_tradeinmarketpricelabel);
                                pnl_tradeinsellingmarginlabel = GetTradeInLabelName(quoteProduct.pnl_TISellingMargin.Value, "Selling Margin %", pnl_tradeinsellingmarginlabel);
                                pnl_tradeinsellingmarginlabel1 = pnl_tradeinsellingmarginlabel;
                                pnl_tradeinrepaircostlabel = GetTradeInLabelName(quoteProduct.pnl_TIRepairCost.Value, "Repair Cost", pnl_tradeinrepaircostlabel);
                                //Values
                                pnl_tradeinmachine2timarketprice = quoteProduct.pnl_TIMarketPrice.Value;
                                pnl_tradeinmachine2tisellingmargin = quoteProduct.pnl_TISellingMargin.Value;
                                pnl_tradeinmachine2tirepaircost = quoteProduct.pnl_TIRepairCost.Value;
                                break;

                            case 3:
                                tradeInMachine3BookValue = quoteProductListPrice;
                                tradeInMachine3Description = GetProductDescription(quoteProduct);
                                //John 6.05.2014 - Added new trade in fields
                                //Labels
                                pnl_tradeinmarketpricelabel = GetTradeInLabelName(quoteProduct.pnl_TIMarketPrice.Value, "Market Price", pnl_tradeinmarketpricelabel);
                                pnl_tradeinsellingmarginlabel = GetTradeInLabelName(quoteProduct.pnl_TISellingMargin.Value, "Selling Margin %", pnl_tradeinsellingmarginlabel);
                                pnl_tradeinsellingmarginlabel1 = pnl_tradeinsellingmarginlabel;
                                pnl_tradeinrepaircostlabel = GetTradeInLabelName(quoteProduct.pnl_TIRepairCost.Value, "Repair Cost", pnl_tradeinrepaircostlabel);
                                //Values
                                pnl_tradeinmachine3timarketprice = quoteProduct.pnl_TIMarketPrice.Value;
                                pnl_tradeinmachine3tisellingmargin = quoteProduct.pnl_TISellingMargin.Value;
                                pnl_tradeinmachine3tirepaircost = quoteProduct.pnl_TIRepairCost.Value;
                                break;
                        }
                    }
                }
                #endregion

                # region Process non-trade-ins, non locally sourced, Products only - Function 1 Machine Costs
                if ((!quoteProduct.pnl_IsTradeIn.HasValue || quoteProduct.pnl_IsTradeIn.Value == false)
                  && (quoteProduct.pnl_islocallysourced == null || quoteProduct.pnl_islocallysourced.Value == LandpowerConst.PNL_OPTIONSET_FALSE)
                    && (quoteProduct.pnl_specType != null && (LandpowerPluginBase.SpecificationType)quoteProduct.pnl_specType.Value == LandpowerPluginBase.SpecificationType.PRODUCT))
                {
                    /* While looping through the quote product lines of type product, they should check to see if any of the records 
                     * has a provisional serial number [pnl_instkprovisional_serial_no] and in stock dealer [pnl_instkisdealerstock] 
                     * set to no. If both of these criteria are met, then In Stock Wholesale Item On Quote [pnl_InStockWholesale] 
                     * should be set to yes, otherwise it should be set to no.*/
                    if (!inStockWholesaleItemExists)
                    {
                        if (!string.IsNullOrWhiteSpace(quoteProduct.pnl_InStkprovisional_serial_no)
                            && quoteProduct.pnl_instkIsDealerStock.HasValue
                            && quoteProduct.pnl_instkIsDealerStock.Value == false)
                        {
                            inStockWholesaleItemExists = true;
                        }
                    }

                    //calculate new values on this quote detail record 
                    QuoteDetail quoteProductUpdate;
                    decimal bpass_DNPDollar, margin;
                    CalculateDealerNetPrice(quoteProduct, basePriceAndSelectedSpecs, todaysUtcDateOnly, ownersBuValue, out quoteProductUpdate, out bpass_DNPDollar, out margin);

                    if (quoteProductUpdate != null)
                        requestWithContinueOnError.Requests.Add(new UpdateRequest() { Target = quoteProductUpdate });

                    // retieve values from the highest value Base-Price-and-Selected-Specs machine
                    if (maxBasePriceAndSelectedSpecs < basePriceAndSelectedSpecs)
                    {
                        maxBasePriceAndSelectedSpecs = basePriceAndSelectedSpecs;
                        scenario1GrossProfitPercentage = margin;
                        highestValueMachineStockClass = quoteProduct.pnl_prod_class;
                        highestValueMachineStockCode = quoteProduct.pnl_specs_sales_stock_code;
                        highestValueMachineName = quoteProduct.ProductDescription;
                    }
                    if (quoteProduct.pnl_QuoteExpirationDate.HasValue && earliestExpirationDate > quoteProduct.pnl_QuoteExpirationDate.Value)
                        earliestExpirationDate = quoteProduct.pnl_QuoteExpirationDate.Value;

                    basePriceLineItemTotal += ValueAsDecimal(quoteProduct.BaseAmount);
                    subTotalListPrice += basePriceAndSelectedSpecs;
                    subtotalDealerNetPrice += ValueAsDecimal(bpass_DNPDollar);

                    //John 8:07:2014: Locally Sourced Machine - Adding Product Only in the productList array so Locally Sourced Machine records can be created from
                    productList.Add(new Tuple<string, string,int?>(quoteProduct.Id.ToString(), quoteProduct.ProductDescription, quoteProduct.exp_linenumber));
                }
                #endregion
                
                # region Process locally sourced items - Function 2
                if (quoteProduct.pnl_islocallysourced != null && quoteProduct.pnl_islocallysourced.Value == LandpowerConst.PNL_OPTIONSET_TRUE)
                {
                              
                    //QuoteDetail quoteProductUpdate;
                    //CalculateLocallySourcedItemStandardDiscount(quoteProductListPrice, quoteProduct, out quoteProductUpdate);
                    //if (quoteProductUpdate != null)
                    //    requestWithContinueOnError.Requests.Add(new UpdateRequest() { Target = quoteProductUpdate });

                    totalLocallySourcedItemsListPrice += quoteProductListPrice;
                    //totalLocallySourcedItemsDealerNetPrice += ValueAsDecimal(quoteProduct.pnl_LocallySourcedDealerNetPrice);
                    totalLocallySourcedItemsDealerNetPrice += ValueAsDecimal(quoteProduct.pnl_LocallySourcedDNP);
                }
                #endregion
            }
            #endregion

            //qpi.Id
            //ranjith's code

            var quotemachine = from qm in _ctx.pnl_quotemachineSet
                               where qm.pnl_Quoteid.Id.Equals(qpi.Id)
                               select qm;
            decimal totalquotemachineScenario1 = 0.00M;
            decimal totalquotemachineScenario2 = 0.00M;
            decimal totalquotemachineScenario3 = 0.00M;
            foreach (pnl_quotemachine qmachine in quotemachine)
            {
                if (qmachine.pnl_Scenario1Value != null && qmachine.pnl_Scenario1Value.Value != null)
                {
                    totalquotemachineScenario1 = totalquotemachineScenario1 + Convert.ToDecimal(qmachine.pnl_Scenario1Value.Value);
                }
                if (qmachine.pnl_Scenario2Value != null && qmachine.pnl_Scenario2Value.Value != null)
                {
                    totalquotemachineScenario2 = totalquotemachineScenario2 + Convert.ToDecimal(qmachine.pnl_Scenario2Value.Value);
                }
                if (qmachine.pnl_Scenario3Value != null && qmachine.pnl_Scenario3Value.Value != null)
                {
                    totalquotemachineScenario3 = totalquotemachineScenario3 + Convert.ToDecimal(qmachine.pnl_Scenario3Value.Value);
                }
            }

            totalBookValue = totalBookValue + totalquotemachineScenario1;



            #region John 8:07:2014: Create or Delete Locally Sourced Machine Records
            //CREATE Locally Sourced Machine
            foreach (Tuple<string, string, int?> product in productList)
            {
                pnl_locallysourcedmachine machine = (from m in _ctx.pnl_locallysourcedmachineSet
                                                     where m.pnl_QuoteId.Id.Equals(qpi.Id) && (m.pnl_QuoteProductId.Equals(product.Item1) || m.pnl_QuoteProductId.Equals("NULL")) && m.pnl_Config.Value.Equals(product.Item3)
                                                     select m).FirstOrDefault();
                if (machine == null)
                {
                    pnl_locallysourcedmachine locallyMachine = new pnl_locallysourcedmachine { pnl_name = product.Item2, pnl_QuoteProductId = product.Item1, pnl_QuoteId = new EntityReference(qpi.LogicalName, qpi.Id), pnl_Config = product.Item3 };
                    _service.Create(locallyMachine);
                }
                else
                {
                    if (machine.pnl_QuoteProductId.Equals("NULL"))
                    {
                        machine.pnl_QuoteProductId = product.Item1;
                        _ctx.UpdateObject(machine);
                        _ctx.SaveChanges();
                    }
                }
            }

            //DELETE Locally Sourced Machine
            // iterate through the set of Locally Sourced Machine records for this Quote.
            var locallyInSet = from l in _ctx.pnl_locallysourcedmachineSet
                               where l.pnl_QuoteId.Id.Equals(qpi.Id)
                               select l;

            bool found = false;

            foreach (pnl_locallysourcedmachine locallymach in locallyInSet)
            {
                found = false;
                foreach (Tuple<string, string, int?> product in productList)
                {
                    if (locallymach.pnl_QuoteProductId == product.Item1)
                    {
                        //Change the name of the Locally Source Machine and description of associated Quote Product
                        if (product.Item2 == null && locallymach.pnl_name != null)
                        {
                            locallymach.pnl_name = null;
                            _ctx.UpdateObject(locallymach);
                            var locallySourceSet = from t in _ctx.pnl_locallysourceditemSet
                                                     where t.pnl_Quote.Id.Equals(qpi.Id) && t.pnl_LocallySourcedMachine.Id.Equals(locallymach.Id)
                                                     select t;
                            foreach (pnl_locallysourceditem locallySource in locallySourceSet)
                            {
                                locallySource.pnl_LocallySourcedMachineDescription = null;
                                _ctx.UpdateObject(locallySource);
                            }
                        }
                        else
                        {
                            if (product.Item2 != locallymach.pnl_name)
                            {
                                locallymach.pnl_name = product.Item2;
                                _ctx.UpdateObject(locallymach);
                                var locallySourceSet = from t in _ctx.pnl_locallysourceditemSet
                                                       where t.pnl_Quote.Id.Equals(qpi.Id) && t.pnl_LocallySourcedMachine.Id.Equals(locallymach.Id)
                                                       select t;
                                foreach (pnl_locallysourceditem locallySource in locallySourceSet)
                                {
                                    locallySource.pnl_LocallySourcedMachineDescription = product.Item2;
                                    _ctx.UpdateObject(locallySource);
                                }
                            }
                            
                        }
                        found = true;
                        break;
                    }
                }
                //Delete Locally Source Machine that doesnt have a machine link to its (Quote Product - Machine)
                if (!found)
                {
                    _service.Delete(locallymach.LogicalName, locallymach.Id);
                }
            }
            _ctx.SaveChanges();

            //Clear the Quote Product Machine Name if the Locally Source Machine doesnt exist
            var locallySourceWithNoMachine = from t in _ctx.pnl_locallysourceditemSet
                                             where t.pnl_Quote.Id.Equals(qpi.Id) && t.pnl_LocallySourcedMachine == null 
                                             select t;
            foreach (pnl_locallysourceditem locallySource in locallySourceWithNoMachine)
            {
                if (locallySource.pnl_LocallySourcedMachineDescription != null || locallySource.pnl_ConfigNumber != null)
                {
                    locallySource.pnl_LocallySourcedMachineDescription = null;
                    locallySource.pnl_ConfigNumber = null; 
                    _ctx.UpdateObject(locallySource);
                }
            }
            _ctx.SaveChanges();

            //set the Config #, LineItemNumber and Description field so Locally Source Quote Product will be grouped in the Quote Report
            foreach (pnl_locallysourcedmachine locallymach in locallyInSet)
            {
                var locallySourceSet = from t in _ctx.pnl_locallysourceditemSet
                                       where t.pnl_Quote.Id.Equals(qpi.Id) && t.pnl_LocallySourcedMachine.Id.Equals(locallymach.Id)
                                       select t;
                bool changes = false;
                foreach (pnl_locallysourceditem locallySource in locallySourceSet)
                {
                    changes = false;
                    //set the Config #
                    if (locallySource.pnl_ConfigNumber != locallymach.pnl_Config)
                    {
                        locallySource.pnl_ConfigNumber = locallymach.pnl_Config;
                        changes = true;
                    }
                    //set the LineItemNumber
                    if (locallySource.pnl_LineNumber == null)
                    {
                        maxLineNumber++;
                        locallySource.pnl_LineNumber = maxLineNumber;
                        changes = true;
                    }
                    //set the Description
                    if (locallySource.pnl_LocallySourcedMachineDescription != locallySource.pnl_name)
                    {
                        locallySource.pnl_LocallySourcedMachineDescription = locallySource.pnl_name;
                        changes = true;
                    }

                    if (changes)
                    {
                        _ctx.UpdateObject(locallySource);
                    }
                }
            }
            _ctx.SaveChanges();

            //John 6:08:2014: Create Quote Product from locally Sourced Item 
            var locallySourceNoQuoteProductSet = from t in _ctx.pnl_locallysourceditemSet
                                   where t.pnl_Quote.Id.Equals(qpi.Id) && t.pnl_QuoteProductId == null
                                   select t;

            foreach (pnl_locallysourceditem locallySource in locallySourceNoQuoteProductSet)
            {
                if (locallySource.pnl_QuoteProductId == null)
                {
                    QuoteDetail quotedetail = new QuoteDetail()
                    {
                        QuoteId = locallySource.pnl_Quote,
                        pnl_LocallySourcedItemId = locallySource.ToEntityReference(),
                        Quantity = 1,
                        IsProductOverridden = true,
                        PricePerUnit = new Money(0),
                        ProductDescription = locallySource.pnl_name
                    };
                    Guid quotedetailid = _service.Create(quotedetail);
                    locallySource.pnl_QuoteProductId = quotedetailid.ToString();
                    _ctx.UpdateObject(locallySource);
                }
            }
            _ctx.SaveChanges();

            //REVISE: Delete Quote Product is Locally Source Item is link to previous quote
            var detailToDelete = from t in _ctx.QuoteDetailSet
                                 join l in _ctx.pnl_locallysourceditemSet on t.pnl_LocallySourcedItemId.Id equals l.Id
                                 where t.QuoteId.Id.Equals(qpi.Id)
                                 where l.pnl_Quote.Id != qpi.Id
                                 select new
                                 {
                                     Id = t.Id
                                 };
            foreach (var c in detailToDelete)
            {
                _service.Delete(QuoteDetail.EntityLogicalName, c.Id);
            }

            #endregion

            #region Update numbered tradein fields

            UpdateIfDifferent(_updateEntity, "pnl_TradeInMachine1BookValue", tradeInMachine1BookValue, qpi.pnl_TradeInMachine1BookValue);
            UpdateIfDifferent(_updateEntity, "pnl_TradeInMachine2BookValue", tradeInMachine2BookValue, qpi.pnl_TradeInMachine2BookValue);
            UpdateIfDifferent(_updateEntity, "pnl_TradeInMachine3BookValue", tradeInMachine3BookValue, qpi.pnl_TradeInMachine3BookValue);
            UpdateIfDifferent(_updateEntity, "pnl_TradeInMachine1Description", tradeInMachine1Description, qpi.pnl_TradeInMachine1Description);
            UpdateIfDifferent(_updateEntity, "pnl_TradeInMachine2Description", tradeInMachine2Description, qpi.pnl_TradeInMachine2Description);
            UpdateIfDifferent(_updateEntity, "pnl_TradeInMachine3Description", tradeInMachine3Description, qpi.pnl_TradeInMachine3Description);

            //John 6.05.2014 - Added new trade in fields
            //Labels
            UpdateIfDifferent(_updateEntity, "pnl_TradeInMarketPriceLabel", pnl_tradeinmarketpricelabel, qpi.pnl_TradeInMarketPriceLabel);
            UpdateIfDifferent(_updateEntity, "pnl_TradeInSellingMarginLabel", pnl_tradeinsellingmarginlabel, qpi.pnl_TradeInSellingMarginLabel);
            UpdateIfDifferent(_updateEntity, "pnl_TradeInSellingMarginLabel1", pnl_tradeinsellingmarginlabel1, qpi.pnl_TradeInSellingMarginLabel1);
            UpdateIfDifferent(_updateEntity, "pnl_TradeInRepairCostLabel", pnl_tradeinrepaircostlabel, qpi.pnl_TradeInRepairCostLabel);
            //Values
            //Machine1
            UpdateIfDifferent(_updateEntity, "pnl_TradeInMachine1TIMarketPrice", pnl_tradeinmachine1timarketprice, qpi.pnl_TradeInMachine1TIMarketPrice);
            UpdateIfDifferent(_updateEntity, "pnl_TradeInMachine1TISellingMargin", pnl_tradeinmachine1tisellingmargin, qpi.pnl_TradeInMachine1TISellingMargin);
            UpdateIfDifferent(_updateEntity, "pnl_TradeInMachine1TIRepairCost", pnl_tradeinmachine1tirepaircost, qpi.pnl_TradeInMachine1TIRepairCost);
            //Machine2
            UpdateIfDifferent(_updateEntity, "pnl_TradeInMachine2TIMarketPrice", pnl_tradeinmachine2timarketprice, qpi.pnl_TradeInMachine2TIMarketPrice);
            UpdateIfDifferent(_updateEntity, "pnl_TradeInMachine2TISellingMargin", pnl_tradeinmachine2tisellingmargin, qpi.pnl_TradeInMachine2TISellingMargin);
            UpdateIfDifferent(_updateEntity, "pnl_TradeInMachine2TIRepairCost", pnl_tradeinmachine2tirepaircost, qpi.pnl_TradeInMachine2TIRepairCost);
            //Machine3
            UpdateIfDifferent(_updateEntity, "pnl_TradeInMachine3TIMarketPrice", pnl_tradeinmachine3timarketprice, qpi.pnl_TradeInMachine3TIMarketPrice);
            UpdateIfDifferent(_updateEntity, "pnl_TradeInMachine3TISellingMargin", pnl_tradeinmachine3tisellingmargin, qpi.pnl_TradeInMachine3TISellingMargin);
            UpdateIfDifferent(_updateEntity, "pnl_TradeInMachine3TIRepairCost", pnl_tradeinmachine3tirepaircost, qpi.pnl_TradeInMachine3TIRepairCost);

            
            decimal? tradeInMachine1Scenario2Value;
            decimal? tradeInMachine1Scenario3Value;
            decimal? tradeInMachine2Scenario2Value;
            decimal? tradeInMachine2Scenario3Value;
            decimal? tradeInMachine3Scenario2Value;
            decimal? tradeInMachine3Scenario3Value;

            // default the scenario 2 & 3 trade in values to the same value the first time the Quote calculation is run - NB even if they have already been given a value
            // NB only write these defaulted scenario 2 & 3 values back to database if this was the first time the Quote calculation was run
            if (qpi.pnl_TradeInMachine1Scenario2Value == null || tradeInMachine1BookValue == null)
            {
                tradeInMachine1Scenario2Value = tradeInMachine1BookValue;
                UpdateIfDifferent(_updateEntity, "pnl_TradeInMachine1Scenario2Value", tradeInMachine1BookValue, qpi.pnl_TradeInMachine1BookValue);
            }
            else
            {
                tradeInMachine1Scenario2Value = ValueAsDecimal(qpi.pnl_TradeInMachine1Scenario2Value);
            }
            if (qpi.pnl_TradeInMachine1Scenario3Value == null || tradeInMachine1BookValue == null)
            {
                tradeInMachine1Scenario3Value = pnl_tradeinmachine1timarketprice;
                UpdateIfDifferent(_updateEntity, "pnl_TradeInMachine1Scenario3Value", tradeInMachine1Scenario3Value, qpi.pnl_TradeInMachine1Scenario3Value);
            }
            else
            {
                tradeInMachine1Scenario3Value = ValueAsDecimal(qpi.pnl_TradeInMachine1Scenario3Value);
            }

            if (qpi.pnl_TradeInMachine2Scenario2Value == null || tradeInMachine2BookValue == null)
            {
                tradeInMachine2Scenario2Value = tradeInMachine2BookValue;
                UpdateIfDifferent(_updateEntity, "pnl_TradeInMachine2Scenario2Value", tradeInMachine2BookValue, qpi.pnl_TradeInMachine2BookValue);
            }
            else
            {
                tradeInMachine2Scenario2Value = ValueAsDecimal(qpi.pnl_TradeInMachine2Scenario2Value);
            }
            if (qpi.pnl_TradeInMachine2Scenario3Value == null || tradeInMachine2BookValue == null)
            {
                tradeInMachine2Scenario3Value = pnl_tradeinmachine2timarketprice;
                UpdateIfDifferent(_updateEntity, "pnl_TradeInMachine2Scenario3Value", tradeInMachine2Scenario3Value, qpi.pnl_TradeInMachine2Scenario3Value);
            }
            else
            {
                tradeInMachine2Scenario3Value = ValueAsDecimal(qpi.pnl_TradeInMachine2Scenario3Value);
            }

            if (qpi.pnl_TradeInMachine3Scenario2Value == null || tradeInMachine3BookValue == null)
            {
                tradeInMachine3Scenario2Value = tradeInMachine3BookValue;
                UpdateIfDifferent(_updateEntity, "pnl_TradeInMachine3Scenario2Value", tradeInMachine3BookValue, qpi.pnl_TradeInMachine3BookValue);
            }
            else
            {
                tradeInMachine3Scenario2Value = ValueAsDecimal(qpi.pnl_TradeInMachine3Scenario2Value);
            }
            if (qpi.pnl_TradeInMachine3Scenario3Value == null || tradeInMachine3BookValue == null)
            {
                tradeInMachine3Scenario3Value = pnl_tradeinmachine3timarketprice;
                UpdateIfDifferent(_updateEntity, "pnl_TradeInMachine3Scenario3Value", tradeInMachine3Scenario3Value, qpi.pnl_TradeInMachine3Scenario3Value);
            }
            else
            {
                tradeInMachine3Scenario3Value = ValueAsDecimal(qpi.pnl_TradeInMachine3Scenario3Value);
            }
            #endregion

            // 7bi 
            UpdateIfDifferent(_updateEntity, "pnl_TotalBookValue", totalBookValue, qpi.pnl_TotalBookValue);

            // update In Stock Wholesale Item On Quote indicator
            UpdateIfDifferent(_updateEntity, "pnl_InStockWholesale", inStockWholesaleItemExists, qpi.pnl_InStockWholesale);

            // NB Cannot be taken inside the following "if" as this also sets the stock class reference
            string stockClassName = GetStockClassName(qpi, highestValueMachineName, highestValueMachineStockClass, ref quoteStockClassId);
            
            //John - 14.07.2014 change if statement check is Stock Class contains data instead of  if (string.IsNullOrWhiteSpace(qpi.Name))
            string prestockClassDescription = string.Format("{0}{1}", quotePreImage.CustomerId.Name == null ? "<customer>" : quotePreImage.CustomerId.Name, quotePreImage.pnl_StockClass == null ? "" : " - " + quotePreImage.pnl_StockClass.Name);
            string prequoteIDDescription = string.Format("{0}{1}", quotePreImage.CustomerId.Name == null ? "<customer>" : quotePreImage.CustomerId.Name, quotePreImage.pnl_LandpowerQuoteID == null ? "" : " - " + quotePreImage.pnl_LandpowerQuoteID);

            string poststockClassDescription = string.Format("{0}{1}", qpi.CustomerId.Name == null ? "<customer>" : qpi.CustomerId.Name, stockClassName == null ? "" : " - " + stockClassName);
            string postquoteIDDescription = string.Format("{0}{1}", qpi.CustomerId.Name == null ? "<customer>" : qpi.CustomerId.Name, qpi.pnl_LandpowerQuoteID == null ? "" : " - " + qpi.pnl_LandpowerQuoteID);

            string postDescription = string.Format("{0}", qpi.Name);

            if ((!string.IsNullOrWhiteSpace(stockClassName)))
            {
                if ((!postDescription.Equals(prestockClassDescription)) && (!postDescription.Equals(poststockClassDescription)) && (!postDescription.Equals(postquoteIDDescription)) && (!postDescription.Equals(prequoteIDDescription)))
                {
                    UpdateIfDifferent(_updateEntity, "Name", postDescription, qpi.Name);
                }
                else
                {
                    if (!prestockClassDescription.Equals(poststockClassDescription))
                    {
                        UpdateIfDifferent(_updateEntity, "Name", poststockClassDescription, qpi.Name);
                    }
                }
            }
            else
            {
                if ((!postDescription.Equals(prestockClassDescription)) && (!postDescription.Equals(poststockClassDescription)) && (!postDescription.Equals(postquoteIDDescription)) && (!postDescription.Equals(prequoteIDDescription)))
                {
                    UpdateIfDifferent(_updateEntity, "Name", postDescription, qpi.Name);
                }
                else
                {
                    UpdateIfDifferent(_updateEntity, "Name", postquoteIDDescription, qpi.Name);
                }
            }
                       
            
            UpdateIfDifferent(_updateEntity, "pnl_HighestValueMachineName", highestValueMachineName, qpi.pnl_HighestValueMachineName);
            UpdateIfDifferent(_updateEntity, "pnl_HighestValueMachineStockCode", highestValueMachineStockCode, qpi.pnl_HighestValueMachineStockCode);
            UpdateIfDifferent(_updateEntity, "pnl_basepricelineitemtotal", basePriceLineItemTotal, qpi.pnl_basepricelineitemtotal);
            UpdateIfDifferent(_updateEntity, "pnl_QuoteExpirationDate", (earliestExpirationDate < HIGHEST_DATE) ? earliestExpirationDate : (DateTime?)null, qpi.pnl_QuoteExpirationDate);

            // update Machine Cost values - Function 1
            UpdateIfDifferent(_updateEntity, "pnl_SubTotalListPrice", subTotalListPrice, qpi.pnl_SubTotalListPrice); // 1a
            UpdateIfDifferent(_updateEntity, "pnl_SubTotalDealerNetPrice", subtotalDealerNetPrice, qpi.pnl_SubTotalDealerNetPrice); // 1b
            UpdateIfDifferent(_updateEntity, "pnl_SubTotalDiscountPercentage", 
                subTotalListPrice == 0 ? DECIMAL0 : (DECIMAL100 * (subTotalListPrice - subtotalDealerNetPrice) / subTotalListPrice),
                qpi.pnl_SubTotalDiscountPercentage); // 1c

            // update Locally Sourced Item values - Function 2
            UpdateIfDifferent(_updateEntity, "pnl_TotalLocallySourcedItemsListPrice", totalLocallySourcedItemsListPrice, qpi.pnl_TotalLocallySourcedItemsListPrice); // 2a
            UpdateIfDifferent(_updateEntity, "pnl_TotalLocallySourcedItemsDealerNetPrice", totalLocallySourcedItemsDealerNetPrice, qpi.pnl_TotalLocallySourceItemsDealerNetPrice); // 2b

            if (totalLocallySourcedItemsListPrice == DECIMAL0 && totalLocallySourcedItemsDealerNetPrice != DECIMAL0)
            {
                UpdateIfDifferent(_updateEntity, "pnl_TotalLocallySourcedItemsDiscountPercentag", 100, qpi.pnl_TotalLocallySourcedItemsDiscountPercentag);
            }
            else
            {
                //please note that this is an OR as the one exception is covered above in the if statement 
                if (totalLocallySourcedItemsListPrice == DECIMAL0 || totalLocallySourcedItemsDealerNetPrice == DECIMAL0)
                {
                    UpdateIfDifferent(_updateEntity, "pnl_TotalLocallySourcedItemsDiscountPercentag", null, qpi.pnl_TotalLocallySourcedItemsDiscountPercentag);
                }
                else
                {
                    UpdateIfDifferent(_updateEntity, "pnl_TotalLocallySourcedItemsDiscountPercentag",
                    (DECIMAL100 * (totalLocallySourcedItemsListPrice - totalLocallySourcedItemsDealerNetPrice) / totalLocallySourcedItemsListPrice),
                    qpi.pnl_TotalLocallySourcedItemsDiscountPercentag); // 2c
                }
            }


            // update Other Dealer Costs - Function 3
            decimal totalOtherDealerCosts =
                ValueAsDecimal(qpi.pnl_OtherDealerCostsFreight) +
                ValueAsDecimal(qpi.pnl_otherdealercostspredelivery) +
                ValueAsDecimal(qpi.pnl_OtherDealerInterestSubsidy);  // 3a

            UpdateIfDifferent(_updateEntity, "pnl_TotalOtherDealerCosts", totalOtherDealerCosts, qpi.pnl_TotalOtherDealerCosts);

            // 4a
            decimal totalListPrice = subTotalListPrice + totalLocallySourcedItemsListPrice;
            UpdateIfDifferent(_updateEntity, "pnl_TotalListPrice", totalListPrice, qpi.pnl_TotalListPrice);

            // 4b
            decimal scenario1TotalDealerNetCost =
                subtotalDealerNetPrice +
                totalLocallySourcedItemsDealerNetPrice +
                totalOtherDealerCosts;
            UpdateIfDifferent(_updateEntity, "pnl_Scenario1TotalDealerNetCost", scenario1TotalDealerNetCost, qpi.pnl_Scenario1TotalDealerNetCost);


            // 4b mod 20130704
            decimal salesAid = qpi.pnl_SalesAidApproved == null ? ValueAsDecimal(qpi.pnl_SalesAidRequested) : ValueAsDecimal(qpi.pnl_SalesAidApproved);

            decimal scenarios2and3TotalDealerNetCost =
                subtotalDealerNetPrice +
                totalLocallySourcedItemsDealerNetPrice +
                totalOtherDealerCosts -
                salesAid;

            UpdateIfDifferent(_updateEntity, "pnl_Scenario2TotalDealerNetCost", scenarios2and3TotalDealerNetCost, qpi.pnl_Scenario2TotalDealerNetCost);
            UpdateIfDifferent(_updateEntity, "pnl_Scenario3TotalDealerNetCost", scenarios2and3TotalDealerNetCost, qpi.pnl_Scenario3TotalDealerNetCost);

            // 4c
            UpdateIfDifferent(_updateEntity, "pnl_PotentialGPatListPrice", subTotalListPrice == 0 ? DECIMAL0 :
                (DECIMAL100 * (totalListPrice - scenario1TotalDealerNetCost) / totalListPrice), qpi.pnl_PotentialGPatListPrice);

            if (ValueAsDecimal(qpi.pnl_Scenario1GrossProfitPercentage) == DECIMAL0)
                // the current value of of the field is empty so assign it from the margin derived from the highest priced machine line
                UpdateIfDifferent(_updateEntity, "pnl_Scenario1GrossProfitPercentage", scenario1GrossProfitPercentage, qpi.pnl_Scenario1GrossProfitPercentage);
            else
                // the current value of of the field currently has a value so use this and do not overwrite it
                scenario1GrossProfitPercentage = ValueAsDecimal(qpi.pnl_Scenario1GrossProfitPercentage);

            // 5a
            decimal scenario1GrossProfitProportion = scenario1GrossProfitPercentage / DECIMAL100;
            decimal scenario1GrossProfitDollar =
                scenario1GrossProfitProportion == DECIMAL1 ? DECIMAL0 :
                ((scenario1TotalDealerNetCost / (1 - scenario1GrossProfitProportion)) * scenario1GrossProfitProportion);
            UpdateIfDifferent(_updateEntity, "pnl_Scenario1GrossProfitDollar", scenario1GrossProfitDollar, qpi.pnl_Scenario1GrossProfitDollar);

            // 5b
            decimal scenario1NetSellingPrice = scenario1TotalDealerNetCost + scenario1GrossProfitDollar;
            UpdateIfDifferent(_updateEntity, "pnl_Scenario1NetSellingPrice", scenario1NetSellingPrice, qpi.pnl_Scenario1NetSellingPrice);

            // 7bii
            decimal scenario2TotalTradedValue =
                ValueAsDecimal(tradeInMachine1Scenario2Value) +
                ValueAsDecimal(tradeInMachine2Scenario2Value) +
                ValueAsDecimal(tradeInMachine3Scenario2Value) +
                totalquotemachineScenario2;
            UpdateIfDifferent(_updateEntity, "pnl_Scenario2TotalTradedValue", scenario2TotalTradedValue, qpi.pnl_Scenario2TotalTradedValue);

            decimal scenario3TotalTradedValue =
                ValueAsDecimal(tradeInMachine1Scenario3Value) +
                ValueAsDecimal(tradeInMachine2Scenario3Value) +
                ValueAsDecimal(tradeInMachine3Scenario3Value) +
                totalquotemachineScenario3; 
            UpdateIfDifferent(_updateEntity, "pnl_Scenario3TotalTradedValue", scenario3TotalTradedValue, qpi.pnl_Scenario3TotalTradedValue);

            // 8a
            decimal scenario2OvertradeAmount = scenario2TotalTradedValue - totalBookValue;
            UpdateIfDifferent(_updateEntity, "pnl_Scenario2OvertradeAmount", scenario2OvertradeAmount, qpi.pnl_Scenario2OvertradeAmount);

            decimal scenario3OvertradeAmount = scenario3TotalTradedValue - totalBookValue;
            UpdateIfDifferent(_updateEntity, "pnl_Scenario3OvertradeAmount", scenario3OvertradeAmount, qpi.pnl_Scenario3OvertradeAmount);

            // 6a
            decimal scenario1ChangeoverPriceExclGST = scenario1NetSellingPrice - totalBookValue;
            UpdateIfDifferent(_updateEntity, "pnl_Scenario1ChangeoverPriceExclGST", scenario1ChangeoverPriceExclGST, qpi.pnl_Scenario1ChangeoverPriceExclGST);

            decimal scenario2ChangeoverPriceExclGST, scenario3ChangeoverPriceExclGST;
            if (qpi.pnl_Scenario2ChangeoverPriceExclGST == null || ValueAsDecimal(qpi.pnl_Scenario2ChangeoverPriceExclGST.Value) == DECIMAL0)
            {
                scenario2ChangeoverPriceExclGST = scenario1ChangeoverPriceExclGST;
                UpdateIfDifferent(_updateEntity, "pnl_Scenario2ChangeoverPriceExclGST", scenario1ChangeoverPriceExclGST, qpi.pnl_Scenario2ChangeoverPriceExclGST);
            }
            else
            {
                scenario2ChangeoverPriceExclGST = ValueAsDecimal(qpi.pnl_Scenario2ChangeoverPriceExclGST);
            }
            if (qpi.pnl_Scenario3ChangeoverPriceExclGST == null || ValueAsDecimal(qpi.pnl_Scenario3ChangeoverPriceExclGST.Value) == DECIMAL0)
            {
                scenario3ChangeoverPriceExclGST = scenario1ChangeoverPriceExclGST;
                UpdateIfDifferent(_updateEntity, "pnl_Scenario3ChangeoverPriceExclGST", scenario1ChangeoverPriceExclGST, qpi.pnl_Scenario3ChangeoverPriceExclGST);
            }
            else
            {
                scenario3ChangeoverPriceExclGST = ValueAsDecimal(qpi.pnl_Scenario3ChangeoverPriceExclGST);
            }

            // 6h
            decimal scenario2ChangeoverDiscounts = totalListPrice - scenario2TotalTradedValue - scenario2ChangeoverPriceExclGST;
            UpdateIfDifferent(_updateEntity, "pnl_Scenario2ChangeoverDiscounts", scenario2ChangeoverDiscounts, qpi.pnl_Scenario2ChangeoverDiscounts);

            decimal scenario3ChangeoverDiscounts = totalListPrice - scenario3TotalTradedValue - scenario3ChangeoverPriceExclGST;
            UpdateIfDifferent(_updateEntity, "pnl_Scenario3ChangeoverDiscounts", scenario3ChangeoverDiscounts, qpi.pnl_Scenario3ChangeoverDiscounts);

            // 6i
            decimal scenario2NetSellingPrice = totalListPrice - scenario2ChangeoverDiscounts;
            UpdateIfDifferent(_updateEntity, "pnl_Scenario2NetSellingPrice", scenario2NetSellingPrice, qpi.pnl_Scenario2NetSellingPrice);

            decimal scenario3NetSellingPrice = totalListPrice - scenario3ChangeoverDiscounts;
            UpdateIfDifferent(_updateEntity, "pnl_Scenario3NetSellingPrice", scenario3NetSellingPrice, qpi.pnl_Scenario3NetSellingPrice);

            UpdateIfDifferent(_updateEntity, "pnl_EstimatedRevenue", scenario3NetSellingPrice, qpi.pnl_EstimatedRevenue);

            // 5g
            decimal scenario2NetSellingPriceInternal = scenario2NetSellingPrice - scenario2OvertradeAmount;
            UpdateIfDifferent(_updateEntity, "pnl_Scenario2NetSellingPriceInternal", scenario2NetSellingPriceInternal, qpi.pnl_Scenario2NetSellingPriceInternal);

            decimal scenario3NetSellingPriceInternal = scenario3NetSellingPrice - scenario3OvertradeAmount;
            UpdateIfDifferent(_updateEntity, "pnl_Scenario3NetSellingPriceInternal", scenario3NetSellingPriceInternal, qpi.pnl_Scenario3NetSellingPriceInternal);

            // 5f, 5d
            decimal scenario2GrossProfitDollar = (scenario2NetSellingPriceInternal - scenarios2and3TotalDealerNetCost);
            UpdateIfDifferent(_updateEntity, "pnl_Scenario2GrossProfitDollar", scenario2GrossProfitDollar, qpi.pnl_Scenario2GrossProfitDollar);

            decimal scenario3GrossProfitDollar = scenario3NetSellingPriceInternal - scenarios2and3TotalDealerNetCost;
            UpdateIfDifferent(_updateEntity, "pnl_Scenario3GrossProfitDollar", scenario3GrossProfitDollar, qpi.pnl_Scenario3GrossProfitDollar);

            // 5e, 5c
            UpdateIfDifferent(_updateEntity, "pnl_Scenario2GrossProfitPercentage", scenario2NetSellingPriceInternal == DECIMAL0 ? DECIMAL0 :
                (DECIMAL100 * scenario2GrossProfitDollar / scenario2NetSellingPriceInternal), qpi.pnl_Scenario2GrossProfitPercentage);

            UpdateIfDifferent(_updateEntity, "pnl_Scenario3GrossProfitPercentage", scenario3NetSellingPriceInternal == DECIMAL0 ? DECIMAL0 :
                (DECIMAL100 * scenario3GrossProfitDollar / scenario3NetSellingPriceInternal), qpi.pnl_Scenario3GrossProfitPercentage);

            // 6b
            decimal scenario1ChangeoverDiscounts = totalListPrice - scenario1NetSellingPrice;
            UpdateIfDifferent(_updateEntity, "pnl_Scenario1ChangeoverDiscounts", scenario1ChangeoverDiscounts, qpi.pnl_Scenario1ChangeoverDiscounts);

            // 6c
            //decimal scenario1NetSellingPrice = totalListPrice - scenario1ChangeoverDiscounts;
            //qpi.pnl_Scenario1NetSellingPrice = new Money(scenario1NetSellingPrice);


            // 6d
            UpdateIfDifferent(_updateEntity, "pnl_Scenario1ChangeoverGrossProfitPercentage", scenario1NetSellingPrice == DECIMAL0 ? DECIMAL0 :
                 (DECIMAL100 * (scenario1NetSellingPrice - scenario1TotalDealerNetCost) / scenario1NetSellingPrice), qpi.pnl_Scenario1ChangeoverGrossProfitPercentage);

            // 6g
            decimal s2Denominator = scenario2NetSellingPrice - scenario2OvertradeAmount;
            UpdateIfDifferent(_updateEntity, "pnl_Scenario2ChangeoverGrossProfitPercentage", s2Denominator == DECIMAL0 ? DECIMAL0 :
                DECIMAL100 * (scenario2NetSellingPrice - scenario2OvertradeAmount - scenarios2and3TotalDealerNetCost) / s2Denominator, qpi.pnl_Scenario2ChangeoverGrossProfitPercentage);

            // 6j
            decimal s3Denominator = scenario3NetSellingPrice - scenario3OvertradeAmount;
            UpdateIfDifferent(_updateEntity, "pnl_Scenario3ChangeoverGrossProfitPercentage", s3Denominator == DECIMAL0 ? DECIMAL0 :
                DECIMAL100 * (scenario3NetSellingPrice - scenario3OvertradeAmount - scenarios2and3TotalDealerNetCost) / s3Denominator, qpi.pnl_Scenario3ChangeoverGrossProfitPercentage);

            // moved h
            ProcessQuoteProductUpdates(context, requestWithContinueOnError);
        }

        private string GetStockClassName(Quote qpi, string highestValueMachineName, string highestValueMachineStockClass, ref Guid? stockClassId)
        {
            if (highestValueMachineName != null)
            {
                var stockClassRef = GetStockClassReference(highestValueMachineStockClass);
                if (stockClassRef != null)
                {
                    if (qpi.pnl_StockClass != stockClassRef)
                        _updateEntity["pnl_stockclass"] = stockClassRef;

                    var stockClass = (from s in _ctx.pnl_StockClassSet where s.Id == stockClassRef.Id select s).FirstOrDefault();
                    stockClassId = stockClass.Id;
                    return stockClass.pnl_name;
                }
                else
                {
                    if (qpi.pnl_StockClass != null)
                    {
                        var stockClass = (from s in _ctx.pnl_StockClassSet where s.Id == qpi.pnl_StockClass.Id select s).FirstOrDefault();
                        stockClassId = stockClass.Id;
                        return stockClass.pnl_name;
                    }
                }
            }
            return null;
        }

        private EntityReference GetStockClassReference(string highestValueMachineStockClass)
        {
            pnl_StockClass stockClass = (from sc in _ctx.pnl_StockClassSet
                                         where sc.pnl_ID == highestValueMachineStockClass
                                         select sc).FirstOrDefault();
            if (stockClass == null)
                return null;
            else
                return new EntityReference("pnl_stockclass", stockClass.Id);
        }

        private pnl_StockClass GetStockClass(Guid stockClassId)
        {
            return (from sc in _ctx.pnl_StockClassSet
                    where sc.pnl_StockClassId == stockClassId
                    select sc).FirstOrDefault();
        }

        private int GetQuoteBuCountryValue(Quote qpi)
        {
            try
            {
                BusinessUnit ownersBu = (from bu in _ctx.BusinessUnitSet
                                         join su in _ctx.SystemUserSet on bu.BusinessUnitId equals su.BusinessUnitId.Id
                                         where su.SystemUserId == qpi.OwnerId.Id
                                         select bu).FirstOrDefault();

                if (ownersBu == null || ownersBu.pnl_BusinessUnitCountry == null)
                    throw new InvalidPluginExecutionException("The Quote owner must be assigned to a Business Unit that has a Country specified.");

                return ownersBu.pnl_BusinessUnitCountry.Value;
            }
            catch (Exception e)
            {
                RethrowExceptionAsInvalidPluginExecutionException(e, "Error retrieving business unit country for this Quote's owner.");
            }

            throw new InvalidPluginExecutionException("Unexpected code path when querying Quote owner's business unit.");
        }

        private void CalculateDealerNetPrice(QuoteDetail quoteDetailImage, decimal listPrice, DateTime todaysUtcDateOnly, int ownersBuValue, out QuoteDetail quoteProductUpdate,
            out decimal bpass_DNPDollar, out decimal margin)
        {
            // If the quote line [quoteproduct] is a �dealer in stock item� (confirmed by [pnl_instkisdealerstock] is set to yes) then don�t apply discount matrix
            bool instkIsDealerStock = (quoteDetailImage.pnl_instkIsDealerStock.HasValue && quoteDetailImage.pnl_instkIsDealerStock.Value);
            quoteProductUpdate = null;
            bpass_DNPDollar = DECIMAL0;
            margin = DECIMAL0;
            if ((quoteDetailImage.pnl_islocallysourced == null || quoteDetailImage.pnl_islocallysourced.Value == LandpowerPluginBase.YES_OR_NO_FALSE)
                  && (!quoteDetailImage.pnl_IsTradeIn.HasValue || !quoteDetailImage.pnl_IsTradeIn.Value))
            {
                decimal? standardDiscount = null;
                decimal? indentDiscount = null;
                decimal? monthlyProgPercent = null;
                decimal? monthlyProgDollar = null;

                // use new Quote Detail object and only set tose fields that need to update as passing unchanged values can trigger unnecessary updates
                quoteProductUpdate = new QuoteDetail() { Id = quoteDetailImage.Id };

                if (quoteDetailImage.pnl_specType != null && (LandpowerPluginBase.SpecificationType)quoteDetailImage.pnl_specType.Value == LandpowerPluginBase.SpecificationType.PRODUCT)
                    ApplyDiscountMatrix(quoteDetailImage, todaysUtcDateOnly, ownersBuValue,
                        ref standardDiscount, ref indentDiscount, ref monthlyProgPercent, ref monthlyProgDollar, ref margin);

                decimal machineCostsDiscountDollars;
                if (instkIsDealerStock)
                {
                    bpass_DNPDollar = ValueAsDecimal(quoteDetailImage.pnl_BPASS_DNPDollar);
                    machineCostsDiscountDollars = listPrice - bpass_DNPDollar;

                    standardDiscount = null;
                    indentDiscount = null;
                    monthlyProgPercent = null;
                    monthlyProgDollar = null;
                }
                else
                {
                    decimal standardDiscountDollars = standardDiscount.HasValue ? listPrice * standardDiscount.Value / DECIMAL100 : DECIMAL0;
                    decimal indentDiscountDollars = indentDiscount.HasValue ? listPrice * indentDiscount.Value / DECIMAL100 : DECIMAL0;
                    decimal monthlyProgPercentDollars = monthlyProgPercent.HasValue ? listPrice * monthlyProgPercent.Value / DECIMAL100 : DECIMAL0;

                    machineCostsDiscountDollars =
                        standardDiscountDollars
                        + indentDiscountDollars
                        + monthlyProgPercentDollars
                        + (monthlyProgDollar.HasValue ? monthlyProgDollar.Value : DECIMAL0);

                    bpass_DNPDollar = listPrice - machineCostsDiscountDollars;
                    quoteProductUpdate.pnl_BPASS_DNPDollar = new Money(bpass_DNPDollar);
                }

                // use new Quote Detail object and only set those fields that need to update as passing unchanged values can trigger unnecessary updates
                quoteProductUpdate = new QuoteDetail() { Id = quoteDetailImage.Id };
                bool updateRequired = false;

                if (!instkIsDealerStock)
                {
                    updateRequired = UpdateIfDifferent(quoteProductUpdate, "pnl_BPASS_DNPDollar", bpass_DNPDollar, quoteDetailImage.pnl_BPASS_DNPDollar) || updateRequired;
                }
                updateRequired = UpdateIfDifferent(quoteProductUpdate, "pnl_MachineCostsStandardDiscount", standardDiscount, quoteDetailImage.pnl_MachineCostsStandardDiscount) || updateRequired;
                updateRequired = UpdateIfDifferent(quoteProductUpdate, "pnl_MachineCostsIndentDiscount", indentDiscount, quoteDetailImage.pnl_MachineCostsIndentDiscount) || updateRequired;
                updateRequired = UpdateIfDifferent(quoteProductUpdate, "pnl_machinecostsmonthlyprogpercent", monthlyProgPercent, quoteDetailImage.pnl_machinecostsmonthlyprogpercent) || updateRequired;
                updateRequired = UpdateIfDifferent(quoteProductUpdate, "pnl_MachineCostsMonthlyProgDollar", monthlyProgDollar, quoteDetailImage.pnl_MachineCostsMonthlyProgDollar) || updateRequired;

                updateRequired = UpdateIfDifferent(quoteProductUpdate, "pnl_BPASS_DNPDiscount"
                    , listPrice == DECIMAL0 ? DECIMAL0 : (machineCostsDiscountDollars / listPrice) * 100
                    , quoteDetailImage.pnl_BPASS_DNPDiscount) 
                    || updateRequired;

                if(!updateRequired)
                {
                    quoteProductUpdate = null;  
                }
            }
        }

        private void ApplyDiscountMatrix(QuoteDetail quoteDetailImage, DateTime todaysUtcDateOnly, int ownersBuCountryValue,
            ref decimal? standardDiscount, ref decimal? indentDiscount, ref decimal? monthlyProgPercent, ref decimal? monthlyProgDollar, ref decimal standardMargin)
        {
            bool standardDiscountApplied = false;
            bool indentDiscountApplied = false;
            bool programmeDiscountApplied = false;

            try
            {

                var discountInSet = (from t in _ctx.pnl_discountSet
                                     where (t.pnl_Country != null && t.pnl_Country.Value == ownersBuCountryValue)
                                     && (t.pnl_DateFrom == null || t.pnl_DateFrom.Value <= todaysUtcDateOnly)
                                     && (t.pnl_DateTo == null || t.pnl_DateTo.Value >= todaysUtcDateOnly)
                                     && (
                                         // if either a serial number match is made
                                             (t.pnl_SerialNo != null && t.pnl_SerialNo != string.Empty && t.pnl_SerialNo.Equals(quoteDetailImage.pnl_InStkserial_no))
                                             ||
                                             (
                                         // OR a serial number match is NOT made
                                                 (t.pnl_SerialNo == null || t.pnl_SerialNo == string.Empty)
                                                 &&
                                                 (
                                         // AND a stock code match is made
                                                     (t.pnl_StockCode != null && t.pnl_StockCode != string.Empty && t.pnl_StockCode.Equals(quoteDetailImage.pnl_specs_sales_stock_code))
                                                     &&
                                         // AND 
                                                     (
                                         //EITHER the model code is empty 
                                                         (t.pnl_modelcode == null || t.pnl_modelcode == string.Empty)
                                                         ||
                                         // OR a model code match is made 
                                                         (t.pnl_modelcode != null && t.pnl_modelcode != string.Empty && t.pnl_modelcode.Equals(quoteDetailImage.pnl_mod_sup_stock_code))
                                                     )
                                                 )
                                             )
                                        )
                                     orderby t.pnl_SerialNo descending // should sort serial nos first
                                     select t).ToList();

                // first process serialno matches, and then the rest if still not defined - ie the respective decimals are still null
                foreach (var disc in discountInSet)
                {
                    switch ((LandpowerPluginBase.DiscountType)disc.pnl_DiscountType.Value)
                    {
                        case LandpowerPluginBase.DiscountType.STANDARD:
                            if (!standardDiscountApplied)
                            {
                                standardDiscount = ValueAsDecimal(disc.pnl_DiscountPercentage);
                                standardMargin = ValueAsDecimal(disc.pnl_TargetMargin);
                                standardDiscountApplied = true;
                            }
                            break;
                        case LandpowerPluginBase.DiscountType.INDENT:
                            if (!indentDiscountApplied)
                            {
                                indentDiscount = ValueAsDecimal(disc.pnl_DiscountPercentage);
                                indentDiscountApplied = true;
                            }
                            break;
                        case LandpowerPluginBase.DiscountType.PROGRAMME:
                            if (!programmeDiscountApplied)
                            {
                                // only use one of these 2 - use percent if defined else the dollars if defined, 
                                // else use nothing (but still note this record exists and defines nothing be used).
                                if (disc.pnl_DiscountPercentage.HasValue && disc.pnl_DiscountPercentage != DECIMAL0)
                                    monthlyProgPercent = ValueAsDecimal(disc.pnl_DiscountPercentage);
                                else if (quoteDetailImage.pnl_specType != null
                                    && (LandpowerPluginBase.SpecificationType)quoteDetailImage.pnl_specType.Value == LandpowerPluginBase.SpecificationType.PRODUCT
                                    && disc.pnl_DiscountDollar != null
                                    && disc.pnl_DiscountDollar.Value != DECIMAL0)
                                    monthlyProgDollar = ValueAsDecimal(disc.pnl_DiscountDollar);
                                programmeDiscountApplied = true;
                            }
                            break;
                    }

                    // only apply on serial number discount - these should have been encountered first if they exist
                    if (disc.pnl_SerialNo != null)
                        break;
                }
            }
            catch (Exception e)
            {
                RethrowExceptionAsInvalidPluginExecutionException(e, "Error while retrieving Discount Matrix for Quote Product");
            }
        }

        private static void RethrowExceptionAsInvalidPluginExecutionException(Exception e, string actionDescription)
        {
            string messages = e.Message;
            while (e.InnerException != null)
            {
                messages = string.Format("{0}{0}{1},Message: {2}", messages, Environment.NewLine, e.Message);
            }
            throw new InvalidPluginExecutionException(string.Format("{0}{1},Message: {2}", actionDescription, Environment.NewLine, messages));
        }

        //private static void CalculateLocallySourcedItemStandardDiscount(decimal listPrice, QuoteDetail quoteDetailForReadOnly, out QuoteDetail quoteProductUpdate)
        //{
        //    quoteProductUpdate = new QuoteDetail() { Id = quoteDetailForReadOnly.Id };
        //    decimal locallySourcedDNP = quoteDetailForReadOnly.pnl_LocallySourcedDNP != null ? quoteDetailForReadOnly.pnl_LocallySourcedDNP.Value : 0m;

        //    if (listPrice == 0 || locallySourcedDNP == 0)
        //        quoteProductUpdate.pnl_LocallySourcedStandardDiscount = null;
        //    else
        //        quoteProductUpdate.pnl_LocallySourcedStandardDiscount = 100 * (listPrice - locallySourcedDNP) / listPrice;
        //}

        private static decimal GetListPrice(QuoteDetail quoteDetailPreImage)
        {
            decimal pricePerUnit = ValueAsDecimal(quoteDetailPreImage.PricePerUnit);
            decimal quantity = quoteDetailPreImage.Quantity.HasValue ? quoteDetailPreImage.Quantity.Value : 0m;
            decimal listPrice = decimal.Round(pricePerUnit * quantity, 2);
            return listPrice;
        }

        private static string GetProductDescription(QuoteDetail tradeInProduct)
        {
            return string.IsNullOrWhiteSpace(tradeInProduct.ProductDescription)
                ? (tradeInProduct.ProductId == null ? null : tradeInProduct.ProductId.Name)
                : tradeInProduct.ProductDescription;
        }

        private static string GetTradeInLabelName(decimal? fieldValue, string labelName, string currentLabelName)
        {
            if (currentLabelName == null)
            {
                return fieldValue == null ? null : labelName;
            }
            return currentLabelName;
        }

        private static string GetTradeInLabelName(string fieldValue, string labelName)
        {
            return string.IsNullOrWhiteSpace(fieldValue) ? null : labelName;
        }

        private static decimal ValueAsDecimal(Money field)
        {
            return field == null ? DECIMAL0 : field.Value;
        }

        private static decimal ValueAsDecimal(decimal? field)
        {
            return field.HasValue ? field.Value : DECIMAL0;
        }

        private void GenerateEmail(Guid from, Guid to, string subject, string description, EntityReference regarding, string recipientNames)
        {
            GenerateEmail(from, to, null, subject, description, regarding, recipientNames);
        }

        private void GenerateEmail(Guid from, Guid to, Guid? cc, string subject, string description, EntityReference regarding, string recipientNames)
        {
            ActivityParty fromParty = new ActivityParty
            {
                PartyId = new EntityReference(SystemUser.EntityLogicalName, from)
            };

            ActivityParty toParty = new ActivityParty
            {
                PartyId = new EntityReference(SystemUser.EntityLogicalName, to)
            };
            
            Email email = new Email
            {
                To = new ActivityParty[] { toParty },
                From = new ActivityParty[] { fromParty },
                Subject = subject,
                Description = description,
                DirectionCode = true,
                RegardingObjectId = regarding
            };

            if (cc.HasValue && cc.Value != Guid.Empty)
            {
                
                ActivityParty ccParty = new ActivityParty
                {
                    PartyId = new EntityReference(SystemUser.EntityLogicalName, cc.Value)
                }; 

                email.Cc = new ActivityParty[] { ccParty };
            }

            Guid emailId = _service.Create(email);

            SendEmailRequest sendEmailreq = new SendEmailRequest()
            {
                EmailId = emailId,
                TrackingToken = "",
                IssueSend = true
            };

            if (sendEmailreq == null)
                throw new InvalidPluginExecutionException("Failed to initialise new SendEmailRequest in GenerateEmail method.");
            if (_service == null)
                throw new InvalidPluginExecutionException("Null OrganizationService in GenerateEmail method.");

            try
            {
                _service.Execute(sendEmailreq);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException(string.Format("Please get your system administrator to check both your email setup and that of the following recipients: {0}.  (Info: {1})", 
                    recipientNames, e.Message));            
            }

        }

        #region functions to conditionally assign update fields
        // TODO: cannot do this with nullable types?
        //private void UpdateIfDifferent<T>(T targetValue, T preImageValue, T newValue) where T : IComparable
        //{
        //    if (preImageValue.Equals(newValue)) return;
        //    targetValue = newValue;
        //}

        private bool UpdateIfDifferent(Entity updateEntity, string targetFieldName, string newValue, string preImageValue)
        {
            bool currentlyNull = (preImageValue == null);

            // both null
            if (newValue == null && currentlyNull) return false;

            // both not null and equal
            if (newValue != null && !currentlyNull && preImageValue.Equals(newValue)) return false;

            updateEntity[targetFieldName.ToLower()] = newValue;

            return true;
        }
        private bool UpdateIfDifferent(Entity updateEntity, string targetFieldName, Decimal? newValue, Decimal? preImageValue)
        {
            bool currentlyNull = (preImageValue==null || !preImageValue.HasValue);
           
            // both null
            if(!newValue.HasValue && currentlyNull) return false;

            // both not null and equal
            if(newValue.HasValue && !currentlyNull && preImageValue.Value.Equals(newValue)) return false;

            // updating to null
            if (!newValue.HasValue)
            {
                updateEntity[targetFieldName.ToLower()] = null;
            }
            else
            {
                updateEntity[targetFieldName.ToLower()] = newValue;
            }
            return true;
        }
        private bool UpdateIfDifferent(Entity updateEntity, string targetFieldName, Decimal? newValue, Money preImageValue)
        {
            bool currentlyNull = (preImageValue==null);
           
            // both null
            if(!newValue.HasValue && currentlyNull) return false;

            // both not null and equal
            if(newValue.HasValue && !currentlyNull && preImageValue.Value.Equals(newValue)) return false;

            // updating to null
            if (!newValue.HasValue)
            {
                updateEntity[targetFieldName.ToLower()] = null;
            }
            else
            {
                updateEntity[targetFieldName.ToLower()] = new Money(newValue.Value);
            }
            return true;
        }

        //private bool UpdateIfDifferent(int? targetValue, int newValue, int preImageValue)
        //{
        //    bool currentlyNull = (preImageValue==null || preImageValue.Value == null);
           
        //    // both null
        //    if(!newValue.HasValue && currentlyNull) return false;

        //    // both not null and equal
        //    if(newValue.HasValue && !currentlyNull && preImageValue.Value.Equals(newValue)) return false;

        //    // updating to null
        //    if (!newValue.HasValue)
        //    {
        //        targetValue = null;
        //    }
        //    else
        //    {
        //        targetValue = newValue;
        //    }
        //    return true;
        //}
        private bool UpdateIfDifferent(Entity updateEntity, string targetFieldName, bool? newValue, bool? preImageValue)
        {
            bool currentlyNull = (preImageValue==null || !preImageValue.HasValue);
           
            // both null
            if(!newValue.HasValue && currentlyNull) return false;

            // both not null and equal
            if(newValue.HasValue && !currentlyNull && preImageValue.Value.Equals(newValue)) return false;

            // updating to null
            if (!newValue.HasValue)
            {
                updateEntity[targetFieldName.ToLower()] = null;
            }
            else
            {
                updateEntity[targetFieldName.ToLower()] = newValue;
            }
            return true;
        }
        private bool UpdateIfDifferent(Entity updateEntity, string targetFieldName, DateTime? newValue, DateTime? preImageValue)
        {
            bool currentlyNull = (preImageValue==null || !preImageValue.HasValue);
           
            // both null
            if(!newValue.HasValue && currentlyNull) return false;

            // both not null and equal
            if(newValue.HasValue && !currentlyNull && preImageValue.Value.Equals(newValue)) return false;

            // updating to null
            if (!newValue.HasValue)
            {
                updateEntity[targetFieldName.ToLower()] = null;
            }
            else
            {
                updateEntity[targetFieldName.ToLower()] = newValue;
            }
            return true;
        }
        #endregion
    }
}
