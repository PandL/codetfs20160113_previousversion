﻿using System;
using System.Linq;
using System.ServiceModel;
using Microsoft.Xrm.Sdk;
using LandPower;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

namespace LandPowerPlugins
{
    public class QuotePlugin2B : IPlugin
    {
        IOrganizationService _service;
        crmServiceContext _ctx;

        public void Execute(IServiceProvider serviceProvider)
        {
            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService =
                (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            // Obtain the execution context from the service provider.
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            
            if (!context.Stage.Equals(40)) return;
            
            // The InputParameters collection contains all the data passed in the message request.
            if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
            {
                // Obtain the target entity from the input parameters.
                Entity quoteUpdateEntity = (Entity)context.InputParameters["Target"];

                // Verify that the target entity represents an account.
                // If not, this plug-in was not registered correctly.
                if (quoteUpdateEntity.LogicalName != "quote") return;

                try
                {
                    // Obtain the organization service reference.
                    IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                    _service = serviceFactory.CreateOrganizationService(context.UserId);

                    // Use reference to invoke pre-built types service 
                    _ctx = new LandPower.crmServiceContext(_service);

                    // next get the PreImage of the quote
                    Entity quotePreImageEntity = null;
                    if (context.PreEntityImages.Contains("quote") && (context.PreEntityImages["quote"] is Entity))
                    {
                        quotePreImageEntity = (Entity)context.PreEntityImages["quote"];
                    }
                    if (quotePreImageEntity == null || quotePreImageEntity.LogicalName != "quote")
                    {
                        throw new InvalidPluginExecutionException("Incorrect Plugin registration: a Pre image has not been correctly configured for this plugin.");
                    }

                    Quote quoteUpdate = quoteUpdateEntity.ToEntity<Quote>();

                    //UpdateEntityAttributes(quoteUpdateEntity, quotePreImageEntity);

                    Quote quotePreImageWithUpdates = quotePreImageEntity.ToEntity<Quote>();

                    if (quoteUpdate.pnl_SalesAidRequest.HasValue && quoteUpdate.pnl_SalesAidRequest.Value)
                    {
                        string subject = string.Format("{2} Sales Aid Request for {0}; Quote ID: {1}", quotePreImageWithUpdates.CustomerId.Name, quotePreImageWithUpdates.pnl_LandpowerQuoteID, DateTime.UtcNow.ToString());
                        string description = string.Format("A Sales Aid Request of amount {0} for Quote {1}.{2}", string.Format("${0}", quotePreImageWithUpdates.pnl_SalesAidRequested.Value), quotePreImageWithUpdates.Name, "<br /><br />");
                        description += string.Format("The reason given for the request is:\"{0}\"{1}", quotePreImageWithUpdates.pnl_SalesAidRequestedReason, "<br /><br />");
                        description += string.Format("Please approve the Sales Aid on Quote:{0}, and then activate the quote.{1}", quotePreImageWithUpdates.Name, "<br /><br />");

                        Guid salesRep = quotePreImageWithUpdates.OwnerId.Id;
                        Guid productManager = Guid.Empty; //SalesAid approver
                        Guid ccRecep = Guid.Empty;
                        var stockClass = GetStockClass(quotePreImageWithUpdates.pnl_StockClass.Id);
                        if (stockClass != null)
                        {
                            productManager = stockClass.pnl_SalesAidApprover.Id;
                            if (stockClass.pnl_SalesAidApproverCC != null)
                                ccRecep = stockClass.pnl_SalesAidApproverCC.Id;
                        }
                        else
                            throw new InvalidPluginExecutionException("Stock Class can not be null! Please enter a valid Stock Class.");

                        if (productManager != Guid.Empty)
                            GenerateEmail(salesRep, productManager, ccRecep, subject, description, new EntityReference(Quote.EntityLogicalName, quotePreImageWithUpdates.QuoteId.Value));

                        //Update quote state to active
                        SetStateRequest state = new SetStateRequest();
                        state.State = new OptionSetValue((int)QuoteState.Draft);
                        state.Status = new OptionSetValue(125760007); //Waiting 4 SA
                        state.EntityMoniker = new EntityReference(Quote.EntityLogicalName, quotePreImageWithUpdates.QuoteId.Value);
                        _service.Execute(state);
                    }

                    if (quoteUpdate.pnl_SalesAidApprovedStatus != null && (quoteUpdate.pnl_SalesAidApprovedStatus.Value == 20 || quoteUpdate.pnl_SalesAidApprovedStatus.Value == 30))
                    {
                        //throw new InvalidPluginExecutionException("approve/disapprove hit");

                        string subject = string.Format("Request for {0} Sales Aid for QuoteID: {1} has been processed.", quotePreImageWithUpdates.CustomerId.Name, quotePreImageWithUpdates.pnl_LandpowerQuoteID);
                        string description = string.Empty;

                        if (quoteUpdate.pnl_SalesAidApprovedStatus.Value == 20)//Approved
                        {
                            description = string.Format("Your request for sales aid of amount {1} has been approved with the following reason: {0}{2} ", quotePreImageWithUpdates.pnl_SalesAidApprovedReason, string.Format("${0}", quotePreImageWithUpdates.pnl_SalesAidApproved.Value), "<br /><br />");
                            description += string.Format("Please provide a PDF version of the quote to {0}.", quotePreImageWithUpdates.CustomerId.Name, "<br /><br />");
                            description += string.Format("If you would like to make any changes to the quote, please go to the quote and revise it.{0}", "<br /><br />");
                        }

                        if (quoteUpdate.pnl_SalesAidApprovedStatus.Value == 30)//Declined
                        {
                            description = string.Format("Your request has been declined due to the following reason: {0}{1} ", quotePreImageWithUpdates.pnl_SalesAidApprovedReason, "<br /><br />");
                            description += string.Format("If you would like to make any changes to the quote, please go to the quote and revise it.{0}", "<br /><br />");
                        }

                        Guid salesRep = quotePreImageWithUpdates.OwnerId.Id;
                        Guid sender = context.UserId; //SalesAid approver

                        //var stockClass = GetStockClass(quotePreImageWithUpdates.pnl_HighestValueMachineName);
                        //if (stockClass != null)
                        //    sender = stockClass.pnl_SalesAidApprover.Id;

                        if (sender != null)
                            GenerateEmail(sender, salesRep, subject, description, new EntityReference(Quote.EntityLogicalName, quotePreImageWithUpdates.QuoteId.Value));

                        //Update quote state to active
                        SetStateRequest state = new SetStateRequest();
                        state.State = new OptionSetValue((int)QuoteState.Active);
                        state.Status = new OptionSetValue(3);   //Open
                        state.EntityMoniker = new EntityReference(Quote.EntityLogicalName, quotePreImageWithUpdates.QuoteId.Value);
                        _service.Execute(state);
                    }
                }
                catch (FaultException<OrganizationServiceFault> ex)
                {
                    if (ex.InnerException != null)
                        throw new InvalidPluginExecutionException(string.Format("An error occurred in Quote 2B Update plug-in.{1}{0}{1}{2}", ex.Message, Environment.NewLine, ex.InnerException.Message));
                    else
                        throw new InvalidPluginExecutionException("An error occurred in Quote 2B Update plug-in.", ex);
                }
            }
        }

        private pnl_StockClass GetStockClass(Guid stockClassId)
        {
            return (from sc in _ctx.pnl_StockClassSet
                    where sc.pnl_StockClassId == stockClassId
                    select sc).FirstOrDefault();
        }

        private void GenerateEmail(Guid from, Guid to, string subject, string description, EntityReference regarding)
        {
            GenerateEmail(from, to, null, subject, description, regarding);
        }

        private void GenerateEmail(Guid from, Guid to, Guid? cc, string subject, string description, EntityReference regarding)
        {
            ActivityParty fromParty = new ActivityParty
            {
                PartyId = new EntityReference(SystemUser.EntityLogicalName, from)
            };

            ActivityParty toParty = new ActivityParty
            {
                PartyId = new EntityReference(SystemUser.EntityLogicalName, to)
            };

            Email email = new Email
            {
                To = new ActivityParty[] { toParty },
                From = new ActivityParty[] { fromParty },
                Subject = subject,
                Description = description,
                DirectionCode = true,
                RegardingObjectId = regarding
            };

            if (cc.HasValue)
            {
                ActivityParty ccParty = new ActivityParty
                {
                    PartyId = new EntityReference(SystemUser.EntityLogicalName, cc.Value)
                };
                email.Cc = new ActivityParty[] { ccParty };
            }
            _service.Create(email);
        }
    }
}
