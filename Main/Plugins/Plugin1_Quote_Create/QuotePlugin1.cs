using System;
using System.Linq;
using System.ServiceModel;
using Microsoft.Xrm.Sdk;
using LandPower;
using Microsoft.Crm.Sdk.Messages;

namespace LandPowerPlugins
{
    public class QuotePlugin1 : IPlugin
    {
        private const char HYPHEN_CHAR = '-';

        public void Execute(IServiceProvider serviceProvider)
        {
            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            // Obtain the execution context from the service provider.
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            // The InputParameters collection contains all the data passed in the message request.
            if (!context.InputParameters.Contains("Target") || !(context.InputParameters["Target"] is Entity))
                return;

            // don't want to redo create logic when resyncing online
            if (context.IsOfflinePlayback)
                return;

            // Obtain the target entity from the input parameters.
            Entity sourceEntity = (Entity)context.InputParameters["Target"];

            // Verify that the target entity represents an account.
            // If not, this plug-in was not registered correctly.
            if (sourceEntity.LogicalName != "quote")
                return;

            try
            {
                Quote quote = sourceEntity.ToEntity<Quote>();

                Quote quoteUpdate = new Quote() { Id = quote.Id };

                if (!quote.RevisionNumber.HasValue)
                    throw new InvalidPluginExecutionException("Unexpected null RevisionNumber in quote");

                // Obtain the organization service reference.
                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = serviceFactory.CreateOrganizationService(null);  // null userid will run as administrator

                // Use reference to invoke pre-built types service 
                var myCrmService = new LandPower.crmServiceContext(service);

                // TODO: remove this test code which is used to show updates are working in the plugin
                //System.Text.StringBuilder debug = new System.Text.StringBuilder();
                //debug.AppendLine(string.Format("Updated at {0}.", DateTime.Now.TimeOfDay));

                bool isRevision = (quote.RevisionNumber != 0);

                if (isRevision)
                {
                    // revised quote so generate find previous revision of this quote
                    var prevQuote = (from c in myCrmService.QuoteSet
                                     where c.QuoteNumber.Equals(quote.QuoteNumber)
                                     where c.RevisionNumber < quote.RevisionNumber
                                     orderby c.RevisionNumber descending
                                     select c).FirstOrDefault();

                    if (prevQuote == null)
                        throw new InvalidPluginExecutionException("Failed to locate previous revision for quote: " + quote.QuoteNumber);

                    if (quote.pnl_LandpowerQuoteID == null || !quote.pnl_LandpowerQuoteID.ToString().Contains(HYPHEN_CHAR))
                        throw new InvalidPluginExecutionException("Previous revision for quote " + quote.QuoteNumber + " does not contain a valid Landpower Quote ID");

                    // copy last Activation date into the current quote entity
                    quoteUpdate.pnl_LastActivatedDate = prevQuote.pnl_LastActivatedDate;

                    // clear sales aid values on revised quote
                    quoteUpdate.pnl_SalesAidApproved = null;
                    quoteUpdate.pnl_SalesAidRequested = null;

                    quoteUpdate.pnl_SalesAidApprovedReason = null;
                    quoteUpdate.pnl_SalesAidRequestedReason = null;
                    quoteUpdate.pnl_SalesAidApprovedStatus = null;
                    quoteUpdate.pnl_SalesAidUserNotification1CreatedDateTime = null;
                    quoteUpdate.pnl_SalesAidRequest1CreatedDateTime = null;

                    quoteUpdate.pnl_SalesAidRequest = null;
                    quoteUpdate.pnl_SavedFromExperlogix = null;
                    quoteUpdate.pnl_DateLost = null;

                    // update the Landpower Quote ID with the revision number and assign the new value to the current quote entity
                    int rightHyphenPos = quote.pnl_LandpowerQuoteID.ToString().LastIndexOf(HYPHEN_CHAR);
                    string baseLandpowerQuoteIdPart = quote.pnl_LandpowerQuoteID.ToString().Substring(0, rightHyphenPos);
                    quoteUpdate.pnl_LandpowerQuoteID = string.Format("{0}{2}{1:D3}", baseLandpowerQuoteIdPart, quote.RevisionNumber, HYPHEN_CHAR);

                    quoteUpdate.pnl_QuoteRevisedFrom = new EntityReference("quote", prevQuote.Id);
                    tracingService.Trace("CopyLocallySourcedEntitiesFromPreviousVersion Start");
                    CopyLocallySourcedEntitiesFromPreviousVersion(quote, service, myCrmService, prevQuote, tracingService);
                    tracingService.Trace("CopyLocallySourcedEntitiesFromPreviousVersion End");
                    tracingService.Trace("CopyQuoteMachineFromPreviousVersion Start");
                    CopyQuoteMachineEntitiesFromPreviousVersion(quote, service, myCrmService, prevQuote, tracingService);
                    tracingService.Trace("CopyQuoteMachineFromPreviousVersion Start");
                    quoteUpdate.pnl_FirstRevision = true;

                    // CANNOT UPDATE A REVISED QUOTE SO LOOK AT MOVING THE FOLLOWING LOGIC TO A NEW PLUGIN
                    //// If the quote is being revised, and the included in pipeline field [pnl_includedinpipeline] is set to yes, 
                    //// set it to no on the old quote ( i.e. the one that will be closed) and the new quote to yes. 
                    //if ((quote.pnl_IncludedInPipeline.HasValue && quote.pnl_IncludedInPipeline.Value) && (!prevQuote.pnl_IncludedInPipeline.HasValue || prevQuote.pnl_IncludedInPipeline.Value))
                    //{
                    //    prevQuote.pnl_IncludedInPipeline = false;
                    //    myCrmService.UpdateObject(prevQuote);
                    //    myCrmService.SaveChanges();
                    //}
                }
                else
                {
                    // Initially set Price List to default for user
                    var systemUser = (from u in myCrmService.SystemUserSet
                                      where u.SystemUserId.Value.Equals(context.InitiatingUserId)
                                      select u).FirstOrDefault();
                    if (systemUser == null)
                    {
                        throw new InvalidPluginExecutionException("Failed to locate user record.");
                    }

                    if (systemUser.pnl_DefaultPriceList == null)
                    {
                        throw new InvalidPluginExecutionException(string.Format("A Price List has not been defined for user: {0}", systemUser.FullName));
                    }
                    quoteUpdate.PriceLevelId = new EntityReference("pricelevel", systemUser.pnl_DefaultPriceList.Id);

                    if (systemUser.TransactionCurrencyId == null)
                    {
                        throw new InvalidPluginExecutionException(string.Format("A currency has not been defined for user: {0}", systemUser.FullName));
                    }
                    quoteUpdate.TransactionCurrencyId = new EntityReference("transactioncurrency", systemUser.TransactionCurrencyId.Id);



                    //// quote.CustomerId.Name has not been populated yet (though ID has) so have to retrieve the Customer name to assign default Quote description
                    var customer = (from c in myCrmService.AccountSet where c.Id == quote.CustomerId.Id select c).FirstOrDefault();
                    //var stockClass = (quote.pnl_StockClass == null) ? null : (from s in myCrmService.pnl_StockClassSet where s.Id == quote.pnl_StockClass.Id select s).FirstOrDefault();
                    //quoteUpdate.Name = string.Format("{0}{1}",
                    //    (customer == null || customer.Name == null) ? "<customer>" : customer.Name,
                    //    (stockClass == null || stockClass.pnl_name == null) ? "" : " - " + stockClass.pnl_name);

                    AssignNewLandpowerQuoteID(context, quote, ref quoteUpdate, service, myCrmService);

                    //John - 14.07.2014 added if statement to set the name (description) of the quote
                    if (string.IsNullOrWhiteSpace(quoteUpdate.Name) && customer != null)
                    {
                        // Set the name [name] field (note on the form it is called description � not to be confused with the inbuilt description 
                        // field), to equal the customers (customer) name, a dash and then name of the quote number [QuoteNumber]
                        quoteUpdate.Name = string.Format("{0}{1}", customer.Name == null ? "<customer>" : customer.Name,
                                                        quoteUpdate.pnl_LandpowerQuoteID == null ? "" : " - " + quoteUpdate.pnl_LandpowerQuoteID);
                    }

                }

                AssignLabelFieldValues(quoteUpdate);

                service.Update(quoteUpdate);
            }

            catch (FaultException<OrganizationServiceFault> ex)
            {
                if (ex.InnerException != null)
                    throw new InvalidPluginExecutionException(string.Format("An error occurred in the Quote Create plug-in.{1}{0}{1}{2}", ex.Message, Environment.NewLine, ex.InnerException.Message));
                else
                    throw new InvalidPluginExecutionException(string.Format("An error occurred in the Quote Create plug-in:-{0}{1}", Environment.NewLine, ex.Message));
            }

            catch (Exception ex)
            {
                tracingService.Trace("QuotePlugin: {0}", ex.ToString());
                throw;
            }

        }

        private static void CopyLocallySourcedEntitiesFromPreviousVersion(Quote quote, IOrganizationService service, crmServiceContext myCrmService, Quote prevQuote, ITracingService tracingService)
        {
            var allLsItems = (from lsi in myCrmService.pnl_locallysourceditemSet where lsi.pnl_Quote.Id == prevQuote.Id select lsi);
            var allLsMachines = (from lsm in myCrmService.pnl_locallysourcedmachineSet where lsm.pnl_QuoteId.Id == prevQuote.Id select lsm).ToList();

            // copy the Locally Sourced Machines and Locally Sourced Items to the new quote
            var orphanMachineItems = allLsItems.Where(x => x.pnl_LocallySourcedMachine.Id == null);
            ReplicateLocallySourcedItems(service, quote.Id, null, orphanMachineItems, tracingService);           
                        
            foreach (pnl_locallysourcedmachine lsMachine in allLsMachines)
            {
                // copy this machine
                pnl_locallysourcedmachine newMachine = new pnl_locallysourcedmachine();
                newMachine.pnl_QuoteId = new EntityReference(Quote.EntityLogicalName, quote.Id);
                //Set to NULL because QuoteDetailrecords wont be created to link, Plugin 2 to set this link
                newMachine.pnl_QuoteProductId = "NULL";
                newMachine.pnl_name = lsMachine.pnl_name;
                newMachine.pnl_Config = lsMachine.pnl_Config;
                newMachine.OwnerId = new EntityReference(SystemUser.EntityLogicalName, lsMachine.OwnerId.Id);
                tracingService.Trace("service.Create(newMachine) Start");
                Guid newMachineId = service.Create(newMachine);
                tracingService.Trace("service.Create(newMachine) End");
                // find the items related to this machine
                var thisMachinesItems = allLsItems.Where(x => x.pnl_LocallySourcedMachine.Id == lsMachine.Id);

                // clone each locally sourced item but now related to the new machine
                ReplicateLocallySourcedItems(service, quote.Id, newMachineId, thisMachinesItems, tracingService);
            }
            
        }

        private static void ReplicateLocallySourcedItems(IOrganizationService service, Guid newQuoteId, Guid? newMachineId, IQueryable<pnl_locallysourceditem> lsItems, ITracingService tracingService)
        {
            if ((lsItems == null))
                return;

            foreach (pnl_locallysourceditem item in lsItems)
            {
                pnl_locallysourceditem newItem = new pnl_locallysourceditem();
                newItem.pnl_name = item.pnl_name;
                newItem.pnl_ConfigNumber = item.pnl_ConfigNumber;
                newItem.pnl_DiscountPercentage = item.pnl_DiscountPercentage;
                newItem.pnl_ListPrice = item.pnl_ListPrice;
                newItem.pnl_LocallySourcedDNP = item.pnl_LocallySourcedDNP;
                newItem.pnl_Quote = new EntityReference(Quote.EntityLogicalName, newQuoteId);
                newItem.OwnerId = new EntityReference(SystemUser.EntityLogicalName, item.OwnerId.Id);
                newItem.pnl_NoChargeItem = item.pnl_NoChargeItem;
                //throw new InvalidPluginExecutionException("Machine - " + newMachineId.Value.ToString() + " is owned by different Customer");
                if (newMachineId.HasValue)
                {                    
                    newItem.pnl_LocallySourcedMachine = new EntityReference(pnl_locallysourcedmachine.EntityLogicalName, newMachineId.Value);
                }
                
                tracingService.Trace("service.Create(newItem) Start");
                Guid itemid = service.Create(newItem);
                tracingService.Trace("service.Create(newItem) End");
                if (newMachineId.HasValue)
                {
                    QuoteDetail quotedetail = new QuoteDetail()
                    {
                        QuoteId = new EntityReference(Quote.EntityLogicalName, newQuoteId),
                        pnl_LocallySourcedItemId = new EntityReference(pnl_locallysourceditem.EntityLogicalName, itemid),
                        //pnl_islocallysourced = new OptionSetValue(1),
                        Quantity = 1,
                        IsProductOverridden = true,
                        PricePerUnit = new Money(0),
                        ProductDescription = item.pnl_name
                    };
                    tracingService.Trace("service.Create(quotedetail) Start");
                    Guid quotedetailid = service.Create(quotedetail);
                    tracingService.Trace("service.Create(quotedetail) End");
                    newItem.Id = itemid;                    
                    newItem.pnl_QuoteProductId = quotedetailid.ToString();
                    tracingService.Trace("service.Update(newItem) Start");                    
                    service.Update(newItem);
                    tracingService.Trace("service.Update(newItem) End");
                }

            }
        }

        // Ranjith's Code - Quote Machine 

        private static void CopyQuoteMachineEntitiesFromPreviousVersion(Quote quote, IOrganizationService service, crmServiceContext myCrmService, Quote prevQuote, ITracingService tracingService)
        {
            var allQuMa = (from qm in myCrmService.pnl_quotemachineSet where qm.pnl_Quoteid.Id == prevQuote.Id select qm);

            foreach (pnl_quotemachine qMachine in allQuMa)
            {
                pnl_quotemachine newQMachine = new pnl_quotemachine();
                newQMachine.pnl_Quoteid = new EntityReference(Quote.EntityLogicalName, quote.Id);
                if (qMachine.pnl_Configurationid != null)
                {
                    newQMachine.pnl_Configurationid = new EntityReference(pnl_quotemachine.EntityLogicalName, qMachine.pnl_Configurationid.Id);
                }
                if (qMachine.pnl_Orderid != null)
                {
                    newQMachine.pnl_Orderid = new EntityReference(SalesOrder.EntityLogicalName, qMachine.pnl_Orderid.Id);                    
                }
                if (qMachine.pnl_Machineid != null)
                {
                    newQMachine.pnl_Machineid = new EntityReference(pnl_machine.EntityLogicalName, qMachine.pnl_Machineid.Id);                    
                }
                if (qMachine.pnl_Scenario1Value != null)
                {
                    newQMachine.pnl_Scenario1Value = new Money(qMachine.pnl_Scenario1Value.Value);                    
                }
                if (qMachine.pnl_Scenario2Value != null)
                {
                    newQMachine.pnl_Scenario2Value = new Money(qMachine.pnl_Scenario2Value.Value);                    
                }
                if (qMachine.pnl_Scenario3Value != null)
                {
                    newQMachine.pnl_Scenario3Value = new Money(qMachine.pnl_Scenario3Value.Value);                    
                }
                if (qMachine.pnl_RepairCost != null)
                {
                    newQMachine.pnl_RepairCost = new Money(qMachine.pnl_RepairCost.Value);                    
                }
                if (qMachine.pnl_MarketPrice != null)
                {
                    newQMachine.pnl_MarketPrice = new Money(qMachine.pnl_MarketPrice.Value);                    
                }
                if (qMachine.pnl_SellingMargin != null)
                {
                    newQMachine.pnl_SellingMargin = new Money(qMachine.pnl_SellingMargin.Value);                    
                }
                if (qMachine.pnl_SellingMarginpercentage != null)
                {
                    newQMachine.pnl_SellingMarginpercentage = qMachine.pnl_SellingMarginpercentage.Value;
                }
                if (qMachine.OwnerId != null)
                {
                    newQMachine.OwnerId = new EntityReference(SystemUser.EntityLogicalName, qMachine.OwnerId.Id);
                }               
                tracingService.Trace("service.Create(newMachine) Start");
                service.Create(newQMachine);
                tracingService.Trace("service.Create(newMachine) End");                
            }
        }
        
        private static void AssignNewLandpowerQuoteID(IPluginExecutionContext context, Quote quote, ref Quote quoteUpdate, IOrganizationService service, crmServiceContext myCrmService)
        {
            // brand new quote so generate a fresh Landpower Quote ID
            // first find the executing user's Business Unit
            var businessUnit = (from u in myCrmService.SystemUserSet
                                join b in myCrmService.BusinessUnitSet
                                on u.BusinessUnitId.Id equals b.BusinessUnitId.Value
                                where u.SystemUserId.Value.Equals(context.InitiatingUserId)
                                select b).FirstOrDefault();

            if (businessUnit == null)
                throw new InvalidPluginExecutionException("Failed to locate business unit for this user");

            if (businessUnit.pnl_BUID == null)
                throw new InvalidPluginExecutionException("Unassigned Landpower ID in this user's business unit");

            // generate a new Landpower quote ID and assign it to the attribute for update                     
            quoteUpdate.pnl_LandpowerQuoteID = string.Format("{0}{3}{1}{3}{2:D3}",
                businessUnit.pnl_BUID.Trim(),
                GetNextUserQuoteNumberPart(context.InitiatingUserId, myCrmService, service),
                quote.RevisionNumber,
                HYPHEN_CHAR);
        }

        private void AssignLabelFieldValues(Quote quoteUpdate)
        {
            quoteUpdate.pnl_DealerNetCostLabel = "Dealer Net Cost";
            quoteUpdate.pnl_ScenarioOneLabel = "Scenario One";
            quoteUpdate.pnl_ScenarioTwoLabel = "Scenario Two";
            quoteUpdate.pnl_ScenarioThreeLabel = "Scenario Three";
            quoteUpdate.pnl_OvertradeAmountLabel = "Overtrade Amount";
            quoteUpdate.pnl_GrossProfitPercentageLabel = "Gross Profit %";
            quoteUpdate.pnl_GrossProfitDollarLabel = "Gross Profit $";
            quoteUpdate.pnl_NetSellingPriceGPLabel = "Net Selling Price";
            quoteUpdate.pnl_NetSellingPriceChangeoverLabel = "Net Selling Price";
            quoteUpdate.pnl_LessDiscountsChangeoverLabel = "Less Discounts";
            quoteUpdate.pnl_ListPriceNewMachinesChangeoverLabel = "List Price - New Machines";
            quoteUpdate.pnl_TotalsTradeInLabel = "Totals (Trade-In)";
            quoteUpdate.pnl_ChangeoverPriceLabel = "Changeover Price";
            quoteUpdate.pnl_TotalsGrossProfitPercentageLabel = "Gross Profit %";
            quoteUpdate.pnl_AutoCalcS2andS3ChangeOverLabel = "Auto Calculate";
        }

        // Calculate the middle section of a Landpower Quote ID from the code on the USer record, 
        // and the "next quote id for this user" which is stored in the new_quoteidSet table.
        private static string GetNextUserQuoteNumberPart(Guid userId, crmServiceContext myCrmService, IOrganizationService service)
        {
            // find the current user record
            var user = (from u in myCrmService.SystemUserSet
                        where u.SystemUserId.Equals(userId)
                        select u).FirstOrDefault();

            if (user == null)
                throw new InvalidPluginExecutionException("Failed to locate current user record: " + userId.ToString());
            if (user.pnl_QuoteIDCode == null)
                throw new InvalidPluginExecutionException(string.Format("The QuoteID Code is unspecified for: {0}{1}{1}This is required to create a new Landpower QuoteId for this quote.", user.FullName, Environment.NewLine));

            // find the next quote id record for this user
            new_quoteid userQuoteId = (from q in myCrmService.new_quoteidSet
                                       where q.OwnerId.Equals(userId)
                                       select q).FirstOrDefault();

            if (userQuoteId == null)
            {
                // create the next quote id record for this user if one does not exist
                userQuoteId = new new_quoteid()
                {
                    OwnerId = new EntityReference("systemuser", userId),
                    new_name = string.Format("Last Quote ID for {0}", user.FullName),
                    new_QuoteCurrentID = 0
                };
                service.Create(userQuoteId);
            }
            else
            {
                // increment and update the ID on this next quote id record for this user
                userQuoteId.new_QuoteCurrentID = userQuoteId.new_QuoteCurrentID + 1;
                myCrmService.UpdateObject(userQuoteId);
                service.Update(userQuoteId);
            }

            // result is the concatenation of the user code with the new "next quote id for this user" = eg "12987" or "PJR005"
            return string.Format("{0}{1:D4}", user.pnl_QuoteIDCode.Trim(), userQuoteId.new_QuoteCurrentID.Value);

        }
    }
}
