﻿using LandPower;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

namespace Machine_Update
{
    public class MachineUpdate : IPlugin
    {
        IOrganizationService _service;

        public void Execute(IServiceProvider serviceProvider)
        {

            ITracingService tracingService = 
                (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));

            _service = serviceFactory.CreateOrganizationService(context.UserId);
            Entity postEntity = (Entity)context.PostEntityImages["entity"];
            if (context.Depth > 3)
                return;

            LandPower.crmServiceContext _ctx = new crmServiceContext(_service);

            if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
            {
                Entity machineEntity = (Entity)context.InputParameters["Target"];
                if (machineEntity.Contains("pnl_currentcustomerid") && machineEntity.Attributes["pnl_currentcustomerid"] != null)
                {
                    //create Customer-Machine if not exist and close the existing customer-machine records
                    EntityReference currentCustomer = (EntityReference)postEntity.Attributes["pnl_currentcustomerid"];

                    var currCustmachine = (from cm in _ctx.pnl_customermachineSet
                                   where cm.pnl_Machineid.Id.Equals(machineEntity.Id) 
                                   &&    cm.pnl_CustomerId.Id.Equals(currentCustomer.Id)
                                   &&    cm.pnl_EndDate.Equals(null)
                                   select cm).ToList();
                    
                    //if (currCustmachine.Contains<pnl_customermachine>(asd => pnl_customermachine))
                    if(currCustmachine.Count == 0)
                    {                        
                        Entity newCmachine = new Entity("pnl_customermachine");
                        newCmachine.Attributes["pnl_customerid"] = (EntityReference)currentCustomer;
                        newCmachine.Attributes["pnl_machineid"] = (EntityReference)postEntity.ToEntityReference();
                        newCmachine.Attributes["pnl_startdate"] = (DateTime)DateTime.Now;
                        _service.Create(newCmachine);
                    }
                    var cmachine = from cm in _ctx.pnl_customermachineSet
                                   where cm.pnl_Machineid.Id.Equals(machineEntity.Id)                                    
                                   select cm;                    
                        foreach (pnl_customermachine cmac in cmachine)
                        {
                            if (cmac.pnl_CustomerId.Id != currentCustomer.Id && cmac.pnl_EndDate == null)
                            {
                                var updatcmac = new pnl_customermachine
                                {
                                    Id = cmac.Id,
                                    pnl_EndDate = DateTime.Now,
                                };
                                _service.Update(updatcmac);
                            }
                        }
                    }

                //only on update - update the quote-machines and quote for recalc
                if (context.MessageName.ToUpper() == "UPDATE")
                {
                    if (machineEntity.Contains("pnl_marketvalue") && machineEntity.Attributes["pnl_marketvalue"] != null ||
                        machineEntity.Contains("pnl_sellingmarginpercentage") && machineEntity.Attributes["pnl_sellingmarginpercentage"] != null ||
                        machineEntity.Contains("pnl_repaircosts") && machineEntity.Attributes["pnl_repaircosts"] != null ||
                        machineEntity.Contains("pnl_approvedbookvalue") && machineEntity.Attributes["pnl_approvedbookvalue"] != null ||
                        machineEntity.Contains("pnl_recommendedbookvalue") && machineEntity.Attributes["pnl_recommendedbookvalue"] != null ||
                        machineEntity.Contains("pnl_appraisalstatus") && machineEntity.Attributes["pnl_appraisalstatus"] != null)
                    {
                       // pnl_machine postImage = (pnl_machine)postEntity;
                        var qmachines = from qm in _ctx.pnl_quotemachineSet
                                   where qm.pnl_Machineid.Id.Equals(machineEntity.Id)
                                   select qm;
                        foreach (pnl_quotemachine quotemachines in qmachines)
                        {
                            pnl_quotemachine UpdateQuoteMachine = new pnl_quotemachine();
                            UpdateQuoteMachine.Id = quotemachines.Id;
                            if (machineEntity.Contains("pnl_marketvalue") && machineEntity.Attributes["pnl_marketvalue"] != null)
                            {
                                UpdateQuoteMachine.pnl_MarketPrice = new Money(((Money)(machineEntity.Attributes["pnl_marketvalue"])).Value);
                                if (quotemachines.pnl_Scenario3Value == null || 
                                    (quotemachines.pnl_Scenario3Value != null && quotemachines.pnl_Scenario3Value.Value == 0))
                                {
                                    UpdateQuoteMachine.pnl_Scenario3Value = new Money(((Money)(machineEntity.Attributes["pnl_marketvalue"])).Value);
                                }
                            }
                            if (machineEntity.Contains("pnl_repaircosts") && machineEntity.Attributes["pnl_repaircosts"] != null)
                            {
                                UpdateQuoteMachine.pnl_RepairCost = new Money(((Money)(machineEntity.Attributes["pnl_repaircosts"])).Value);
                            }
                            if (machineEntity.Contains("pnl_sellingmarginpercentage") && machineEntity.Attributes["pnl_sellingmarginpercentage"] != null)
                            {
                                UpdateQuoteMachine.pnl_SellingMarginpercentage =  Convert.ToDecimal(machineEntity.Attributes["pnl_sellingmarginpercentage"].ToString());
                            }
                            if (machineEntity.Contains("pnl_recommendedbookvalue") && machineEntity.Attributes["pnl_recommendedbookvalue"] != null)
                            {
                                UpdateQuoteMachine.pnl_Scenario1Value = new Money(((Money)(machineEntity.Attributes["pnl_recommendedbookvalue"])).Value);
                                if (quotemachines.pnl_Scenario2Value == null || 
                                    (quotemachines.pnl_Scenario2Value != null && quotemachines.pnl_Scenario2Value.Value == 0))
                                {
                                    UpdateQuoteMachine.pnl_Scenario2Value = new Money(((Money)(machineEntity.Attributes["pnl_recommendedbookvalue"])).Value);
                                }
                            }
                            if (machineEntity.Contains("pnl_approvedbookvalue") && machineEntity.Attributes["pnl_approvedbookvalue"] != null)
                            {

                                if (((Money)(machineEntity.Attributes["pnl_approvedbookvalue"])).Value != 0)
                                {

                                    UpdateQuoteMachine.pnl_Scenario1Value = new Money(((Money)(machineEntity.Attributes["pnl_approvedbookvalue"])).Value);
                                    if (quotemachines.pnl_Scenario2Value == null ||
                                        (quotemachines.pnl_Scenario2Value != null && quotemachines.pnl_Scenario2Value.Value == 0))
                                    {
                                        UpdateQuoteMachine.pnl_Scenario2Value = new Money(((Money)(machineEntity.Attributes["pnl_approvedbookvalue"])).Value);
                                    }
                                }
                            }
                            if (machineEntity.Contains("pnl_sellingmargin") && machineEntity.Attributes["pnl_sellingmargin"] != null)
                            {
                                UpdateQuoteMachine.pnl_SellingMargin = new Money(((Money)(machineEntity.Attributes["pnl_sellingmargin"])).Value);
                            }
                            if (machineEntity.Contains("pnl_appraisalstatus") && machineEntity.Attributes["pnl_appraisalstatus"] != null)
                            {
                                UpdateQuoteMachine.pnl_AppraisalStatus = new OptionSetValue(((OptionSetValue)(machineEntity.Attributes["pnl_sellingmargin"])).Value);
                            }
                            _service.Update(UpdateQuoteMachine);

                            var quotes = from q in _ctx.QuoteSet
                                         where q.Id.Equals(quotemachines.pnl_Quoteid.Id)
                                                
                                         select q;
                            //foreach (Quote qq in quotes)
                            //{                                
                            //        Quote qqq = new Quote();
                            //        qqq.Id = qq.Id;
                            //        qqq.pnl_LastSavedFromLocallySourcedItemOn = DateTime.Now;
                            //        _service.Update(qqq);
                                
                            //}

                            //below functionality will be implemented later
                            foreach (Quote qq in quotes)
                            {
                                if (qq.StateCode.Equals(QuoteState.Active))
                                {
                                    throw new InvalidPluginExecutionException("Appraisal is associated with an Active Quote, please revise the quote to make changes");
                                }

                            }
                            foreach (Quote qq in quotes)
                            {
                                if (qq.StateCode.Equals(QuoteState.Draft))
                                {
                                    Quote qqq = new Quote();
                                    qqq.Id = qq.Id;
                                    qqq.pnl_LastSavedFromLocallySourcedItemOn = DateTime.Now;
                                    _service.Update(qqq);
                                }
                            }
                        }
                    }
                }

                //create new quote machine on creating a machine from quote
                if (context.MessageName.ToUpper() == "CREATE" 
                    && machineEntity.Contains("pnl_createdfromquote") && machineEntity.Attributes["pnl_createdfromquote"] != null)
                {
                    bool createdfromquote = (bool)machineEntity.Attributes["pnl_createdfromquote"];
                    if (createdfromquote.Equals(true))
                    {
                        pnl_quotemachine newQuoteMachine = new pnl_quotemachine();
                        if (machineEntity.Contains("pnl_marketvalue") && machineEntity.Attributes["pnl_marketvalue"] != null)
                        {
                            newQuoteMachine.pnl_MarketPrice = new Money(((Money)(machineEntity.Attributes["pnl_marketvalue"])).Value);
                            newQuoteMachine.pnl_Scenario3Value = new Money(((Money)(machineEntity.Attributes["pnl_marketvalue"])).Value);
                        }
                        if (machineEntity.Contains("pnl_repaircosts") && machineEntity.Attributes["pnl_repaircosts"] != null)
                        {
                            newQuoteMachine.pnl_RepairCost = new Money(((Money)(machineEntity.Attributes["pnl_repaircosts"])).Value);
                        }
                        if (machineEntity.Contains("pnl_sellingmarginpercentage") && machineEntity.Attributes["pnl_sellingmarginpercentage"] != null)
                        {
                            newQuoteMachine.pnl_SellingMarginpercentage = Convert.ToDecimal(machineEntity.Attributes["pnl_sellingmarginpercentage"].ToString());
                        }
                        if (machineEntity.Contains("pnl_recommendedbookvalue") && machineEntity.Attributes["pnl_recommendedbookvalue"] != null)
                        {
                            newQuoteMachine.pnl_Scenario1Value = new Money(((Money)(machineEntity.Attributes["pnl_recommendedbookvalue"])).Value);
                            newQuoteMachine.pnl_Scenario2Value = new Money(((Money)(machineEntity.Attributes["pnl_recommendedbookvalue"])).Value);
                        }
                        if (machineEntity.Contains("pnl_approvedbookvalue") && machineEntity.Attributes["pnl_approvedbookvalue"] != null)
                        {
                            newQuoteMachine.pnl_Scenario1Value = new Money(((Money)(machineEntity.Attributes["pnl_approvedbookvalue"])).Value);
                            newQuoteMachine.pnl_Scenario2Value = new Money(((Money)(machineEntity.Attributes["pnl_approvedbookvalue"])).Value);
                        }
                        if (machineEntity.Contains("pnl_sellingmargin") && machineEntity.Attributes["pnl_sellingmargin"] != null)
                        {
                            newQuoteMachine.pnl_SellingMargin = new Money(((Money)(machineEntity.Attributes["pnl_sellingmargin"])).Value);
                        }
                        if (machineEntity.Contains("pnl_quoteidmachinecreatedfromquoteid") && machineEntity.Attributes["pnl_quoteidmachinecreatedfromquoteid"] != null)
                        {
                            newQuoteMachine.pnl_Quoteid = (EntityReference)(machineEntity.Attributes["pnl_quoteidmachinecreatedfromquoteid"]);
                        }
                        if (machineEntity.Contains("pnl_lsm_machinecreatedfromquoteid") && machineEntity.Attributes["pnl_lsm_machinecreatedfromquoteid"] != null)
                        {
                            newQuoteMachine.pnl_Configurationid = (EntityReference)(machineEntity.Attributes["pnl_lsm_machinecreatedfromquoteid"]);
                        }
                        newQuoteMachine.pnl_Machineid = postEntity.ToEntityReference();
                        _service.Create(newQuoteMachine);
                    }
                    if (machineEntity.Contains("pnl_quoteidmachinecreatedfromquoteid") && machineEntity.Attributes["pnl_quoteidmachinecreatedfromquoteid"] != null)
                    {
                        EntityReference quoteUpdate = (EntityReference)machineEntity.Attributes["pnl_quoteidmachinecreatedfromquoteid"];
                        Entity UpdateQuote = new Entity("quote");
                        UpdateQuote.Id = quoteUpdate.Id;
                        UpdateQuote.Attributes["pnl_newmachinesavedonquote"] = (DateTime)DateTime.Now;
                        _service.Update(UpdateQuote);
                    }
                }


                //naming convection for machine "year make model"
                pnl_machine machine = new pnl_machine();
                machine.Id = machineEntity.Id;
                string nameValue = "";
                Entity postMachine = (Entity)context.PostEntityImages["entity"];
                if ((context.MessageName.ToUpper() == "CREATE" && (!postMachine.Contains("pnl_name") || (postMachine.Contains("pnl_name") && postMachine.Attributes["pnl_name"] == null)))
                    || context.MessageName.ToUpper() == "UPDATE")
                    {

                        if (postMachine.Contains("pnl_year") && postMachine.Attributes["pnl_year"] != null)
                        {
                            nameValue = postMachine.Attributes["pnl_year"].ToString();
                        }
                        else
                        {
                            nameValue = "<Not Specified>";
                        }
                        if (postMachine.Contains("pnl_machinetypeid") && postMachine.Attributes["pnl_machinetypeid"] != null)
                        {
                            EntityReference machinetypeValue = (EntityReference)postMachine.Attributes["pnl_machinetypeid"];
                            if (machinetypeValue.Name.ToString() == "General")
                            {
                                if (postMachine.Contains("pnl_makestring") && postMachine.Attributes["pnl_makestring"] != null)
                                {
                                    nameValue = nameValue + " " + postMachine.Attributes["pnl_makestring"].ToString();
                                }
                                else
                                {
                                    nameValue = nameValue + " <Not Specified>";
                                }
                                if (postMachine.Contains("pnl_modelstring") && postMachine.Attributes["pnl_modelstring"] != null)
                                {
                                    nameValue = nameValue + " " + postMachine.Attributes["pnl_modelstring"].ToString();
                                }
                                else
                                {
                                    nameValue = nameValue + " <Not Specified>";
                                }
                            }
                            else
                            {
                                if (postMachine.Contains("pnl_makeid") && postMachine.Attributes["pnl_makeid"] != null)
                                {
                                    EntityReference makeValue = (EntityReference)postMachine.Attributes["pnl_makeid"];
                                    nameValue = nameValue + " " + makeValue.Name.ToString();
                                }
                                else
                                {
                                    nameValue = nameValue + " <Not Specified>";
                                }
                                if (postMachine.Contains("pnl_modelid") && postMachine.Attributes["pnl_modelid"] != null)
                                {
                                    EntityReference modelValue = (EntityReference)postMachine.Attributes["pnl_modelid"];
                                    if (modelValue.Name.ToString() == "Other")
                                    {
                                        if (postMachine.Contains("pnl_modelstring") && postMachine.Attributes["pnl_modelstring"] != null)
                                        {
                                            nameValue = nameValue + " " + postMachine.Attributes["pnl_modelstring"].ToString();
                                        }
                                        else
                                        {
                                            nameValue = nameValue + " <Not Specified>";
                                        }
                                    }
                                    else
                                    {
                                        nameValue = nameValue + " " + modelValue.Name.ToString();
                                    }
                                }
                                else
                                {
                                    nameValue = nameValue + " <Not Specified>";
                                }
                            }
                        }

                        machine.pnl_name = nameValue;
                        _service.Update(machine);
                    }
                
               }
           }
      }
}

