using CrmEarlyBound;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;


namespace LandPower
{
    public class Appraisal_PostUpdate : IPlugin
    {
        #region Private Variables
        class CalcValues
        {
            public List<Decimal> SourceValues = new List<Decimal>();
            public Decimal CALCULATED_AVERAGE { get; set; }
            public Decimal REC_LIST_PRICE { get; set; }
            public Decimal REPAIR_COSTS { get; set; }
            public Decimal FREIGHT_COSTS { get; set; }
            public Decimal PD_EXPENSES { get; set; }
            public Decimal OTHER_COSTS { get; set; }
            public Decimal TOTAL_COSTS { get; set; }
            public Decimal MARGIN_PERCENT { get; set; }
            public Decimal RETAINED_MARGIN { get; set; }
            public Decimal CALC_TRADE_PRICE { get; set; }
            public Decimal REC_TRADE_PRICE { get; set; }
            public Decimal APPROVED_TRADE_PRICE { get; set; }

            internal void SetDecimalPropertyFromStringValue(string calcCode, string stringValue)
            {
                Decimal decimalValue;
                if (!Decimal.TryParse(stringValue, out decimalValue))
                {
                    decimalValue = 0;
                }

                SetPropertyFromDecimalValue(calcCode, decimalValue);
            }

            internal void SetPropertyFromDecimalValue(string calcCode, Decimal decimalValue)
            {
                var calcValueProperty = typeof(CalcValues).GetProperty(calcCode);

                if (null != calcValueProperty)
                {
                    calcValueProperty.SetValue(this, decimalValue, null);
                }

                // Load Source value - Assumes source item number no larger than 9.
                else if (calcCode.StartsWith("SOURCE_"))
                {
                    // get Source number (1-4 at the moment)
                    int sourceNumber;
                    if (!int.TryParse(calcCode.Substring(7, 1), out sourceNumber))
                    {
                        string message = string.Format("Unable to determine Source number from Calculation Code '{0}' for Appraisal Item.", calcCode);
                        throw new InvalidPluginExecutionException(message);
                    }

                    // check source values is long enough and update or add that number
                    while (SourceValues.Count < sourceNumber)
                        SourceValues.Add(0);

                    // update the value - SourceValues are index 0-3, Source numbers are 1-4.
                    SourceValues[sourceNumber - 1] = decimalValue;
                }
            }

            internal void Recalculate()
            {
                int nonZeroSourceValueCount = SourceValues.Where(x => x != 0).Count();
                if (nonZeroSourceValueCount == 0)
                    CALCULATED_AVERAGE = 0;
                else
                {
                    Decimal totalValues = SourceValues.Sum();
                    CALCULATED_AVERAGE = Decimal.Round(totalValues / (Decimal)nonZeroSourceValueCount);
                }

                TOTAL_COSTS = Decimal.Round(REPAIR_COSTS + FREIGHT_COSTS + PD_EXPENSES + OTHER_COSTS);
                RETAINED_MARGIN = Decimal.Round(REC_LIST_PRICE * MARGIN_PERCENT / 100);
                CALC_TRADE_PRICE = Decimal.Round(REC_LIST_PRICE - TOTAL_COSTS - RETAINED_MARGIN);
            }
        }

        const string pnl_NameFieldName = "pnl_name";
        const string pnl_ValueFieldName = "pnl_value";
        const string pnl_AppraisalItemPicklistItemIdFieldName = "pnl_appraisalitempicklistitemid";
        const string pnl_AppraisalRecalcedOnFieldName = "pnl_appraisalrecalcedon";
        const string pnl_IsReadonlyFieldName = "pnl_isreadonly";
        const string pnl_syncEndedOnFieldName = "pnl_syncendedon";
        const string pnl_itemUpdatedOnFieldName = "pnl_itemupdatedon";
        const string pnl_DisplayNameFieldName = "pnl_displayname";
        const string pnl_PrimaryApproverFieldName = "pnl_primaryapprover";
        const string pnl_ValuationApprovedByFieldName = "pnl_valuationapprovedby";
        const string pnl_ValuationApprovedOnFieldName = "pnl_valuationapprovedon";
        const string pnl_ApprovalRequestedOnFieldName = "pnl_approvalrequestedon";
        const string pnl_ApprovedTradePriceFieldName = "pnl_approvedtradeprice";
        //const string stateCodeFieldName = "statecode";
        const string statusCodeFieldName = "statuscode";

        IPluginExecutionContext _executionContext;
        IOrganizationService _service;
        CrmServiceContext _crmServiceContext;
        Entity _appraisalPreImageEntity, _updateAppraisalEntity;
        pnl_machine _updateMachine;
        pnl_appraisal _thisAppraisalWithUpdates;
        CalcValues _calculatedValues = new CalcValues();
        string _appraisalName;

        List<pnl_appraisalitem> _appraisalItems;
        List<pnl_appraisalitemgroup> _appraisalItemGroups;
        List<pnl_appraisalphoto> _appraisalPhotos;
        // prepare lists of update objects for child updates
        List<Entity> _updateAppraisalItems = new List<Entity>();
        List<Entity> _updateAppraisalItemGroups = new List<Entity>();
        List<Entity> _updateAppraisalPhotos = new List<Entity>();


        // CALC_CODE and pnl_fieldName from Appraisal - 
        // NB Values are case-sensitive :(
        Dictionary<string, string> _appraisalFieldToCodeMappings = new Dictionary<string, string>()
        {
            { "IN_STOCK", "pnl_InStock" },
            { "SERIAL_NUMBER", "pnl_SerialNumber" },
            { "MACHINE_YEAR", "pnl_Year" },
            { "MACHINE_HOURS", "pnl_Hours" },
            { "APPRAISAL_DATE", "pnl_appraisaldate" },
            { "CUSTOMER_NAME", "pnl_CustomerName" },
            { "MACHINE_MAKE", "pnl_Make" },
            { "MACHINE_MODEL", "pnl_Model" },
            { "SOURCE_1", "pnl_Source1Value" },
            { "SOURCE_2", "pnl_Source2Value" },
            { "SOURCE_3", "pnl_Source3Value" },
            { "SOURCE_4", "pnl_Source4Value" },
            { "CALCULATED_AVERAGE", "pnl_CalculatedAverage" },
            { "REC_LIST_PRICE", "pnl_RecommededListPrice" },
            { "REPAIR_COSTS", "pnl_RepairCosts" },
            { "FREIGHT_COSTS", "pnl_Freight" },
            { "PD_EXPENSES", "pnl_PreDeliveryExpenses" },
            { "OTHER_COSTS", "pnl_OtherCosts" },
            { "TOTAL_COSTS", "pnl_TotalCosts" },
            { "MARGIN_PERCENT", "pnl_ExpectedRetainedMarginPercentage" },
            { "RETAINED_MARGIN", "pnl_ExpectedRetainedMarginDollar" },
            { "CALC_TRADE_PRICE", "pnl_CalcuatedTradePrice" },
            { "REC_TRADE_PRICE", "pnl_RecommendedTradePrice" },
            { "APPROVED_TRADE_PRICE", "pnl_ApprovedTradePrice" },
            { "DESCRIPTION_NOTES", "pnl_DescriptionNotes" },
            { "WORKSHOP_NOTES", "pnl_WorkshopNotes" }
        };

        #endregion

        public void Execute(IServiceProvider serviceProvider)
        {
            #region Startup initialisation and checks
            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            // Obtain the execution context from the service provider.
            _executionContext = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            
            // don't want to redo create logic when resyncing online
            if (_executionContext.IsOfflinePlayback)
                return;

            // The InputParameters collection contains all the data passed in the message request.
            if (!_executionContext.InputParameters.Contains("Target") || !(_executionContext.InputParameters["Target"] is Entity))
                return;

            if (_executionContext.Depth > 3) return;

            // Obtain the target entity from the input parameters.
            _updateAppraisalEntity = (Entity)_executionContext.InputParameters["Target"];

            // Verify that the target entity represents an appraisal.
            // If not, this plug-in was not registered correctly.
            if (_updateAppraisalEntity.LogicalName != pnl_appraisal.EntityLogicalName)
                return;

            #endregion
            try
            {
                #region Initialise context-related variables
                // Obtain the organization service reference.
                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                _service = serviceFactory.CreateOrganizationService(_executionContext.UserId);

                // TODO: create an OrganizationServiceProxy as this is reuqired to execute multipel udpates with one round trip
                // I dont think this is correct
                // https://msdn.microsoft.com/en-us/library/jj863631.aspx
                // OrganizationServiceProxy serviceProxy = (OrganizationServiceProxy)_service;

                // Use reference to invoke pre-built types service 
                _crmServiceContext = new CrmServiceContext(_service);

                
                if (_executionContext.PreEntityImages.Contains("PreImage") && (_executionContext.PreEntityImages["PreImage"] is Entity))
                {
                    _appraisalPreImageEntity = (Entity)_executionContext.PreEntityImages["PreImage"];
                }
                if (_appraisalPreImageEntity == null || _appraisalPreImageEntity.LogicalName != "pnl_appraisal")
                {
                    throw new InvalidPluginExecutionException("Incorrect Plugin registration: a 'pnl_appraisal' Pre-image has not been correctly configured for this plugin.");
                }
                #endregion

                #region Initialise local appraisal and child values
                // update the appraisalPreImageEntity with values from the _updateAppraisalEntity
                foreach (var attrib in _updateAppraisalEntity.Attributes)
                {
                    _appraisalPreImageEntity.Attributes[attrib.Key] = attrib.Value;
                }
                _thisAppraisalWithUpdates = _appraisalPreImageEntity.ToEntity<pnl_appraisal>();

                // Check that we aren't "in" a sync - if we are then do nothing else
                if (_thisAppraisalWithUpdates.pnl_syncstartedon.HasValue &&
                    (!_thisAppraisalWithUpdates.pnl_syncendedon.HasValue ||
                    _thisAppraisalWithUpdates.pnl_syncendedon.Value < _thisAppraisalWithUpdates.pnl_syncstartedon.Value))
                    return;

                //_updateAppraisalEntity.ToEntity<pnl_appraisal>();
                _calculatedValues = new CalcValues();

                // retrieve child items
                _appraisalItems = _crmServiceContext.pnl_appraisalitemSet.Where(x => x.pnl_AppraisalId != null && x.pnl_AppraisalId.Id.Equals(_thisAppraisalWithUpdates.Id)).ToList();
                _appraisalItemGroups = _crmServiceContext.pnl_appraisalitemgroupSet.Where(x => x.pnl_AppraisalId != null && x.pnl_AppraisalId.Id.Equals(_thisAppraisalWithUpdates.Id)).ToList();
                _appraisalPhotos = _crmServiceContext.pnl_appraisalphotoSet.Where(x => x.pnl_AppraisalId != null && x.pnl_AppraisalId.Id.Equals(_thisAppraisalWithUpdates.Id)).ToList();

                _appraisalName = GetAppraisalName();

                SetAttributeValue(_updateAppraisalEntity, pnl_NameFieldName, _appraisalName);
                #endregion

                CheckCanApplyChanges(_executionContext.InitiatingUserId, _thisAppraisalWithUpdates);

                ProcessStatusReasonChangeIfRequired(_executionContext.InitiatingUserId);

                PerformRename(_appraisalName);

                SyncAppraisalAndItsItemsValues();

                PerformRecalc();

                // If a MachineID is specified for the Appraisal, then update this Machine from the Appraisal or it's items
                Guid? machineIdToUpdate = (null != _thisAppraisalWithUpdates.pnl_Machineid) ? _thisAppraisalWithUpdates.pnl_Machineid.Id : (Guid?)null;

                if (_thisAppraisalWithUpdates.pnl_Machineid != null)
                    UpdateMachine();

                #region Apply updates

                foreach (var item in _updateAppraisalItems)
                {
                    _service.Update(item);
                }
                foreach (var item in _updateAppraisalItemGroups)
                {
                    _service.Update(item);
                }
                foreach (var item in _updateAppraisalPhotos)
                {
                    _service.Update(item);
                }
                if (_updateMachine != null)
                {
                    _service.Update(_updateMachine);
                }
                #endregion

            }
            #region catch - Exception handling
            catch (FaultException<OrganizationServiceFault> ex)
            {
               throw new InvalidPluginExecutionException(string.Format("An error occurred in the Appraisal Update plug-in:-{0}{1}", Environment.NewLine, ex.Message) );
            }
            #endregion
        }

        #region Internal/private methods

        private void ProcessStatusReasonChangeIfRequired(Guid thisUserId)
        {
            OptionSetValue newStatusReason = _updateAppraisalEntity.GetAttributeValue<OptionSetValue>(statusCodeFieldName);

            var approvalRequested = 
                (newStatusReason != null && newStatusReason.Value == (int)pnl_appraisal_statuscode.InspectionWaitingforApproval)
                || 
                (_updateAppraisalEntity.Attributes.ContainsKey(pnl_ApprovalRequestedOnFieldName) && (_updateAppraisalEntity[pnl_ApprovalRequestedOnFieldName] != null));

            var approvalGranted =
                ((newStatusReason != null) && ((newStatusReason.Value == (int)pnl_appraisal_statuscode.ApprovedAppraisal) || (newStatusReason.Value == (int)pnl_appraisal_statuscode.ApprovedApprasial)))
                ||
                (_updateAppraisalEntity.Attributes.ContainsKey(pnl_ValuationApprovedOnFieldName) && (_updateAppraisalEntity[pnl_ValuationApprovedOnFieldName] != null));

            var inspectionSelected =
                (newStatusReason != null && ((newStatusReason.Value == (int)pnl_appraisal_statuscode.Inspection_1) || (newStatusReason.Value == (int)pnl_appraisal_statuscode.Inspection_2)));

            AssertOneOrLessIsTrue(approvalRequested, approvalGranted, inspectionSelected);

            // if we are attempting to approve this appraisal and other checks have been passed then
            // chekc a Customer and a Machine have both been specified for this Appraisal before allowing Approval.
            if(approvalGranted)
            {
                if (_thisAppraisalWithUpdates.pnl_Customerid == null)
                {
                    const string message = "A Customer must be specified before the Appraisal can be approved.";
                    throw new InvalidPluginExecutionException(message);
                }

                if (_thisAppraisalWithUpdates.pnl_Machineid == null)
                {
                    const string message = "A Machine must be specified before the Appraisal can be approved.";
                    throw new InvalidPluginExecutionException(message);
                }

                Guid fromUserId = _executionContext.InitiatingUserId;

                if (!IsTeamMember(fromUserId, "Appraisal Approvers"))
                    throw new InvalidPluginExecutionException("Permission denied. You need to be a member of the 'Appraisal Approvers' team.");

                CreateAndSendApprovedEmail(_thisAppraisalWithUpdates.OwnerId.Id, fromUserId);

                SetAttributeValue(_updateAppraisalEntity, pnl_ValuationApprovedByFieldName, new EntityReference(SystemUser.EntityLogicalName, thisUserId));
                SetAttributeValue(_updateAppraisalEntity, pnl_ValuationApprovedOnFieldName, DateTime.Now);
				SetAttributeValue(_updateAppraisalEntity, statusCodeFieldName, new OptionSetValue((int)pnl_appraisal_statuscode.ApprovedAppraisal));

                SetAppraisalItemsReadOnly(_appraisalPreImageEntity.Id, true);
            }
            
            if(inspectionSelected)
            {
                SetAttributeValue(_updateAppraisalEntity, pnl_ValuationApprovedByFieldName, null);
                SetAttributeValue(_updateAppraisalEntity, pnl_ValuationApprovedOnFieldName, null);
                SetAttributeValue(_updateAppraisalEntity, pnl_ApprovalRequestedOnFieldName, null);
                SetAttributeValue(_updateAppraisalEntity, pnl_ApprovedTradePriceFieldName , null);
                SetAttributeValue(_updateAppraisalEntity, pnl_PrimaryApproverFieldName, null);

                SetAttributeValue(_updateAppraisalEntity, statusCodeFieldName, new OptionSetValue((int)pnl_appraisal_statuscode.Inspection_1));
                SetAppraisalItemsReadOnly(_appraisalPreImageEntity.Id, false);
            }
            
            #region Approval Request
            if (approvalRequested)
            {
                if (_thisAppraisalWithUpdates.pnl_Customerid == null)
                {
                    const string message = "A Customer must be specified before requesting approval.";
                    throw new InvalidPluginExecutionException(message);
                }

                if (_thisAppraisalWithUpdates.pnl_Machineid == null)
                {
                    const string message = "A Machine must be specified before requesting approval.";
                    throw new InvalidPluginExecutionException(message);
                }

                // get activity parties for Approval
                var approvers = new List<ActivityParty>();

                var dfa = (from d in _crmServiceContext.pnl_delegatedfinancialauthoritySet
                           where d.pnl_SalesRepID.Id == _thisAppraisalWithUpdates.OwnerId.Id
                               && d.statecode == pnl_delegatedfinancialauthorityState.Active
                               && d.pnl_Type.Value == (int)pnl_delegatedfinancialauthoritytype.Tradein // pnl_TypeEnum causes attributename error.
                           select d).FirstOrDefault();

                if (null != dfa)
                {
                    var toUser = FetchSystemUserById(dfa.pnl_EmailTo.Id);
                    if (dfa.pnl_EmailCC != null)
                    {
                        var ccUser = FetchSystemUserById(dfa.pnl_EmailCC.Id);
                        approvers.Add(CreateCcParty(ccUser));
                    }
                    approvers.Add(CreateToParty(toUser));
                }

                var primaryApprover =
                    approvers.FirstOrDefault(a => a.ParticipationTypeMaskEnum == activityparty_participationtypemask.ToRecipient);

                if (null == primaryApprover) 
                    throw new InvalidPluginExecutionException("There are no Approvers available.");

                CreateAndSendApprovalRequestEmail(_service, approvers, _executionContext.InitiatingUserId);

                // Update the Primary Approver for this Appraisal
                SetAttributeValue(_updateAppraisalEntity, pnl_PrimaryApproverFieldName, new EntityReference(SystemUser.EntityLogicalName, primaryApprover.PartyId.Id));

                // Change Status To InspectionWaitingForApproval for this Appraisal
                SetAttributeValue(_updateAppraisalEntity, statusCodeFieldName, new OptionSetValue((int)pnl_appraisal_statuscode.InspectionWaitingforApproval));
                SetAttributeValue(_updateAppraisalEntity, pnl_ValuationApprovedByFieldName, null);
                SetAttributeValue(_updateAppraisalEntity, pnl_ValuationApprovedOnFieldName, null);
                SetAttributeValue(_updateAppraisalEntity, pnl_ApprovedTradePriceFieldName, null);

                SetAppraisalItemsReadOnly(_appraisalPreImageEntity.Id, true);
            }
            #endregion
        }

        private static void AssertOneOrLessIsTrue(bool approvalRequested, bool approvalGranted, bool inspectionSelected)
        {
            // TODO: Assert that none or one of these is true, and no more
            int trueCount = 0;
            trueCount += approvalRequested ? 1 : 0;
            trueCount += approvalGranted ? 1 : 0;
            trueCount += inspectionSelected ? 1 : 0;
            if (trueCount > 1)
            {
                string message = string.Format("Unexpected multiple status reason changes. Inspection={0}, ApprovalRequested={1}, Approved={2}", inspectionSelected, approvalRequested, approvalGranted);
                throw new InvalidPluginExecutionException(message);
            }
        }
    
        private void SetAppraisalItemsReadOnly(Guid appraisalId,  bool newValue)
        {
            foreach (var item in _appraisalItems)
            {
                var updateItem = FindOrCreateAppraisalItemUpdateEntity(item.Id);
                SetAttributeValue(updateItem, pnl_IsReadonlyFieldName, newValue);
            }
        }

        private void SyncAppraisalAndItsItemsValues()
        {
            // For each Appraisal Item
            foreach (pnl_appraisalitem item in _appraisalItems)
            {
                // Is there a mapping for this item's Calc Code?
                string appraisalSchemaName = AppraisalSchemaNameFromCalcCode(item.pnl_CalcCode);
                if (null != appraisalSchemaName)
                {
                    var appraisalProp = GetAppraisalPropInfo(item.pnl_CalcCode, appraisalSchemaName);

                    ProcessDenormalisedAppraisalItemValue(item, appraisalSchemaName, appraisalProp);

                    // Is this a Source 1-4 item - if so then the mapped value field will have been processed 
                    // above but now we must do the same for the display name.
                    if (item.pnl_CalcCode.StartsWith("SOURCE_"))
                    {
                        // get Source number (1-4 at the moment)
                        int sourceNumber;
                        if (!int.TryParse(item.pnl_CalcCode.Substring(7, 1), out sourceNumber))
                        {
                            string message = string.Format("Unable to determine Source number from Calculation Code '{0}' for Appraisal Item.", item.pnl_CalcCode);
                            throw new InvalidPluginExecutionException(message);
                        }
                        string appraisalSourceNameSchemaName = string.Format("pnl_Source{0}Name", sourceNumber);
                        var appraisalSourceNameFieldProp = GetAppraisalPropInfo(item.pnl_CalcCode, appraisalSourceNameSchemaName);

                        ProcessDenormalisedAppraisalItemDisplayName(item, appraisalSourceNameSchemaName, appraisalSourceNameFieldProp);
                    }
                }
            }
        }

        private void ProcessDenormalisedAppraisalItemValue(pnl_appraisalitem item, string appraisalSchemaName, System.Reflection.PropertyInfo appraisalProp)
        {
            // this is a mapped property so check if this is being updated in Appraisal 
            // - if so we use this value in the update object
            // - if not we use the value from the Appraisal Item
            string appraisalFieldName = appraisalSchemaName.ToLower();

            if (_updateAppraisalEntity.Attributes.ContainsKey(appraisalFieldName))
            {
                // Update Appraisal Item Value If Different
                if((_updateAppraisalEntity[appraisalFieldName] == null) || (item.pnl_Value != _updateAppraisalEntity[appraisalFieldName].ToString()))
                {
                    // add or update the item in the update Appraisal Items list
                    Entity updateAppraisalItem = FindOrCreateAppraisalItemUpdateEntity(item.Id);
                    updateAppraisalItem[pnl_ValueFieldName] = AppraisalFieldAsString(_updateAppraisalEntity[appraisalFieldName], appraisalProp);
                }
            }
            else
            {
                UpdateAppraisalFieldIfDifferent(item.pnl_Value, appraisalFieldName, appraisalProp, item.pnl_AppraisalItemNameId.Name);
            }
        }

        private void ProcessDenormalisedAppraisalItemDisplayName(pnl_appraisalitem item, string appraisalSchemaName, System.Reflection.PropertyInfo appraisalProp)
        {
            // this is a mapped property so check if this is being updated in Appraisal 
            // - if so we use this value in the update object
            // - if not we use the DisplayName from the Appraisal Item

            string appraisalFieldName = appraisalSchemaName.ToLower();

            if (_updateAppraisalEntity.Attributes.ContainsKey(appraisalFieldName))
            {
                // Update Appraisal Item Value If Different
                if (item.pnl_DisplayName != _updateAppraisalEntity[appraisalFieldName].ToString())
                {
                    // add or update the item in the update Appraisal Items list
                    Entity updateAppraisalItem = FindOrCreateAppraisalItemUpdateEntity(item.Id);
                    SetAttributeValue(updateAppraisalItem, pnl_DisplayNameFieldName, AppraisalFieldAsString(_updateAppraisalEntity[appraisalFieldName], appraisalProp));
                }
            }
            else
            {
                UpdateAppraisalFieldIfDifferent(item.pnl_DisplayName, appraisalFieldName, appraisalProp, item.pnl_AppraisalItemNameId.Name);
            }
        }

        private string AppraisalFieldAsString(object appraisalField, System.Reflection.PropertyInfo appraisalProp)
        {
            if (appraisalField == null)
                return null;

            if (appraisalProp.PropertyType == typeof(System.String))
                return appraisalField.ToString();

            if (appraisalProp.PropertyType == typeof(Microsoft.Xrm.Sdk.Money))
                return ((Money)appraisalField).Value.ToString();

            if (appraisalProp.PropertyType == typeof(decimal?))
                return ((decimal)appraisalField).ToString();
            

            if (appraisalProp.PropertyType == typeof(System.Boolean) || appraisalProp.PropertyType == typeof(bool?))
                return ((Boolean)appraisalField)?"Yes":"No" ;
            

            if (appraisalProp.PropertyType == typeof(System.Int32) || appraisalProp.PropertyType == typeof(int?))
                return ((int)appraisalField).ToString();

            string message = string.Format("Unsupported type '{0}' Appraisal field: {1}", appraisalProp.PropertyType, appraisalProp.Name);
            throw new InvalidPluginExecutionException(message);

        }
        

        private void UpdateAppraisalFieldIfDifferent(string itemValue, string appraisalFieldName, System.Reflection.PropertyInfo appraisalProp, string itemName)
        {
            object propertyValue = appraisalProp.GetValue(_thisAppraisalWithUpdates, null);
            bool propertyValueIsNull = (propertyValue == null);
            // currently null, new value is null, so do nothing.
            if (propertyValue == null && itemValue == null)
                return;

            // current value is not null and the new value is null, then can assign this without casting and checking the actual value
            if (propertyValue != null && itemValue == null)
            {
                SetAttributeValue(_updateAppraisalEntity, appraisalFieldName, itemValue);
                return;
            }

            try
            {

                // Compare values and update Appraisal if different
                if (appraisalProp.PropertyType == typeof(System.String))
                {
                    // if different, add or update the field in the update Appraisal object
                    if (propertyValueIsNull || propertyValue.ToString() != itemValue)
                    {
                        SetAttributeValue(_updateAppraisalEntity, appraisalFieldName, itemValue);
                    }
                    return;
                }

                if (appraisalProp.PropertyType == typeof(Microsoft.Xrm.Sdk.Money))
                {
                    decimal proposedValue = Decimal.Parse(itemValue);

                    // if different, add or update the field in the update Appraisal object
                    if (propertyValueIsNull || ((Money)propertyValue).Value != proposedValue)
                    {
                        SetAttributeValue(_updateAppraisalEntity, appraisalFieldName, new Money(proposedValue));
                    }
                    return;
                }

                if (appraisalProp.PropertyType == typeof(decimal?))
                {
                    decimal proposedValue = Decimal.Parse(itemValue);

                    // if different, add or update the field in the update Appraisal object
                    if (propertyValueIsNull || (Decimal)propertyValue != proposedValue)
                    {
                        SetAttributeValue(_updateAppraisalEntity, appraisalFieldName, proposedValue);
                    }
                    return;
                }

                if (appraisalProp.PropertyType == typeof(System.Boolean) || appraisalProp.PropertyType == typeof(bool?))
                {
                    bool currentItemValue = !(itemValue.ToLower() == "no" || itemValue.ToLower() == "false" || itemValue.ToLower() == "0");

                    // if different, add or update the field in the update Appraisal object
                    if (propertyValueIsNull || (System.Boolean)propertyValue != currentItemValue)
                    {
                        SetAttributeValue(_updateAppraisalEntity, appraisalFieldName, currentItemValue);
                    }
                    return;
                }

                if (appraisalProp.PropertyType == typeof(System.Int32) || appraisalProp.PropertyType == typeof(int?))
                {
                    int proposedValue = System.Int32.Parse(itemValue, System.Globalization.NumberStyles.Number);

                    // if different, add or update the field in the update Appraisal object
                    if (propertyValueIsNull || (System.Int32)propertyValue != proposedValue)
                    {
                        SetAttributeValue(_updateAppraisalEntity, appraisalFieldName, proposedValue);
                    }
                    return;
                }
            }
            catch (Exception ex)
            {
                string message = string.Format("Failed to convert Appraisal Item value '{0}' to type: {1} for item: {2}", itemValue, appraisalProp.PropertyType.ToString(), itemName);
                throw new InvalidPluginExecutionException(message);
            }

            string message2 = string.Format("Unsupported type '{0}' Appraisal field: {1}", appraisalProp.PropertyType, appraisalFieldName);
            throw new InvalidPluginExecutionException(message2);
        }

        // TODO: merge this with similar/identical method UdpateAPpraisalIfDifferent
        private void UpdateMachineFieldIfDifferent(object itemValue, string machineFieldName, pnl_machine updateMachine, pnl_machine thisMachine, string referencedEntityLogicalName )
        {
            System.Reflection.PropertyInfo machineProp = typeof(pnl_machine).GetProperty(machineFieldName);

            if (null == machineProp)
            {
                string message = string.Format("Unable to locate Machine property '{0}'", machineFieldName);
                throw new InvalidPluginExecutionException(message);
            }
                        
            object currentValueObject = machineProp.GetValue(thisMachine, null);

            if (itemValue == null && currentValueObject == null)
                return;

            if(itemValue == null && currentValueObject != null)
            {
                SetAttributeValue(updateMachine, machineFieldName, null);
                return;
            }

            // Update Machine If value is Different();
            if (machineProp.PropertyType == typeof(System.String))
            {
                // if different, add or update the field in the update Machine object
                if (currentValueObject == null || currentValueObject.ToString() != itemValue.ToString())
                {
                    SetAttributeValue(updateMachine, machineFieldName, itemValue);
                }
            }
            else if (machineProp.PropertyType == typeof(Microsoft.Xrm.Sdk.Money))
            {
                // if different, add or update the field in the update Machine object
                if ((currentValueObject == null || ((Money)currentValueObject).Value != ((Money)itemValue).Value))
                {
                    SetAttributeValue(updateMachine, machineFieldName, itemValue);
                }
            }
            else if (machineProp.PropertyType == typeof(System.Boolean))
            {
                bool currentItemValue = !(itemValue.ToString().ToLower() == "no" || itemValue.ToString().ToLower() == "false" || itemValue.ToString().ToLower() == "0");

                // if different, add or update the field in the update Machine object
                if ((currentValueObject == null || (Boolean)currentValueObject != currentItemValue))
                {
                    SetAttributeValue(updateMachine, machineFieldName, currentItemValue);
                }
            } else if (machineProp.PropertyType == typeof(EntityReference))
            {
                EntityReference currentValue = (EntityReference)machineProp.GetValue(thisMachine, null);
                EntityReference itemRef = (EntityReference)itemValue;

                if( (currentValueObject == null) || !AreEqual((EntityReference)currentValueObject, (EntityReference)itemValue))
                {
                    SetAttributeValue(updateMachine, machineFieldName, itemValue);
                }
            }
            else
            {
                string message = string.Format("Unsupported type '{0}' Machine field: {1}", machineProp.PropertyType, machineFieldName);
                throw new InvalidPluginExecutionException(message);
            }
        }

        private static bool AreEqual(EntityReference e1, EntityReference e2)
        {
            if (e1 == null && e2 == null)
                return true;

            if (e1 == null || e2 == null)
                return false;

            // neither is null, so equality determined by values
            return e1.LogicalName.Equals(e2.LogicalName) && e1.Id.Equals(e2.Id) && e1.Name.Equals(e2.Name);
        }


        private void PerformRename(string appraisalName)
        {
            foreach (var existingAppraisalItemGroup in _appraisalItemGroups)
            {
                string newGroupName = string.Format("{0} : {1}", appraisalName, existingAppraisalItemGroup.pnl_GroupNameId.Name);
                if (existingAppraisalItemGroup.pnl_name != newGroupName)
                {
                    Entity updateItemGroup = FindOrCreateAppraisalItemGroupUpdateEntity(existingAppraisalItemGroup.Id);
                    SetAttributeValue(updateItemGroup, pnl_NameFieldName, TrimToMaxLength(newGroupName, 300));
                }
                foreach (var existingAppraisalItem in _appraisalItems.Where(x=>x.pnl_AppraisalItemGroupId.Id.Equals(existingAppraisalItemGroup.Id)))
                {
                    string newItemName = string.Format("{0} : {1}", newGroupName, existingAppraisalItem.pnl_AppraisalItemNameId.Name);
                    if (existingAppraisalItem.pnl_name != newItemName)
                    {
                        Entity updateItem = FindOrCreateAppraisalItemUpdateEntity(existingAppraisalItem.Id);
                        SetAttributeValue(updateItem, pnl_NameFieldName, TrimToMaxLength(newItemName, 300));
                    }
                }
            }

            foreach (var existingAppraisalPhoto in _appraisalPhotos)
            {
                string newPhotoName = string.Format("{0} : {1}", appraisalName, existingAppraisalPhoto.pnl_DisplayOrder);
                if (existingAppraisalPhoto.pnl_name != newPhotoName)
                {
                    Entity updateAppraisalPhoto = FindOrCreateAppraisalPhotoUpdateEntity(existingAppraisalPhoto.Id);
                    SetAttributeValue(updateAppraisalPhoto, pnl_NameFieldName, TrimToMaxLength(newPhotoName, 300));
                }
            }
        }

        private object TrimToMaxLength(string target, int maxLength)
        {
            if (target.Length > maxLength)
                return target.Substring(0, maxLength);
            else
                return target;
        }

        private string AppraisalSchemaNameFromCalcCode(string calcCode)
        {
            KeyValuePair<string, string>? appraisalSchemaNameMapping = _appraisalFieldToCodeMappings.Where(x => x.Key.Equals(calcCode)).FirstOrDefault();
            if ((null == appraisalSchemaNameMapping))
                return null;
            else
                return appraisalSchemaNameMapping.Value.Value;
        }

        private void PerformRecalc()
        {
            #region Load Appraisal Items into recalc variables
            foreach (var item in _appraisalItems)
            {
                // Do we have a CalcCode and a valid decimal value?
                if (string.IsNullOrWhiteSpace(item.pnl_CalcCode) || !item.pnl_FieldTypeEnum.Equals(pnl_appraisalitemfieldtype.Integer))
                    continue;

                _calculatedValues.SetDecimalPropertyFromStringValue(item.pnl_CalcCode, item.pnl_Value);
            }
            #endregion

            #region Check Update Appraisal for differences and update CalcValues object with these if found
            // NB ignore other exisiting Appraisal fields - if these are different to Appraisal Item equivalents, then the Appraisal Items' values should be used instead.
            // TODO: update this mapping list to include types and field names (lowercase schema names)
            foreach (var mapping in _appraisalFieldToCodeMappings.Where(x => !x.Key.Equals("CUSTOMER_NAME")))
            {
                var fieldName = mapping.Value.ToLower();
                if (!_updateAppraisalEntity.Attributes.ContainsKey(fieldName))
                    continue;

                if (_updateAppraisalEntity.Attributes[fieldName] is Money)
                {
                    _calculatedValues.SetPropertyFromDecimalValue(mapping.Key, ((Money)_updateAppraisalEntity.Attributes[fieldName]).Value);
                    continue;
                }

                if (_updateAppraisalEntity.Attributes[fieldName] is decimal)
                {
                    _calculatedValues.SetPropertyFromDecimalValue(mapping.Key, (decimal)_updateAppraisalEntity.Attributes[fieldName]);
                }
            }
            #endregion

            _calculatedValues.Recalculate();

            #region update Appraisal and Appraisal Items where relcaculated values are different to current value.

            // NB Source values are not recalculated here so no change will result.   However Source values and names may be modified in SyncAppraisalAndItsItemsValues()
            foreach (var prop in typeof(CalcValues).GetProperties())
            {
                Decimal newValue = Decimal.Parse(prop.GetValue(_calculatedValues, null).ToString());
                string calcCode = prop.Name;

                #region Update Appraisal field if recalculated value is different
                // find the Appraisal fieldname from mappings
                string appraisalFieldName = AppraisalSchemaNameFromCalcCode(calcCode);

                if (appraisalFieldName != null)
                {
                    var appraisalProp = GetAppraisalPropInfo(calcCode, appraisalFieldName);

                    //if (appraisalProp.PropertyType != typeof(Microsoft.Xrm.Sdk.Money))
                    //{
                    //    string message = string.Format("Unexpected type of Appraisal property '{0}'.  Type 'Decimal' expected but type '{1}' found instead.", appraisalFieldName, appraisalProp.PropertyType.ToString());
                    //    throw new InvalidPluginExecutionException(message);
                    //}

                    UpdateAppraisalFieldIfDifferent(newValue.ToString(), appraisalFieldName, appraisalProp, calcCode);
                }
                #endregion

                #region update Appraisal Items where they are different to recalculated variables

                // find the existing appraisal item matching this property by calc code
                var existingAppraisalItem = _appraisalItems.Where(x => (x.pnl_CalcCode!= null) && (x.pnl_CalcCode.Equals(calcCode))).FirstOrDefault();

                if (null != existingAppraisalItem)
                {
                    // get the item's value and if different then update it
                    if (existingAppraisalItem.pnl_Value != newValue.ToString())
                    {
                        // add or update the item in the update Appraisal Items list
                        Entity updateAppraisalItem = FindOrCreateAppraisalItemUpdateEntity(existingAppraisalItem.Id);
                        SetAttributeValue(updateAppraisalItem, pnl_ValueFieldName, newValue.ToString());
                    }
                }
                #endregion
            }
            #endregion
        }

        private static System.Reflection.PropertyInfo GetAppraisalPropInfo(string calcCode, string appraisalFieldName)
        {
            // get the field's value
            var appraisalProp = typeof(pnl_appraisal).GetProperty(appraisalFieldName);
            if (null == appraisalProp)
            {
                string message = string.Format("Unable to locate Appraisal property '{0}' specified for Calc Code: {1}", appraisalFieldName, calcCode);
                throw new InvalidPluginExecutionException(message);
            }
            return appraisalProp;
        }

        private Entity FindOrCreateAppraisalItemUpdateEntity(Guid existingAppraisalItemId)
        {
            Entity updateAppraisalItem = _updateAppraisalItems.Where(x => x.Id.Equals(existingAppraisalItemId)).FirstOrDefault();
            if (null == updateAppraisalItem)
            {
                updateAppraisalItem = new Entity(pnl_appraisalitem.EntityLogicalName) { Id = existingAppraisalItemId };
                updateAppraisalItem.Attributes.Add(pnl_AppraisalRecalcedOnFieldName, DateTime.Now);
                _updateAppraisalItems.Add(updateAppraisalItem);
            }
            return updateAppraisalItem;
        }

        private Entity FindOrCreateAppraisalItemGroupUpdateEntity(Guid existingAppraisalItemGroupId)
        {
            Entity updateAppraisalItemGroup = _updateAppraisalItemGroups.Where(x => x.Id.Equals(existingAppraisalItemGroupId)).FirstOrDefault();
            if (null == updateAppraisalItemGroup)
            {
                updateAppraisalItemGroup = new Entity(pnl_appraisalitemgroup.EntityLogicalName) { Id = existingAppraisalItemGroupId };
                _updateAppraisalItemGroups.Add(updateAppraisalItemGroup);
            }
            return updateAppraisalItemGroup;
        }

        private Entity FindOrCreateAppraisalPhotoUpdateEntity(Guid existingAppraisalPhotoId)
        {
            Entity updateAppraisalPhoto = _updateAppraisalPhotos.Where(x => x.Id.Equals(existingAppraisalPhotoId)).FirstOrDefault();
            if (null == updateAppraisalPhoto)
            {
                updateAppraisalPhoto = new Entity(pnl_appraisalphoto.EntityLogicalName) { Id = existingAppraisalPhotoId };
                _updateAppraisalPhotos.Add(updateAppraisalPhoto);
            }
            return updateAppraisalPhoto;
        }

        private bool UpdatingRecalcFields()
        {
            if (_updateAppraisalEntity.Attributes.ContainsKey("pnl_source1value")) return true;
            if (_updateAppraisalEntity.Attributes.ContainsKey("pnl_source2value")) return true;
            if (_updateAppraisalEntity.Attributes.ContainsKey("pnl_source3value")) return true;
            if (_updateAppraisalEntity.Attributes.ContainsKey("pnl_source4value")) return true;
            if (_updateAppraisalEntity.Attributes.ContainsKey("pnl_calculatedaverage")) return true;
            if (_updateAppraisalEntity.Attributes.ContainsKey("pnl_recommendedlistprice")) return true;
            if (_updateAppraisalEntity.Attributes.ContainsKey("pnl_repaircosts")) return true;
            if (_updateAppraisalEntity.Attributes.ContainsKey("pnl_freight")) return true;
            if (_updateAppraisalEntity.Attributes.ContainsKey("pnl_predeliveryexpenses")) return true;
            if (_updateAppraisalEntity.Attributes.ContainsKey("pnl_othercosts")) return true;
            if (_updateAppraisalEntity.Attributes.ContainsKey("pnl_totalcosts")) return true;
            if (_updateAppraisalEntity.Attributes.ContainsKey("pnl_expectedretainedmarginpercentage")) return true;
            if (_updateAppraisalEntity.Attributes.ContainsKey("pnl_expectedretainedmargindollar")) return true;
            if (_updateAppraisalEntity.Attributes.ContainsKey("pnl_calculatedtradeprice")) return true;
            if (_updateAppraisalEntity.Attributes.ContainsKey("pnl_recommendedtradeprice")) return true;

            return false;
        }

        private void SetAttributeValue(Entity thisEntity, string schemaName, object newValue)
        {
            string fieldName = schemaName.ToLower();
 	        if(thisEntity.Attributes.ContainsKey(fieldName))
                thisEntity.Attributes[fieldName] = newValue;
            else
                thisEntity.Attributes.Add(fieldName, newValue);
        } 

        private static Guid? NullOrReferencedId(EntityReference eRef)
        {
            return eRef == null ? (Guid?)null : eRef.Id;
        }

        private static EntityReference NullOrNewEntityReference(string entityLogicalName, Guid? id)
        {
            return id == null ? null : new EntityReference(entityLogicalName, id.Value);
        }

        private static Decimal? NullOrDecimal(Money moneyValue)
        {
            return moneyValue == null ?  (Decimal?)null : (Decimal?)moneyValue.Value;
        }

        private void UpdateMachine()
        {
            EntityReference appraisalRef = new EntityReference(pnl_appraisal.EntityLogicalName, _thisAppraisalWithUpdates.Id);

            
            _updateMachine = new pnl_machine() { 
                Id = _thisAppraisalWithUpdates.pnl_Machineid.Id, 
                pnl_Appraisalid = appraisalRef,
                pnl_AppraisalStatus = _updateAppraisalEntity.Attributes.Contains(statusCodeFieldName) 
                ? (OptionSetValue)_updateAppraisalEntity[statusCodeFieldName] 
                : _thisAppraisalWithUpdates.statuscode
            };

            // Declare nullable variables for calculating current value for each type of mapped Appraisal Item
            Guid? thisAppraisalItemId;
            String currentValue, yearString = string.Empty, makeString = string.Empty, modelString = string.Empty;
            Decimal? currentDecimalValue;

            // pnl_machinetypeid and pnl_machientypeappraisalitemid
            Guid? currentPicklistItemId = LatestAppraisalItemPicklistIdFromCalcCode("MACHINE_TYPE", out thisAppraisalItemId);
            _updateMachine.pnl_MachineTypeid = NullOrNewEntityReference(pnl_appraisalitempicklistitem.EntityLogicalName, currentPicklistItemId);
            _updateMachine.pnl_MachineTypeAppraisalItemid = NullOrNewEntityReference(pnl_appraisalitem.EntityLogicalName, thisAppraisalItemId);

            // pnl_serialnnumber and pnl_serialnnumberappraislaitemid
            currentValue = LatestAppraisalItemStringValue("SERIAL_NUMBER", out thisAppraisalItemId);
            _updateMachine.pnl_SerialNumber = currentValue;
            _updateMachine.pnl_SerialNumberAppraisalItemid = NullOrNewEntityReference(pnl_appraisalitem.EntityLogicalName, thisAppraisalItemId);

            // pnl_makeid and pnl_makeappraisalitemid and pnl_makestring
            currentPicklistItemId = LatestAppraisalItemPicklistIdFromCalcCode("MACHINE_MAKE", out thisAppraisalItemId);
            _updateMachine.pnl_Makeid = NullOrNewEntityReference(pnl_appraisalitempicklistitem.EntityLogicalName, currentPicklistItemId);
            _updateMachine.pnl_MakeAppraisalItemid = NullOrNewEntityReference(pnl_appraisalitem.EntityLogicalName, thisAppraisalItemId);

            // MakeString field not editable on Appraisal so can just use Appraisal Item value if defined.
            Guid? makeAppraisalItemId = thisAppraisalItemId;
            makeString = OtherAppraisalItemValue(makeAppraisalItemId, "make", out thisAppraisalItemId);
            _updateMachine.pnl_MakeString = makeString;
            _updateMachine.pnl_MakeStringAppraisalItemid = NullOrNewEntityReference(pnl_appraisalitem.EntityLogicalName, thisAppraisalItemId);

            // pnl_modelid and pnl_modelappraisalitemid and pnl_modelstring
            currentPicklistItemId = LatestAppraisalItemPicklistIdFromCalcCode("MACHINE_MODEL", out thisAppraisalItemId);
            _updateMachine.pnl_Modelid = NullOrNewEntityReference(pnl_appraisalitempicklistitem.EntityLogicalName, currentPicklistItemId);
            _updateMachine.pnl_ModelAppraisalItemid = NullOrNewEntityReference(pnl_appraisalitem.EntityLogicalName, thisAppraisalItemId);

            // ModelString field not editable on Appraisal so can just use Appraisal Item value if defined.
            Guid? modelAppraisalItemId = thisAppraisalItemId;
            modelString = OtherAppraisalItemValue(modelAppraisalItemId, "model", out thisAppraisalItemId);
            _updateMachine.pnl_ModelString = modelString;
            _updateMachine.pnl_ModelStringAppraisalItemid = NullOrNewEntityReference(pnl_appraisalitem.EntityLogicalName, thisAppraisalItemId);

            // pnl_hours and pnl_hoursappraisalitemid
            currentValue = LatestAppraisalItemStringValue("MACHINE_HOURS", out thisAppraisalItemId);
            _updateMachine.pnl_Hours = currentValue;
            _updateMachine.pnl_HourAppraisalItemid = NullOrNewEntityReference(pnl_appraisalitem.EntityLogicalName, thisAppraisalItemId);

            // pnl_year and pnl_yearappraisalitemid
            currentValue = LatestAppraisalItemStringValue("MACHINE_YEAR", out thisAppraisalItemId);
            _updateMachine.pnl_Year = currentValue;
            _updateMachine.pnl_YearAppraisalItemid = NullOrNewEntityReference(pnl_appraisalitem.EntityLogicalName, thisAppraisalItemId);

            // pnl_conditionid and pnl_conditionappraisalitemid 
            currentPicklistItemId = LatestAppraisalItemPicklistIdFromName("Overall Condition", out thisAppraisalItemId);
            _updateMachine.pnl_Conditionid = NullOrNewEntityReference(pnl_appraisalitempicklistitem.EntityLogicalName, currentPicklistItemId);
            _updateMachine.pnl_ConditionAppraisalItemid = NullOrNewEntityReference(pnl_appraisalitem.EntityLogicalName, thisAppraisalItemId);

            // pnl_recommendedbookvalue and pnl_recommendedbookappraisalitemid
            currentDecimalValue = LatestAppraisalItemDecimalValue("REC_TRADE_PRICE", out thisAppraisalItemId);
            _updateMachine.pnl_RecommendedBookValue = currentDecimalValue.HasValue ? new Money(currentDecimalValue.Value) : null;
            _updateMachine.pnl_RecommendedBookValueAppraisalItem = NullOrNewEntityReference(pnl_appraisalitem.EntityLogicalName, thisAppraisalItemId);

            // pnl_approvedbookvalue and pnl_approvedbookappraisalitemid
            currentDecimalValue = LatestAppraisalItemDecimalValue("APPROVED_TRADE_PRICE", out thisAppraisalItemId);
            _updateMachine.pnl_ApprovedBookValue = currentDecimalValue.HasValue ? new Money(currentDecimalValue.Value) : null;
            _updateMachine.pnl_ApprovedBookValueAppraisalItem = NullOrNewEntityReference(pnl_appraisalitem.EntityLogicalName, thisAppraisalItemId);

            // pnl_marketvalue and pnl_marketvalueappraisalitemid
            currentDecimalValue = LatestAppraisalItemDecimalValue("REC_LIST_PRICE", out thisAppraisalItemId);
            _updateMachine.pnl_MarketValue = currentDecimalValue.HasValue ? new Money(currentDecimalValue.Value) : null;
            _updateMachine.pnl_MarketValueAppraisalItemid = NullOrNewEntityReference(pnl_appraisalitem.EntityLogicalName, thisAppraisalItemId);

            // pnl_sellingmargin and pnl_sellingmarginappraisalid
            currentDecimalValue = LatestAppraisalItemDecimalValue("RETAINED_MARGIN", out thisAppraisalItemId);
            _updateMachine.pnl_SellingMargin = currentDecimalValue.HasValue ? new Money(currentDecimalValue.Value) : null;
            _updateMachine.pnl_SellingMarginAppraisalItemid = NullOrNewEntityReference(pnl_appraisalitem.EntityLogicalName, thisAppraisalItemId);

            // pnl_sellingmargin and pnl_sellingmarginappraisalid
            currentDecimalValue = LatestAppraisalItemDecimalValue("MARGIN_PERCENT", out thisAppraisalItemId);
            _updateMachine.pnl_SellingMarginpercentage = currentDecimalValue.HasValue ? currentDecimalValue.Value : (decimal?)null;
            _updateMachine.pnl_SellingMarginPercentageAppraisalItem = NullOrNewEntityReference(pnl_appraisalitem.EntityLogicalName, thisAppraisalItemId);

            // pnl_repaircosts and pnl_repaircostappraisalitemid
            currentDecimalValue = LatestAppraisalItemDecimalValue("REPAIR_COSTS", out thisAppraisalItemId);
            _updateMachine.pnl_RepairCosts = currentDecimalValue.HasValue ? new Money(currentDecimalValue.Value) : null;
            _updateMachine.pnl_RepairCostAppraisalItemid = NullOrNewEntityReference(pnl_appraisalitem.EntityLogicalName, thisAppraisalItemId);

            currentValue = LatestAppraisalItemStringValue("WORKSHOP_NOTES", out thisAppraisalItemId);
            _updateMachine.pnl_WorkshopNotes = currentValue;

            currentValue = LatestAppraisalItemStringValue("DESCRIPTION_NOTES", out thisAppraisalItemId);
            _updateMachine.pnl_DescriptionNotes = currentValue;

            if(_updateAppraisalEntity.Attributes.ContainsKey(pnl_ValuationApprovedByFieldName))
                _updateMachine.pnl_ApprovedByid = (EntityReference)_updateAppraisalEntity.Attributes[pnl_ValuationApprovedByFieldName];
            else
                _updateMachine.pnl_ApprovedByid = _thisAppraisalWithUpdates.pnl_ValuationApprovedBy;
        }

        /// <summary>
        /// Locate the Appraisal Item with "Other" in it's name that has the supplied ID as it's visibility item
        /// </summary>
        /// <param name="visibilityAppraisalItemId">The ID of the Make or Model Appraisal Item that makes this Appraisal visible or hidden</param>
        /// <param name="appraisalItemId">The ID of this Other Make or Other Model Appraisal Item</param>
        /// <returns></returns>
        private string OtherAppraisalItemValue(Guid? visibilityAppraisalItemId, string specificationGroupSearchText, out Guid? appraisalItemId)
        {
            if (visibilityAppraisalItemId.HasValue)
            {
                pnl_appraisalitem otherItem = _appraisalItems.FirstOrDefault(
                    x => x.pnl_VisibilityPropertyId != null
                        && x.pnl_VisibilityPropertyId.Id.Equals(visibilityAppraisalItemId.Value)
                        && x.pnl_AppraisalItemNameId.Name.ToLower().Contains("other"));
                if (null != otherItem)
                {
                    appraisalItemId = otherItem.Id;
                    return otherItem.pnl_Value;
                }
            }

            // For some appraisals (eg those for General template)  there are only string fields for Make or Model in which case we search 
            // for specific text as there is no Visibility Property set
            pnl_appraisalitem backupItem = _appraisalItems.FirstOrDefault(x=>x.pnl_AppraisalItemNameId.Name.ToLower().Contains(specificationGroupSearchText)
                && x.pnl_AppraisalItemGroupId.Name.ToLower().Contains("specification"));
            if (null != backupItem)
            {
                appraisalItemId = backupItem.Id;
                return backupItem.pnl_Value;
            }

            // nothing worked
            appraisalItemId = null;
            return null;
        }

        private string LatestAppraisalItemStringValue(string calcCode, out Guid? appraisalItemId)
        {
            // locate the existing Appraisal Item for this calc code
            appraisalItemId = null;
            pnl_appraisalitem currentItem = _appraisalItems.Where(x => x.pnl_CalcCode != null).FirstOrDefault(x => x.pnl_CalcCode.Equals(calcCode));
            if (null == currentItem)
            {
                return null;
            }
            appraisalItemId = currentItem.Id;

            Entity newItem = _updateAppraisalItems.FirstOrDefault(x => x.Id.Equals(currentItem.Id));
            if (null == newItem || !newItem.Attributes.ContainsKey(pnl_ValueFieldName))
            {
                // if the value has already been set to Update then this is the "current" value
                return currentItem.pnl_Value;
            }
            // otherwise use the current value
            return newItem[pnl_ValueFieldName].ToString();
        }

        private Guid? LatestAppraisalItemPicklistIdFromCalcCode(string calcCode, out Guid? appraisalItemId)
        {
            // locate the existing Appraisal Item for this calc code
            appraisalItemId = null;
            pnl_appraisalitem currentItem = _appraisalItems.Where(x => x.pnl_CalcCode != null).FirstOrDefault(x => x.pnl_CalcCode.Equals(calcCode));
            if (null == currentItem)
            {
                return null;
            }
            appraisalItemId = currentItem.Id;

            // then need to check if this Appraisal Item has already been set to update with a new value elsewhere
            Entity newItem = _updateAppraisalItems.FirstOrDefault(x => x.Id.Equals(currentItem.Id));
            if (null == newItem || !newItem.Attributes.ContainsKey(pnl_AppraisalItemPicklistItemIdFieldName))
            {
                // if the picklist item has already been set to Update then this is the "current" value
                return null == currentItem.pnl_AppraisalItemPicklistItemId ? (Guid?)null : currentItem.pnl_AppraisalItemPicklistItemId.Id;
            }
            // Otherwise use the current value
            return ((EntityReference)newItem[pnl_AppraisalItemPicklistItemIdFieldName]).Id;
        }

        private Guid? LatestAppraisalItemPicklistIdFromName(string nameString, out Guid? appraisalItemId)
        {
            // locate the existing Appraisal Item for this calc code
            appraisalItemId = null;
            pnl_appraisalitem currentItem = _appraisalItems.Where(
                x => x.pnl_AppraisalItemNameId != null).FirstOrDefault(
                x => x.pnl_AppraisalItemNameId.Name.ToLower().Equals(nameString.ToLower()));
            if (null == currentItem)
            {
                return null;
            }
            appraisalItemId = currentItem.Id;

            // then need to check if this Appraisal Item has already been set to update with a new value elsewhere
            Entity newItem = _updateAppraisalItems.FirstOrDefault(x => x.Id.Equals(currentItem.Id));
            if (null == newItem || !newItem.Attributes.ContainsKey(pnl_AppraisalItemPicklistItemIdFieldName))
            {
                // if the picklist item has already been set to Update then this is the "current" value
                return null == currentItem.pnl_AppraisalItemPicklistItemId ? (Guid?)null : currentItem.pnl_AppraisalItemPicklistItemId.Id;
            }
            // Otherwise use the current value
            return ((EntityReference)newItem[pnl_AppraisalItemPicklistItemIdFieldName]).Id;
        }

        //private Guid? LatestAppraisalItemPicklistId(string calcCode, out Guid? appraisalItemId)
        //{
        //    // locate the existing Appraisal Item for this calc code
        //    appraisalItemId = null;
        //    pnl_appraisalitem currentItem = _appraisalItems.FirstOrDefault(x => x.pnl_CalcCode.Equals(calcCode));
        //    if (null == currentItem)
        //    {
        //        return null;
        //    }
        //    appraisalItemId = currentItem.Id;

        //    // then need to check if this Appraisal Item has already been set to update with a new value elsewhere
        //    pnl_appraisalitem newItem = _updateAppraisalItems.FirstOrDefault(x => x.Id.Equals(currentItem.Id));
        //    if (null == newItem || null == newItem.pnl_AppraisalItemPicklistItemId)
        //    {
        //        // if the picklist item has already been set to Update then this is the "current" value
        //        return null == currentItem.pnl_AppraisalItemPicklistItemId ? (Guid?)null : currentItem.pnl_AppraisalItemPicklistItemId.Id;
        //    }
        //    // Otherwise use the current value
        //    return newItem.pnl_AppraisalItemPicklistItemId.Id;
        //}

        private Decimal? LatestAppraisalItemDecimalValue(string calcCode, out Guid? appraisalItemId)
        {
            // locate the existing Appraisal Item for this calc code
            appraisalItemId = null;
            pnl_appraisalitem currentItem = _appraisalItems.Where(x => x.pnl_CalcCode != null).FirstOrDefault(x => x.pnl_CalcCode.Equals(calcCode));
            if (null == currentItem)
            {
                return null;
            }
            appraisalItemId = currentItem.Id;

            Entity newItem = _updateAppraisalItems.FirstOrDefault(x => x.Id.Equals(currentItem.Id));
            if (null == newItem || !newItem.Attributes.ContainsKey(pnl_ValueFieldName))
            {
                // if the value has already been set to Update then this is the "current" value
                return Decimal.Parse(currentItem.pnl_Value);
            }
            // otherwise use the current value
            if (newItem[pnl_ValueFieldName] == null)
                return (Decimal?)null;

            return Decimal.Parse(newItem[pnl_ValueFieldName].ToString());
        }


        private void CheckCanApplyChanges(Guid userId, pnl_appraisal thisAppraisal)
        {
            bool canUpdate = false;
            var user = new SystemUser { Id = userId };

            if (thisAppraisal.statuscode.Value == (int)pnl_appraisal_statuscode.ApprovedApprasial
             || thisAppraisal.statuscode.Value == (int)pnl_appraisal_statuscode.ApprovedAppraisal)
            {
                canUpdate = IsInRole(userId, "System Administrator") || IsInRole(userId, "System Administrator � Sales");
            }

            else if (thisAppraisal.statuscode.Value == (int)pnl_appraisal_statuscode.Inspection_1 ||
                thisAppraisal.statuscode.Value == (int)pnl_appraisal_statuscode.Inspection_2)
            {
                canUpdate = true;
            }
            else
            {
                canUpdate = IsTeamMember(userId, "Appraisal Approvers");
            }

            if (!canUpdate)
            {
                const string message =
                    "Permission denied. The Appraisal can be updated if it is in inspection, or you are an approver and it is waiting for approval, " +
                        "or you are a System Administrator and it has been Approved.";
                throw new InvalidPluginExecutionException(message);
            }
        }

        private bool IsTeamMember(Guid userId, string teamName)
        {
            var teamMember = (from tm in _crmServiceContext.TeamMembershipSet
                              join t in _crmServiceContext.TeamSet on tm.TeamId.GetValueOrDefault() equals t.Id
                              where tm.SystemUserId.GetValueOrDefault() == userId && t.Name == teamName
                              select tm).FirstOrDefault();

            return teamMember != null;
        }

        private bool IsInRole(Guid userId, string roleName)
        {
            var userRole = (from s in _crmServiceContext.SystemUserRolesSet
                            join r in _crmServiceContext.RoleSet on s.RoleId.Value equals r.RoleId.Value
                            where s.SystemUserId.Value == userId
                                && r.Name == roleName
                            select s).FirstOrDefault();

            return userRole != null;
        }

        private string GetAppraisalName()
        {
            var item = _appraisalItems.Where(x=>x.pnl_CalcCode != null).FirstOrDefault(x => x.pnl_CalcCode.Equals("CUSTOMER_NAME"));
            string custName = (item == null || item.pnl_Value == null) ? "<Not Specified>" : item.pnl_Value;

            item = _appraisalItems.Where(x => x.pnl_CalcCode != null).FirstOrDefault(x => x.pnl_CalcCode.Equals("MACHINE_TYPE"));
            string machineTypeName = (item == null || item.pnl_Value == null) ? "<Not Specified>" : item.pnl_Value;

            string dateName = _thisAppraisalWithUpdates.pnl_appraisaldate.HasValue?string.Format("{0:dd/M/yy}", _thisAppraisalWithUpdates.pnl_appraisaldate.Value.ToLocalTime()) :"<Not Specified>";

            return string.Format("{0} : {1} : {2}", custName, dateName, machineTypeName);
        }

        public void CreateAndSendApprovedEmail(Guid toUserId, Guid fromUserId)
        {
            var subject = "Trade-in Inspection: " + _appraisalName + " was Approved";
            var description = string.Format("The following Trade-in Inspection was approved.<br/><br/>{0}", BuildAppraisalLink());
            var email = new Email
            {
                To = new[] { new ActivityParty { PartyId = new EntityReference("systemuser", toUserId) } },
                From = new[] { new ActivityParty { PartyId = new EntityReference("systemuser", fromUserId) } },
                Subject = subject,
                Description = description,
                DirectionCode = true, // Outgoing
                RegardingObjectId = new EntityReference("pnl_appraisal", _thisAppraisalWithUpdates.Id)
            };
            var emailId = _service.Create(email);

            SendEmail(emailId);
        }

        private void CreateAndSendApprovalRequestEmail(IOrganizationService orgService, List<ActivityParty> activityParties, Guid fromUserId)
        {
            var subject = "Approval Request for Trade-in Inspection: " + _appraisalName;
            var description = string.Format("Please review and approve the following Trade-in Inspection.<br/><br/>{0}", BuildAppraisalLink());
            var email = new Email();
            email.From = new[] { new ActivityParty { PartyId = new EntityReference(SystemUser.EntityLogicalName, fromUserId) } };
            email.To = new[] { activityParties.First(a => a.ParticipationTypeMaskEnum == activityparty_participationtypemask.ToRecipient) };
            email.Subject = subject;
            email.Description = description;
            email.DirectionCode = true;
            email.RegardingObjectId = new EntityReference(pnl_appraisal.EntityLogicalName, _thisAppraisalWithUpdates.Id);
            //email.RegardingObjectId = new EntityReference(pnl_appraisal.EntityLogicalName, _thisAppraisalWithUpdates.Id)


            //{
            //    //To = new[] { activityParties.First(a => a.ParticipationTypeMaskEnum == activityparty_participationtypemask.ToRecipient) },
            //    //From = new[] { new ActivityParty { PartyId = new EntityReference(SystemUser.EntityLogicalName, fromUserId) } }
            //    //,
            //    //Cc = activityParties.Where(a => a.ParticipationTypeMaskEnum == activityparty_participationtypemask.CCRecipient),
            //    //Subject = subject,
            //    //Description = description,
            //    //DirectionCode = true, // Outgoing
            //    //RegardingObjectId = new EntityReference(pnl_appraisal.EntityLogicalName, _thisAppraisalWithUpdates.Id)
            //};
          
          var emailId = orgService.Create(email);
          var request = new SendEmailRequest
          {
              EmailId = emailId,
              TrackingToken = "",
              IssueSend = true
          };

          orgService.Execute(request);
           // SendEmail(emailId);
        }

        private void SendEmail(Guid emailId)
        {
            var request = new SendEmailRequest
            {
                EmailId = emailId,
                TrackingToken = "",
                IssueSend = true
            };

            _service.Execute(request);
        }

        private string BuildAppraisalLink()
        {
            var random = new Random();
            var histKey = random.Next(100000000, 900000000);
            var url = string.Format("https://{0}.landpower.co.nz/main.aspx?etn=pnl_appraisal&histKey={1}&id={2}&pagetype=entityrecord",
                _executionContext.OrganizationName, histKey, _appraisalPreImageEntity.Id);
            url = Uri.EscapeUriString(url);

            return string.Format("<a href=\"{0}\">{1}</a>", url, System.Net.WebUtility.HtmlEncode(_appraisalName));
        }

        private SystemUser FetchSystemUserById(Guid userId)
        {
            return (from u in _crmServiceContext.SystemUserSet
                    where u.SystemUserId.Value == userId
                    select u).FirstOrDefault();
        }

        private static ActivityParty CreateToParty(SystemUser approver)
        {
            var toParty = new ActivityParty
            {
                PartyId = new EntityReference(SystemUser.EntityLogicalName, approver.Id),
                ParticipationTypeMaskEnum = activityparty_participationtypemask.ToRecipient
            };
            toParty.PartyId.Name = approver.FullName;

            return toParty;
        }

        private static ActivityParty CreateCcParty(SystemUser approver)
        {
            var ccParty = new ActivityParty
            {
                PartyId = new EntityReference(SystemUser.EntityLogicalName, approver.Id),
                ParticipationTypeMaskEnum = activityparty_participationtypemask.CCRecipient
            };
            ccParty.PartyId.Name = approver.FullName;

            return ccParty;
        }
        #endregion    
    }
}
