﻿using System;
using System.Linq;
using System.ServiceModel;
using Microsoft.Xrm.Sdk;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using System.Data.SqlTypes;
using System.Text;
using System.Collections.Generic;

namespace LandPowerPlugins
{
    public class AssignProductHierarchy : IPlugin
    {
        IPluginExecutionContext _Context;
        IOrganizationService _Service;
        Guid ZEROGUID = Guid.NewGuid();

        struct ProductHierarchyValues
        {
            public EntityReference NewUsed;
            public EntityReference StockClassMaster;
            public EntityReference StockGroup;
            public EntityReference StockCategory;
            public string StockClassName;
        }

        public void Execute(IServiceProvider serviceProvider)
        {
            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            // Obtain the execution context from the service provider.
            _Context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (_Context.Depth > 1)
                return;

            // don't want to redo create logic when resyncing online
            if (_Context.IsOfflinePlayback)
                return;

            //CREATE AND UPDATE
            if ((_Context.MessageName.ToUpper() == "CREATE" || _Context.MessageName.ToUpper() == "UPDATE") && _Context.InputParameters.Contains("Target") && _Context.InputParameters["Target"] is Entity)
            {
                try
                {
                    IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                    _Service = serviceFactory.CreateOrganizationService(_Context.InitiatingUserId);

                    Entity entity = (Entity)_Context.InputParameters["Target"];

                    if (entity.LogicalName == "pnl_productenquiry")
                    {
                        switch (_Context.MessageName.ToUpper())
                        {
                            case "CREATE":
                                ExecuteCreate();
                                break;
                            case "UPDATE":
                                ExecuteUpdate();
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new InvalidPluginExecutionException(string.Format("ERROR: {0}", ex.Message, ex.InnerException, ex.StackTrace));
                }
            }
        }

        private void ExecuteCreate()
        {
            Entity productenquiry = (Entity)_Context.InputParameters["Target"];
            bool HasUpdated = false;

            #region Update if Pronto Model is selected

            if (productenquiry.Contains("pnl_prontomodelid") && productenquiry["pnl_prontomodelid"] != null)
            {
                Guid prontomodelid = ((EntityReference)productenquiry["pnl_prontomodelid"]).Id;
                ProductHierarchyValues values = GetProductHierarchyByProntoModel(prontomodelid);

                if (productenquiry.Contains("pnl_stockgroup"))
                {
                    IsEqualOrNull((EntityReference)productenquiry["pnl_stockgroup"], values.StockGroup, @"Stock Group");
                    productenquiry["pnl_stockgroup"] = values.StockGroup;
                }
                else
                {
                    productenquiry.Attributes.Add("pnl_stockgroup", values.StockGroup);
                }
                              
                if (productenquiry.Contains("pnl_stockclassmasterid"))
                {
                    IsEqualOrNull((EntityReference)productenquiry["pnl_stockclassmasterid"], values.StockClassMaster, @"Stock Class");
                    productenquiry["pnl_stockclassmasterid"] = values.StockClassMaster;

                    if (productenquiry.Contains("pnl_name"))
                    {
                        productenquiry["pnl_name"] = GetProductEnquiryNameOnCreate(values.StockClassMaster.Name);
                    }
                    else
                    {
                        productenquiry.Attributes.Add("pnl_name", GetProductEnquiryNameOnCreate(values.StockClassMaster.Name));
                    }
                }
                else
                {
                    productenquiry.Attributes.Add("pnl_stockclassmasterid", values.StockClassMaster);

                    if (productenquiry.Contains("pnl_name"))
                    {
                        productenquiry["pnl_name"] = GetProductEnquiryNameOnCreate(values.StockClassMaster.Name);
                    }
                    else
                    {
                        productenquiry.Attributes.Add("pnl_name", GetProductEnquiryNameOnCreate(values.StockClassMaster.Name));
                    }
                }

                if (productenquiry.Contains("pnl_stockcategoryid"))
                {
                    IsEqualOrNull((EntityReference)productenquiry["pnl_stockcategoryid"], values.StockCategory, @"Stock Category");
                    productenquiry["pnl_stockcategoryid"] = values.StockCategory;
                }
                else
                {
                    productenquiry.Attributes.Add("pnl_stockcategoryid", values.StockCategory);
                }

                if (productenquiry.Contains("pnl_newusedid"))
                {
                    IsEqualOrNull((EntityReference)productenquiry["pnl_newusedid"], values.NewUsed, @"New / Used");
                    productenquiry["pnl_newusedid"] = values.NewUsed;
                }
                else
                {
                    productenquiry.Attributes.Add("pnl_newusedid", values.NewUsed);
                }
                HasUpdated = true;
            }

            #endregion

            #region Update if Stock Group is selected

            if (productenquiry.Contains("pnl_stockgroup") && productenquiry["pnl_stockgroup"] != null && !HasUpdated)
            {
                Guid stockgroupid = ((EntityReference)productenquiry["pnl_stockgroup"]).Id;
                ProductHierarchyValues values = GetProductHierarchyByStockGroup(stockgroupid);

                if (productenquiry.Contains("pnl_stockclassmasterid"))
                {
                    IsEqualOrNull((EntityReference)productenquiry["pnl_stockclassmasterid"], values.StockClassMaster, @"Stock Class");
                    productenquiry["pnl_stockclassmasterid"] = values.StockClassMaster;

                    if (productenquiry.Contains("pnl_name"))
                    {
                        productenquiry["pnl_name"] = GetProductEnquiryNameOnCreate(values.StockClassMaster.Name);
                    }
                    else
                    {
                        productenquiry.Attributes.Add("pnl_name", GetProductEnquiryNameOnCreate(values.StockClassMaster.Name));
                    }
                }
                else
                {
                    productenquiry.Attributes.Add("pnl_stockclassmasterid", values.StockClassMaster);

                    if (productenquiry.Contains("pnl_name"))
                    {
                        productenquiry["pnl_name"] = GetProductEnquiryNameOnCreate(values.StockClassMaster.Name);
                    }
                    else
                    {
                        productenquiry.Attributes.Add("pnl_name", GetProductEnquiryNameOnCreate(values.StockClassMaster.Name));
                    }
                }

                if (productenquiry.Contains("pnl_stockcategoryid"))
                {
                    IsEqualOrNull((EntityReference)productenquiry["pnl_stockcategoryid"], values.StockCategory, @"Stock Category");
                    productenquiry["pnl_stockcategoryid"] = values.StockCategory;

                }
                else
                {
                    productenquiry.Attributes.Add("pnl_stockcategoryid", values.StockCategory);
                }

                if (productenquiry.Contains("pnl_newusedid"))
                {
                    IsEqualOrNull((EntityReference)productenquiry["pnl_newusedid"], values.NewUsed, @"New / Used");
                    productenquiry["pnl_newusedid"] = values.NewUsed;
                }
                else
                {
                    productenquiry.Attributes.Add("pnl_newusedid", values.NewUsed);
                }
                HasUpdated = true;
            }

            #endregion

            #region Update if Stock Class Master has value

            if (productenquiry.Contains("pnl_stockclassmasterid") && productenquiry["pnl_stockclassmasterid"] != null && !HasUpdated)
            {
                ProductHierarchyValues values = GetProductHierarchyByStockClassMaster((EntityReference)productenquiry["pnl_stockclassmasterid"]);
                 
                if (productenquiry.Contains("pnl_name"))
                {
                    productenquiry["pnl_name"] = GetProductEnquiryNameOnCreate(values.StockClassName);
                }
                else
                {
                    productenquiry.Attributes.Add("pnl_name", GetProductEnquiryNameOnCreate(values.StockClassName));
                }
               
                if (productenquiry.Contains("pnl_stockcategoryid"))
                {
                    //IsEqualOrNull((EntityReference)productenquiry["pnl_stockcategoryid"], values.StockCategory, @"Stock Category");
                    productenquiry["pnl_stockcategoryid"] = values.StockCategory;
                }
                else
                {
                    productenquiry.Attributes.Add("pnl_stockcategoryid", values.StockCategory);
                }

                if (productenquiry.Contains("pnl_newusedid"))
                {
                    IsEqualOrNull((EntityReference)productenquiry["pnl_newusedid"], values.NewUsed, @"New / Used");
                    productenquiry["pnl_newusedid"] = values.NewUsed;
                }
                else
                {
                    productenquiry.Attributes.Add("pnl_newusedid", values.NewUsed);
                }
                HasUpdated = true;
            }

            #endregion
        }

        private void ExecuteUpdate()
        {
            Entity productenquiry = (Entity)_Context.PostEntityImages["PostImage"];
            bool HasUpdated = false;
            DateTime? createdon = null;

            if (productenquiry.Contains("createdon"))
            {
                createdon = (DateTime?)productenquiry["createdon"];
            }

            #region Update if Pronto Model is selected

            if (productenquiry.Contains("pnl_prontomodelid") && productenquiry["pnl_prontomodelid"] != null)
            {
                Guid prontomodelid = ((EntityReference)productenquiry["pnl_prontomodelid"]).Id;
                ProductHierarchyValues values = GetProductHierarchyByProntoModel(prontomodelid);

                if (productenquiry.Contains("pnl_stockgroup"))
                {
                    IsEqualOrNull((EntityReference)productenquiry["pnl_stockgroup"], values.StockGroup, @"Stock Group");
                    productenquiry["pnl_stockgroup"] = values.StockGroup;
                }
                else
                {
                    productenquiry.Attributes.Add("pnl_stockgroup", values.StockGroup);
                }

                if (productenquiry.Contains("pnl_stockclassmasterid"))
                {
                    IsEqualOrNull((EntityReference)productenquiry["pnl_stockclassmasterid"], values.StockClassMaster, @"Stock Class");
                    productenquiry["pnl_stockclassmasterid"] = values.StockClassMaster;

                    if (productenquiry.Contains("pnl_name"))
                    {
                        productenquiry["pnl_name"] = GetProductEnquiryNameOnUpdate(values.StockClassMaster.Name, createdon);
                    }
                    else
                    {
                        productenquiry.Attributes.Add("pnl_name", GetProductEnquiryNameOnUpdate(values.StockClassMaster.Name, createdon));
                    }
                }
                else
                {
                    productenquiry.Attributes.Add("pnl_stockclassmasterid", values.StockClassMaster);

                    if (productenquiry.Contains("pnl_name"))
                    {
                        productenquiry["pnl_name"] = GetProductEnquiryNameOnUpdate(values.StockClassMaster.Name, createdon);
                    }
                    else
                    {
                        productenquiry.Attributes.Add("pnl_name", GetProductEnquiryNameOnUpdate(values.StockClassMaster.Name, createdon));
                    }
                }

                if (productenquiry.Contains("pnl_stockcategoryid"))
                {
                    IsEqualOrNull((EntityReference)productenquiry["pnl_stockcategoryid"], values.StockCategory, @"Stock Category");
                    productenquiry["pnl_stockcategoryid"] = values.StockCategory;
                }
                else
                {
                    productenquiry.Attributes.Add("pnl_stockcategoryid", values.StockCategory);
                }

                if (productenquiry.Contains("pnl_newusedid"))
                {
                    IsEqualOrNull((EntityReference)productenquiry["pnl_newusedid"], values.NewUsed, @"New / Used");
                    productenquiry["pnl_newusedid"] = values.NewUsed;
                }
                else
                {
                    productenquiry.Attributes.Add("pnl_newusedid", values.NewUsed);
                }
                HasUpdated = true;
            }

            #endregion

            #region Update if Stock Group is selected

            if (productenquiry.Contains("pnl_stockgroup") && productenquiry["pnl_stockgroup"] != null && !HasUpdated)
            {
                Guid stockgroupid = ((EntityReference)productenquiry["pnl_stockgroup"]).Id;
                ProductHierarchyValues values = GetProductHierarchyByStockGroup(stockgroupid);

                if (productenquiry.Contains("pnl_stockclassmasterid"))
                {
                    IsEqualOrNull((EntityReference)productenquiry["pnl_stockclassmasterid"], values.StockClassMaster, @"Stock Class");
                    productenquiry["pnl_stockclassmasterid"] = values.StockClassMaster;

                    if (productenquiry.Contains("pnl_name"))
                    {
                        productenquiry["pnl_name"] = GetProductEnquiryNameOnUpdate(values.StockClassMaster.Name, createdon);
                    }
                    else
                    {
                        productenquiry.Attributes.Add("pnl_name", GetProductEnquiryNameOnUpdate(values.StockClassMaster.Name, createdon));
                    }
                }
                else
                {
                    productenquiry.Attributes.Add("pnl_stockclassmasterid", values.StockClassMaster);

                    if (productenquiry.Contains("pnl_name"))
                    {
                        productenquiry["pnl_name"] = GetProductEnquiryNameOnUpdate(values.StockClassMaster.Name, createdon);
                    }
                    else
                    {
                        productenquiry.Attributes.Add("pnl_name", GetProductEnquiryNameOnUpdate(values.StockClassMaster.Name, createdon));
                    }
                }

                if (productenquiry.Contains("pnl_stockcategoryid"))
                {
                    IsEqualOrNull((EntityReference)productenquiry["pnl_stockcategoryid"], values.StockCategory, @"Stock Category");
                    productenquiry["pnl_stockcategoryid"] = values.StockCategory;

                }
                else
                {
                    productenquiry.Attributes.Add("pnl_stockcategoryid", values.StockCategory);
                }

                if (productenquiry.Contains("pnl_newusedid"))
                {
                    IsEqualOrNull((EntityReference)productenquiry["pnl_newusedid"], values.NewUsed, @"New / Used");
                    productenquiry["pnl_newusedid"] = values.NewUsed;
                }
                else
                {
                    productenquiry.Attributes.Add("pnl_newusedid", values.NewUsed);
                }
                HasUpdated = true;
            }

            #endregion

            #region Update if Stock Class Master has value

            if (productenquiry.Contains("pnl_stockclassmasterid") && productenquiry["pnl_stockclassmasterid"] != null && !HasUpdated)
            {
                ProductHierarchyValues values = GetProductHierarchyByStockClassMaster((EntityReference)productenquiry["pnl_stockclassmasterid"]);

                if (productenquiry.Contains("pnl_name"))
                {
                    productenquiry["pnl_name"] = GetProductEnquiryNameOnUpdate(values.StockClassName, createdon);
                }
                else
                {
                    productenquiry.Attributes.Add("pnl_name", GetProductEnquiryNameOnUpdate(values.StockClassName, createdon));
                }

                if (productenquiry.Contains("pnl_stockcategoryid"))
                {
                    //IsEqualOrNull((EntityReference)productenquiry["pnl_stockcategoryid"], values.StockCategory, @"Stock Category");
                    productenquiry["pnl_stockcategoryid"] = values.StockCategory;
                }
                else
                {
                    productenquiry.Attributes.Add("pnl_stockcategoryid", values.StockCategory);
                }

                if (productenquiry.Contains("pnl_newusedid"))
                {
                    IsEqualOrNull((EntityReference)productenquiry["pnl_newusedid"], values.NewUsed, @"New / Used");
                    productenquiry["pnl_newusedid"] = values.NewUsed;
                }
                else
                {
                    productenquiry.Attributes.Add("pnl_newusedid", values.NewUsed);
                }
                HasUpdated = true;
            }

            #endregion

            _Service.Update(productenquiry);
        }

        private bool IsEqualOrNull(EntityReference ref1, EntityReference ref2, string fieldname)
        {
            if (ref1 == null)
            {
                return true;
            }

            if (ref2 == null)
            {
                throw new InvalidPluginExecutionException("Please contact your CRM System Administrator");
            }

            if (ref1.Id.Equals(ref2.Id))
            {
                return true;
            }
            if (fieldname.Equals("Stock Category"))
            {
                //throw new InvalidPluginExecutionException("Please contact your CRM System Administrator, the Stock Category needs to be setup");
                return true;
            }
            else
            {
                throw new InvalidPluginExecutionException(fieldname + " does not match the items selected beneath it");
            }
        }

        private string GetProductEnquiryNameOnCreate(string stockclassname)
        {
            return DateTime.Today.ToString("dd-MMM-yy") + " " + stockclassname;
        }

        private string GetProductEnquiryNameOnUpdate(string stockclassname, DateTime? createon)
        {
            if (createon == null)
            {
                return DateTime.Today.ToString("dd-MMM-yy") + " " + stockclassname;
            }
            else
            {
                return ((DateTime)createon).ToString("dd-MMM-yy") + " " + stockclassname;
            }
        }

        private EntityReference GetStockCategoryId(Guid stockgroupid)
        {
            Guid stockcategoryid = ZEROGUID;

            ColumnSet attributes = new ColumnSet(new string[] { "pnl_stockclassmaster"});
            Entity stockgroup = _Service.Retrieve("new_stockgroup", stockgroupid, attributes);

            if (stockgroup != null && stockgroup.Contains("pnl_stockclassmaster") && stockgroup["pnl_stockclassmaster"] != null)
            {
                Guid stockclassmasterid = ((EntityReference)stockgroup["pnl_stockclassmaster"]).Id;
                attributes = new ColumnSet(new string[] { "pnl_stockcategory", "pnl_newused" });
                Entity stockclassmaster = _Service.Retrieve("pnl_stockclassmaster", stockclassmasterid, attributes);
                if (stockclassmaster != null && stockclassmaster.Contains("pnl_stockcategory") && stockclassmaster["pnl_stockcategory"] != null)
                {
                    stockcategoryid = ((EntityReference)stockclassmaster["pnl_stockcategory"]).Id;
                }
            }
            if (stockcategoryid == ZEROGUID)
            {
                return null;
            }
            return new EntityReference("new_stockcategory", stockcategoryid);
        }

        private ProductHierarchyValues GetProductHierarchyByProntoModel(Guid prontomodelid)
        {
            ProductHierarchyValues values = new ProductHierarchyValues();
            values.StockGroup = GetStockGroup(prontomodelid);
            if (values.StockGroup != null)
            {
                values.StockClassMaster = GetStockClassMasterId(values.StockGroup.Id);
                if(values.StockClassMaster != null){
                    GetTop3Hierarchy(ref values);
                }
            }
            return values;
        }

        private ProductHierarchyValues GetProductHierarchyByStockGroup(Guid stockgroupid)
        {
            ProductHierarchyValues values = new ProductHierarchyValues();
            values.StockGroup = new EntityReference("new_stockgroup",stockgroupid);
            if (values.StockGroup != null)
            {
                values.StockClassMaster = GetStockClassMasterId(values.StockGroup.Id);
                if (values.StockClassMaster != null)
                {
                    GetTop3Hierarchy(ref values);
                }
            }
            return values;
        }

        private ProductHierarchyValues GetProductHierarchyByStockClassMaster(EntityReference stockclassmaster)
        {
            ProductHierarchyValues values = new ProductHierarchyValues();
            values.StockClassMaster = new EntityReference(stockclassmaster.LogicalName,stockclassmaster.Id);
            GetTop3Hierarchy(ref values);
            return values;
        }

        private EntityReference GetStockGroup(Guid prontomodelid)
        {
            ColumnSet attributes = new ColumnSet(new string[] { "pnl_stockgroupid" });
            Entity prontomodel = _Service.Retrieve("pnl_prontomodel", prontomodelid, attributes);

            if (prontomodel != null && prontomodel.Contains("pnl_stockgroupid") && prontomodel["pnl_stockgroupid"] != null)
            {
                return (EntityReference)prontomodel["pnl_stockgroupid"];
            }
            return null;
        }

        private EntityReference GetStockClassMasterId(Guid stockgroupid)
        {
            ColumnSet attributes = new ColumnSet(new string[] { "pnl_stockclassmaster" });
            Entity stockgroup = _Service.Retrieve("new_stockgroup", stockgroupid, attributes);

            if (stockgroup != null && stockgroup.Contains("pnl_stockclassmaster") && stockgroup["pnl_stockclassmaster"] != null)
            {
                return (EntityReference)stockgroup["pnl_stockclassmaster"];
            }
            return null;
        }

        private void GetTop3Hierarchy(ref ProductHierarchyValues values)
        {
            ColumnSet attributes = new ColumnSet(new string[] { "pnl_stockcategory", "pnl_newused", "pnl_name" });
            Entity stockclassmaster = _Service.Retrieve("pnl_stockclassmaster", values.StockClassMaster.Id, attributes);

            if (stockclassmaster != null)
            {
                if (stockclassmaster.Contains("pnl_name") && stockclassmaster["pnl_name"] != null)
                {
                    values.StockClassName = (string)stockclassmaster["pnl_name"];
                }                
                if (stockclassmaster.Contains("pnl_stockcategory") && stockclassmaster["pnl_stockcategory"] != null)
                {
                    values.StockCategory = (EntityReference)stockclassmaster["pnl_stockcategory"];
                }
                if (stockclassmaster.Contains("pnl_newused") && stockclassmaster["pnl_newused"] != null)
                {
                    values.NewUsed = (EntityReference)stockclassmaster["pnl_newused"];
                }
            }
        }
        
    }

}