﻿using Microsoft.Xrm.Sdk;
namespace LandPowerPlugins
{
    public class LandpowerConst
    {
        internal const int PNL_OPTIONSET_FALSE = 0;
        internal const int PNL_OPTIONSET_TRUE = 1;
    }
	
	public class LandpowerPluginBase
    {
        internal const int YES_OR_NO_FALSE = 0;
        internal const int YES_OR_NO_TRUE = 1;

        internal enum DiscountType
        {
            STANDARD = 125760000,
            PROGRAMME = 125760001,
            INDENT = 125760002,
            SERIAL_NO = 125760003
        }

        internal enum SpecificationType
        {
            PRODUCT = 125760000,
            ITEM = 0,
            LABEL = 125760001,
            GROUP = 125760002,
            SECTION = 125760003
        }

        internal enum SalesAidApprovedStatus
        {
            APPROVED = 20,
            DECLINED = 30,
            IN_PROGRESS = 10,
            _DoubleDashes = 100000000 //12032013 MT: Added to accomodate default changes in CRM 2013 (this may not be in option sets in other version of CRM)
        }

        internal const char HYPHEN_CHAR = '-';
        internal const decimal DECIMAL100 = 100.0M;
        internal const decimal DECIMAL0 = 0.0M;
        internal const decimal DECIMAL1 = 1.0M;

        internal static decimal ValueAsDecimal(Money field)
        {
            return field == null ? DECIMAL0 : field.Value;
        }

        internal static decimal ValueAsDecimal(decimal? field)
        {
            return field.HasValue ? field.Value : DECIMAL0;
        }
    }
}
