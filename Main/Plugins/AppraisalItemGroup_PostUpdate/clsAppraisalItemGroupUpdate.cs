﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LandPowerPlugins.Extensions;

namespace LandPowerPlugins
{
    class AppraisalItemGroupUpdate
    {

        public AppraisalItemGroupUpdate(IOrganizationService service, IPluginExecutionContext context, CrmServiceContext crmContext, Entity postImageEntity, Entity updateEntity)
        {
            _service = service;
            _context = context;
            _crmContext = crmContext;
            _postImageEntity = postImageEntity;
            _inputEntity = updateEntity;
        }

        public void ExecuteAppraisalItemGroupUpdate()
        {
            var appraisalItemGroup = _postImageEntity.ToEntity<pnl_appraisalitemgroup>();
            if (null == appraisalItemGroup) return;

            if (appraisalItemGroup.pnl_AppraisalId == null) return;

            CheckCanUpdateAppraisal(appraisalItemGroup.pnl_AppraisalId.Id);

            if (_context.MessageName.ToLower() != "update") return;

            if (null == _inputEntity || !_inputEntity.Contains("pnl_appraisalid")) return;

            var inputAppraisalId = ((EntityReference)_inputEntity["pnl_appraisalid"]).Id;
            if (inputAppraisalId == appraisalItemGroup.pnl_AppraisalId.Id) return;
            CheckCanUpdateAppraisal(inputAppraisalId);

            //update the pnl_ItemUpdatedField as per tech spec PL002
            var appraisal = _crmContext.pnl_appraisalSet.Where(x => x.Id == inputAppraisalId).FirstOrDefault();
            appraisal.pnl_itemupdatedon = DateTime.Now;
            _service.Update(appraisal);

        }

        public void ExecuteAppraisalItemGroupNamesUpdate()
        {
            UpdateAppraisalItemGroupNames(_context.PrimaryEntityId);
        }

        internal void UpdateAppraisalItemGroupNames(Guid appraisalId)
        {            
            var itemGroups = _crmContext.Appraisal().FetchAppraisalItemGroups(appraisalId).ToList();

            foreach (var itemGroup in itemGroups)
            {
                var entity = new Entity("pnl_appraisalitemgroup") { Id = itemGroup.Id };
                entity.Attributes.Add("pnl_name", DateTime.UtcNow.ToString("O"));
                _service.Update(entity);
            }
        }


        internal void CheckCanUpdateAppraisal(Guid appraisalId)
        {
            var appraisal = _crmContext.Appraisal().FetchAppraisalById(appraisalId);
            var canUpdate = appraisal.CanApplyChanges(_context.InitiatingUserId, _crmContext);

            if (canUpdate) return;

            throw new InvalidPluginExecutionException("Permission denied. The Appraisal can be updated if it is in inspection or you are an approver and it is waiting for approval.");
        }

        #region Private Vars

        IOrganizationService _service;
        CrmServiceContext _crmContext;
        IPluginExecutionContext _context;
        private Entity _postImageEntity;
        private Entity _inputEntity;

        #endregion
    }
}
