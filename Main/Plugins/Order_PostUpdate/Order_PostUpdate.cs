﻿using System;
using System.Linq;
using System.ServiceModel;
using Microsoft.Xrm.Sdk;

using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using System.Data.SqlTypes;
using System.Text;
using System.Collections.Generic;
using CrmEarlyBound;


namespace LandPowerPlugins
{
    public class Order_PostUpdate : IPlugin
    {
        #region Private Vars

        IOrganizationService _service;
        CrmServiceContext _ctx;
        Entity _updateEntity;

        #endregion

        #region Public Methods

        public void Execute(IServiceProvider serviceProvider)
        {
            #region Startup initialisation and checks
            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            // Obtain the execution context from the service provider.
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            // don't want to redo create logic when resyncing online
            if (context.IsOfflinePlayback)
                return;

            // The InputParameters collection contains all the data passed in the message request.
            if (!context.InputParameters.Contains("Target") || !(context.InputParameters["Target"] is Entity))
                return;

            if (context.Depth > 3)
                return;

            // Obtain the target entity from the input parameters.
            _updateEntity = (Entity)context.InputParameters["Target"];

            // Verify that the target entity represents an appraisal.
            // If not, this plug-in was not registered correctly.
            if (_updateEntity.LogicalName != "salesorder")
                return;

            ExecuteMultipleRequest requestWithContinueOnError =
                new ExecuteMultipleRequest() { Settings = new ExecuteMultipleSettings() { ContinueOnError = false, ReturnResponses = true }, Requests = new OrganizationRequestCollection() };
            #endregion

            try
            {

                #region Initialise context-related variables
                // Obtain the organization service reference.
                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                _service = serviceFactory.CreateOrganizationService(context.UserId);

                // Use reference to invoke pre-built types service 
                _ctx = new CrmServiceContext(_service);

                Entity appraisalPreImageEntity = null;
                if (context.PreEntityImages.Contains("PreImage") && (context.PreEntityImages["PreImage"] is Entity))
                {
                    appraisalPreImageEntity = (Entity)context.PreEntityImages["PreImage"];
                }
                if (appraisalPreImageEntity == null || appraisalPreImageEntity.LogicalName != "salesorder")
                {
                    throw new InvalidPluginExecutionException("Incorrect Plugin registration: a Pre image has not been correctly configured for this plugin.");
                }
                #endregion
            }
            #region catch - Exception handling
            catch (FaultException<OrganizationServiceFault> ex)
            {
                throw new InvalidPluginExecutionException(string.Format("An error occurred in the Order Update plug-in:-{0}{1}", Environment.NewLine, ex.Message));
            }
            #endregion
        }

        #endregion

        #region Private Methods



        #endregion

    }
}
