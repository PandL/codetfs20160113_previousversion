﻿using LandPower;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using System;
using System.ServiceModel;
using System.Linq;
using Microsoft.Crm.Sdk.Messages;

namespace LandPowerPlugins
{
    public class QuotePluginActivate : LandpowerPluginBase, IPlugin
    {
        IOrganizationService _service;

        public void Execute(IServiceProvider serviceProvider)
        {
            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService =
                (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            // Obtain the execution context from the service provider.
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.Depth > 1)
                return;

            try
            {
                // Obtain the organization service reference.
                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                _service = serviceFactory.CreateOrganizationService(context.UserId);

                // next get the PreImage of the quote
                Entity quotePreImageEntity = null;
                if (context.PreEntityImages.Contains("quote") && (context.PreEntityImages["quote"] is Entity))
                {
                    quotePreImageEntity = (Entity)context.PreEntityImages["quote"];
                }
                if (quotePreImageEntity == null || quotePreImageEntity.LogicalName != "quote")
                {
                    throw new InvalidPluginExecutionException("Incorrect Plugin registration: a Pre image has not been correctly configured for this plugin.");
                }

                if (context.InputParameters.Contains("EntityMoniker") && context.InputParameters["EntityMoniker"] is EntityReference)
                {
                    if (context.MessageName == "SetStateDynamicEntity" && context.InputParameters.Contains("State"))
                    {
                        // if User is attempting to ACTIVATE the Quote
                        if (((OptionSetValue)context.InputParameters["State"]).Value == 1)
                        {
                            Quote quotePreImage = quotePreImageEntity.ToEntity<Quote>();

                            // if sales aid has been requested
                            if (quotePreImage.pnl_SalesAidRequested != null && quotePreImage.pnl_SalesAidRequested.Value != DECIMAL0)
                            {
                                //12032013 MT: updated to accomodate default changes in CRM 2013 (this may not be in option sets in other version of CRM)
                                if (quotePreImage.pnl_SalesAidApprovedStatus == null || (SalesAidApprovedStatus)quotePreImage.pnl_SalesAidApprovedStatus.Value == SalesAidApprovedStatus.IN_PROGRESS || (SalesAidApprovedStatus)quotePreImage.pnl_SalesAidApprovedStatus.Value == SalesAidApprovedStatus._DoubleDashes)
                                    throw new InvalidPluginExecutionException("Cannot activate this Quote as Sales Aid was requested but Sales Aid has not been approved or declined.");

                            // if non-zero sales aid is requested and no sales aid has been approved then cannot Activate the Quote
                                if ((quotePreImage.pnl_SalesAidApprovedStatus != null && (SalesAidApprovedStatus)quotePreImage.pnl_SalesAidApprovedStatus.Value == SalesAidApprovedStatus.APPROVED)
                                    && (quotePreImage.pnl_SalesAidApproved == null))
                                    throw new InvalidPluginExecutionException("Cannot activate this Quote as Sales Aid was requested but no Sales Aid has been approved.");

                                // If Sales Aid Approved Reason [pnl_SalesAidApprovedReason] is blank, and Sales Aid Approved [pnl_salesaidapproved] has a value, do not allow activation
                                if (((quotePreImage.pnl_SalesAidApprovedStatus != null && (SalesAidApprovedStatus)quotePreImage.pnl_SalesAidApprovedStatus.Value == SalesAidApprovedStatus.APPROVED)
                                    || (quotePreImage.pnl_SalesAidApprovedStatus != null && (SalesAidApprovedStatus)quotePreImage.pnl_SalesAidApprovedStatus.Value == SalesAidApprovedStatus.DECLINED))
                                    && (quotePreImage.pnl_SalesAidApprovedReason == null && quotePreImage.pnl_SalesAidApproved != null))
                                    throw new InvalidPluginExecutionException("You cannot approve Sales Aid without a reason.");                                
                            }
                        }
                    }
                }

                // check trade in machines are valid to change the status to active
                if (context.MessageName == "SetStateDynamicEntity" && context.InputParameters.Contains("State"))
                {
                    if (((OptionSetValue)context.InputParameters["State"]).Value == 1)
                    {
                        crmServiceContext _ctx = new crmServiceContext(_service);                                      
                        var qMachines = from qm in _ctx.pnl_quotemachineSet
                                        where qm.pnl_Quoteid.Id.Equals(quotePreImageEntity.Id)
                                        select qm;
                        foreach (pnl_quotemachine qmac in qMachines)
                        {
                            if (qmac.pnl_Configurationid == null)
                            {
                                throw new InvalidPluginExecutionException("Please make sure all Trade-in Machines have a value in the Configuration column");
                            }
                            var machine = (from m in _ctx.pnl_machineSet
                                           where m.Id.Equals(qmac.pnl_Machineid.Id)
                                           select m).FirstOrDefault();
                            if (quotePreImageEntity.Contains("customerid") && quotePreImageEntity.Attributes["customerid"] != null)
                            {
                                EntityReference quoteCustomer = (EntityReference)quotePreImageEntity.Attributes["customerid"];
                                if (machine.pnl_CurrentCustomerid.Id != quoteCustomer.Id)
                                {
                                    throw new InvalidPluginExecutionException("Machine - " + machine.pnl_name + " is owned by different Customer");
                                }                                
                                if (machine.statecode.Value == pnl_machineState.Inactive)
                                {
                                    throw new InvalidPluginExecutionException("Machine - " + machine.pnl_name + " is already sold");
                                }
                                var app = (from a in _ctx.pnl_appraisalSet
                                           where a.pnl_Machineid.Id.Equals(machine.Id)
                                           orderby a.pnl_appraisaldate descending
                                           select a).FirstOrDefault();
                                int appraisal_approved = 125760001;
                                if (app != null)
                                {
                                    if (app.statuscode != null && app.statuscode.Value != appraisal_approved)
                                    {
                                        throw new InvalidPluginExecutionException("Appraisal attached to machine - " + machine.pnl_name + " is not approved");
                                    }
                                    if (app.pnl_ValuationApprovedOn < DateTime.Now.AddMonths(-3))
                                    {
                                        throw new InvalidPluginExecutionException("Machine - " + machine.pnl_name + " has expired");
                                    }
                                }
                            }
                        }
                    }
                }
               // John - 8/08/2014: Deactivate Locally Source Items when Quote is Activated
                if (context.MessageName == "SetStateDynamicEntity" && context.InputParameters.Contains("State"))
                {
                    if (((OptionSetValue)context.InputParameters["State"]).Value == 1)
                    {
                        crmServiceContext _ctx = new crmServiceContext(_service);

                        var items = from lsi in _ctx.pnl_locallysourceditemSet
                                    where lsi.pnl_Quote.Id.Equals(quotePreImageEntity.Id)
                                    select new
                                    {
                                        Id = lsi.Id,
                                        statecode = lsi.statecode,
                                        LogicalName = lsi.LogicalName
                                    };
                        
                        foreach (var item in items)
                        {
                            SetStateRequest setStateRequest = new SetStateRequest()
                            {
                                EntityMoniker = new EntityReference
                                {
                                    Id = item.Id,
                                    LogicalName = item.LogicalName,
                                },
                                State = new OptionSetValue(1),
                                Status = new OptionSetValue(2)
                            };
                            _service.Execute(setStateRequest);
                        }
                    }

                }
            }

            catch (FaultException<OrganizationServiceFault> ex)
            {
                throw new InvalidPluginExecutionException(string.Format("An error occurred in the Quote Activate plug-in:-{0}{1}", Environment.NewLine, ex.Message));                    
            }
        }
    }
}
