using System;
using System.Linq;
using System.ServiceModel;
using Microsoft.Xrm.Sdk;

using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using System.Data.SqlTypes;
using System.Text;
using System.Collections.Generic;


namespace LandPowerPlugins
{
    public class AppraisalItem_CreateUpdate : IPlugin
    {
        #region Private Vars

        IOrganizationService _service;
        CrmServiceContext _ctx;
        Entity _updateEntity;

        #endregion

        #region Public Methods

        public void Execute(IServiceProvider serviceProvider)
        {
            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            // Obtain the execution context from the service provider.
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            // don't want to redo create logic when resyncing online
            if (context.IsOfflinePlayback)
                return;

            // The InputParameters collection contains all the data passed in the message request.
            if (!context.InputParameters.Contains("Target") || !(context.InputParameters["Target"] is Entity))
                return;

            if (context.Depth > 3) return;

            // Obtain the target entity from the input parameters.
            _updateEntity = (Entity)context.InputParameters["Target"];

            //ignore for now as we want to run this in the debugger
            if (_updateEntity.Attributes.Contains("pnl_syncupdatedon"))
            {
                return;
            }

            // Verify that the target entity represents an appraisal.
            // If not, this plug-in was not registered correctly.
            if (_updateEntity.LogicalName != "pnl_appraisalitem")
                return;

            ExecuteMultipleRequest requestWithContinueOnError = 
                new ExecuteMultipleRequest() { Settings = new ExecuteMultipleSettings() { ContinueOnError = false, ReturnResponses = true }, Requests = new OrganizationRequestCollection() };

            try
            {
                // Obtain the organization service reference.
                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                _service = serviceFactory.CreateOrganizationService(context.UserId);

                // Use reference to invoke pre-built types service 
                _ctx = new CrmServiceContext(_service);

                Entity appraisalPreImageEntity = null;
                if (context.PreEntityImages.Contains("PreImage") && (context.PreEntityImages["PreImage"] is Entity))
                {
                    appraisalPreImageEntity = (Entity)context.PreEntityImages["PreImage"];
                }
                if (appraisalPreImageEntity == null || appraisalPreImageEntity.LogicalName != "pnl_appraisalitem")
                {
                    throw new InvalidPluginExecutionException("Incorrect Plugin registration: a Pre image has not been correctly configured for this plugin.");
                }

                pnl_appraisalitem  appraisalUpdate = _updateEntity.ToEntity<pnl_appraisalitem>();
                pnl_appraisalitem appraisalPreImage = appraisalPreImageEntity.ToEntity<pnl_appraisalitem>();

                var appraisal = (from a in _ctx.pnl_appraisalSet
                                 where a.Id == appraisalUpdate.pnl_AppraisalId.Id
                                 select a).FirstOrDefault();

                appraisal.pnl_itemupdatedon = DateTime.Now.ToLocalTime();
                _service.Update(appraisal);
                

            }
            #region catch - Exception handling
            catch (FaultException<OrganizationServiceFault> ex)
            {
               throw new InvalidPluginExecutionException(string.Format("An error occurred in the Appraisal Update plug-in:-{0}{1}", Environment.NewLine, ex.Message) );
            }
            #endregion
        }

        #endregion

        #region Private Methods

        

        #endregion

    }
}
