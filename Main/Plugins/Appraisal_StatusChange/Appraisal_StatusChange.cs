﻿using CrmEarlyBound;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LandPower
{
    public class Appraisal_StatusChange: IPlugin
    {
        #region Private Variables
        IOrganizationService _service;
        CrmServiceContext _ctx;
        #endregion

        public void Execute(IServiceProvider serviceProvider)
        {
            #region Startup initialisation and checks
            //Extract the tracing service for use in debugging sandboxed plug-ins.
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            // Obtain the execution context from the service provider.
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            // don't want to redo create logic when resyncing online
            if (context.IsOfflinePlayback)
                return;

            // The InputParameters collection contains all the data passed in the message request.
            if (!context.InputParameters.Contains("Target") || !(context.InputParameters["Target"] is Entity))
                return;

            if (context.Depth > 3) return;

            #endregion

            #region Initialise context-related variables
            // Obtain the organization service reference.
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            _service = serviceFactory.CreateOrganizationService(context.UserId);

            // Use reference to invoke pre-built types service 
            _ctx = new CrmServiceContext(_service);

            Entity appraisalPreImageEntity = null;
            if (context.PreEntityImages.Contains("PreImage") && (context.PreEntityImages["PreImage"] is Entity))
            {
                appraisalPreImageEntity = (Entity)context.PreEntityImages["PreImage"];
            }
            if (appraisalPreImageEntity == null || appraisalPreImageEntity.LogicalName != "pnl_appraisal")
            {
                throw new InvalidPluginExecutionException("Incorrect Plugin registration: a 'pnl_appraisal' Pre-image has not been correctly configured for this plugin.");
            }
            #endregion

            var preImage = context.PreEntityImages["preimage"];
            pnl_appraisal thisAppraisal = preImage.ToEntity<pnl_appraisal>();

            CheckCanApplyChanges(context.InitiatingUserId, thisAppraisal);

            var inputParameters = context.InputParameters;
            if (inputParameters.Contains("EntityMoniker") && inputParameters["EntityMoniker"] is EntityReference)
            {
                // Note, inputParameters["EntityMoniker"] is an EntityReference.
                OptionSetValue preStatus = preImage.GetAttributeValue<OptionSetValue>("statuscode");
                OptionSetValue status = (OptionSetValue)inputParameters["Status"];

                // if we are attempting to approve this appraisal and other checks have been passed then
                // chekc a Customer and a Machine have both been specified for this Appraisal before allowing Approval.
                if (status.Value == (int)pnl_appraisal_statuscode.ApprovedAppraisal ||
                    status.Value == (int)pnl_appraisal_statuscode.ApprovedApprasial)
                {
                    if (thisAppraisal.pnl_Customerid == null)
                    {
                        const string message = "A Customer must be specified before the Appraisal can be approved.";
                        throw new InvalidPluginExecutionException(message);
                    }

                    if (thisAppraisal.pnl_Machineid == null)
                    {
                        const string message = "A Machine must be specified before the Appraisal can be approved.";
                        throw new InvalidPluginExecutionException(message);
                    }
                }

                if (preStatus.Value == (int)pnl_appraisal_statuscode.Inspection_1 && status.Value != preStatus.Value)
                {
                    SetAppraisalItemsReadOnly(preImage.Id);
                }
            }
        }

        private void CheckCanApplyChanges(Guid userId, pnl_appraisal thisAppraisal)
        {
            bool canUpdate = false;

            var user = new SystemUser { Id = userId };

            if (thisAppraisal.statuscode.Value == (int)pnl_appraisal_statuscode.ApprovedApprasial
             || thisAppraisal.statuscode.Value == (int)pnl_appraisal_statuscode.ApprovedAppraisal)
            {
                canUpdate = IsInRole(userId, "System Administrator");
            }

            else if (thisAppraisal.statuscode.Value == (int)pnl_appraisal_statuscode.Inspection_1 ||
                thisAppraisal.statuscode.Value == (int)pnl_appraisal_statuscode.Inspection_2)
            {
                canUpdate = true;
            }
            else
            {
                canUpdate = IsTeamMember(userId, "Appraisal Approvers");
            }

            if (!canUpdate)
            {
                const string message =
                    "Permission denied. The Appraisal can be updated if it is in inspection, or you are an approver and it is waiting for approval, " +
                        "or you are a System Administrator and it has been Approved.";
                throw new InvalidPluginExecutionException(message);
            }
        }

        private void SetAppraisalItemsReadOnly(Guid appraisalId)
        {
            var appraisalItems = _ctx.pnl_appraisalitemSet.Where(x => x.pnl_AppraisalId != null && x.pnl_AppraisalId.Id.Equals(appraisalId)).ToList();
            foreach (var item in appraisalItems)
            {
                var entity = new Entity("pnl_appraisalitem") { Id = item.Id  };
                entity.Attributes.Add("pnl_isreadonly", true);
                _service.Update(entity);
            }
        }

        private bool IsTeamMember(Guid userId, string teamName)
        {
            var teamMember = (from tm in _ctx.TeamMembershipSet
                              join t in _ctx.TeamSet on tm.TeamId.GetValueOrDefault() equals t.Id
                              where tm.SystemUserId.GetValueOrDefault() == userId && t.Name == teamName
                              select tm).FirstOrDefault();

            return teamMember != null;
        }

        private bool IsInRole(Guid userId, string roleName)
        {
            var userRole = (from s in _ctx.SystemUserRolesSet
                            join r in _ctx.RoleSet on s.RoleId.Value equals r.RoleId.Value
                            where s.SystemUserId.Value == userId
                                && r.Name == roleName
                            select s).FirstOrDefault();

            return userRole != null;
        }
    }
}
