﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LandPowerPlugins
{
    public interface IAppraisalPhotoExtensions
    {
        pnl_appraisalphoto FetchAppraisalPhotoById(Guid appraisalPhotoId);

        int GetPhotoCount(Guid appraisalPhotoId);
    }

    public static class AppraisalPhotoExtension
    {
        public static Func<CrmServiceContext, IAppraisalPhotoExtensions> AppraisalPhotoFactory = serviceContext => new AppraisalPhotoExtensions(serviceContext);

        public static IAppraisalPhotoExtensions AppraisalPhoto(this CrmServiceContext serviceContext)
        {
            return AppraisalPhotoFactory(serviceContext);
        }
    }

    internal class AppraisalPhotoExtensions : IAppraisalPhotoExtensions
    {
        private readonly CrmServiceContext _serviceContext;

        public AppraisalPhotoExtensions(CrmServiceContext serviceContext)
        {
            _serviceContext = serviceContext;
        }

        public int GetPhotoCount(Guid appraisalPhotoId)
        {
            // Note, returning select a.Id).Count() causes an error.
            var notes = (from a in _serviceContext.AnnotationSet
                         where a.ObjectId.Id == appraisalPhotoId
                             && a.IsDocument.GetValueOrDefault()
                         select a.Id).ToList();
            return notes.Count();
        }

        public pnl_appraisalphoto FetchAppraisalPhotoById(Guid appraisalPhotoId)
        {
            return (from a in _serviceContext.pnl_appraisalphotoSet where a.Id == appraisalPhotoId select a).FirstOrDefault();
        }
    }
}
