﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LandPowerPlugins.Extensions;

namespace LandPowerPlugins
{
    //clear fields on Appraisal Revert
    class AppraisalEdit
    {
        public AppraisalEdit(IOrganizationService service, IPluginExecutionContext context, CrmServiceContext crmContext, Entity preImageEntity, Entity postImageEntity, Entity updateEntity)
        {
            _service = service;
            _context = context;
            _crmContext = crmContext;
            _preImageEntity = preImageEntity;
            _postImageEntity = postImageEntity;
            _inputEntity = updateEntity;
        }

        public void CheckCanEdit()
        {
            var appraisal = _preImageEntity == null
                ? _postImageEntity == null ? null : _postImageEntity.ToEntity<pnl_appraisal>()
                : _preImageEntity.ToEntity<pnl_appraisal>();
            if (appraisal == null) return;

            var canUpdate = appraisal.CanApplyChanges(_context.InitiatingUserId, _crmContext);
            if (!canUpdate)
            {
                const string message =
                    "Permission denied. The Appraisal can be updated if it is in inspection, or you are an approver and it is waiting for approval, " +
                        "or you are a System Administrator and it has been Approved.";
                throw new InvalidPluginExecutionException(message);
            }

            if (_context.MessageName.ToLower() == "delete" || _preImageEntity == null) return;

            if (_inputEntity.Contains("statuscode"))
            {
                var preStatus = _preImageEntity.GetAttributeValue<OptionSetValue>("statuscode");
                var status = _inputEntity.GetAttributeValue<OptionSetValue>("statuscode");
                if (status.Value == (int)pnl_appraisal_statuscode.Inspection_1)
                {
                    // Reset fields so the appraisal can be reappraised.
                    //SetInputAttribute(_inputEntity, "pnl_appraisaldate", null);
                    SetInputAttribute(_inputEntity, "pnl_approvedtradeprice", new Money(0m));
                    SetInputAttribute(_inputEntity, "pnl_approvalrequestedon", null);
                    SetInputAttribute(_inputEntity, "pnl_valuationapprovedby", null);
                    SetInputAttribute(_inputEntity, "pnl_valuationapprovedon", null);
                    SetInputAttribute(_inputEntity, "pnl_primaryapprover", null);

                    _preImageEntity.ToEntity<pnl_appraisal>().SetAppraisalItemsReadOnly(_service, false);
                }
                if (preStatus.Value == (int)pnl_appraisal_statuscode.ApprovedApprasial_125760001 && status.Value != preStatus.Value)
                {
                    // Note, this conditional check is made separately to check above because the plug-in will run again on status reason change.
                    if (status.Value == (int)pnl_appraisal_statuscode.Inspection_1)
                    {
                        // Reset fields so the appraisal can be reappraised.
                        //SetInputAttribute(_inputEntity, "pnl_appraisaldate", null);
                        SetInputAttribute(_inputEntity, "pnl_approvedtradeprice", new Money(0m));
                        SetInputAttribute(_inputEntity, "pnl_approvalrequestedon", null);
                        SetInputAttribute(_inputEntity, "pnl_valuationapprovedby", null);
                        SetInputAttribute(_inputEntity, "pnl_valuationapprovedon", null);
                        SetInputAttribute(_inputEntity, "pnl_primaryapprover", null);

                        _preImageEntity.ToEntity<pnl_appraisal>().SetAppraisalItemsReadOnly(_service, false);
                    }
                    // Note, this conditional check is made separately to check above because the plug-in will run again on status reason change.
                    else if (status.Value == (int)pnl_appraisal_statuscode.InspectionWaitingforApproval)
                    {
                        // Reset fields so the appraisal can be reapproved.
                        //SetInputAttribute(_inputEntity, "pnl_appraisaldate", null);
                        /*
                        SetInputAttribute(_inputEntity, "pnl_approvalrequestedon", null);
                         */
                        SetInputAttribute(_inputEntity, "pnl_approvedtradeprice", new Money(0m));
                        SetInputAttribute(_inputEntity, "pnl_valuationapprovedby", null);
                        SetInputAttribute(_inputEntity, "pnl_valuationapprovedon", null);
                        SetInputAttribute(_inputEntity, "pnl_primaryapprover", null);
                        //change made by ranjith
                        // start comment
                        //_preImageEntity.ToEntity<pnl_appraisal>().SetAppraisalItemsReadOnly(OrganizationService, false);
                        //end comment
                        _preImageEntity.ToEntity<pnl_appraisal>().SetAppraisalItemsReadOnly(_service);
                    }
                    else
                    {
                        const string message = "An approved Appraisal may only have its Status Reason changed to \"Inspection\" or \"Inspection - Awaiting Approval\".";
                        throw new InvalidPluginExecutionException(message);
                    }
                }

                if (preStatus.Value == (int)pnl_appraisal_statuscode.Inspection_1 && status.Value != preStatus.Value)
                {
                    // Set appraisalitems readonly when the appraisal leaves the inspection status.
                    _preImageEntity.ToEntity<pnl_appraisal>().SetAppraisalItemsReadOnly(_service);
                }
            }

            UpdateAppraisalItemValues(_context.PrimaryEntityId); // Update appraisalitems if summary/cost value has changed
        }

        #region Internal Methods

        internal void UpdateAppraisalItemValues(Guid appraisalId)
        {
            var calculatedAverage = GetAttributeValueFromInputOrPreImage("pnl_calculatedaverage");
            var totalCosts = GetAttributeValueFromInputOrPreImage("pnl_totalcosts");
            var expectedRetainedMarginDollar = GetAttributeValueFromInputOrPreImage("pnl_expectedretainedmargindollar");
            // TODO: Fix pnl_calcuatedtradeprice attribute name when HK updates CRM customizations.
            var calculatedTradePrice = GetAttributeValueFromInputOrPreImage("pnl_calcuatedtradeprice");
            var repairCost = GetAttributeValueFromInputOrPreImage("pnl_repaircosts");
            var freightCost = GetAttributeValueFromInputOrPreImage("pnl_freight");
            var preDeliveryExpense = GetAttributeValueFromInputOrPreImage("pnl_predeliveryexpenses");
            var otherCosts = GetAttributeValueFromInputOrPreImage("pnl_othercosts");
            var appraisal = new pnl_appraisal { Id = appraisalId };

            appraisal.UpdateAppraisalItemValue("Calculated Average", calculatedAverage, _service, "Valuation");
            appraisal.UpdateAppraisalItemValue("Total Costs", totalCosts, _service);
            appraisal.UpdateAppraisalItemValue("Expected Retained Margin", expectedRetainedMarginDollar, _service);
            appraisal.UpdateAppraisalItemValue("Calculated Trade Price", calculatedTradePrice, _service);
            appraisal.UpdateAppraisalItemValue("Repair Costs", repairCost, _service);
            appraisal.UpdateAppraisalItemValue("Freight", freightCost, _service);
            appraisal.UpdateAppraisalItemValue("PD Expenses", preDeliveryExpense, _service);
            appraisal.UpdateAppraisalItemValue("Other", otherCosts, _service);
        }

        internal Money GetAttributeValueFromInputOrPreImage(string attributeName)
        {
            var value = _inputEntity.GetAttributeValue<Money>(attributeName) ??
                        _preImageEntity.GetAttributeValue<Money>(attributeName);

            return value;
        }

        internal void SetInputAttribute(Entity inputEntity, string attributeName, object attributeValue)
        {
            if (inputEntity.Attributes.Contains(attributeName))
            {
                inputEntity.Attributes[attributeName] = attributeValue;
            }
            else
            {
                inputEntity.Attributes.Add(attributeName, attributeValue);
            }
        }

        #endregion

        #region Private Vars

        IOrganizationService _service;
        private Entity _preImageEntity;
        private Entity _postImageEntity;
        private Entity _inputEntity;
        IPluginExecutionContext _context;
        CrmServiceContext _crmContext;

        #endregion
    }
}
