﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LandPowerPlugins.Extensions;

namespace LandPowerPlugins
{
    class AppraisalStatus
    {
        public AppraisalStatus(IOrganizationService service, IPluginExecutionContext context, CrmServiceContext crmContext, Entity preImageEntity, Entity updateEntity)
        {
            _service = service;
            context = _context;
            _crmContext = crmContext;
            _preImageEntity = preImageEntity;
            _inputEntity = updateEntity;
        }

        public void ExecuteAppraisalStatus()
        {
            var appraisal = _preImageEntity.ToEntity<pnl_appraisal>();
            var canEdit = appraisal.CanApplyChanges(_context.InitiatingUserId, _crmContext);

            if (!canEdit)
            {
                const string message = "Permission denied. The Appraisal can be updated if it is in inspection or you are an approver and it is waiting for approval.";
                throw new InvalidPluginExecutionException(message);
            }

            var inputParameters = _context.InputParameters;
            if (inputParameters.Contains("EntityMoniker") && inputParameters["EntityMoniker"] is EntityReference)
            {
                // Note, inputParameters["EntityMoniker"] is an EntityReference.
                var preStatus = _preImageEntity.GetAttributeValue<OptionSetValue>("statuscode");
                var status = (OptionSetValue)inputParameters["Status"];
                if (preStatus.Value == (int)pnl_appraisal_statuscode.Inspection_1 && status.Value != preStatus.Value)
                {
                    _preImageEntity.ToEntity<pnl_appraisal>().SetAppraisalItemsReadOnly(_service);
                }
            }
        }

        #region Private Vars

        IOrganizationService _service;
        CrmServiceContext _crmContext;
        IPluginExecutionContext _context;
        private Entity _preImageEntity;
        private Entity _inputEntity;

        #endregion
    }
}
