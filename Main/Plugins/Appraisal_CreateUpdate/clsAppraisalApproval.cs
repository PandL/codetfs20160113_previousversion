﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LandPowerPlugins.Extensions;
using Microsoft.Crm.Sdk.Messages;

namespace LandPowerPlugins
{
    class AppraisalApproval
    {
        #region Properties

        public CrmServiceContext Context { get; set; }
        public IPluginExecutionContext LocalContext { get; set; }

        #endregion

        #region Public Methods

        public AppraisalApproval(CrmServiceContext context, IPluginExecutionContext localContext, ITracingService tracingService, IOrganizationService service)
        {
            this.Context = context;
            this.LocalContext = localContext;
            _trace = new Trace(localContext, tracingService);
            _service = service;
        }

        public void TryToApproveAppraisal(Entity postImageEntity, Guid fromUserId)
        {
            try
            {
                ApproveAppraisal(postImageEntity, fromUserId);
            }
            catch (Exception ex)
            {
                _trace.TraceMessage("Inner Exception: " + ex.InnerException);
                _trace.TraceMessage("StackTrace: " + ex.StackTrace);
                throw new InvalidPluginExecutionException(string.Format("AppraisalApprovalPlugin exception: {0}", ex.Message));
            }
        }

        public void TryToRequestApproval(Entity postImageEntity, Guid fromUserId)
        {
            try
            {
                RequestApproval(postImageEntity, fromUserId);
            }
            catch (Exception ex)
            {
                _trace.TraceMessage("Inner Exception: " + ex.InnerException);
                _trace.TraceMessage("StackTrace: " + ex.StackTrace);
                throw new InvalidPluginExecutionException(string.Format("RequestApprovalPlugin exception: {0}", ex.Message));
            }
        }


        public void ApproveAppraisal(Entity postImageEntity, Guid fromUserId)
        {
            var xrmContext = Context;
            var appraisal = postImageEntity.ToEntity<pnl_appraisal>();

            if (appraisal.pnl_ValuationApprovedOn == null) return;

            if (!appraisal.HasRequiredItemValues(xrmContext))
                throw new InvalidPluginExecutionException("Please complete the value of required items.");

            if (!appraisal.HaveAllPhotosUploaded(xrmContext))
                throw new InvalidPluginExecutionException("Please ensure all photos have uploaded.");

            var user = new SystemUser { Id = fromUserId };
            if (!user.IsTeamMember("Appraisal Approvers", xrmContext))
                throw new InvalidPluginExecutionException("Permission denied. You need to be a member of the 'Appraisal Approvers' team.");

            CreateAndSendEmail(appraisal.OwnerId.Id, appraisal, fromUserId);
            UpdateApprovedBy(postImageEntity.Id, fromUserId);
            ChangeStatusToApproved(postImageEntity.Id);
        }

        public void RequestApproval(Entity postImageEntity, Guid fromUserId)
        {
            var xrmContext = Context;
            var appraisal = postImageEntity.ToEntity<pnl_appraisal>();

            if (!appraisal.HasRequiredItemValues(xrmContext))
                throw new InvalidPluginExecutionException("Please complete the value of required items.");

            if (!appraisal.HaveAllPhotosUploaded(xrmContext))
                throw new InvalidPluginExecutionException("Please ensure all photos have uploaded.");

            var approvers = appraisal.GetActivityPartiesForApproval(xrmContext).ToList();
            var primaryApprover =
                approvers.FirstOrDefault(a => a.ParticipationTypeMaskEnum == activityparty_participationtypemask.ToRecipient);
            if (null == primaryApprover) throw new InvalidPluginExecutionException("There are no Approvers available.");

            CreateAndSendEmail(approvers, appraisal, fromUserId);
            UpdatePrimaryApprover(postImageEntity.Id, primaryApprover.PartyId.Id);
            ChangeStatusToInspectionWaitingForApproval(postImageEntity.Id);
        }

        // ** Could refactor similar methods as they did come from different classes in the original, but pretty much do the same thing.
        public void CreateAndSendEmail(Guid toUserId, pnl_appraisal appraisal, Guid fromUserId)
        {
            var subject = "Trade-in Inspection: " + appraisal.pnl_name + " was Approved";
            var description = string.Format("The following Trade-in Inspection was approved.<br/><br/>{0}", appraisal.BuildAppraisalLink(LocalContext.OrganizationName));
            var email = new Email
            {
                To = new[] { new ActivityParty { PartyId = new EntityReference("systemuser", toUserId) } },
                From = new[] { new ActivityParty { PartyId = new EntityReference("systemuser", fromUserId) } },
                Subject = subject,
                Description = description,
                DirectionCode = true, // Outgoing
                RegardingObjectId = new EntityReference("pnl_appraisal", appraisal.Id)
            };
            var emailId = _service.Create(email);

            SendEmail(emailId);
        }

        public void CreateAndSendEmail(List<ActivityParty> activityParties, pnl_appraisal appraisal, Guid fromUserId)
        {
            var subject = "Approval Request for Trade-in Inspection: " + appraisal.pnl_name;
            var description = string.Format("Please review and approve the following Trade-in Inspection.<br/><br/>{0}", appraisal.BuildAppraisalLink(LocalContext.OrganizationName));
            var email = new Email
            {
                To = new[] { activityParties.First(a => a.ParticipationTypeMaskEnum == activityparty_participationtypemask.ToRecipient) },
                From = new[] { new ActivityParty { PartyId = new EntityReference("systemuser", fromUserId) } },
                Cc = activityParties.Where(a => a.ParticipationTypeMaskEnum == activityparty_participationtypemask.CCRecipient),
                Subject = subject,
                Description = description,
                DirectionCode = true, // Outgoing
                RegardingObjectId = new EntityReference("pnl_appraisal", appraisal.Id)
            };
            var emailId = _service.Create(email);

            SendEmail(emailId);
        }


        #endregion

        #region Internal Methods

        // ditto for two methods above. Can refactor later when have time.
        
        internal void UpdateApprovedBy(Guid appraisalId, Guid approverId)
        {
            var appraisal = new pnl_appraisal
            {
                Id = appraisalId,
                pnl_ValuationApprovedBy = new EntityReference("systemuser", approverId)
            };
            _service.Update(appraisal.ToEntity<Entity>());
        }

        internal void UpdatePrimaryApprover(Guid appraisalId, Guid approverId)
        {
            var appraisal = new pnl_appraisal
            {
                Id = appraisalId,
                pnl_PrimaryApprover = new EntityReference("systemuser", approverId)
            };
            _service.Update(appraisal.ToEntity<Entity>());
        }

        internal void ChangeStatusToApproved(Guid appraisalId)
        {
            var request = new SetStateRequest
            {
                EntityMoniker = new EntityReference("pnl_appraisal", appraisalId),
                State = new OptionSetValue((int)pnl_appraisalState.Active),
                Status = new OptionSetValue((int)pnl_appraisal_statuscode.ApprovedApprasial_125760001)
            };
            _service.Execute(request);
        }

        internal void ChangeStatusToInspectionWaitingForApproval(Guid appraisalId)
        {
            var request = new SetStateRequest
            {
                EntityMoniker = new EntityReference("pnl_appraisal", appraisalId),
                State = new OptionSetValue((int)pnl_appraisalState.Active),
                Status = new OptionSetValue((int)pnl_appraisal_statuscode.InspectionWaitingforApproval)
            };
            _service.Execute(request);
        }

        internal void SendEmail(Guid emailId)
        {
            var request = new SendEmailRequest
            {
                EmailId = emailId,
                TrackingToken = "",
                IssueSend = true
            };

            _service.Execute(request);
        }

        #endregion

        #region Private Vars

        private Trace _trace;
        private IOrganizationService _service;

        #endregion
    }
}
