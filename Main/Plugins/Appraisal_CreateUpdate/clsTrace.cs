﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LandPowerPlugins
{
    class Trace
    {

        public Trace(IPluginExecutionContext localContext, ITracingService tracingService)
        {
            _localContext = localContext;
            _tracingService = tracingService;
        }

        public void TraceMessage(string message)
        {
            if (string.IsNullOrWhiteSpace(message) || _tracingService == null)
            {
                return;
            }

            if (_localContext == null)
            {
                _tracingService.Trace(message);
            }
            else
            {
                _tracingService.Trace(
                    "{0}, Correlation Id: {1}, Initiating User: {2}",
                    message,
                    _localContext.CorrelationId,
                    _localContext.InitiatingUserId);
            }
        }

        private IPluginExecutionContext _localContext;
        private ITracingService _tracingService;
    }
}
