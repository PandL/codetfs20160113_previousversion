﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LandPowerPlugins
{
        public interface IAppraisalExtensions
	    {
		    pnl_appraisal FetchAppraisalById(Guid appraisalId);

		    string FetchAppraisalCustomer(Guid appraisalId);

		    string FetchAppraisalMake(Guid appraisalId);

		    string FetchAppraisalMachineType(Guid appraisalId);

		    IEnumerable<pnl_appraisalitem> FetchAppraisalValuationItems(Guid appraisalId);

		    IEnumerable<pnl_appraisalitem> FetchAppraisalItems(Guid appraisalId);

		    IEnumerable<pnl_appraisalitemgroup> FetchAppraisalItemGroups(Guid appraisalId);

		    int FetchAppraisalPhotoAttachmentCount(Guid appraisalId);
	    }

	    public static class AppraisalExtension
	    {
		    public static Func<CrmServiceContext, IAppraisalExtensions> AppraisalFactory = serviceContext => new AppraisalExtensions(serviceContext);

		    public static IAppraisalExtensions Appraisal(this CrmServiceContext serviceContext)
		    {
			    return AppraisalFactory(serviceContext);
		    }
	    }

	    internal class AppraisalExtensions : IAppraisalExtensions
	    {
		    private readonly CrmServiceContext _serviceContext;

		    public AppraisalExtensions(CrmServiceContext serviceContext)
		    {
			    _serviceContext = serviceContext;
		    }

		    public pnl_appraisal FetchAppraisalById(Guid appraisalId)
		    {
			    return (from a in _serviceContext.pnl_appraisalSet where a.Id == appraisalId select a).FirstOrDefault();
		    }

		    public IEnumerable<pnl_appraisalitem> FetchAppraisalValuationItems(Guid appraisalId)
		    {
			    // Note, filter on AppraisalItemName name to remove total valuation items etc (until subgroup or better entity design is implemented).
			    // Also, can't reference a.pnl_AppraisalItemNameId.Name directly (in where clause).
			    return (from a in _serviceContext.pnl_appraisalitemSet
				    join g in _serviceContext.pnl_appraisalitemgroupSet on a.pnl_AppraisalItemGroupId.Id equals g.Id
				    join gn in _serviceContext.pnl_groupnameSet on g.pnl_GroupNameId.Id equals gn.Id
				    join n in _serviceContext.pnl_appraisalitemnameSet on a.pnl_AppraisalItemNameId.Id equals n.Id
				    where a.pnl_AppraisalId.Id == appraisalId
				          && a.statecode == pnl_appraisalitemState.Active
				          && gn.pnl_name == "Valuation"
					      //&& (n.pnl_name == "Fastrack Dealer Net" || n.pnl_name == "Marketbook" || n.pnl_name == "TradeMe" || n.pnl_name == "Last Landpower Sale")
                          && (a.pnl_CalcCode == "SOURCE_1" || a.pnl_CalcCode == "SOURCE_2" || a.pnl_CalcCode == "SOURCE_3" || a.pnl_CalcCode == "SOURCE_4")
				    select a);
		    }

		    public IEnumerable<pnl_appraisalitemgroup> FetchAppraisalItemGroups(Guid appraisalId)
		    {
			    var itemGroups = (from g in _serviceContext.pnl_appraisalitemgroupSet
				    where g.pnl_AppraisalId.Id == appraisalId
				          && g.statecode == pnl_appraisalitemgroupState.Active
				    select g);

			    return itemGroups;
		    }

		    public IEnumerable<pnl_appraisalitem> FetchRequiredAppraisalItems(Guid appraisalId)
		    {
			    return (from a in _serviceContext.pnl_appraisalitemSet
				    where a.pnl_AppraisalId.Id == appraisalId
				          && a.statecode == pnl_appraisalitemState.Active
				          && a.pnl_Required.GetValueOrDefault()
				    select a);
		    }

		    public IEnumerable<pnl_appraisalitem> FetchAppraisalItems(Guid appraisalId)
		    {
			    return (from a in _serviceContext.pnl_appraisalitemSet
				    where a.pnl_AppraisalId.Id == appraisalId
				          && a.statecode == pnl_appraisalitemState.Active
				    select a);
		    }

		    public int FetchAppraisalPhotoAttachmentCount(Guid appraisalId)
		    {
			    var notes = (from n in _serviceContext.AnnotationSet
					    join p in _serviceContext.pnl_appraisalphotoSet on n.ObjectId.Id equals p.pnl_appraisalphotoId.GetValueOrDefault()
					    where p.statecode == pnl_appraisalphotoState.Active
                            && n.FileName.StartsWith("Photo-")
						    && p.pnl_AppraisalId.Id == appraisalId
						    && p.pnl_PhotoUploadedOn != null
						    && n.IsDocument.GetValueOrDefault()
					    select n.Id);
			    return notes.ToList().Count;
		    }

		    public string FetchAppraisalCustomer(Guid appraisalId)
		    {
			    return FetchAppraisalItemValueByType(appraisalId, "Customer");
		    }

		    public string FetchAppraisalMake(Guid appraisalId)
		    {
			    return FetchAppraisalItemValueByType(appraisalId, "Make");
		    }

		    public string FetchAppraisalMachineType(Guid appraisalId)
		    {
			    return FetchAppraisalItemValueByType(appraisalId, "Machine Type");
		    }

		    internal string FetchAppraisalItemValueByType(Guid appraisalId, string typeName)
		    {
			    var item = (from a in _serviceContext.pnl_appraisalitemSet
						    join n in _serviceContext.pnl_appraisalitemnameSet on a.pnl_AppraisalItemNameId.Id equals n.Id
						    where a.pnl_AppraisalId.Id == appraisalId
							      && a.statecode == pnl_appraisalitemState.Active
							      && n.pnl_name == typeName
						    select a).FirstOrDefault();

			    return item == null ? string.Empty : item.pnl_Value;
		    }
	    }
    
}
