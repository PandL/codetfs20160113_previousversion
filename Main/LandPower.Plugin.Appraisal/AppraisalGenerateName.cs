﻿using System;

using LandPower.BusinessLogic;
using LandPower.Data;

namespace LandPower.Plugin.TradeIn
{
	using Microsoft.Xrm.Sdk;

	public class AppraisalGenerateName : Plugin
	{
		private const string PostImageAlias = "postimage";
	    private IOrganizationService _organizationService;
		internal Entity InputEntity;

        public AppraisalGenerateName() : base(typeof(AppraisalGenerateName))
        {
			RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create", "pnl_appraisal", ExecuteAppraisalGenerateName));
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "pnl_appraisal", ExecuteAppraisalGenerateName));
        }

		protected void ExecuteAppraisalGenerateName(LocalPluginContext localContext)
		{
			if (localContext == null)
			{
				throw new ArgumentNullException("localContext");
			}

			var context = localContext.PluginExecutionContext;

			if (context.Depth > 3) return;

			InputEntity = context.PostEntityImages[PostImageAlias];

			_organizationService = localContext.OrganizationService;

			var name = TryToGenerateName(context.PrimaryEntityId);
			TryToUpdateName(context.PrimaryEntityId, name);
		}

		internal string TryToGenerateName(Guid appraisalId)
		{
			var context = new CrmServiceContext(_organizationService);
			return InputEntity.ToEntity<pnl_appraisal>().GenerateName(context);
		}

		internal void TryToUpdateName(Guid appraisalId, string name)
		{
			if (InputEntity.GetAttributeValue<string>("pnl_name") == name) return;

			var entity = new Entity("pnl_appraisal") {Id = appraisalId};
			entity.Attributes.Add("pnl_name", name);
			entity.Attributes.Add("pnl_forceappraisalitemgroupnamefieldupdate", DateTime.Now); // Get AppraisalItemGroup names to update too.
			_organizationService.Update(entity);
		}
	}
}
