﻿using LandPower.BusinessLogic;
using LandPower.Data;

namespace LandPower.Plugin.TradeIn
{
	using System;

	using Microsoft.Xrm.Sdk;

	// Calculates average valuation (on change of pnl_lastsavedfromappraisalitemon).
	// Value is only changed if it is different to prevent endless loops.
	public class Appraisal : Plugin
	{
		private LocalPluginContext _localPluginContext;

		public IOrganizationService OrganizationService { get; set; }

		public Appraisal()
			: base(typeof (Appraisal))
		{
			RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Update", "pnl_appraisal", ExecuteAppraisalUpdate));
		}

		protected void ExecuteAppraisalUpdate(LocalPluginContext localContext)
		{
			if (localContext == null)
			{
				throw new ArgumentNullException("localContext");
			}

			var context = localContext.PluginExecutionContext;
			var inputEntity = (Entity) context.InputParameters["Target"];
			var preImageEntity = (context.PreEntityImages != null && context.PreEntityImages.Contains("preimage"))
				? context.PreEntityImages["preimage"]
				: null;

			if (context.IsOfflinePlayback || context.Depth > 3)
				return; // No need to re-calculate average valution if/when (Outlook) clients run offline.

			OrganizationService = localContext.OrganizationService;
			_localPluginContext = localContext;

			TryToUpdateAppraisal(inputEntity, preImageEntity);
		}

		public void TryToUpdateAppraisal(Entity inputEntity, Entity preImageEntity)
		{
			try
			{
				var xrmContext = new CrmServiceContext(OrganizationService);
				var appraisal = inputEntity.ToEntity<pnl_appraisal>();
				var preAppraisal = preImageEntity.ToEntity<pnl_appraisal>();
				var averageValuation = appraisal.GetAverageValuation(xrmContext);
				
				// Get the summary values from appraisal items from iTrade first for use in the calculations.
				GetAppraisalItemValues(preAppraisal, inputEntity);

				// Changes to valuation affect Expected Retained Margin and Calculated Trade Price.
				var expectedRetainedMargin = preAppraisal.GetExpectedRetainedMarginDollars();
				var calculatedTradePrice = preAppraisal.GetCalculatedTradePrice(averageValuation);

				if (null == preAppraisal.pnl_CalculatedAverage || preAppraisal.pnl_CalculatedAverage.Value != averageValuation)
				{
					SetInputAttribute(inputEntity, "pnl_calculatedaverage", new Money(averageValuation));
				}
				if (null == preAppraisal.pnl_ExpectedRetainedMarginDollar ||
					preAppraisal.pnl_ExpectedRetainedMarginDollar.Value != expectedRetainedMargin)
				{
					SetInputAttribute(inputEntity, "pnl_expectedretainedmargindollar", new Money(expectedRetainedMargin));
				}
				if (null == preAppraisal.pnl_CalcuatedTradePrice ||
					preAppraisal.pnl_CalcuatedTradePrice.Value != calculatedTradePrice)
				{
					//TODO: correct spelling once HK has fixed the attribute definition in CRM.
					SetInputAttribute(inputEntity, "pnl_calcuatedtradeprice", new Money(calculatedTradePrice));
				}
			}
			catch (Exception ex)
			{
				_localPluginContext.Trace("Inner Exception: " + ex.InnerException);
				_localPluginContext.Trace("StackTrace: " + ex.StackTrace);
				throw new InvalidPluginExecutionException(string.Format("PreAppraisalUpdatePlugin exception: {0}", ex.Message));
			}
		}

		internal void GetAppraisalItemValues(pnl_appraisal appraisal, Entity inputEntity)
		{




			var recommendedListPrice = appraisal.GetAppraisalItemValue("My Recommended List Price", OrganizationService, "Valuation");
			if (appraisal.pnl_RecommededListPrice == null || appraisal.pnl_RecommededListPrice.Value != recommendedListPrice)
			{
				appraisal.pnl_RecommededListPrice = new Money(recommendedListPrice);
				SetInputAttribute(inputEntity, "pnl_recommededlistprice", new Money(recommendedListPrice));
			}

			var repairCost = appraisal.GetAppraisalItemValue("Repair Costs", OrganizationService);
			if (appraisal.pnl_RepairCosts == null || appraisal.pnl_RepairCosts.Value != repairCost)
			{
				appraisal.pnl_RepairCosts = new Money(repairCost);
				SetInputAttribute(inputEntity, "pnl_repaircosts", new Money(repairCost));
			}

			var freightCost = appraisal.GetAppraisalItemValue("Freight", OrganizationService);
			if (appraisal.pnl_Freight == null || appraisal.pnl_Freight.Value != freightCost)
			{
				appraisal.pnl_Freight = new Money(freightCost);
				SetInputAttribute(inputEntity, "pnl_freight", new Money(freightCost));
			}

			var preDeliveryExpenses = appraisal.GetAppraisalItemValue("PD Expenses", OrganizationService);
			if (appraisal.pnl_PreDeliveryExpenses == null || appraisal.pnl_PreDeliveryExpenses.Value != preDeliveryExpenses)
			{
				appraisal.pnl_PreDeliveryExpenses = new Money(preDeliveryExpenses);
				SetInputAttribute(inputEntity, "pnl_predeliveryexpenses", new Money(preDeliveryExpenses));
			}

			var otherCosts = appraisal.GetAppraisalItemValue("Other", OrganizationService);
			if (appraisal.pnl_OtherCosts == null || appraisal.pnl_OtherCosts.Value != otherCosts)
			{
				appraisal.pnl_OtherCosts = new Money(otherCosts);
				SetInputAttribute(inputEntity, "pnl_othercosts", new Money(otherCosts));
			}

			var recommendedTradePrice = appraisal.GetAppraisalItemValue("Recommended Trade Price", OrganizationService);
			if (appraisal.pnl_RecommendedTradePrice == null || appraisal.pnl_RecommendedTradePrice.Value != recommendedTradePrice)
			{
				appraisal.pnl_RecommendedTradePrice = new Money(recommendedTradePrice);
				SetInputAttribute(inputEntity, "pnl_recommendedtradeprice", new Money(recommendedTradePrice));
			}

			var expectedRetainedMarginPercent = appraisal.GetAppraisalItemValue("Margin (%)", OrganizationService);
			if (appraisal.pnl_ExpectedRetainedMarginPercentage == null ||
			    appraisal.pnl_ExpectedRetainedMarginPercentage.Value != expectedRetainedMarginPercent)
			{
				appraisal.pnl_ExpectedRetainedMarginPercentage = expectedRetainedMarginPercent;
				SetInputAttribute(inputEntity, "pnl_expectedretainedmarginpercentage", expectedRetainedMarginPercent);
			}
		}

		internal void SetInputAttribute(Entity inputEntity, string attributeName, object attributeValue)
		{
			if (inputEntity.Attributes.Contains(attributeName))
			{
				inputEntity.Attributes[attributeName] = attributeValue;
			}
			else
			{
				inputEntity.Attributes.Add(attributeName, attributeValue);
			}
		}
	}
}