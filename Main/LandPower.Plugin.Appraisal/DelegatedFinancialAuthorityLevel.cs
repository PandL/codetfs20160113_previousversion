﻿namespace LandPower.Plugin.TradeIn
{
	using System;
	using System.Linq;

	using Microsoft.Crm.Sdk.Messages;
	using Microsoft.Xrm.Sdk;

	using Data;

	// Email notification of which values have changed.
	public class DelegatedFinancialAuthorityLevel : Plugin
	{
		private LocalPluginContext _localPluginContext;
		private const string EmailTo = "iDEALDFAChanges@landpower.co.nz";
		public IOrganizationService OrganizationService { get; set; }
		private DfaHelper _dfaHelper;

		public DelegatedFinancialAuthorityLevel() : base(typeof(DelegatedFinancialAuthorityLevel))
		{
			RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create", "pnl_delegatedfinancialauthoritylevel", ExecuteDfaLevelUpdate));
			RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Update", "pnl_delegatedfinancialauthoritylevel", ExecuteDfaLevelUpdate));
			RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Delete", "pnl_delegatedfinancialauthoritylevel", ExecuteDfaLevelUpdate));
		}

		protected void ExecuteDfaLevelUpdate(LocalPluginContext localContext)
		{
			if (localContext == null)
			{
				throw new ArgumentNullException("localContext");
			}
			_localPluginContext = localContext;

			var context = localContext.PluginExecutionContext;
			// Target is type EntityReference for delete message. We don't need to use the input parameters for delete.
			var inputEntity = context.MessageName.ToLower() == "delete" ? new Entity("pnl_delegatedfinancialauthoritylevel") : (Entity)context.InputParameters["Target"];
			var preImageEntity = context.PreEntityImages.Contains("preimage") ? context.PreEntityImages["preimage"] : null;
			var messageName = context.MessageName.ToLower();

			if (context.Depth > 3) return;

			OrganizationService = localContext.OrganizationService;
			_dfaHelper = new DfaHelper(OrganizationService);

			if (IsDuplicate(messageName, preImageEntity, inputEntity, context.PrimaryEntityId))
			{
				throw new InvalidPluginExecutionException("A record with the same Level and Type already exists. Please enter different values.");
			}

			if (messageName == "delete")
			{
				TryToCheckDfaLevelDelete(preImageEntity);
			}
			else
			{
				// We can get the primaryentityid post create but pass guid.empty so email knows not to set regardingobjectid (since the DFA record
				// does not exist until after this plug-in has finished executing).
				TryToCheckDfaLevelChanges(inputEntity, preImageEntity,
					context.MessageName.ToLower() == "create" ? Guid.Empty : context.PrimaryEntityId);
			}
		}

		internal bool IsDuplicate(string messageName, Entity preImageEntity, Entity inputEntity, Guid currentRecordId)
		{
			var xrmContext = new CrmServiceContext(OrganizationService);

			switch (messageName.ToLower())
			{
				case "create":
					var level = inputEntity.GetAttributeValue<string>("pnl_level");
					var dfaType = inputEntity.GetAttributeValue<OptionSetValue>("pnl_type");
					return (!string.IsNullOrEmpty(level) && dfaType != null) && xrmContext.DelegatedFinancialAuthorityLevel().IsDuplicate(level, dfaType.Value, currentRecordId);
				case "update":
					level = inputEntity.Contains("pnl_level")
						? inputEntity.GetAttributeValue<string>("pnl_level")
						: preImageEntity.GetAttributeValue<string>("pnl_level");
					dfaType = inputEntity.Contains("pnl_type")
						? inputEntity.GetAttributeValue<OptionSetValue>("pnl_type")
						: preImageEntity.GetAttributeValue<OptionSetValue>("pnl_type");
					return (!string.IsNullOrEmpty(level) && dfaType != null) && xrmContext.DelegatedFinancialAuthorityLevel().IsDuplicate(level, dfaType.Value, currentRecordId);
				default: // delete
					return false;
			}
		}

		public void TryToCheckDfaLevelChanges(Entity inputEntity, Entity preImageEntity, Guid dfaId)
		{
			try
			{
				var changedAttributes = _dfaHelper.GetChangedAttributes(inputEntity, preImageEntity).ToList();
				if (!changedAttributes.Any()) return;
				var emailBody = string.Format("The following changes have been made to the DFA Level record by {0}:<br/><br/>{1}",
					_dfaHelper.GetEntityReferenceName((EntityReference)inputEntity["modifiedby"]),
					_dfaHelper.GetChangedValueTable(changedAttributes, "pnl_delegatedfinancialauthoritylevel"));
				var dfaName = null == preImageEntity ? inputEntity["pnl_name"].ToString() : preImageEntity["pnl_name"].ToString();
				CreateAndSendEmail(_localPluginContext.PluginExecutionContext.InitiatingUserId, emailBody, dfaName, dfaId);
			}
			catch (Exception ex)
			{
				_localPluginContext.Trace("Inner Exception: " + ex.InnerException);
				_localPluginContext.Trace("StackTrace: " + ex.StackTrace);
				throw new InvalidPluginExecutionException(string.Format("DelegatedFinancialAuthorityLevelUpdatePlugin exception: {0}", ex.Message));
			}
		}

		public void TryToCheckDfaLevelDelete(Entity preImageEntity)
		{
			try
			{
				var dfaId = preImageEntity.Id;
				var dfaName = preImageEntity["pnl_name"].ToString();
				var initiatingUserId = _localPluginContext.PluginExecutionContext.InitiatingUserId;
				var emailBody = string.Format("The DFA Level record, {0} ({1}), was deleted by {2}.",
					dfaName, dfaId, _dfaHelper.GetEntityReferenceName(new EntityReference("systemuser", initiatingUserId)));
				CreateAndSendEmail(initiatingUserId, emailBody, dfaName, Guid.Empty);
			}
			catch (Exception ex)
			{
				_localPluginContext.Trace("Inner Exception: " + ex.InnerException);
				_localPluginContext.Trace("StackTrace: " + ex.StackTrace);
				throw new InvalidPluginExecutionException(string.Format("DelegatedFinancialAuthorityLevelDeletePlugin exception: {0}", ex.Message));
			}
		}

		internal void CreateAndSendEmail(Guid fromUserId, string body, string dfaName, Guid dfaId)
		{
			var subject = "Change Notification for Delegated Financial Authority Level: " + dfaName;
			var email = new Email
			{
				To = new[] { new ActivityParty { AddressUsed = EmailTo } },
				From = new[] { new ActivityParty { PartyId = new EntityReference("systemuser", fromUserId) } },
				Subject = subject,
				Description = body,
				DirectionCode = true // Outgoing
			};
			if (dfaId != Guid.Empty)
			{
				email.RegardingObjectId = new EntityReference("pnl_delegatedfinancialauthoritylevel", dfaId);
			}
			var emailId = OrganizationService.Create(email);

			SendEmail(emailId);
		}

		internal void SendEmail(Guid emailId)
		{
			var request = new SendEmailRequest
			{
				EmailId = emailId,
				TrackingToken = "",
				IssueSend = true
			};

			OrganizationService.Execute(request);
		}
	}
}