﻿using System;

namespace LandPower.Plugin.TradeIn
{
	using Microsoft.Xrm.Sdk;

	using BusinessLogic;
	using Data;

	public class AppraisalStatus : IPlugin
	{
		internal IPluginExecutionContext PluginExecutionContext { get; set; }
		internal IOrganizationService OrganizationService { get; set; }
		internal ITracingService TracingService { get; set; }

		public void Execute(IServiceProvider serviceProvider)
		{
			if (serviceProvider == null)
			{
				throw new ArgumentNullException("serviceProvider");
			}

			PluginExecutionContext = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
			TracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
			var factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
			OrganizationService = factory.CreateOrganizationService(PluginExecutionContext.UserId);

			if (PluginExecutionContext.Depth > 3) return;

			Trace("Entered AppraisalStatusPlugin.Execute()");

			var preImage = PluginExecutionContext.PreEntityImages["preimage"];
			var appraisal = preImage.ToEntity<pnl_appraisal>();
			var xrmContext = new CrmServiceContext(OrganizationService);
			var canEdit = appraisal.CanApplyChanges(PluginExecutionContext.InitiatingUserId, xrmContext);

			if (!canEdit)
			{
				const string message =
				"Permission denied. The Appraisal can be updated if it is in inspection or you are an approver and it is waiting for approval.";
				throw new InvalidPluginExecutionException(message);
			}

			var inputParameters = PluginExecutionContext.InputParameters;
			if (inputParameters.Contains("EntityMoniker") && inputParameters["EntityMoniker"] is EntityReference)
			{
				// Note, inputParameters["EntityMoniker"] is an EntityReference.
				var preStatus = preImage.GetAttributeValue<OptionSetValue>("statuscode");
				var status = (OptionSetValue)inputParameters["Status"];
				if (preStatus.Value == (int) pnl_appraisal_statuscode.Inspection_1 && status.Value != preStatus.Value)
				{
					preImage.ToEntity<pnl_appraisal>().SetAppraisalItemsReadOnly(OrganizationService);
				}
			}

			Trace("Exited AppraisalStatusPlugin.Execute()");
		}

		internal void Trace(string message)
		{
			if (string.IsNullOrWhiteSpace(message) || TracingService == null) return;

			if (PluginExecutionContext == null)
			{
				TracingService.Trace(message);
			}
			else
			{
				TracingService.Trace("{0}, Correlation Id: {1}, Initiating User: {2}",
					message, PluginExecutionContext.CorrelationId, PluginExecutionContext.InitiatingUserId);
			}
		}
	}
}
