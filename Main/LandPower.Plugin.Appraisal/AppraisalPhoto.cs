﻿namespace LandPower.Plugin.TradeIn
{
	using System;

	using Microsoft.Xrm.Sdk;
	
	using BusinessLogic;
	using Data;

	public class AppraisalPhoto : Plugin
	{
		private const string PostImageAlias = "postimage";
	    private const string PreImageAlias = "preimage";
	    private IOrganizationService _organizationService;
	    private Guid _initiatingUserId;
		
        public AppraisalPhoto()
            : base(typeof(AppraisalPhoto))
        {
			RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "SetState", "pnl_appraisalphoto", ExecuteAppraisalPhotoUpdate));
			RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "SetStateDynamicEntity", "pnl_appraisalphoto", ExecuteAppraisalPhotoUpdate));
			RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create", "pnl_appraisalphoto", ExecuteAppraisalPhotoUpdate));
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Update", "pnl_appraisalphoto", ExecuteAppraisalPhotoUpdate));
			RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Delete", "pnl_appraisalphoto", ExecuteAppraisalPhotoUpdate));
        }

		/// <summary>
		/// Prevent edits unless the Appraisal status is Inspection or is Waiting for Approval and the user is a member of the Approvers team.
		/// </summary>
		protected void ExecuteAppraisalPhotoUpdate(LocalPluginContext localContext)
		{
			if (localContext == null)
			{
				throw new ArgumentNullException("localContext");
			}

			var context = localContext.PluginExecutionContext;
			var preImageEntity = (context.PreEntityImages != null && context.PreEntityImages.Contains(PreImageAlias)) ? context.PreEntityImages[PreImageAlias] : null;
			var postImageEntity = (context.PostEntityImages != null && context.PostEntityImages.Contains(PostImageAlias)) ? context.PostEntityImages[PostImageAlias] : null;

			if (context.Depth > 3) return;

			_organizationService = localContext.OrganizationService;
			_initiatingUserId = context.InitiatingUserId;

			// Use preimage on update and also the inputParams so that appraisal cannot be changed as a workaround.
			var appraisalPhoto = preImageEntity == null ? postImageEntity == null ? null : postImageEntity.ToEntity<pnl_appraisalphoto>() : preImageEntity.ToEntity<pnl_appraisalphoto>();
			if (null == appraisalPhoto) return;

			if (appraisalPhoto.pnl_AppraisalId == null) return;

			CheckCanUpdateAppraisal(appraisalPhoto.pnl_AppraisalId.Id);

			if (context.MessageName.ToLower() != "update") return;
			var inputEntity = context.InputParameters.Contains("Target") ? (Entity) context.InputParameters["Target"] : null;
			if (null == inputEntity || !inputEntity.Contains("pnl_appraisalid")) return;

			var inputAppraisalId = ((EntityReference) inputEntity["pnl_appraisalid"]).Id;
			if (inputAppraisalId == appraisalPhoto.pnl_AppraisalId.Id) return;
			CheckCanUpdateAppraisal(inputAppraisalId);
		}

		internal void CheckCanUpdateAppraisal(Guid appraisalId)
		{
			var context = new CrmServiceContext(_organizationService);
			var appraisal = context.Appraisal().FetchAppraisalById(appraisalId);
			var canUpdate = appraisal.CanApplyChanges(_initiatingUserId, context);

			if (canUpdate) return;

			throw new InvalidPluginExecutionException("Permission denied. The Appraisal can be updated if it is in inspection or you are an approver and it is waiting for approval.");
		}
	}
}
