﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppraisalImageresize
{
    using System;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Drawing.Imaging;
    public static class ImageExtensions_related_Appraisal_imageresize
    {
        public static Image Scale(this Image img, int maxWidth, int maxHeight)
        {
            double scale = 1;

            if (img.Width > maxWidth || img.Height > maxHeight)
            {
                double scaleW, scaleH;

                scaleW = maxWidth / (double)img.Width;
                scaleH = maxHeight / (double)img.Height;

                scale = scaleW < scaleH ? scaleW : scaleH;
            }

            return img.Resize((int)(img.Width * scale), (int)(img.Height * scale));
        }

        public static Image Scale(this Image img, Size maxDimensions)
        {
            return img.Scale(maxDimensions.Width, maxDimensions.Height);
        }

        public static Image Resize(this Image img, int width, int height)
        {
            // This is dumb. If the image contains a thumbnail and you want to resize to larger than 120px square it will try to scale
            // the thumbnail image up with crappy resulting quality.
            // return img.GetThumbnailImage(width, height, null, IntPtr.Zero);

            var thumbnailBitmap = new Bitmap(width, height, PixelFormat.Format24bppRgb);
            thumbnailBitmap.SetResolution(72, 72); // This doesn't change the file size just the physical i.e. printed size, so it's not really necessary.
            var thumbnailGraph = Graphics.FromImage(thumbnailBitmap);
            // TODO: try changing back to CompositingQuality.HighQuality and SmoothingMode.HighQuality and InterpolationMode.HighQualityBicubic
            // and see whether this affects the file size. The key to reducing the file size was encoding back to jpeg.
            thumbnailGraph.CompositingQuality = CompositingQuality.Default;
            thumbnailGraph.SmoothingMode = SmoothingMode.AntiAlias;
            thumbnailGraph.InterpolationMode = InterpolationMode.Low;

            var imageRectangle = new Rectangle(0, 0, width, height);
            thumbnailGraph.DrawImage(img, imageRectangle);
            thumbnailGraph.Dispose();

            return thumbnailBitmap;
        }
    }
}
