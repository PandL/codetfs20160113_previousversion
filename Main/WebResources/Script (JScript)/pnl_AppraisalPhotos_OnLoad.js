
function onLoad() {

	if (IsParentAppraisalFormReadOnly()) {
		MakeAppraisalPhotoReadOnly();
	}
}


function MakeAppraisalPhotoReadOnly() {

	SetReadOnly("pnl_name",true);
	SetReadOnly("modifiedon",true);
	SetReadOnly("modifiedby",true);
	SetReadOnly("createdon",true);
	SetReadOnly("createdby",true);
	SetReadOnly("pnl_date",true);
	SetReadOnly("pnl_deviceimagelocation",true);
	SetReadOnly("pnl_displayorder",true);
		
}

function IsParentAppraisalFormReadOnly() {
    var isReadOnly = true;

    //if  (AllowedToChangeForm()) {
    //	isReadOnly = false;
    //} else
    if (CanApproveTradePrice("System Administrator,Appraisal Approver")) { // use comma delimited roles eg. "System Administrator,Appraisal Approver"
        if (AllowedToChangeForm()) {
            isReadOnly = false;
        }
    }
    return isReadOnly;
}

function AllowedToChangeForm() {
    ///<summary>
    /// Sends synchronous requests to retrieve some key appraisal fields that would affect this form.
    ///</summary>

	var isAllowed = true;

	// gets Id associated with the parent Appraisal entity
	var AppraisalId = Xrm.Page.data.entity.attributes.get("pnl_appraisalid");
	
	if (AppraisalId.getValue() == null) {
        alert("Error: Appraisal Photo does not have parent Appraisal");
        return false;
	}		

	var AppraisalIdGUID = AppraisalId.getValue()[0].id;
	var AppraisalIdName = AppraisalId.getValue()[0].name;

	var querystart = "<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>" +
                    "<entity name='pnl_appraisal'>" +
                    "<attribute name='pnl_name' />" +
                    "<attribute name='statuscode' />" +
                    "<order attribute='pnl_name' descending='false' />" +
					"<filter type='and'>" +
                          "<condition attribute='pnl_appraisalid' operator='eq' value='" + AppraisalIdGUID + "' />" +
                        "</filter>" ;

	var queryend =  "</entity></fetch>";
	
						
	// fetch the parent status code
    var xml =   [	querystart + queryend   ].join("");
    var fetchResult = XrmServiceToolkit.Soap.Fetch(xml);

    if (fetchResult !== null && typeof fetchResult != 'undefined') {
        for (var i = 0; i < fetchResult.length; i++) {
			if (fetchResult[i].attributes["statuscode"]) {
				var StatusReason = fetchResult[i].attributes["statuscode"].value;

				if (StatusReason == 125760000 || StatusReason == 125760001) {
					isAllowed =  false;  // appraisal is readonly
				}
				
			}
        }
    }
	return isAllowed;  
}	
	
function CanApproveTradePrice(CommaDelimitedStringOfRolesThatCanDoThis) {
    var roles = getAllUserRoles();

    var canApprove = false;

	//CommaDelimitedStringOfRolesThatCanDoThis = ",," + CommaDelimitedStringOfRolesThatCanDoThis + ",";
	
    //var loginuser = Xrm.Page.context.getUserId();
    //var oDataEndpointUrl = "SystemUserSet?$select=systemuserroles_association&$expand=systemuserroles_association&$filter=SystemUserId eq guid'" + loginuser + "'";
    //var requestResults = GetDataUsingODataServiceWithJQuery(oDataEndpointUrl);
    //if (requestResults != null && requestResults.results.length > 0) {
    //    for (var i = 0; i < requestResults.results[0].systemuserroles_association.results.length; i++) {
    //        var roleName = requestResults.results[0].systemuserroles_association.results[i].Name;
    //        //Get all the Roles of the loginUser and see if they are listed in the CommaDelimitedStringOfRolesThatCanDoThis
	//		if (roleName != null && roleName != "") 
	//		{
	//			 if (CommaDelimitedStringOfRolesThatCanDoThis.indexOf("," + roleName + ",")>0) {
	//				canApprove = true;
	//			 }
    //        }
    //    }
	//}

    for (var i = 0; i < roles.length; i++) {
        var roleName = roles[i];
        //Get all the Roles of the loginUser and see if they are listed in the CommaDelimitedStringOfRolesThatCanDoThis
        if (roleName != null && roleName != "") {
            if (CommaDelimitedStringOfRolesThatCanDoThis.indexOf(roleName) > 0) {
                canApprove = true;
                break;
            }
        }
    }
	return canApprove;  
}



function SetReadOnly(editName, IsReadOnly) {
    var thisEdit = Xrm.Page.getControl(editName);
	if (!thisEdit) { return;}
    if (thisEdit.getDisabled() == IsReadOnly) {
	        return;
	}
	thisEdit.setDisabled(IsReadOnly);

    var labelEdit = document.getElementById(editName);
	if (labelEdit) {
		HideLockIcon(labelEdit);
	}
}

// hides read-only lock icon
function HideLockIcon(editObject) {
    var imgnodes = editObject.getElementsByTagName('img');
    if (imgnodes != undefined && imgnodes != null && imgnodes.length > 0) {
		for (var i = 0; i < imgnodes.length; i++) {
			if (imgnodes[i].className != undefined && imgnodes[i].className != null && imgnodes[i].className == "ms-crm-ImageStrip-inlineedit_locked") {
				imgnodes[i].setAttribute("class", "");
			}
		}
    }
}

// ========== OData Service - JQuery Request Functions Start ==========

function GetDataUsingODataServiceWithJQuery(ODataSelect) {
    var resultOfGet = null;
    var CRMContext = GetContext();
    if (CRMContext != null) {
        var serverUrl = CRMContext.getServerUrl();
        var ODataEndpoint = "/XRMServices/2011/OrganizationData.svc";
        var ODataURL = serverUrl + ODataEndpoint + "/" + ODataSelect;
        try {
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                async: false,
                url: ODataURL,
                beforeSend: function (XMLHttpRequest) { XMLHttpRequest.setRequestHeader("Accept", "application/json"); },
                success: function (data, textStatus, XmlHttpRequest) {
                    resultOfGet = data.d;
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    DisplayError("ODataService", "Fetch", textStatus + " - " + JSON.parse(XMLHttpRequest.responseText).error.message.value);
                    //resultOfGet = null;
                }
            });
        } catch (e) {
            DisplayError("ODataService", "Fetch", e.description);
            //resultOfGet = null;
        }
    }
    return resultOfGet;
}

function GetContext() {
    var _context = null;
    if (typeof GetGlobalContext != "undefined")
        _context = GetGlobalContext();
    else if (typeof Xrm != "undefined")
        _context = Xrm.Page.context;
    return _context
}


 function DisplayError(cntrl, func, err) {
    alert("Control : " + cntrl + "\nODataCommonFunctions.: " + func + "\nError : " + err);
}

 function getAllUserRoles() {
     var guid = "[A-z0-9]{8}-[A-z0-9]{4}-[A-z0-9]{4}-[A-z0-9]{4}-[A-z0-9]{12}";

     var serverUrl = Xrm.Page.context.getClientUrl();
     var userId = Xrm.Page.context.getUserId();
     userId = userId.match(guid);

     var teamQuery = "TeamMembershipSet?$select=TeamId&$filter=SystemUserId eq guid'" + userId + "'";
     var teamRoleQuery = "TeamRolesSet?$select=RoleId&$filter=";
     var roleQuery = "RoleSet?$select=Name&$filter=";

     var teams = makeRequest(serverUrl, teamQuery, 0);

     teamRoleQuery = composeQuery(teamRoleQuery, "TeamId", teams);
     var teamRoles = makeRequest(serverUrl, teamRoleQuery, 1);

     userRoles = Xrm.Page.context.getUserRoles();

     if (userRoles != null) {
         for (var i = 0; i < userRoles.length; i++) {
             teamRoles.push(userRoles[i].match(guid));
         }
     }

     roleQuery = composeQuery(roleQuery, "RoleId", teamRoles);
     var roles = makeRequest(serverUrl, roleQuery, 2);

     return roles;
 }

 function makeRequest(serverUrl, query, type) {

     var oDataEndpointUrl = serverUrl + "/XRMServices/2011/OrganizationData.svc/";
     oDataEndpointUrl += query;

     var service = GetRequestObject();

     if (service != null) {

         service.open("GET", oDataEndpointUrl, false);
         service.setRequestHeader("X-Requested-With", "XMLHttpRequest");
         service.setRequestHeader("Accept", "application/json, text/javascript, */*");
         service.send(null);

         var retrieved = $.parseJSON(service.responseText).d;

         var results = new Array();

         switch (type) {

             case 0:
                 for (var i = 0; i < retrieved.results.length; i++) {
                     results.push(retrieved.results[i].TeamId);
                 }
                 break;
             case 1:
                 for (var i = 0; i < retrieved.results.length; i++) {
                     results.push(retrieved.results[i].RoleId);
                 }
                 break;

             case 2:
                 for (var i = 0; i < retrieved.results.length; i++) {
                     if (results.indexOf(retrieved.results[i].Name) == -1) {
                         results.push(retrieved.results[i].Name);
                     }
                 }
                 break;
         }
         return results;
     }
     return null;
 }

 function GetRequestObject() {

     if (window.XMLHttpRequest) {
         return new window.XMLHttpRequest;
     }
     else {
         try {
             return new ActiveXObject("MSXML2.XMLHTTP.3.0");
         }
         catch (ex) {
             return null;
         }
     }
 }

 function composeQuery(queryBase, attribute, items) {
     if (items != null) {
         for (var i = 0; i < items.length; i++) {
             if (i == 0) {
                 queryBase += attribute + " eq (guid'" + items[i] + "')";
             }
             else {
                 queryBase += " or " + attribute + " eq (guid'" + items[i] + "')";
             }
         }
     }
     return queryBase;
 }