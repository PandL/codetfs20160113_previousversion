﻿if (LAT === undefined) {
    var LAT = {};
}

LAT.Lcid = null;
LAT.Entities = [];
LAT.Context = null;
LAT.ServerURL = null;

LAT.GetEntityMaps = function () {
    $.ajax({
        dataType: "json",
        url: LAT.ServerURL + "/XRMServices/2011/OrganizationData.svc/EntityMapSet()",
        success: LAT.GetEntityMapData,
        error: function () {
            alert("Error Retrieving Mappings");
        }
    });
}

LAT.GetMetaData = function () {
    SDK.Metadata.RetrieveAllEntities(SDK.Metadata.EntityFilters.Entity, true, LAT.GetMetaDataSuccess, LAT.GetMetaDataError);
}

LAT.GetMetaDataSuccess = function (entityLogicalNames) {
    $(entityLogicalNames).each(function () {
        var s = this.LogicalName;
        var d;
        $(this.DisplayName.LocalizedLabels).each(function () {
            if (this.LanguageCode == LAT.Lcid) {
                d = this.Label;
            }
        });

        LAT.Entities.push([{ "SchemaName": s, "DisplayName": d }]);
    });

    LAT.GetEntityMaps();
}

LAT.GetMetaDataError = function (error) {
    $("#img-loadDiv").fadeOut();
    alert("Error Retreiving Metadata\n" + "Check Organization Data Service\n" + LAT.ServerURL + "/XRMServices/2011/OrganizationData.svc");
}

LAT.GetEntityMapData = function (retrieveReq) {

    $("#img-loadDiv").fadeOut();
    $.each(retrieveReq.d.results, function (x, y) {

        var s, t = null;
        $(LAT.Entities).each(function (i, elem) {
            if (elem[0].SchemaName == y.SourceEntityName) {
                s = elem[0].DisplayName;
            }

            if (elem[0].SchemaName == y.TargetEntityName) {
                t = elem[0].DisplayName;
            }

            if (s != null && t != null) {
                return (false);
            }
        });

        $("<tr id='" + this.EntityMapId + "'><td><img src='styles/ico_16_systemRelationship.gif' alt='' /></td>" +
            "<td>" + s + "</td>" +
            "<td>" + t + "</td>" +
            "</tr>").appendTo("#list tbody");
    });

    $("#list").tablesorter({
        sortList: [[1, 0], [2, 0]],
        headers: {
            0: {
                sorter: false
            }
        }
    });
}

LAT.GetContext = function () {
    if (typeof GetGlobalContext != "undefined") {
        LAT.Context = GetGlobalContext;
    }
    else if (typeof Xrm != "undefined") {
        LAT.Context = Xrm.Page.context;
    }
    else {
        alert("Error Retrieving Context");
    }

    LAT.Lcid = Xrm.Page.context.getUserLcid();
}

$(document).on("dblclick", "#list tbody tr", function () {
    window.open(LAT.ServerURL + "/tools/systemcustomization/relationships/mappings/mappingList.aspx?mappingId=%7b" +
        this.id + "%7d", "_blank", "width=751");
});

LAT.GetServerURL = function () {
    try {
        LAT.ServerURL = Xrm.Page.context.getClientUrl();
    }
    catch (err) {
        LAT.ServerURL = Xrm.Page.context.getServerUrl();
    }
}

$(document).ready(function () {
    $("#contentIFrame", window.parent.document).css({ "overflow": "" });
    LAT.GetContext();
    LAT.GetServerURL();
    LAT.GetMetaData();
});