﻿function onLoad() {
    var isdraft = QuoteIsDraft_Onload();
    if (!isdraft) {
        DisableControls(true);
    }
}

function DisableControls(disablestatus) {
    Xrm.Page.ui.controls.get().forEach(function (control, index) {
        if (control.getDisabled() != disablestatus) {
            control.setDisabled(disablestatus);
        }
    });
}
function GetQuoteId() {
    var cols = ["pnl_quoteid"];
    var recordId = Xrm.Page.data.entity.getId();
    var retrievedQuote = XrmServiceToolkit.Soap.Retrieve("pnl_locallysourcedmachine", recordId, cols);
    var quote = retrievedQuote.attributes["pnl_quoteid"];
    if (quote != undefined && quote != null && quote.id != undefined && quote.id != null) {
        return quote.id;
    }
    return null;
}
function QuoteIsDraft_Onload() {
    var CRM_FORM_TYPE = Xrm.Page.ui.getFormType();
    var result = false;
    var statecode = Xrm.Page.getAttribute('statecode').getValue();
    if (statecode != 1) {
        if (CRM_FORM_TYPE == 1) {
            result = true;
        }
        else {
            var quoteid = Xrm.Page.getAttribute('pnl_quoteid').getValue();
            if (quoteid != null && quoteid.length > 0 && quoteid[0].id != null) {
                var cols = ["statecode", "statuscode"];
                var retrievedQuote = XrmServiceToolkit.Soap.Retrieve("quote", quoteid[0].id, cols);
                var objState = retrievedQuote.attributes["statecode"];
                var objStatus = retrievedQuote.attributes["statuscode"];

                if (objState != undefined && objState != null && objState.value != null && objStatus != undefined && objStatus != null && objStatus.value != null) {
                    if (objState.value == 0 && objStatus.value == 1) {
                        result = true;
                    }
                }
            }
        }
    }
    return result;
}