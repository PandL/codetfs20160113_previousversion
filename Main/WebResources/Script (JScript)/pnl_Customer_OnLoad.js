


function onLoadCustomer() {
    load_css_file('/WebResources/pnl_mscrmFormSection.css');
    load_css_file('/WebResources/pnl_mscrmInlineTabRead.css');
    //window.setTimeout(ReplaceCss, 2000);
    ReplaceCss();

    var CRM_FORM_TYPE = Xrm.Page.ui.getFormType();

}

function ReplaceCss() {
    try {
        var sectionTags = document.getElementsByClassName("ms-crm-FormSection");
        for (var i = 0; i < sectionTags.length; i++) {
            sectionTags[i].setAttribute("class", "pnl-ms-crm-FormSection");
        }

        var tab0 = document.getElementById("tab0");
        tab0.setAttribute("class", "pnl-ms-crm-InlineTab-Read");

        var tab1 = document.getElementById("tab1");
        tab1.setAttribute("class", "pnl-ms-crm-InlineTab-Read");

        var tab2 = document.getElementById("tab2");
        tab2.setAttribute("class", "pnl-ms-crm-InlineTab-Read");

        var tab3 = document.getElementById("tab3");
        tab3.setAttribute("class", "pnl-ms-crm-InlineTab-Read");

        var tab4 = document.getElementById("tab4");
        tab4.setAttribute("class", "pnl-ms-crm-InlineTab-Read");

        var tab5 = document.getElementById("tab5");
        tab5.setAttribute("class", "pnl-ms-crm-InlineTab-Read");

        var tab6 = document.getElementById("tab6");
        tab6.setAttribute("class", "pnl-ms-crm-InlineTab-Read");
    } catch (ex) {
        alert(ex.message);
    }
}

function load_css_file(filename) {
    var fileref = document.createElement("link")
    fileref.setAttribute("rel", "stylesheet")
    fileref.setAttribute("type", "text/css")
    fileref.setAttribute("href", filename)
    document.getElementsByTagName("head")[0].appendChild(fileref)
}

function DisableTabAttributes() {
    var toggleSection = Xrm.Page.getAttribute('pnl_prontocustomer').getValue();
    var accountId = Xrm.Page.data.entity.getId();
    var state = null;

    XrmServiceToolkit.Rest.Retrieve(
        accountId,
        "AccountSet",
        null, null,
        function (result) {
            state = result.StateCode.Value;
        },
        function (error) {
            alert("JS Error: " + error.message);
        },
        false
    );

    if ((toggleSection != null && !HasRole()) || state == 1) {

        tabToggle(2, true);
        tabToggle(3, true);
    }
    else {

        tabToggle(2, false);
        tabToggle(3, false);
    }
}

function sectionToggle(sectionName, disableStatus) {
    var ctrlName = Xrm.Page.ui.controls.get();
    for (var i in ctrlName) {
        var ctrl = ctrlName[i];
        var ctrlSection = ctrl.getParent().getName();
        if (ctrlSection == sectionName) {
            if (!ctrl.getDisabled())
                ctrl.setDisabled(disableStatus);
        }
    }
}

function tabToggle(tabName, disableStatus) {

    var tabControl = Xrm.Page.ui.tabs.get(tabName);
    if (tabControl != null) {
        Xrm.Page.ui.controls.forEach(
         function (control) {
             if (control.getParent() != undefined && control.getParent() != null) {
                 if (control.getParent().getParent() === tabControl && control.getControlType() != "subgrid") {
                     control.setDisabled(disableStatus);
                 }
             }
         });
    }

    //set accountnumber to disable or not
    Xrm.Page.ui.controls.get("accountnumber").setDisabled(disableStatus);
}

function disableRibbonButton(buttonId) {
    var buttonID = buttonId;
    var btn = window.top.document.getElementById(buttonID);

    var intervalId = window.setInterval(function () {
        if (btn != null) {
            window.clearInterval(intervalId);
            btn.disabled = true;
            //btn.style.display='none';//Hide
            //Xrm.Page.ui.refreshRibbon();
        }

    }, 50);
}

function CanDeactivate(recordId) {
    var canDeactivate = false;
    if (HasRole()) {
        canDeactivate = true;
    }
    else {
        var cols = ["ownerid", "statuscode"];
        var retrievedAccount = XrmServiceToolkit.Soap.Retrieve("account", recordId, cols);
        var owner = retrievedAccount.attributes['ownerid'].id;
        var status = retrievedAccount.attributes['statuscode'].value;
        owner = "{" + owner.toUpperCase() + "}";

        var isOwner = false;

        if (owner == Xrm.Page.context.getUserId()) {
            isOwner = true;
        }

        if (status == 125760000 && isOwner) {
            canDeactivate = true;
        }
    }

    return canDeactivate;
}

function HasRole() {

    return UserHasRole("Admin Support Staff");
}

function UserHasRole(roleName) {

    var roles = GetCurrentUserRoles();
    if (roles != null) {
        for (i = 0; i < roles.length; i++) {
            if (roles[i] == roleName) {
                return true;
            }
        }
    }
    //}
    return false;
}

function GetCurrentUserRoles() {
    ///<summary>
    /// Sends synchronous request to retrieve the list of the current user's roles.
    ///</summary>

    var xml =
            [
                "<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>" +
                  "<entity name='role'>" +
                    "<attribute name='name' />" +
                    "<attribute name='businessunitid' />" +
                    "<attribute name='roleid' />" +
                    "<order attribute='name' descending='false' />" +
                    "<link-entity name='systemuserroles' from='roleid' to='roleid' visible='false' intersect='true'>" +
                      "<link-entity name='systemuser' from='systemuserid' to='systemuserid' alias='aa'>" +
                        "<filter type='and'>" +
                          "<condition attribute='systemuserid' operator='eq-userid' />" +
                        "</filter>" +
                      "</link-entity>" +
                    "</link-entity>" +
                  "</entity>" +
                "</fetch>"
            ].join("");

    var fetchResult = XrmServiceToolkit.Soap.Fetch(xml);
    var roles = [];

    if (fetchResult !== null && typeof fetchResult != 'undefined') {
        for (var i = 0; i < fetchResult.length; i++) {
            roles[i] = fetchResult[i].attributes["name"].value;
        }
    }

    return roles;
}


//});

function HideQuoteSubGridAddButton() {
    var attrName = "QuotesSubGrid_addImageButton"
    var attr = document.getElementById(attrName);
    if (attr != undefined && attr != null) {
        attr.href = "";
        attr.title = "";
        attr.setAttribute("style", "visibility: hidden");
        attr.setAttribute("class", "");
    }
}

function OnChange_Contractor() {
    var contractor = Xrm.Page.getAttribute("pnl_customertypecontractor").getValue();
    if (contractor == false) {
        Xrm.Page.getAttribute("pnl_contractorharvesting").setValue(null);
        Xrm.Page.getAttribute("pnl_contractorspraying").setValue(null);
        Xrm.Page.getAttribute("pnl_contractorhaysilage").setValue(null);
        Xrm.Page.getAttribute("pnl_contractorspreading").setValue(null);
        Xrm.Page.getAttribute("pnl_contractorcultivation").setValue(null);
        Xrm.Page.getAttribute("pnl_contractormixed").setValue(null);
    }
}

function OnChange_Farmer() {
    var farmer = Xrm.Page.getAttribute("pnl_customertypefarmer").getValue();
    if (farmer == false) {
        Xrm.Page.getAttribute("pnl_customertypefarmerarable").setValue(false);
        Xrm.Page.getAttribute("pnl_customertypefarmerlivestock").setValue(false);
        Xrm.Page.getAttribute("pnl_customertypefarmerdairy").setValue(false);
        OnChange_Arable();
        OnChange_Livestock();
        OnChange_Dairy();
    }
}

function OnChange_Arable() {
    var arable = Xrm.Page.getAttribute("pnl_customertypefarmerarable").getValue();
    if (arable == false) {
        Xrm.Page.getAttribute("pnl_farmercerealsha").setValue(null);
        Xrm.Page.getAttribute("pnl_farmercottonha").setValue(null);
        Xrm.Page.getAttribute("pnl_farmerpotatoesha").setValue(null);
        Xrm.Page.getAttribute("pnl_farmeroilseedsha").setValue(null);
        Xrm.Page.getAttribute("pnl_farmerhorticultureha").setValue(null);
        Xrm.Page.getAttribute("pnl_farmerotherha").setValue(null);
        Xrm.Page.getAttribute("pnl_farmerotherpleasespecify").setValue(null);
    }
}

function OnChange_Livestock() {
    var livestock = Xrm.Page.getAttribute("pnl_customertypefarmerlivestock").getValue();
    if (livestock == false) {
        Xrm.Page.getAttribute("pnl_livestocksheephead").setValue(null);
        Xrm.Page.getAttribute("pnl_livestockbeefhead").setValue(null);
    }
}

function OnChange_Dairy() {
    var dairy = Xrm.Page.getAttribute("pnl_customertypefarmerdairy").getValue();
    if (dairy == false) {
        Xrm.Page.getAttribute("pnl_cowshead").setValue(null);
        Xrm.Page.getAttribute("pnl_dairypastureha").setValue(null);
    }
}

function removeFieldFade_OnLoad() {
    HideMachineCostsSubGridAddButton();
    var attributes = Xrm.Page.data.entity.attributes.get();
    for (var i in attributes) {
        //        attributes[i].setRequiredLevel("required");
        var thisEdit = document.getElementById(attributes[i].getName());
        if (thisEdit != null) {
            var nodes = thisEdit.getElementsByTagName('div');
            if (nodes != undefined && nodes != null) {
                for (var i = 0; i < nodes.length; i++) {
                    if (nodes[i].className != undefined && nodes[i].className != null && nodes[i].className == "ms-crm-Inline-GradientMask") {
                        nodes[i].setAttribute("class", "");
                    }
                }
            }
        }
    }
}

function customersegmentation_onLoad() {
    if (Xrm.Page.getAttribute("pnl_customerstatusid").getValue() != "OK") {
        if (Xrm.Page.getAttribute("pnl_customerstatusid").getValue() != null) {
            var dstatus = Xrm.Page.getAttribute("pnl_customerstatusid").getValue();
            Xrm.Page.ui.setFormNotification('Please note that debtor status is ' + dstatus[0].name, 'WARNING', 'debtor_status');
        }
    }
    else {
        Xrm.Page.ui.clearFormNotification("debtor_status");
    }
}

function HideMachineCostsSubGridAddButton() {
    var attrName = "QuoteSubgrid_addImageButton";
    var attr = document.getElementById(attrName);
    if (attr != undefined && attr != null) {
        attr.href = "";
        attr.title = "";
        attr.setAttribute("style", "visibility: hidden");
        attr.setAttribute("class", "");
    }
}

// Code to increase the Header Tiles
function WaitHeaderFieldFade() {
    if (!isIosDevice()) {
        window.setTimeout(HeaderFieldFade, 10);
    }
}
var resizeTimer;
$(window).resize(function () {
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(WaitHeaderFieldFade, 10);
});

function HeaderFieldFade() {
    var headermain = document.getElementById("HeaderTilesWrapperLayoutElement");
    var headerclass = headermain.getElementsByClassName("ms-crm-HeaderTilesWrapperElement");
    var header1 = headerclass[0].getElementsByClassName("ms-crm-HeaderTileElement ms-crm-HeaderTileElementPosition0");
    var header2 = headerclass[0].getElementsByClassName("ms-crm-HeaderTileElement ms-crm-HeaderTileElementPosition1");
    var header3 = headerclass[0].getElementsByClassName("ms-crm-HeaderTileElement ms-crm-HeaderTileElementPosition2");
    var header4 = headerclass[0].getElementsByClassName("ms-crm-HeaderTileElement ms-crm-HeaderTileElementPosition3");
    header1[0].style.width = '200px';
    header2[0].style.width = '200px';
    header3[0].style.width = '200px';
    header4[0].style.width = '200px';
}

function isIosDevice() {
    var uaString = navigator.userAgent;
    if (IsNull(uaString)) return false;
    uaString = uaString.toLowerCase();
    return uaString != "" && uaString.search("ipad|ipod|iphone") > -1
}
