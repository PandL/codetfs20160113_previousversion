/// <reference path="XrmServiceToolkit.js" />

var currentDateTime = new Date();

function onLoad() {
    //console.time("OnLoad");

    load_css_file('/WebResources/new_left_align_css');
    var headermain = document.getElementById("HeaderTilesWrapperLayoutElement");
    var headerclass = headermain.getElementsByClassName("ms-crm-HeaderTilesWrapperElement");
    var header1 = headerclass[0].getElementsByClassName("ms-crm-HeaderTileElement ms-crm-HeaderTileElementPosition0");
    var header2 = headerclass[0].getElementsByClassName("ms-crm-HeaderTileElement ms-crm-HeaderTileElementPosition1");
    var header3 = headerclass[0].getElementsByClassName("ms-crm-HeaderTileElement ms-crm-HeaderTileElementPosition2");
    header1[0].style.width = '210px';
    header2[0].style.width = '210px';
    header3[0].style.width = '210px';

/*
styling changed to different format-- rg -- 27.02.2014
   header1[0].setAttribute("style", "width: 210px;");
   header2[0].setAttribute("style", "width: 210px;");
    header3[0].setAttribute("style", "width: 210px;");
*/

    var CRM_FORM_TYPE = Xrm.Page.ui.getFormType();
    if (CRM_FORM_TYPE != 1) {
        var expiryDate = Xrm.Page.getAttribute("pnl_quoteexpirationdate").getValue();
        var currdate = new Date();
        currdate.setHours(0, 0, 0, 0);
        if (expiryDate != null && expiryDate < currdate) {
            Xrm.Page.ui.setFormNotification("This quote can not be activated because the quote pricing has expired!");
        }
    }
    
    SalesAidRequested_OnChange();
    SalesAidApproved_OnChange();
    //debugger

    //console.time("SetWorksheetStyle");

    SetWorksheetStyle();

    //console.timeEnd("SetWorksheetStyle");

    if (CRM_FORM_TYPE != 1) {
        DisableSalesAidAttributes();
    }

    if (Xrm.Page.getAttribute("pnl_savedfromexperlogix").getValue() == 1) {
        Xrm.Page.ui.tabs.get("machinecosts").setDisplayState("expanded");
        Xrm.Page.ui.tabs.get("salesgp").setDisplayState("expanded");
    }

    //console.timeEnd("OnLoad");
}

function lostQuote() {
    var dialogId = '67257112-0D1B-4CB7-BBB0-2E363F4F31EF'; //This is the GUID of the target dialog process
    var entityName = 'quote';
    var recordId = Xrm.Page.data.entity.getId();

    LaunchDialog(dialogId, entityName, recordId);
    Mscrm.RefreshPageCommandHandler.executeCommand;
}


function LaunchDialog(dialogId, entityName, recordId) {
    var serverUrl = Xrm.Page.context.getServerUrl();
    recordId = recordId.replace("{", "");
    recordId = recordId.replace("}", "");

    var dialogPath = serverUrl + '/cs/dialog/rundialog.aspx?DialogId=%7b' + dialogId + '%7d&EntityName=' + entityName + '&ObjectId=%7b' + recordId + '%7d';
    window.open(dialogPath);

}

function disableRibbonButton(buttonId) {
    var buttonID = buttonId;
    var btn = window.top.document.getElementById(buttonID);

    var intervalId = window.setInterval(function () {
        if (btn != null) {
            window.clearInterval(intervalId);
            btn.disabled = true;
        }
    }, 50);
}

function isLost(recordId) {
    var cols = ["pnl_datelost"];
    var retrievedQuote = XrmServiceToolkit.Soap.Retrieve("quote", recordId, cols);
    var objDateLost = retrievedQuote.attributes['pnl_datelost'];
    if (objDateLost != undefined && objDateLost.value != null) {
        isLost = true;
    }

    return isLost;
}

function SalesAidRequested_OnChange() {
    if (Xrm.Page.getAttribute("pnl_salesaidrequested").getValue() != null)
        Xrm.Page.getAttribute("pnl_salesaidrequestedreason").setRequiredLevel("required");
    else
        Xrm.Page.getAttribute("pnl_salesaidrequestedreason").setRequiredLevel("none");
}

function SalesAidApproved_OnChange() {
    var saStatus = Xrm.Page.getAttribute("pnl_salesaidapprovedstatus");

    if (saStatus != null && saStatus.getValue() == 20 || saStatus.getValue() == 30) {
        disableFormFields(true);
    }
}

function QuoteExpired(recordId) {
    var expired = false;
    var cols = ["pnl_quoteexpirationdate"];
    var retrievedQuote = XrmServiceToolkit.Soap.Retrieve("quote", recordId, cols);
    var objExpirationDate = retrievedQuote.attributes['pnl_quoteexpirationdate'];

    if ((objExpirationDate != undefined && objExpirationDate.value != null) && (objExpirationDate.value.getTime() < currentDateTime)) {
        expired = true;
    }

    return expired;
}

function SalesAidRequest() {
    var sa = Xrm.Page.getAttribute("pnl_salesaidrequest");

    if (sa == null || !sa.getValue()) {
        sa.setSubmitMode("always");
        sa.setValue(true);
        Xrm.Page.data.entity.save();
        reloadQuotePage();
    }
    else
        alert("Sales Aid has already been requested!");
}

function SalesAidApprove() {
    var saStatus = Xrm.Page.getAttribute("pnl_salesaidapprovedstatus");
    saStatus.setSubmitMode("always");
    saStatus.setValue(20);

    Xrm.Page.data.entity.save();
    reloadQuotePage();
}

function SalesAidDecline() {
    var saStatus = Xrm.Page.getAttribute("pnl_salesaidapprovedstatus");
    saStatus.setSubmitMode("always");
    saStatus.setValue(30);

    Xrm.Page.data.entity.save();
    reloadQuotePage();
}

function reloadQuotePage() {
    window.location.reload(true);
}

function DisableSalesAidAttributes() {
    var saRequest = Xrm.Page.getAttribute('pnl_salesaidrequest').getValue();
    if (saRequest) {
        if (HasRole("Product Manager") || HasRole("System Administrator")) {
            Xrm.Page.ui.controls.get('pnl_salesaidrequested').setDisabled(true);
            Xrm.Page.ui.controls.get('pnl_salesaidrequestedreason').setDisabled(true);

        }
        else {
            disableFormFields(true);
        }
    }
}

function sectionToggle(sectionName, disableStatus) {
    var ctrlName = Xrm.Page.ui.controls.get();
    for (var i in ctrlName) {
        var ctrl = ctrlName[i];
        var ctrlSection = ctrl.getParent().getName();
        if (ctrlSection == sectionName) {
            ctrl.setDisabled(disableStatus);
        }
    }
}

function tabToggle(tabName, disableStatus) {
    var tabControl = Xrm.Page.ui.tabs.get(tabName);
    if (tabControl != null) {
        Xrm.Page.ui.controls.forEach(
         function (control) {
             if (control.getParent() != undefined && control.getParent() != null) {
                 if (control.getParent().getParent() === tabControl && control.getControlType() != "subgrid") {
                     control.setDisabled(disableStatus);
                 }
             }
         });
    }
}

function HasRole(roleName) {
    var roles = GetCurrentUserRoles();
    if (roles != null) {
        for (i = 0; i < roles.length; i++) {
            if (roles[i] == roleName) {
                return true;
            }
        }
    }
    //}
    return false;
}

function GetCurrentUserRoles() {
    ///<summary>
    /// Sends synchronous request to retrieve the list of the current user's roles.
    ///</summary>
    var xml =
            [
                "<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>" +
                  "<entity name='role'>" +
                    "<attribute name='name' />" +
                    "<attribute name='businessunitid' />" +
                    "<attribute name='roleid' />" +
                    "<order attribute='name' descending='false' />" +
                    "<link-entity name='systemuserroles' from='roleid' to='roleid' visible='false' intersect='true'>" +
                      "<link-entity name='systemuser' from='systemuserid' to='systemuserid' alias='aa'>" +
                        "<filter type='and'>" +
                          "<condition attribute='systemuserid' operator='eq-userid' />" +
                        "</filter>" +
                      "</link-entity>" +
                    "</link-entity>" +
                  "</entity>" +
                "</fetch>"
            ].join("");

    var fetchResult = XrmServiceToolkit.Soap.Fetch(xml);
    var roles = [];

    if (fetchResult !== null && typeof fetchResult != 'undefined') {
        for (var i = 0; i < fetchResult.length; i++) {
            roles[i] = fetchResult[i].attributes["name"].value;
        }
    }

    return roles;
}

function doesControlHaveAttribute(control) {
    var controlType = control.getControlType();
    return controlType != "iframe" && controlType != "webresource" && controlType != "subgrid";
}

function disableFormFields(onOff) {

    Xrm.Page.ui.controls.forEach(function (control, index) {
        if (doesControlHaveAttribute(control)) {
            control.setDisabled(onOff);
        }
    });
}

function ActivateQuote() {

    if (Xrm.Page.data.entity.getIsDirty()) {
        alert("Please save your changes before cancelling the quote.");
        return;
    }

    // create the request
    var request = "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">";
    request += "<s:Body>";
    request += "<Execute xmlns=\"http://schemas.microsoft.com/xrm/2011/Contracts/Services\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">";
    request += "<request i:type=\"b:SetStateRequest\" xmlns:a=\"http://schemas.microsoft.com/xrm/2011/Contracts\" xmlns:b=\"http://schemas.microsoft.com/crm/2011/Contracts\">";
    request += "<a:Parameters xmlns:c=\"http://schemas.datacontract.org/2004/07/System.Collections.Generic\">";
    request += "<a:KeyValuePairOfstringanyType>";
    request += "<c:key>EntityMoniker</c:key>";
    request += "<c:value i:type=\"a:EntityReference\">";
    request += "<a:Id>" + Xrm.Page.data.entity.getId() + "</a:Id>";
    request += "<a:LogicalName>Quote</a:LogicalName>";
    request += "<a:Name i:nil=\"true\" />";
    request += "</c:value>";
    request += "</a:KeyValuePairOfstringanyType>";
    request += "<a:KeyValuePairOfstringanyType>";
    request += "<c:key>State</c:key>";
    request += "<c:value i:type=\"a:OptionSetValue\">";
    request += "<a:Value>1</a:Value>";
    request += "</c:value>";
    request += "</a:KeyValuePairOfstringanyType>";
    request += "<a:KeyValuePairOfstringanyType>";
    request += "<c:key>Status</c:key>";
    request += "<c:value i:type=\"a:OptionSetValue\">";
    request += "<a:Value>3</a:Value>";
    request += "</c:value>";
    request += "</a:KeyValuePairOfstringanyType>";
    request += "</a:Parameters>";
    request += "<a:RequestId i:nil=\"true\" />";
    request += "<a:RequestName>SetState</a:RequestName>";
    request += "</request>";
    request += "</Execute>";
    request += "</s:Body>";
    request += "</s:Envelope>";

    //send set state request
    $.ajax({
        type: "POST",
        contentType: "text/xml; charset=utf-8",
        datatype: "xml",
        url: Xrm.Page.context.getServerUrl() + "/XRMServices/2011/Organization.svc/web",
        data: request,
        beforeSend: function (XMLHttpRequest) {
            XMLHttpRequest.setRequestHeader("Accept", "application/xml, text/xml, */*");
            XMLHttpRequest.setRequestHeader("SOAPAction", "http://schemas.microsoft.com/xrm/2011/Contracts/Services/IOrganizationService/Execute");
        },
        success: function (data, textStatus, XmlHttpRequest) {
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
}

function SetWorksheetStyle() {
    //console.time("SetReadOnly");
    // statecode=0 is Draft
    var status = Xrm.Page.getAttribute("statecode").getValue();
 
    if (status != 0) {

        SetReadOnly("pnl_tradeinmachine1scenario2value", true);
        SetReadOnly("pnl_tradeinmachine1scenario3value", true);
        SetReadOnly("pnl_tradeinmachine2scenario2value", true);
        SetReadOnly("pnl_tradeinmachine2scenario3value", true);
        SetReadOnly("pnl_tradeinmachine3scenario2value", true);
        SetReadOnly("pnl_tradeinmachine3scenario3value", true);
    }
    else {
        var readOnly = EditIsEmpty("pnl_tradeinmachine1description");
        SetReadOnly("pnl_tradeinmachine1scenario2value", readOnly);
        SetReadOnly("pnl_tradeinmachine1scenario3value", readOnly);
        readOnly = EditIsEmpty("pnl_tradeinmachine2description");
        SetReadOnly("pnl_tradeinmachine2scenario2value", readOnly);
        SetReadOnly("pnl_tradeinmachine2scenario3value", readOnly);
        readOnly = EditIsEmpty("pnl_tradeinmachine3description");
        SetReadOnly("pnl_tradeinmachine3scenario2value", readOnly);
        SetReadOnly("pnl_tradeinmachine3scenario3value", readOnly);
    }
    //console.timeEnd("SetReadOnly");

 
    SetCurrencyEditStyle("pnl_estimatedrevenue");

    //console.time("MachineCosts");
    SetCurrencyEditStyle("pnl_subtotallistprice");
    SetCurrencyEditStyle("pnl_subtotaldealernetprice");
    SetCurrencyEditStyle("pnl_subtotaldiscountpercentage");

    SetCurrencyEditStyle("pnl_totallocallysourceditemslistprice");
    SetCurrencyEditStyle("pnl_totallocallysourceditemsdealernetprice");
    SetCurrencyEditStyle("pnl_totallocallysourceditemsdiscountpercentag");

    SetCurrencyEditStyle("pnl_otherdealercostsfreight");
    SetCurrencyEditStyle("pnl_otherdealercostspredelivery");
    SetCurrencyEditStyle("pnl_otherdealerinterestsubsidy");
    SetCurrencyEditStyle("pnl_totalotherdealercosts");


    //pnl_salesaidapproved
    SetCurrencyEditStyle("pnl_totallistprice");
    SetCurrencyEditStyle("pnl_scenario1totaldealernetcost");
    SetCurrencyEditStyle("pnl_potentialgpatlistprice");
    //console.timeEnd("MachineCosts");

    //console.time("GrossProfit");
    SetLabelStyle("pnl_scenarioonelabel", true);
    SetLabelStyle("pnl_scenariotwolabel", true);
    SetLabelStyle("pnl_scenariothreelabel", true);

    SetLabelStyle("pnl_dealernetcostlabel", false);
    SetCurrencyEditStyle("pnl_scenario1totaldealernetcost1");
    SetCurrencyEditStyle("pnl_scenario2totaldealernetcost");
    SetCurrencyEditStyle("pnl_scenario3totaldealernetcost");

    SetLabelStyle("pnl_grossprofitpercentagelabel", false);
    SetCurrencyEditStyle("pnl_scenario1grossprofitpercentage");
    SetCurrencyEditStyle("pnl_scenario2grossprofitpercentage");
    SetCurrencyEditStyle("pnl_scenario3grossprofitpercentage");

    SetLabelStyle("pnl_grossprofitdollarlabel", false);
    SetCurrencyEditStyle("pnl_scenario1grossprofitdollar");
    SetCurrencyEditStyle("pnl_scenario2grossprofitdollar");
    SetCurrencyEditStyle("pnl_scenario3grossprofitdollar");

    SetLabelStyle("pnl_netsellingpricegplabel", false);
    SetCurrencyEditStyle("pnl_scenario1netsellingprice");
    SetCurrencyEditStyle("pnl_scenario2netsellingpriceinternal");
    SetCurrencyEditStyle("pnl_scenario3netsellingpriceinternal");
    //console.timeEnd("GrossProfit");
    
    //console.time("ChangeOver");
    SetLabelStyle("pnl_listpricenewmachineschangeoverlabel", false);
    SetCurrencyEditStyle("pnl_totallistprice1");
    SetCurrencyEditStyle("pnl_totallistprice2");
    SetCurrencyEditStyle("pnl_totallistprice3");

    SetLabelStyle("pnl_lessdiscountschangeoverlabel", false);
    SetCurrencyEditStyle("pnl_scenario1changeoverdiscounts");
    SetCurrencyEditStyle("pnl_scenario2changeoverdiscounts");
    SetCurrencyEditStyle("pnl_scenario3changeoverdiscounts");

    SetLabelStyle("pnl_netsellingpricechangeoverlabel", false);
    SetCurrencyEditStyle("pnl_scenario1netsellingprice1");
    SetCurrencyEditStyle("pnl_scenario2netsellingprice");
    SetCurrencyEditStyle("pnl_scenario3netsellingprice");
    //console.timeEnd("ChangeOver");

    //console.time("TradeIn");
    SetLabelStyle("pnl_tradeinmachine1description", false);
    SetCurrencyEditStyle("pnl_tradeinmachine1bookvalue");
    SetCurrencyEditStyle("pnl_tradeinmachine1scenario2value");
    SetCurrencyEditStyle("pnl_tradeinmachine1scenario3value");

    SetLabelStyle("pnl_tradeinmachine2description", false);
    SetCurrencyEditStyle("pnl_tradeinmachine2bookvalue");
    SetCurrencyEditStyle("pnl_tradeinmachine2scenario2value");
    SetCurrencyEditStyle("pnl_tradeinmachine2scenario3value");

    SetLabelStyle("pnl_tradeinmachine3description", false);
    SetCurrencyEditStyle("pnl_tradeinmachine3bookvalue");
    SetCurrencyEditStyle("pnl_tradeinmachine3scenario2value");
    SetCurrencyEditStyle("pnl_tradeinmachine3scenario3value");

    SetLabelStyle("pnl_totalstradeinlabel", false);
    SetCurrencyEditStyle("pnl_totalbookvalue");
    SetCurrencyEditStyle("pnl_scenario2totaltradedvalue");
    SetCurrencyEditStyle("pnl_scenario3totaltradedvalue");
    //console.timeEnd("TradeIn");

    //console.time("Totals");
    SetLabelStyle("pnl_changeoverpricelabel", false);
    SetCurrencyEditStyle("pnl_scenario1changeoverpriceexclgst");
    SetCurrencyEditStyle("pnl_scenario2changeoverpriceexclgst");
    SetCurrencyEditStyle("pnl_scenario3changeoverpriceexclgst");

    SetLabelStyle("pnl_totalsgrossprofitpercentagelabel", false);
    SetCurrencyEditStyle("pnl_scenario1changeovergrossprofitpercentage");
    SetCurrencyEditStyle("pnl_scenario2changeovergrossprofitpercentage");
    SetCurrencyEditStyle("pnl_scenario3changeovergrossprofitpercentage");

    SetLabelStyle("pnl_overtradeamountlabel", false);
    SetCurrencyEditStyle("pnl_scenario2overtradeamount");
    SetCurrencyEditStyle("pnl_scenario3overtradeamount");
    //console.timeEnd("Totals");

    /*
    pnl_scenario1totaldealernetcost1");
    pnl_totallistprice1");
    pnl_totallistprice2");
    pnl_totallistprice3");
    pnl_scenario1netsellingprice1");
    */


}

function EditIsEmpty(editName) {

    var thisEditValue = Xrm.Page.getAttribute(editName).getValue();
    if (thisEditValue == undefined || thisEditValue == null || thisEditValue == "") {
        return true;
    }

    return false;
}

function SetReadOnly(editName, IsReadOnly) {
    var thisEdit = Xrm.Page.getControl(editName);
    if (thisEdit.getDisabled() == IsReadOnly)
        return;
   
   thisEdit.setDisabled(IsReadOnly);   
}

function SetLabelStyle(labelName, isRightAligned) {
    SetReadOnly(labelName, true);
    //Xrm.Page.getControl(labelName).setDisabled(true);
    var labelEdit = document.getElementById(labelName);

    if (labelEdit == null) {
        throw "Failed to locate control: " + labelName;
    }

    var nodes = labelEdit.getElementsByTagName('div');
    if (nodes != undefined && nodes != null && nodes.length > 0) {
        if (isRightAligned) {
            nodes[0].style.textAlign = 'right';
            //nodes[0].disabled = true;
        }
        RemoveFadeOnCurrency(nodes);
    }

    //var labelEditContainer = document.getElementById(labelName + "_d");
    //labelEdit.disabled = true;
    labelEdit.style.color = "black";
}

function SetCurrencyEditStyle(editName) {

    var thisEdit = document.getElementById(editName);

    if (thisEdit == null) {
        throw "Failed to locate control: " + editName;
    }

    var nodes = thisEdit.getElementsByTagName('div');
    if (nodes != undefined && nodes != null && nodes.length > 0) {
        nodes[0].style.textAlign = 'right';
        RemoveFadeOnCurrency(nodes);
    }
   
    var imgnodes = thisEdit.getElementsByTagName('img');
    if (imgnodes != undefined && imgnodes != null && imgnodes.length > 0) {
        HideLockIcon(imgnodes);
    }

    //var currencyEditContainer = document.getElementById(editName + "_d");

    thisEdit.style.textAlign = 'right';
    thisEdit.style.color = "black";

    if (Xrm.Page.getControl(editName).getDisabled() == false) {
        thisEdit.style.borderBottomColor = 'yellow';
        thisEdit.style.backgroundColor = 'yellow';

        var currencySymbolContainer = document.getElementById(editName + "_sym");
        if (currencySymbolContainer != null) {
            currencySymbolContainer.style.borderBottomColor = 'yellow';
            currencySymbolContainer.style.backgroundColor = 'yellow';
        }
    }
    else {
        thisEdit.style.borderBottomColor = '';
        thisEdit.style.backgroundColor = '';
    }

    //thisEdit.style.display = 'none';
    //var redrawFix = thisEdit.offsetHeight;
    thisEdit.style.display = 'block';
}

function OpenReport(id) {

    var errorMessage = "Context is not available.";
    var context;
    if (typeof GetGlobalContext != "undefined") {
        context = GetGlobalContext();
    }
    else {
        if (typeof Xrm != "undefined") {

            context = Xrm.Page.context;
        }
        else {
            alert(errorMessage);
            return;
        }
    }

    var orgUrl = context.getServerUrl();
    var reportUrl = orgUrl + "/crmreports/viewer/viewer.aspx?action=run&context=records&helpID=Landpower%20Quote%20Detailed%20v1.9.rdl&id=%7bB1B7BE63-8146-E311-9409-00155D15CF09%7d&records=" + encodeURIComponent(id) + "&recordstype=1084";
    window.open(reportUrl);
}
function OnTransactionCurrencyChange() { crmForm.all.pricelevelid.DataValue = ""; }

function load_css_file(filename) {
    var fileref = document.createElement("link")
    fileref.setAttribute("rel", "stylesheet")
    fileref.setAttribute("type", "text/css")
    fileref.setAttribute("href", filename)
    document.getElementsByTagName("head")[0].appendChild(fileref)
}

function ModifiedOn_OnChange() {
    Xrm.Page.getControl('MachineCostsSubGrid').refresh();
    Xrm.Page.getControl('LocallySourcedItemsSubGrid').refresh();
}


function RemoveFadeOnCurrency(divnodes) {
    //if (divnodes != undefined && divnodes != null) {
        for (var i = 0; i < divnodes.length; i++) {
            if (divnodes[i].className != undefined && divnodes[i].className != null && divnodes[i].className == "ms-crm-Inline-GradientMask") {
                divnodes[i].setAttribute("class", "");
                //break;
            }
        }
    //}
}

function HideLockIcon(imgnodes) {
    //if (imgnodes != undefined && imgnodes != null) {
        for (var i = 0; i < imgnodes.length; i++) {
            if (imgnodes[i].className != undefined && imgnodes[i].className != null && imgnodes[i].className == "ms-crm-ImageStrip-inlineedit_locked") {
                imgnodes[i].setAttribute("class", "");
                //break;
            }
        }
    //}
}




