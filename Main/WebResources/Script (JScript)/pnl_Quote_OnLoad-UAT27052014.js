/// <reference path="XrmServiceToolkit.js" />

var currentDateTime = new Date();
var isLoading = true;

function onLoad() {
    //alert("onLoad function triggered");
    //Xrm.Page.getAttribute("statecode").setSubmitMode("never");
    //Xrm.Page.getAttribute("statuscode").setSubmitMode("never");
    //console.log(Xrm.Page.data.entity.getId());
    load_css_file('/WebResources/new_left_align_css');

    var CRM_FORM_TYPE = Xrm.Page.ui.getFormType();
    if (CRM_FORM_TYPE != 1) {
        var expiryDate = Xrm.Page.getAttribute("pnl_quoteexpirationdate").getValue();
        var currdate = new Date();
        currdate.setHours(0, 0, 0, 0);
        if (expiryDate != null && expiryDate < currdate) {
            Xrm.Page.ui.setFormNotification("This quote can not be activated because the quote pricing has expired! To fix this, click 'Configure' and then click 'Edit' and 'Save and Close' for each machine. This ensures you have current pricing for your quote.");
        }
    }

    SalesAidRequested_OnChange();
    SalesAidApproved_OnChange();
    SetWorksheetStyle();

    if (CRM_FORM_TYPE != 1) {
        DisableSalesAidAttributes();
    }

    if (Xrm.Page.getAttribute("pnl_savedfromexperlogix").getValue() == 1) {
        Xrm.Page.ui.tabs.get("machinecosts").setDisplayState("expanded");
        Xrm.Page.ui.tabs.get("salesgp").setDisplayState("expanded");
    }
    isLoading = false;
    WaitHeaderFieldFade();
    LockSalesAidApprovedStatus();
}

function WaitHeaderFieldFade() {
    window.setTimeout(HeaderFieldFade, 10);
}

var resizeTimer;
$(window).resize(function () {
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(HeaderFieldFade, 10);
});

function HeaderFieldFade() {
    var headermain = document.getElementById("HeaderTilesWrapperLayoutElement");
    var headerclass = headermain.getElementsByClassName("ms-crm-HeaderTilesWrapperElement");
    var header1 = headerclass[0].getElementsByClassName("ms-crm-HeaderTileElement ms-crm-HeaderTileElementPosition0");
    var header2 = headerclass[0].getElementsByClassName("ms-crm-HeaderTileElement ms-crm-HeaderTileElementPosition1");
    var header3 = headerclass[0].getElementsByClassName("ms-crm-HeaderTileElement ms-crm-HeaderTileElementPosition2");
    header1[0].style.width = '210px';
    header2[0].style.width = '210px';
    header3[0].style.width = '210px';
}

function LockSalesAidApprovedStatus() {
    Xrm.Page.ui.controls.get('pnl_salesaidapprovedstatus').setDisabled(true);
}


function statusofForm() {

    var statusstate = Xrm.Page.getAttribute("statuscode").getValue();
    //alert (Xrm.Page.getAttribute("statuscode").getValue() );
    //debugger;
    if (statusstate == 125760007) {
        return true;
    }
    if (statusstate == 1) {
        return true;
    }
    if (statusstate == 3) {
        return true;
    }
    if (statusstate == 4) {
        return false;
    }
}

function lostQuote() {
    var dialogId = '67257112-0D1B-4CB7-BBB0-2E363F4F31EF'; //This is the GUID of the target dialog process
    var entityName = 'quote';
    var recordId = Xrm.Page.data.entity.getId();

    LaunchDialog(dialogId, entityName, recordId);
    Mscrm.RefreshPageCommandHandler.executeCommand;
}

function LaunchDialog(dialogId, entityName, recordId) {
    var serverUrl = Xrm.Page.context.getServerUrl();
    recordId = recordId.replace("{", "");
    recordId = recordId.replace("}", "");

    var dialogPath = serverUrl + '/cs/dialog/rundialog.aspx?DialogId=%7b' + dialogId + '%7d&EntityName=' + entityName + '&ObjectId=%7b' + recordId + '%7d';
    window.open(dialogPath);

}

function disableRibbonButton(buttonId) {
    var buttonID = buttonId;
    var btn = window.top.document.getElementById(buttonID);

    var intervalId = window.setInterval(function () {
        if (btn != null) {
            window.clearInterval(intervalId);
            btn.disabled = true;
        }
    }, 50);
}

function isLost(recordId) {
    var result = false;
    var cols = ["statecode"];
    var retrievedQuote = XrmServiceToolkit.Soap.Retrieve("quote", recordId, cols);
    var objState = retrievedQuote.attributes["statecode"];
    //alert(objState.value);
    if (objState != undefined && objState.value != null) {
        if (objState.value == 2 || objState.value == 3) {
            result = true;
        }
    }
    //alert(result);
    return result;
}

function SalesAidRequested_OnChange() {
    if (Xrm.Page.getAttribute("pnl_salesaidrequested").getValue() != null)
        Xrm.Page.getAttribute("pnl_salesaidrequestedreason").setRequiredLevel("required");
    else
        Xrm.Page.getAttribute("pnl_salesaidrequestedreason").setRequiredLevel("none");
}

function SalesAidApproved_OnChange() {
    var saStatus = Xrm.Page.getAttribute("pnl_salesaidapprovedstatus");

    if (saStatus != null && saStatus.getValue() == 20 || saStatus.getValue() == 30) {
        disableFormFields(true);
    }
}

//function QuoteExpired(recordId) {
function QuoteExpired() {
    var expired = false;
    //var cols = ["pnl_quoteexpirationdate"];
    //var retrievedQuote = XrmServiceToolkit.Soap.Retrieve("quote", recordId, cols);
    //var objExpirationDate = retrievedQuote.attributes['pnl_quoteexpirationdate'];
    var objExpirationDate = Xrm.Page.getAttribute("pnl_quoteexpirationdate");

    //if ((objExpirationDate != undefined && objExpirationDate.value != null) && (objExpirationDate.value.getTime() < currentDateTime)) {
    if (objExpirationDate != undefined && objExpirationDate != null) {
        var objValue = objExpirationDate.getValue();
        if (objValue < currentDateTime) {
            expired = true;
        }
    }

    return expired;
}

function SalesAidRequest() {
    try {
        Xrm.Page.ui.setFormNotification("Requesting Sales Aid, Please Wait...", "WARNING", "2");
        window.setTimeout(SalesAidRequest2, 10);
    }
    catch (err) {
        alert(err.message);
        Xrm.Page.ui.clearFormNotification("2");
    }
    HeaderFieldFade();
}

function SalesAidRequest2() {
    var sa = Xrm.Page.getAttribute("pnl_salesaidrequest");
    if (sa == null || !sa.getValue()) {
        sa.setSubmitMode("always");
        sa.setValue(true);
        Xrm.Page.data.save().then(
            function () {
                sa.setSubmitMode("dirty");
                if (XrmServiceToolkit.Soap.SetState("quote", Xrm.Page.data.entity.getId(), 0, 125760007)) {
                    window.setTimeout(forceHardPageRefresh, 10);
                }
                Xrm.Page.ui.clearFormNotification("2");
            },
            function () {
                sa.setSubmitMode("dirty");
                Xrm.Page.ui.clearFormNotification("2");
            });
    }
    else
        alert("Sales Aid has already been requested!");
}

function SalesAidApprove() {
    try {
        Xrm.Page.ui.setFormNotification("Approving Sales Aid, Please Wait...", "WARNING", "3");
        window.setTimeout(SalesAidApprove2, 10);
    }
    catch (err) {
        alert(err.message);
        Xrm.Page.ui.clearFormNotification("3");
    }
    HeaderFieldFade();
}

function SalesAidApprove2() {
    var saStatus = Xrm.Page.getAttribute("pnl_salesaidapprovedstatus");
    saStatus.setSubmitMode("always");
    saStatus.setValue(20);
    Xrm.Page.data.save().then(
        function () {
            saStatus.setSubmitMode("dirty");
            if (XrmServiceToolkit.Soap.SetState("quote", Xrm.Page.data.entity.getId(), 1, 3)) {
                window.setTimeout(forceHardPageRefresh, 10);
            }
            Xrm.Page.ui.clearFormNotification("3");
        },
        function () {
            alert("Save Fail");
            saStatus.setSubmitMode("dirty");
            Xrm.Page.ui.clearFormNotification("3");
        });
}

function SalesAidDecline() {
    try {
        Xrm.Page.ui.setFormNotification("Declining Sales Aid, Please Wait...", "WARNING", "4");
        window.setTimeout(SalesAidDecline2, 10);
    }
    catch (err) {
        alert(err.message);
        Xrm.Page.ui.clearFormNotification("4");
    }
    HeaderFieldFade();
}

function SalesAidDecline2() {
    var saStatus = Xrm.Page.getAttribute("pnl_salesaidapprovedstatus");
    saStatus.setSubmitMode("always");
    saStatus.setValue(30);
    Xrm.Page.data.save().then(
        function () {
            saStatus.setSubmitMode("dirty");
            if (XrmServiceToolkit.Soap.SetState("quote", Xrm.Page.data.entity.getId(), 1, 3)) {
                window.setTimeout(forceHardPageRefresh, 10);
            }
            Xrm.Page.ui.clearFormNotification("4");
        },
        function () {
            alert("Save Fail");
            saStatus.setSubmitMode("dirty");
            Xrm.Page.ui.clearFormNotification("4");
        });
}

function forceHardPageRefresh() {
    Mscrm.ReadFormUtilities.openInSameFrame(window._etc, Xrm.Page.data.entity.getId());
}

function checkForDirty() {
    alert("Is Form Dirty: " + Xrm.Page.data.entity.getIsDirty());

    Xrm.Page.data.entity.attributes.forEach(function (attribute, index) {
        if (attribute.getIsDirty() == true) {
            alert("Attribute: " + attribute.getName() + " is dirty.");
        }
    });
}

function checkForSubmitAlways() {
    Xrm.Page.data.entity.attributes.forEach(function (attribute, index) {
        if (attribute.getSubmitMode() != "dirty") {
            alert("Attribute: " + attribute.getName() + " is Submit:" + attribute.getSubmitMode());
        }
    });
}

function setIsDirtyToSubmitNever() {
    Xrm.Page.data.entity.attributes.forEach(function (attribute, index) {
        if (attribute.getIsDirty() == true) {
            //alert("Attribute: " + attribute.getName() + " is dirty.");
            attribute.setSubmitMode("never");
        }
    });
}

//function SalesAidRequest() {
//    var sa = Xrm.Page.getAttribute("pnl_salesaidrequest");
//     if (sa == null || !sa.getValue()) {
//        sa.setSubmitMode("always");
//        sa.setValue(true);
//         // Xrm.Page.data.entity.save();
//        Xrm.Page.data.save().then(successCallback_Request, null);
//        Xrm.Page.data.entity.refresh();


//    }
//    else
//        alert("Sales Aid has already been requested!");
//}

//function successCallback_Request() {
//    var entityId = Xrm.Page.data.entity.getId();
//    var entityName = 'quote';
//    var SalesAidResponse = "Request";
//   var ActionUniqueName = 'pnl_SalesAidActions';
//   SalesaAidResponseExecuteAction(entityId, entityName, SalesAidResponse, ActionUniqueName);
//   Xrm.Page.data.refresh();
////   Xrm.Page.data.entity.save();
//   //Mscrm.ReadFormUtilities.openInSameFrame(window._etc, Xrm.Page.data.entity.getId());

//}



//function SalesAidApprove() {
//    var saStatus = Xrm.Page.getAttribute("pnl_salesaidapprovedstatus");
//    saStatus.setSubmitMode("always");
//    saStatus.setValue(20);
//    // Xrm.Page.data.entity.save();
//    Xrm.Page.data.save().then(successCallback_Authorised, null);
//    Xrm.Page.data.entity.refresh();

//}

//function SalesAidDecline() {
//    var saStatus = Xrm.Page.getAttribute("pnl_salesaidapprovedstatus");
//    saStatus.setSubmitMode("always");
//    saStatus.setValue(30);
//    Xrm.Page.data.save().then(successCallback_Authorised, null);
//    Xrm.Page.data.entity.refresh();

//    //Xrm.Page.data.save();
//}

//function successCallback_Authorised() {
//    var entityId = Xrm.Page.data.entity.getId();
//    var entityName = 'quote';
//    var SalesAidResponse = "Authorised";
//    var ActionUniqueName = 'pnl_SalesAidActions';
//    SalesaAidResponseExecuteAction(entityId, entityName, SalesAidResponse, ActionUniqueName);
//   // Xrm.Page.data.entity.save();

//}


//function SalesaAidResponseExecuteAction(EntityId, EntityName, SalesAidResponse, ActionUniqueName) {
//    // Creating the request XML for calling the Action
//    var requestXML = ""
//    requestXML += "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">";
//    requestXML += "  <s:Body>";
//    requestXML += "    <Execute xmlns=\"http://schemas.microsoft.com/xrm/2011/Contracts/Services\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">";
//    requestXML += "      <request xmlns:a=\"http://schemas.microsoft.com/xrm/2011/Contracts\">";
//    requestXML += "        <a:Parameters xmlns:b=\"http://schemas.datacontract.org/2004/07/System.Collections.Generic\">";
//    requestXML += "          <a:KeyValuePairOfstringanyType>";
//    requestXML += "            <b:key>Target</b:key>";
//    requestXML += "            <b:value i:type=\"a:EntityReference\">";
//    requestXML += "              <a:Id>" + EntityId + "</a:Id>";
//    requestXML += "              <a:LogicalName>" + EntityName + "</a:LogicalName>";
//    requestXML += "              <a:Name i:nil=\"true\" />";
//    requestXML += "            </b:value>";
//    requestXML += "          </a:KeyValuePairOfstringanyType>";
//    requestXML += "          <a:KeyValuePairOfstringanyType>";
//    requestXML += "            <b:key>SalesAidResponse</b:key>";
//    requestXML += "            <b:value i:type=\"c:string\" xmlns:c=\"http://www.w3.org/2001/XMLSchema\">" + SalesAidResponse + "</b:value>";
//    requestXML += "          </a:KeyValuePairOfstringanyType>";
//    requestXML += "        </a:Parameters>";
//    requestXML += "        <a:RequestId i:nil=\"true\" />";
//    requestXML += "        <a:RequestName>" + ActionUniqueName + "</a:RequestName>";
//    requestXML += "      </request>";
//    requestXML += "    </Execute>";
//    requestXML += "  </s:Body>";
//    requestXML += "</s:Envelope>";
//    var req = new XMLHttpRequest();
//    req.open("POST", GetClientUrl(), false)
//    req.setRequestHeader("Accept", "application/xml, text/xml, */*");
//    req.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
//    req.setRequestHeader("SOAPAction", "http://schemas.microsoft.com/xrm/2011/Contracts/Services/IOrganizationService/Execute");
//    req.send(requestXML);
//    //Get the Resonse from the CRM Execute method
//    var response = req.responseXML;
//    return response;
//}

//function GetClientUrl() {
//    if (typeof Xrm.Page.context == "object") {
//        clientUrl = Xrm.Page.context.getClientUrl();
//    }
//    var ServicePath = "/XRMServices/2011/Organization.svc/web";
//    return clientUrl + ServicePath;
//}



function preventAutoSave(econtext) {

    var eventArgs = econtext.getEventArgs();
    if (eventArgs.getSaveMode() == 70 || eventArgs.getSaveMode() == 2) {
        eventArgs.preventDefault();
    }
}

function DisableSalesAidAttributes() {

    var saRequest = Xrm.Page.getAttribute('pnl_salesaidrequest').getValue();
    if (saRequest) {
        var roleName = ["D4BC776C-6B55-42AE-B3AD-7F45D45BE640",//LIVE Product Manager - NZ
                 "86ADFA88-02FC-4C3B-A533-F6797FC9628B",//LIVE Product Manager - AUS
                 "BFE5DF82-B029-E211-9DF3-1CC1DE79B4CA",//LIVE System Administrator - Landpower
                 "D812B114-966B-49D8-A2A9-04A8B15DADB1",//LIVE System Administrator - NZ
                 "421788C6-B91D-44E5-AD09-2543BF915C68",//LIVE System Administrator - AUS
                 "63E8ED0D-F0D4-46DE-8AFF-3C48A524088C",//UAT Product Manager - NZ
                 "4416FE0E-DD18-44F2-8892-3E969394B81D",//UAT Product Manager - AUS
                 "40F4A74E-FD8A-E311-9B7C-B4B52F5E28B4",//UAT System Administrator - Landpower
                 "ADF19F78-94E1-4A46-BEC5-957CA617908B",//UAT System Administrator - NZ
                 "84073AA1-5994-463C-BA29-7CF6F98299E1",//UAT System Administrator - AUS
                 "6EC7E850-C2ED-E211-BD6A-1CC1DE6E4BB7",//ALL Product Manager - Landpower
                 "7B69A8C3-E88A-E311-8F08-D89D6779161C",//DEV1 System Administrator - Landpower
                 "24E8BEB8-EA20-4126-AFA5-202961F52F31",//DEV1 System Administrator - NZ
                 "EF7251DD-E556-412E-BAF1-83EF1836395A",//DEV1 System Administrator - AUS
                 "5796D128-B853-4223-9E7C-08A7B534B879",//DEV1 Product Manager - NZ
                 "9E110A6F-C0C8-49A8-9432-B405695EAEBB"];//DEV1 Product Manager - AUS

        if (HasRole(roleName)) {
            Xrm.Page.ui.controls.get('pnl_salesaidrequested').setDisabled(true);
            Xrm.Page.ui.controls.get('pnl_salesaidrequestedreason').setDisabled(true);
            //Xrm.Page.ui.controls.get('pnl_salesaidapprovedstatus').setDisabled(true);
        }
        else {
            disableFormFields(true);
        }
    }
}

function HasRole(roleName) {
    var roles = Xrm.Page.context.getUserRoles();

    if (roles != null) {
        for (i = 0; i < roles.length; i++) {
            for (j = 0; j < roleName.length; j++) {

                if (roles[i] == roleName[j].toLowerCase()) {
                    return true;
                }
            }
        }
    }
    return false;
}

function doesControlHaveAttribute(control) {
    var controlType = control.getControlType();
    return controlType != "iframe" && controlType != "webresource" && controlType != "subgrid";
}

function disableFormFields(onOff) {

    Xrm.Page.ui.controls.forEach(function (control, index) {
        if (doesControlHaveAttribute(control)) {
            control.setDisabled(onOff);
        }
    });
}

function ActivateQuote() {

    if (Xrm.Page.data.entity.getIsDirty()) {
        alert("Please save your changes before cancelling the quote.");
        return;
    }

    // create the request
    var request = "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">";
    request += "<s:Body>";
    request += "<Execute xmlns=\"http://schemas.microsoft.com/xrm/2011/Contracts/Services\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">";
    request += "<request i:type=\"b:SetStateRequest\" xmlns:a=\"http://schemas.microsoft.com/xrm/2011/Contracts\" xmlns:b=\"http://schemas.microsoft.com/crm/2011/Contracts\">";
    request += "<a:Parameters xmlns:c=\"http://schemas.datacontract.org/2004/07/System.Collections.Generic\">";
    request += "<a:KeyValuePairOfstringanyType>";
    request += "<c:key>EntityMoniker</c:key>";
    request += "<c:value i:type=\"a:EntityReference\">";
    request += "<a:Id>" + Xrm.Page.data.entity.getId() + "</a:Id>";
    request += "<a:LogicalName>Quote</a:LogicalName>";
    request += "<a:Name i:nil=\"true\" />";
    request += "</c:value>";
    request += "</a:KeyValuePairOfstringanyType>";
    request += "<a:KeyValuePairOfstringanyType>";
    request += "<c:key>State</c:key>";
    request += "<c:value i:type=\"a:OptionSetValue\">";
    request += "<a:Value>1</a:Value>";
    request += "</c:value>";
    request += "</a:KeyValuePairOfstringanyType>";
    request += "<a:KeyValuePairOfstringanyType>";
    request += "<c:key>Status</c:key>";
    request += "<c:value i:type=\"a:OptionSetValue\">";
    request += "<a:Value>3</a:Value>";
    request += "</c:value>";
    request += "</a:KeyValuePairOfstringanyType>";
    request += "</a:Parameters>";
    request += "<a:RequestId i:nil=\"true\" />";
    request += "<a:RequestName>SetState</a:RequestName>";
    request += "</request>";
    request += "</Execute>";
    request += "</s:Body>";
    request += "</s:Envelope>";

    //send set state request
    $.ajax({
        type: "POST",
        contentType: "text/xml; charset=utf-8",
        datatype: "xml",
        url: Xrm.Page.context.getServerUrl() + "/XRMServices/2011/Organization.svc/web",
        data: request,
        beforeSend: function (XMLHttpRequest) {
            XMLHttpRequest.setRequestHeader("Accept", "application/xml, text/xml, */*");
            XMLHttpRequest.setRequestHeader("SOAPAction", "http://schemas.microsoft.com/xrm/2011/Contracts/Services/IOrganizationService/Execute");
        },
        success: function (data, textStatus, XmlHttpRequest) {
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
}

function SetWorksheetStyle() {

    // statecode=0 is Draft
    var status = Xrm.Page.getAttribute("statecode").getValue();

    if (status != 0) {
        SetReadOnly("pnl_tradeinmachine1scenario2value", true);
        SetReadOnly("pnl_tradeinmachine1scenario3value", true);
        SetReadOnly("pnl_tradeinmachine2scenario2value", true);
        SetReadOnly("pnl_tradeinmachine2scenario3value", true);
        SetReadOnly("pnl_tradeinmachine3scenario2value", true);
        SetReadOnly("pnl_tradeinmachine3scenario3value", true);
    }
    else {
        var readOnly = EditIsEmpty("pnl_tradeinmachine1description");
        SetReadOnly("pnl_tradeinmachine1scenario2value", readOnly);
        SetReadOnly("pnl_tradeinmachine1scenario3value", readOnly);
        readOnly = EditIsEmpty("pnl_tradeinmachine2description");
        SetReadOnly("pnl_tradeinmachine2scenario2value", readOnly);
        SetReadOnly("pnl_tradeinmachine2scenario3value", readOnly);
        readOnly = EditIsEmpty("pnl_tradeinmachine3description");
        SetReadOnly("pnl_tradeinmachine3scenario2value", readOnly);
        SetReadOnly("pnl_tradeinmachine3scenario3value", readOnly);
    }

    SetCurrencyEditStyle("pnl_estimatedrevenue");

    SetCurrencyEditStyle("pnl_subtotallistprice");
    SetCurrencyEditStyle("pnl_subtotaldealernetprice");
    SetCurrencyEditStyle("pnl_subtotaldiscountpercentage");

    SetCurrencyEditStyle("pnl_totallocallysourceditemslistprice");
    SetCurrencyEditStyle("pnl_totallocallysourceditemsdealernetprice");
    SetCurrencyEditStyle("pnl_totallocallysourceditemsdiscountpercentag");

    SetCurrencyEditStyle("pnl_otherdealercostsfreight");
    SetCurrencyEditStyle("pnl_otherdealercostspredelivery");
    SetCurrencyEditStyle("pnl_otherdealerinterestsubsidy");
    SetCurrencyEditStyle("pnl_totalotherdealercosts");

    //pnl_salesaidapproved
    SetCurrencyEditStyle("pnl_totallistprice");
    SetCurrencyEditStyle("pnl_scenario1totaldealernetcost");
    SetCurrencyEditStyle("pnl_potentialgpatlistprice");

    SetLabelStyle("pnl_scenarioonelabel", true);
    SetLabelStyle("pnl_scenariotwolabel", true);
    SetLabelStyle("pnl_scenariothreelabel", true);

    SetLabelStyle("pnl_dealernetcostlabel", false);
    SetCurrencyEditStyle("pnl_scenario1totaldealernetcost1");
    SetCurrencyEditStyle("pnl_scenario2totaldealernetcost");
    SetCurrencyEditStyle("pnl_scenario3totaldealernetcost");

    SetLabelStyle("pnl_grossprofitpercentagelabel", false);
    SetCurrencyEditStyle("pnl_scenario1grossprofitpercentage");
    SetCurrencyEditStyle("pnl_scenario2grossprofitpercentage");
    SetCurrencyEditStyle("pnl_scenario3grossprofitpercentage");

    SetLabelStyle("pnl_grossprofitdollarlabel", false);
    SetCurrencyEditStyle("pnl_scenario1grossprofitdollar");
    SetCurrencyEditStyle("pnl_scenario2grossprofitdollar");
    SetCurrencyEditStyle("pnl_scenario3grossprofitdollar");

    SetLabelStyle("pnl_netsellingpricegplabel", false);
    SetCurrencyEditStyle("pnl_scenario1netsellingprice");
    SetCurrencyEditStyle("pnl_scenario2netsellingpriceinternal");
    SetCurrencyEditStyle("pnl_scenario3netsellingpriceinternal");

    SetLabelStyle("pnl_listpricenewmachineschangeoverlabel", false);
    SetCurrencyEditStyle("pnl_totallistprice1");
    SetCurrencyEditStyle("pnl_totallistprice2");
    SetCurrencyEditStyle("pnl_totallistprice3");

    SetLabelStyle("pnl_lessdiscountschangeoverlabel", false);
    SetCurrencyEditStyle("pnl_scenario1changeoverdiscounts");
    SetCurrencyEditStyle("pnl_scenario2changeoverdiscounts");
    SetCurrencyEditStyle("pnl_scenario3changeoverdiscounts");

    SetLabelStyle("pnl_netsellingpricechangeoverlabel", false);
    SetCurrencyEditStyle("pnl_scenario1netsellingprice1");
    SetCurrencyEditStyle("pnl_scenario2netsellingprice");
    SetCurrencyEditStyle("pnl_scenario3netsellingprice");

    SetLabelStyle("pnl_tradeinmachine1description", false);
    SetCurrencyEditStyle("pnl_tradeinmachine1bookvalue");
    SetCurrencyEditStyle("pnl_tradeinmachine1scenario2value");
    SetCurrencyEditStyle("pnl_tradeinmachine1scenario3value");

    SetLabelStyle("pnl_tradeinmachine2description", false);
    SetCurrencyEditStyle("pnl_tradeinmachine2bookvalue");
    SetCurrencyEditStyle("pnl_tradeinmachine2scenario2value");
    SetCurrencyEditStyle("pnl_tradeinmachine2scenario3value");

    SetLabelStyle("pnl_tradeinmachine3description", false);
    SetCurrencyEditStyle("pnl_tradeinmachine3bookvalue");
    SetCurrencyEditStyle("pnl_tradeinmachine3scenario2value");
    SetCurrencyEditStyle("pnl_tradeinmachine3scenario3value");

    SetLabelStyle("pnl_totalstradeinlabel", false);
    SetCurrencyEditStyle("pnl_totalbookvalue");
    SetCurrencyEditStyle("pnl_scenario2totaltradedvalue");
    SetCurrencyEditStyle("pnl_scenario3totaltradedvalue");

    SetLabelStyle("pnl_changeoverpricelabel", false);
    SetCurrencyEditStyle("pnl_scenario1changeoverpriceexclgst");
    SetCurrencyEditStyle("pnl_scenario2changeoverpriceexclgst");
    SetCurrencyEditStyle("pnl_scenario3changeoverpriceexclgst");

    SetLabelStyle("pnl_totalsgrossprofitpercentagelabel", false);
    SetCurrencyEditStyle("pnl_scenario1changeovergrossprofitpercentage");
    SetCurrencyEditStyle("pnl_scenario2changeovergrossprofitpercentage");
    SetCurrencyEditStyle("pnl_scenario3changeovergrossprofitpercentage");

    SetLabelStyle("pnl_overtradeamountlabel", false);
    SetCurrencyEditStyle("pnl_scenario2overtradeamount");
    SetCurrencyEditStyle("pnl_scenario3overtradeamount");

    /*
    pnl_scenario1totaldealernetcost1");
    pnl_totallistprice1");
    pnl_totallistprice2");
    pnl_totallistprice3");
    pnl_scenario1netsellingprice1");
    */

    //Set the Trade-In Extra Fields - John 1.05.2014
    if (Xrm.Page.getAttribute('pnl_tradeinmarketpricelabel') != undefined && Xrm.Page.getAttribute('pnl_tradeinmarketpricelabel') != null) {

        if (Xrm.Page.getAttribute('pnl_tradeinspacer1') != undefined && Xrm.Page.getAttribute('pnl_tradeinspacer1') != null) {
            SetSpacerStyle("pnl_tradeinspacer1", true);
            SetSpacerStyle("pnl_tradeinspacer2", true);
            SetSpacerStyle("pnl_tradeinspacer3", true);
            SetSpacerStyle("pnl_tradeinspacer4", true);
        }

        SetLabelStyle("pnl_tradeinmarketpricelabel", true);
        SetLabelStyle("pnl_tradeinsellingmarginlabel1", true);
        SetLabelStyle("pnl_tradeinrepaircostlabel", true);
        //Machine 1
        SetReadOnly("pnl_tradeinmachine1timarketprice", true);
        SetCurrencyEditStyle("pnl_tradeinmachine1timarketprice");
        SetReadOnly("pnl_tradeinmachine1tisellingmargin", true);
        SetCurrencyEditStyle("pnl_tradeinmachine1tisellingmargin");
        SetReadOnly("pnl_tradeinmachine1tirepaircost", true);
        SetCurrencyEditStyle("pnl_tradeinmachine1tirepaircost");
        //Machine 2
        SetReadOnly("pnl_tradeinmachine2timarketprice", true);
        SetCurrencyEditStyle("pnl_tradeinmachine2timarketprice");
        SetReadOnly("pnl_tradeinmachine2tisellingmargin", true);
        SetCurrencyEditStyle("pnl_tradeinmachine2tisellingmargin");
        SetReadOnly("pnl_tradeinmachine2tirepaircost", true);
        SetCurrencyEditStyle("pnl_tradeinmachine2tirepaircost");
        //Machine 3
        SetReadOnly("pnl_tradeinmachine3timarketprice", true);
        SetCurrencyEditStyle("pnl_tradeinmachine3timarketprice");
        SetReadOnly("pnl_tradeinmachine3tisellingmargin", true);
        SetCurrencyEditStyle("pnl_tradeinmachine3tisellingmargin");
        SetReadOnly("pnl_tradeinmachine3tirepaircost", true);
        SetCurrencyEditStyle("pnl_tradeinmachine3tirepaircost");

        //Note: the numbers at the end of the field name might change depending on future Roll Ups
        SetWhiteLabelStyle("pnl_scenariothreelabel1", false);
        SetWhiteLabelStyle("pnl_dealernetcostlabel1", false);
        SetWhiteLabelStyle("pnl_grossprofitpercentagelabel1", false);
        SetWhiteLabelStyle("pnl_grossprofitdollarlabel1", false);
        SetWhiteLabelStyle("pnl_netsellingpricegplabel1", false);
        SetWhiteLabelStyle("pnl_listpricenewmachineschangeoverlabel1", false);
        SetWhiteLabelStyle("pnl_lessdiscountschangeoverlabel1", false);
        SetWhiteLabelStyle("pnl_netsellingpricechangeoverlabel1", false);
        SetWhiteLabelStyle("pnl_tradeinsellingmarginlabel", false);
        SetWhiteLabelStyle("pnl_tradeinmachine1description1", false);
        SetWhiteLabelStyle("pnl_tradeinmachine2description1", false);
        SetWhiteLabelStyle("pnl_tradeinmachine3description1", false);
        SetWhiteLabelStyle("pnl_totalstradeinlabel1", false);
    }


}

function SetWhiteLabelStyle(labelName, isRightAligned) {

    var labelEdit = document.getElementById(labelName);

    if (labelEdit == null) {
        throw "Failed to locate control: " + labelName;
    }

    var nodes = labelEdit.getElementsByTagName('div');
    if (nodes != undefined && nodes != null && nodes.length > 0) {
        if (isRightAligned) {
            nodes[0].style.textAlign = 'right';
        }
        RemoveFadeOnCurrency(nodes);
        nodes[0].style.color = "white";
    }

    var imgnodes = labelEdit.getElementsByTagName('img');
    if (imgnodes != undefined && imgnodes != null && imgnodes.length > 0) {
        HideLockIcon(imgnodes);
    }
}

function SetSpacerStyle(labelName, isRightAligned) {
    SetReadOnly(labelName, true);
    var labelEdit = document.getElementById(labelName);

    if (labelEdit == null) {
        throw "Failed to locate control: " + labelName;
    }

    var nodes = labelEdit.getElementsByTagName('div');
    if (nodes != undefined && nodes != null && nodes.length > 0) {
        var span = nodes[0].getElementsByTagName('span');
        if (span != undefined && span != null && span.length > 0) {
            span[0].innerText = "";
        }
    }

    var imgnodes = labelEdit.getElementsByTagName('img');
    if (imgnodes != undefined && imgnodes != null && imgnodes.length > 0) {
        HideLockIcon(imgnodes);
    }

    labelEdit.style.color = "black";
}

function EditIsEmpty(editName) {

    var thisEditValue = Xrm.Page.getAttribute(editName).getValue();
    if (thisEditValue == undefined || thisEditValue == null || thisEditValue == "") {
        return true;
    }

    return false;
}

function SetReadOnly(editName, IsReadOnly) {
    var thisEdit = Xrm.Page.getControl(editName);
    if (thisEdit.getDisabled() == IsReadOnly)
        return;

    thisEdit.setDisabled(IsReadOnly);
}

function SetLabelStyle(labelName, isRightAligned) {
    SetReadOnly(labelName, true);
    //Xrm.Page.getControl(labelName).setDisabled(true);
    var labelEdit = document.getElementById(labelName);

    if (labelEdit == null) {
        throw "Failed to locate control: " + labelName;
    }

    var nodes = labelEdit.getElementsByTagName('div');
    if (nodes != undefined && nodes != null && nodes.length > 0) {
        if (isRightAligned) {
            nodes[0].style.textAlign = 'right';
            //nodes[0].disabled = true;
        }
        RemoveFadeOnCurrency(nodes);
    }

    var imgnodes = labelEdit.getElementsByTagName('img');
    if (imgnodes != undefined && imgnodes != null && imgnodes.length > 0) {
        HideLockIcon(imgnodes);
    }

    //var labelEditContainer = document.getElementById(labelName + "_d");
    //labelEdit.disabled = true;
    labelEdit.style.color = "black";
}

function SetCurrencyEditStyle(editName) {

    var thisEdit = document.getElementById(editName);

    if (thisEdit == null) {
        throw "Failed to locate control: " + editName;
    }

    var nodes = thisEdit.getElementsByTagName('div');
    if (nodes != undefined && nodes != null && nodes.length > 0) {
        nodes[0].style.textAlign = 'right';
        RemoveFadeOnCurrency(nodes);
    }

    var imgnodes = thisEdit.getElementsByTagName('img');
    if (imgnodes != undefined && imgnodes != null && imgnodes.length > 0) {
        HideLockIcon(imgnodes);
    }

    //var currencyEditContainer = document.getElementById(editName + "_d");

    thisEdit.style.textAlign = 'right';
    thisEdit.style.color = "black";

    if (Xrm.Page.getControl(editName).getDisabled() == false) {
        thisEdit.style.borderBottomColor = 'yellow';
        thisEdit.style.backgroundColor = 'yellow';

        var currencySymbolContainer = document.getElementById(editName + "_sym");
        if (currencySymbolContainer != null) {
            currencySymbolContainer.style.borderBottomColor = 'yellow';
            currencySymbolContainer.style.backgroundColor = 'yellow';
        }
    }
    else {
        thisEdit.style.borderBottomColor = '';
        thisEdit.style.backgroundColor = '';
    }

    //thisEdit.style.display = 'none';
    //var redrawFix = thisEdit.offsetHeight;
    thisEdit.style.display = 'block';
}

function OpenReport(id) {

    var errorMessage = "Context is not available.";
    var context;
    if (typeof GetGlobalContext != "undefined") {
        context = GetGlobalContext();
    }
    else {
        if (typeof Xrm != "undefined") {

            context = Xrm.Page.context;
        }
        else {
            alert(errorMessage);
            return;
        }
    }

    var orgUrl = context.getServerUrl();
    var reportUrl = orgUrl + "/crmreports/viewer/viewer.aspx?action=run&context=records&helpID=Landpower%20Quote%20Detailed%20v1.9.rdl&id=%7bB1B7BE63-8146-E311-9409-00155D15CF09%7d&records=" + encodeURIComponent(id) + "&recordstype=1084";
    window.open(reportUrl);
}

function OnTransactionCurrencyChange() {
    crmForm.all.pricelevelid.DataValue = "";
}

function load_css_file(filename) {
    var fileref = document.createElement("link")
    fileref.setAttribute("rel", "stylesheet")
    fileref.setAttribute("type", "text/css")
    fileref.setAttribute("href", filename)
    document.getElementsByTagName("head")[0].appendChild(fileref)
}

function ModifiedOn_OnChange() {
    Xrm.Page.getControl('MachineCostsSubGrid').refresh();
    Xrm.Page.getControl('LocallySourcedItemsSubGrid').refresh();
    onLoad();
}

function RemoveFadeOnCurrency(divnodes) {
    //if (divnodes != undefined && divnodes != null) {
    for (var i = 0; i < divnodes.length; i++) {
        if (divnodes[i].className != undefined && divnodes[i].className != null && divnodes[i].className == "ms-crm-Inline-GradientMask") {
            divnodes[i].setAttribute("class", "");
        }
    }
    //}
}

function HideLockIcon(imgnodes) {
    //if (imgnodes != undefined && imgnodes != null) {
    for (var i = 0; i < imgnodes.length; i++) {
        if (imgnodes[i].className != undefined && imgnodes[i].className != null && imgnodes[i].className == "ms-crm-ImageStrip-inlineedit_locked") {
            imgnodes[i].setAttribute("class", "");
        }
    }
    //}
}

function RecalculateWorksheet() {
    // OnChange fields
    // pnl_otherdealtercostsfreight
    // pnl_otherdealtercostspredelivery
    // pnl_otherdealterinterestsubsidy
    // pnl_scenario1grossprofitpercentage
    // pnl_tradeinmachine1scenario2value
    // pnl_tradeinmachine1scenario3value
    // pnl_tradeinmachine2scenario2value
    // pnl_tradeinmachine2scenario3value
    // pnl_tradeinmachine3scenario2value
    // pnl_tradeinmachine3scenario3value
    // pnl_scenario2changeoverpriceexclgst
    // pnl_scenario3changeoverpriceexclgst
    // pnl_salesaidrequested
    // pnl_salesaidapproved
    //debugger;

    if (isLoading)
        return;

    var totalOtherDealerCosts =
        GetDecimalValue("pnl_otherdealercostsfreight") +
        GetDecimalValue("pnl_otherdealercostspredelivery") +
        GetDecimalValue("pnl_otherdealerinterestsubsidy");
    SetDecimalValue("pnl_totalotherdealercosts", totalOtherDealerCosts);

    var subtotalDealerNetPrice = GetDecimalValue("pnl_subtotaldealernetprice");
    var totalLocallySourcedItemsDealerNetPrice = GetDecimalValue("pnl_totallocallysourceditemsdealernetprice");
    var scenario1TotalDealerNetCost =
        subtotalDealerNetPrice +
        totalLocallySourcedItemsDealerNetPrice +
        totalOtherDealerCosts;
    SetDecimalValue("pnl_scenario1totaldealernetcost", scenario1TotalDealerNetCost);

    var salesAid = 0.0;
    if (ValueIsNull("pnl_salesaidapproved")) {
        salesAid = GetDecimalValue("pnl_salesaidrequested");
    }
    else {
        salesAid = GetDecimalValue("pnl_salesaidapproved");
    }

    var scenarios2and3TotalDealerNetCost =
        subtotalDealerNetPrice +
        totalLocallySourcedItemsDealerNetPrice +
        totalOtherDealerCosts -
        salesAid;
    SetDecimalValue("pnl_scenario2totaldealernetcost", scenarios2and3TotalDealerNetCost);
    SetDecimalValue("pnl_scenario3totaldealernetcost", scenarios2and3TotalDealerNetCost);


    var potentialGPatListPrice = 0.0;
    var subTotalListPrice = GetDecimalValue("pnl_subtotallistprice");
    var totalListPrice = GetDecimalValue("pnl_totallistprice");
    if (subTotalListPrice != 0.0) {
        potentialGPatListPrice = (100.0 * (totalListPrice - scenario1TotalDealerNetCost) / totalListPrice);
    }
    SetDecimalValue("pnl_potentialgpatlistprice", potentialGPatListPrice);


    var scenario1GrossProfitProportion = GetDecimalValue("pnl_scenario1grossprofitpercentage") / 100.00;
    var scenario1GrossProfitDollar = 0.0;
    if (scenario1GrossProfitProportion != 0.0) {
        scenario1GrossProfitDollar = (GetDecimalValue("pnl_scenario1totaldealernetcost") / (1 - scenario1GrossProfitProportion))
            * scenario1GrossProfitProportion;
    }
    SetDecimalValue("pnl_scenario1grossprofitdollar", scenario1GrossProfitDollar);

    var scenario1NetSellingPrice =
        scenario1TotalDealerNetCost +
        scenario1GrossProfitDollar;
    SetDecimalValue("pnl_scenario1netsellingprice", scenario1NetSellingPrice);

    var scenario2TotalTradedValue =
        GetDecimalValue("pnl_tradeinmachine1scenario2value") +
        GetDecimalValue("pnl_tradeinmachine2scenario2value") +
        GetDecimalValue("pnl_tradeinmachine3scenario2value");
    SetDecimalValue("pnl_scenario2totaltradedvalue", scenario2TotalTradedValue);

    var scenario3TotalTradedValue =
        GetDecimalValue("pnl_tradeinmachine1scenario3value") +
        GetDecimalValue("pnl_tradeinmachine2scenario3value") +
        GetDecimalValue("pnl_tradeinmachine3scenario3value");
    SetDecimalValue("pnl_scenario3totaltradedvalue", scenario3TotalTradedValue);

    var totalBookValue = GetDecimalValue("pnl_totalbookvalue");
    var scenario2OvertradeAmount = scenario2TotalTradedValue - totalBookValue;
    SetDecimalValue("pnl_scenario2overtradeamount", scenario2OvertradeAmount);

    var scenario3OvertradeAmount = scenario3TotalTradedValue - totalBookValue;
    SetDecimalValue("pnl_scenario3overtradeamount", scenario3OvertradeAmount);

    var scenario1ChangeoverPriceExclGST = scenario1NetSellingPrice - totalBookValue;
    SetDecimalValue("pnl_scenario1changeoverpriceexclgst", scenario1ChangeoverPriceExclGST);

    var scenario2ChangeoverPriceExclGST = GetDecimalValue("pnl_scenario2changeoverpriceexclgst");
    if (scenario2ChangeoverPriceExclGST == 0.0) {
        scenario2ChangeoverPriceExclGST = scenario1ChangeoverPriceExclGST;
        SetDecimalValue("pnl_scenario2changeoverpriceexclgst", scenario1ChangeoverPriceExclGST);
    }

    var scenario3ChangeoverPriceExclGST = GetDecimalValue("pnl_scenario3changeoverpriceexclgst");
    if (scenario3ChangeoverPriceExclGST == 0.0) {
        scenario3ChangeoverPriceExclGST = scenario1ChangeoverPriceExclGST;
        SetDecimalValue("pnl_scenario3changeoverpriceexclgst", scenario1ChangeoverPriceExclGST);
    }

    var totalListPrice = GetDecimalValue("pnl_totallistprice")
    var scenario2ChangeoverDiscounts = totalListPrice - scenario2TotalTradedValue - scenario2ChangeoverPriceExclGST;
    SetDecimalValue("pnl_scenario2changeoverdiscounts", scenario2ChangeoverDiscounts);

    var scenario3ChangeoverDiscounts = totalListPrice - scenario3TotalTradedValue - scenario3ChangeoverPriceExclGST;
    SetDecimalValue("pnl_scenario3changeoverdiscounts", scenario3ChangeoverDiscounts);

    var scenario2NetSellingPrice = totalListPrice - scenario2ChangeoverDiscounts;
    SetDecimalValue("pnl_scenario2netsellingprice", scenario2NetSellingPrice);

    var scenario3NetSellingPrice = totalListPrice - scenario3ChangeoverDiscounts;
    SetDecimalValue("pnl_scenario3netsellingprice", scenario3NetSellingPrice);

    SetDecimalValue("pnl_estimatedrevenue", scenario3NetSellingPrice);

    var scenario2NetSellingPriceInternal = scenario2NetSellingPrice - scenario2OvertradeAmount;
    SetDecimalValue("pnl_scenario2netsellingpriceinternal", scenario2NetSellingPriceInternal);

    var scenario3NetSellingPriceInternal = scenario3NetSellingPrice - scenario3OvertradeAmount;
    SetDecimalValue("pnl_scenario3netsellingpriceinternal", scenario3NetSellingPriceInternal);

    var scenario2GrossProfitDollar = (scenario2NetSellingPriceInternal - scenarios2and3TotalDealerNetCost);
    SetDecimalValue("pnl_scenario2grossprofitdollar", scenario2GrossProfitDollar);

    var scenario3GrossProfitDollar = scenario3NetSellingPriceInternal - scenarios2and3TotalDealerNetCost;
    SetDecimalValue("pnl_scenario3grossprofitdollar", scenario3GrossProfitDollar);

    if (scenario2NetSellingPriceInternal == 0.0)
        SetDecimalValue("pnl_scenario2grossprofitpercentage", 0.0);
    else
        SetDecimalValue("pnl_scenario2grossprofitpercentage",
            (100.0 * scenario2GrossProfitDollar / scenario2NetSellingPriceInternal));

    if (scenario3NetSellingPriceInternal == 0.0)
        SetDecimalValue("pnl_scenario3grossprofitpercentage", 0.0);
    else
        SetDecimalValue("pnl_scenario3grossprofitpercentage",
            (100.0 * scenario3GrossProfitDollar / scenario3NetSellingPriceInternal));

    var scenario1ChangeoverDiscounts = totalListPrice - scenario1NetSellingPrice;
    SetDecimalValue("pnl_scenario1changeoverdiscounts", scenario1ChangeoverDiscounts);

    if (scenario1NetSellingPrice == 0.0)
        SetDecimalValue("pnl_scenario1changeovergrossprofitpercentage", 0.0);
    else
        SetDecimalValue("pnl_scenario1changeovergrossprofitpercentage",
            (100.0 * (scenario1NetSellingPrice - scenario1TotalDealerNetCost) / scenario1NetSellingPrice));

    // 6g
    var s2Denominator = scenario2NetSellingPrice - scenario2OvertradeAmount;
    if (s2Denominator == 0.0)
        SetDecimalValue("pnl_scenario2changeovergrossprofitpercentage", 0.0);
    else
        SetDecimalValue("pnl_scenario2changeovergrossprofitpercentage", (100.0 * (scenario2NetSellingPrice - scenario2OvertradeAmount - scenarios2and3TotalDealerNetCost) / s2Denominator));

    // 6j
    var s3Denominator = scenario3NetSellingPrice - scenario3OvertradeAmount;
    if (s3Denominator == 0.0)
        SetDecimalValue("pnl_scenario3changeovergrossprofitpercentage", 0.0);
    else
        SetDecimalValue("pnl_scenario3changeovergrossprofitpercentage", (100.0 * (scenario3NetSellingPrice - scenario3OvertradeAmount - scenarios2and3TotalDealerNetCost) / s3Denominator));
}

function GetDecimalValue(editName) {
    var sa = Xrm.Page.getAttribute(editName);

    if (sa == null) {
        alert("Recalc GetDecimalValue() failed to find control: " + editName);
        return 0.0;
    }

    if (!sa.getValue()) {
        return 0.0;
    }

    return sa.getValue();
}

function ValueIsNull(editName) {
    var sa = Xrm.Page.getAttribute(editName);

    if (sa == null) {
        alert("Recalc GetDecimalValue() failed to find control: " + editName);
        return true;
    }

    if (!sa.getValue()) {
        return true;
    }

    return false;
}

function SetDecimalValue(editName, newValue) {
    var sa = Xrm.Page.getAttribute(editName);

    if (sa == null) {
        alert("Recalc SetDecimalValue() failed to find control: " + editName);
        return;
    }

    sa.setValue(newValue);
}

function btnRecalculate() {
    try {
        Xrm.Page.ui.setFormNotification("Recalculating Quote, Please Wait...", "WARNING", "1");
        window.setTimeout(btnRecalculate2, 10);
    }
    catch (err) {
        alert(err.message);
        Xrm.Page.ui.clearFormNotification("1");
    }
    HeaderFieldFade();
}

function btnRecalculate2() {
    try {
        //plugin triggered on save. Force save even if not dirty (changes came from external source that didnt trigger plugin)
        isLoading = true;
        if (Xrm.Page.getAttribute("pnl_recalculate").getValue() == 0) {
            Xrm.Page.getAttribute("pnl_recalculate").setValue(1);
        }
        else {
            Xrm.Page.getAttribute("pnl_recalculate").setValue(0);
        }

        if (Xrm.Page.data.entity.getIsDirty()) {
            Xrm.Page.data.save().then(
                function () {
                    //alert("Save Success");
                    Xrm.Page.ui.clearFormNotification("1");
                    //checkForSubmitAlways();
                    //checkForDirty();
                },
                function () {
                    alert("Save Fail");
                    Xrm.Page.ui.clearFormNotification("1");
                    //checkForSubmitAlways();
                    //checkForDirty();
                });
        }
        else {
            window.location.reload(true);
        }
    }
    catch (err) {
        alert(err.message);
        Xrm.Page.ui.clearFormNotification("1");
    }
}

function StatusReason_OnChange() {
    Xrm.Page.data.refresh();
    Xrm.Page.ui.refreshRibbon();
}

function CustomerLetterRefresh() {
    var editorControl = Xrm.Page.ui.controls.get("WebResource_CKEditor");
    if (editorControl != null)
        editorControl.setSrc(editorControl.getSrc());
}
