function onLoad() {
    var switchingToLocallySourced = switchForm();
    if (switchingToLocallySourced) {
        var formtype = Xrm.Page.ui.getFormType();
        var CREATE = 1;
        preLoadMachine();
        isLocallySourced();
        if (formtype == CREATE) {
            setQuantity();
        }
    }
	
	//overridePrice.addOnChange(isLocallySourced);
	//listPrice.addOnChange(isLocallySourced);
	//discount.addOnChange(isLocallySourced);
}

function isLocallySourced() {
    var machine = Xrm.Page.getAttribute('pnl_locallysourcedmachineid').getValue();
    var listPrice = Xrm.Page.getAttribute('priceperunit');
	var discount = Xrm.Page.getAttribute('pnl_locallysourcedstandarddiscount');
	var priceOverride = Xrm.Page.getAttribute('isproductoverridden');
	var noChargeItem = Xrm.Page.getAttribute('pnl_nochargeitem').getValue();
	
	if (machine != null)
	{
	    Xrm.Page.getAttribute('pnl_locallysourcedmachinename').setValue(machine[0].name);
		priceOverride.setValue(true);
		priceOverride.fireOnChange();
		
		if(listPrice != null)
		{
		    var isLocallySourced = Xrm.Page.getAttribute('pnl_islocallysourced');
		    isLocallySourced.setValue(true);

		    if (!noChargeItem) {
		        var dealerNetPrice = Xrm.Page.getAttribute('pnl_locallysourceddnp');
		        var newValue = listPrice.getValue();

		        if (discount != null) {
		            newValue = listPrice.getValue() * ((100 - discount.getValue()) / 100);
		        }

		        dealerNetPrice.setValue(newValue);
		    }
		}
	}
	else
	{
	    var formtype = Xrm.Page.ui.getFormType();
	    var CREATE = 1;
	    if (formtype == CREATE) {
	        priceOverride.setValue(false);
	        priceOverride.fireOnChange();
	        Xrm.Page.getAttribute('pnl_locallysourcedmachinename').setValue(null);
	    }
	}

	if (noChargeItem) {
	    Xrm.Page.getControl('priceperunit').setDisabled(true);
	    Xrm.Page.getControl('pnl_locallysourcedstandarddiscount').setDisabled(true);
	} else {
	    Xrm.Page.getControl('priceperunit').setDisabled(false);
	    Xrm.Page.getControl('pnl_locallysourcedstandarddiscount').setDisabled(false);
	}
}

function setQuantity() {
    var quantity = Xrm.Page.getAttribute('quantity')
    if (quantity != undefined && quantity != null) {
        if (quantity.getValue() == null) {
            quantity.setSubmitMode('always');
            quantity.setValue(1);
        }
    }
}

function preLoadMachine() {
    var locallySourceFormName = 'Locally Sourced Information';
    var currentFormName = Xrm.Page.ui.formSelector.getCurrentItem().getLabel();
    var itemLookup = Xrm.Page.getAttribute('pnl_locallysourcedmachineid').getValue();
    var quoteId = Xrm.Page.getAttribute('quoteid').getValue();
    var formtype = Xrm.Page.ui.getFormType();
    var CREATE = 1;
    if (itemLookup == null && formtype == CREATE && quoteId != null && currentFormName == locallySourceFormName) {
        var fetchXml =
                    "<fetch mapping='logical'>" +
                       "<entity name='pnl_locallysourcedmachine'>" +
                          "<attribute name='pnl_locallysourcedmachineid' />" +
                          "<attribute name='pnl_name' />" +
                          "<filter>" +
                             "<condition attribute='pnl_quoteid' operator='eq' value='" + quoteId + "' />" +
                          "</filter>" +
                       "</entity>" +
                    "</fetch>";

        var retrievedItem = XrmServiceToolkit.Soap.Fetch(fetchXml);

        if (retrievedItem.length == 1 && Xrm.Page.getAttribute("pnl_locallysourcedmachineid").getValue() == null) {
            var locallyid = retrievedItem[0].attributes['pnl_locallysourcedmachineid'].value;
            var locallyname = retrievedItem[0].attributes['pnl_name'].value;
            Xrm.Page.getAttribute("pnl_locallysourcedmachineid").setValue([{ id: locallyid, name: locallyname, entityType: "pnl_locallysourcedmachine" }]);
        }
    }
}

function switchForm() {
    var isLocallySourcedValue = Xrm.Page.getAttribute('pnl_islocallysourced').getValue();
    var locallySourceFormName = 'Locally Sourced Information';
    var informationFormName = 'Information';
    var currentFormName = Xrm.Page.ui.formSelector.getCurrentItem().getLabel();
    var formtype = Xrm.Page.ui.getFormType();
    var CREATE = 1;

    if (formtype == CREATE) {
        if (currentFormName != locallySourceFormName) {
            changeForm(locallySourceFormName);
            return false;
        }
        return true;
    }
    else {
        if (isLocallySourcedValue == 1) {
            if (currentFormName != locallySourceFormName) {
                changeForm(locallySourceFormName);
                return false;
            }
            return true;
        }
        else {
            if (isLocallySourcedValue == 0 && formtype != CREATE) {
                if (currentFormName != informationFormName)
                    changeForm(informationFormName);
                return false;
            }
        }
    }
    return true;
}

function changeForm(formName) {
    var items = Xrm.Page.ui.formSelector.items.get();
    for (var i in items) {
        var item = items[i];
        var itemLabel = item.getLabel()
        if (itemLabel == formName) {
            item.navigate();
            break;
        }
    }
}

function NoChargeItem_OnChange() {
    var noChargeItem = Xrm.Page.getAttribute('pnl_nochargeitem').getValue();
    if (noChargeItem) {
        Xrm.Page.getAttribute('pnl_locallysourcedstandarddiscount').setSubmitMode('always');
        Xrm.Page.getAttribute('pnl_locallysourcedstandarddiscount').setValue(100);
        Xrm.Page.getAttribute('priceperunit').setSubmitMode('always');
        Xrm.Page.getAttribute('priceperunit').setValue(0);
    } else {
        Xrm.Page.getAttribute('pnl_locallysourcedstandarddiscount').setValue(null);
        Xrm.Page.getAttribute('priceperunit').setValue(null);
    }
    isLocallySourced();
}