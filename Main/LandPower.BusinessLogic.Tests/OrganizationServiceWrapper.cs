﻿using System;

using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace LandPower.BusinessLogic.Tests
{
	/// <summary>
	/// Wrapper class which provides overridable methods to facilitate unit testing.
	/// </summary>
	public class OrganizationServiceWrapper : IOrganizationService
	{
		private readonly IOrganizationService _service;

		public OrganizationServiceWrapper(IOrganizationService service)
		{
			_service = service;
		}

		public virtual void Associate(string entityName, Guid entityId, Relationship relationship, EntityReferenceCollection relatedEntities)
		{
			_service.Associate(entityName, entityId, relationship, relatedEntities);
		}

		public virtual Guid Create(Entity entity)
		{
			return _service.Create(entity);
		}

		public virtual void Delete(string entityName, Guid entityId)
		{
			_service.Delete(entityName, entityId);
		}

		public virtual void Disassociate(string entityName, Guid entityId, Relationship relationship, EntityReferenceCollection relatedEntities)
		{
			_service.Disassociate(entityName, entityId, relationship, relatedEntities);
		}

		public virtual OrganizationResponse Execute(OrganizationRequest request)
		{
			return _service.Execute(request);
		}

		public virtual Entity Retrieve(string entityName, Guid id, ColumnSet columnSet)
		{
			return _service.Retrieve(entityName, id, columnSet);
		}

		public virtual EntityCollection RetrieveMultiple(QueryBase query)
		{
			return _service.RetrieveMultiple(query);
		}

		public virtual void Update(Entity entity)
		{
			_service.Update(entity);
		}
	}
}
